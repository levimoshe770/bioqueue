import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import '../imports/startup/server/index';
import initSys from '../imports/startup/server/index'
import { LOGINSERVICE, LoginCallback } from '../imports/api/LoginService/LoginService';
import { cleanDB, FillGarbageData } from '../imports/api/Lists/fillGarbage';
import fixData from '../imports/api/Lists/fixData';

//import FillLabTests, { clearTests } from '../imports/api/Lists/LabTests/garbageData';

const fillGarbage = () =>
{
  const switches = 
  {
    appointments: false,
    messages: false
  }

  cleanDB(switches);

  FillGarbageData(switches);
}

const fixAllData = () =>
{
  const switches = 
  {
    messages: false
  }

  fixData(switches);
}

Meteor.startup(() => {
  
  initSys();

  Accounts.config(
    {
      loginExpirationInDays: 1
    }
  );

  Accounts.registerLoginHandler(
    LOGINSERVICE,
    LoginCallback
  );

  fillGarbage();

  fixAllData();

});


