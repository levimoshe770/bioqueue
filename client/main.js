import React from 'react';
import {Meteor} from 'meteor/meteor';
import {render} from 'react-dom';
import {Provider} from 'react-redux'
import {createLogger}  from 'redux-logger'
import { createStore, applyMiddleware, compose } from 'redux'
import {updateState} from '../imports/ui/Redux/reducers'
import {BrowserRouter} from 'react-router-dom'
import { CookiesProvider } from 'react-cookie'
import IntlSupport from '../imports/ui/IntlSupport';


Meteor.startup(
  () => 
  {

    console.log('GRAND RESET');

    // for (var key in IntlMessages)    
    // {
    //   console.log(key);
    // }

    const logger = createLogger();

    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    store = createStore(
      updateState, 
      //applyMiddleware(logger)
      composeEnhancers(applyMiddleware(logger))
    );

    //console.log(process.env);

    render(
      <CookiesProvider>
        <BrowserRouter>
          <Provider store={store}>
            <IntlSupport />
          </Provider>
        </BrowserRouter>
      </CookiesProvider>,
      document.getElementById('render-target')
    )
  }
)
