import {SET_USER_LOGGEDIN, SET_PATIENT_MR, SET_LOCALE, SET_ACCESSTOKEN} from './constants'

export const setUserLoggedIn = (user) =>
{
    return(
        {
            type: SET_USER_LOGGEDIN,
            payload: user
        }
    );
}

export const setPatientMR = (patientId) =>
{
    return(
        {
            type: SET_PATIENT_MR,
            payload: patientId
        }
    )
}

export const setLocale = (locale) =>
{
    return(
        {
            type: SET_LOCALE,
            payload: locale
        }
    )
}

export const setAccessToken = (token) =>
{
    return(
        {
            type: SET_ACCESSTOKEN,
            payload: token
        }
    )
}