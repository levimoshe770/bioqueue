
import { DefaultLocale } from '../../../translation/IntlMessages';
import {SET_USER_LOGGEDIN, SET_PATIENT_MR, SET_LOCALE, SET_ACCESSTOKEN} from './constants';

export let initialState = {
    userName: 
    {
        Name: '',
        Email: '',
        Role: '',
        userId: '',
        InitialPath: '/',
        access_token: ''
    },
    patientId: '',
    access_token: '',
    locale: DefaultLocale
}

export const updateState = (state=initialState, action={}) =>
{
    switch (action.type)
    {
        case SET_USER_LOGGEDIN:
            return Object.assign({}, state, {userName: action.payload});
        case SET_PATIENT_MR:
            return Object.assign({}, state, {patientId: action.payload});
        case SET_LOCALE:
            return Object.assign({}, state, {locale: action.payload});
        case SET_ACCESSTOKEN:
            return Object.assign({}, state, {access_token: action.payload});
        default:
            return state;
    }
}