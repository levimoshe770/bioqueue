import { Meteor } from 'meteor/meteor';

const mrserverurl = Meteor.settings.public.auth_server_url;

export default mrserverurl;