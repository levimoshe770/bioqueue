import React, { Component } from 'react';
import './PharmacyPage.css'
import StatusSign from '../Components/Utils/StatusSign.jsx'
import { withTracker } from 'meteor/react-meteor-data';
import Remote from '../Remote';
import { connect } from 'react-redux';
import Pharmacies from '../../api/Lists/Pharmacies/Pharmacies';
import PrescriptionList from '../Components/PrescriptionList/PrescriptionList';
import PrescriptionDetails from '../Components/PrescriptionDetails/PrescriptionDetails';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import { Approved } from '../../api/Lists/GeneralTypes/AuthorizationStatus';

const mapStateToProps = state =>
{
    return {access_token: state.access_token, locale: state.locale};
}

class PharmacyPage extends Component
{
    constructor(props)
    {
        super(props);
        this.state =
        {
            showDetails: false,
            prescriptionSelected: undefined
        }
    }

    render()
    {
        //console.log(this.props);
        return(
            <div className='statebasic-nogrid pharmacystate flex flex-column items-center'>
                <StatusSign 
                    className='self-start mt2'
                    status=
                    {
                        this.props.profile.length === 0 
                            ? -1
                            : this.props.profile[0].AuthorizationStatus
                    }
                />
                {
                    (this.props.profile.length > 0 && this.props.profile[0].AuthorizationStatus === Approved)
                    ?
                    <div>
                    {
                        this.state.showDetails &&
                        <PrescriptionDetails
                            className='absolute w-80'
                            prescription={this.state.prescriptionSelected}
                            onClose={() => this.setState({showDetails: false})}
                        />
                    }
                    <PrescriptionList
                        className='mt3'
                        access_token={this.props.access_token}
                        onDetailsOpen=
                        {
                            (prescription) => 
                            {
                                this.setState(
                                    {
                                        showDetails: true, 
                                        prescriptionSelected: prescription
                                    }
                                );
                            }
                        }
                    />
                    </div>
                    :
                    <div className='pa2 ma4 bg-lightblue2-opac br3 shadow-6 w-80 c-white f-oswald-30 tc flex flex-column items-center'>
                        <FormattedMessage
                            id='pharmacy.greetingmessage'
                            defaultMessage='Hello, your status is still pending. You will not be able to operate the system, until the administrator approves you. Please make sure, you have filled all the necessary profile data'
                            description='greetingmessage'
                        />
                        <Link className='c-blue' to='/profile'>
                            <FormattedMessage
                                id='physiciangreeting.updateprofile'
                                defaultMessage='Click here to update your profile'
                                description='update profile'
                            />
                        </Link>
                    </div>
                }
            </div>
        )
    }
}

export default 
connect(mapStateToProps)
(
    withTracker(
        ({access_token}) =>
        {
            const profileHandler = Remote.subscribe('pharmacies.myPharmacy', access_token);
            const profileLoading = !profileHandler.ready();

            return(
                {
                    profile: !profileLoading ? Pharmacies.find({}).fetch() : []
                }
            )
        }
    )
    (PharmacyPage)
);
