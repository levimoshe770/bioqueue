import React, { Component } from 'react';
import './LoggingInPage.css';
import { withRouter } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { LOGINSERVICE } from '../../api/LoginService/LoginService';

class LoggingInPage extends Component
{
    componentDidMount()
    {
        const urlSearchParams = new URLSearchParams(this.props.location.search);
        const authorizationCode = urlSearchParams.get('code');

        console.log(authorizationCode);

        //console.log('POST /oauth/token');

        Meteor.call('requestToken',
            {
                'grant_type': 'authorization_code',
                'client_id': Meteor.settings.public.client_data.ClientId,
                'client_secret': Meteor.settings.public.client_data.client_secret,
                'code': authorizationCode
            },
            (err, result) =>
            {
                if (err)
                {
                    console.log(err);
                    return;
                }

                console.log('Res', result);

                const token = result.access_token;

                Meteor.call('getUserData', token,
                    (err1, userData) =>
                    {
                        if (err1)
                        {
                            console.log(err1);
                            return;
                        }

                        console.log('User data', userData);

                        Accounts.callLoginMethod(
                            {
                                methodArguments:
                                [
                                    {
                                        custom: LOGINSERVICE,
                                        token: token,
                                        user: 
                                        {
                                            _id: userData._id,
                                            email: userData.emails[0].address,
                                            name: userData.name,
                                            role: userData.role
                                        }
                                    }
                                ],
                                validateResult:
                                    (res) => {window.close(); return true;},
                                userCallback: (err) => console.log(err)
                            }
                        )
                    }
                );

            }
        );
    }

    render()
    {
        return(
            <div className='statebasic-nogrid'>
                <div className='centered f-oswald-40 t-shadow-1 bg-lightblue pa5 br3 shadow-3'>
                    Logging in...
                </div>
            </div>
        );
    }
}

export default withRouter(LoggingInPage);