import React from 'react'
import SignInForm from './SignInForm'
import './SignInStateUi.css'

class SignInStateUi extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    onFailLogin = () =>
    {
        this.props.history.push("/")
    }

    render()
    {
        return (
            <div className="statebasic signinstate">
                <SignInForm onSignInClosed={this.onFailLogin} history={this.props.history}/>
            </div>
        );
    }
}

export default SignInStateUi;