import React from 'react'
import { FormattedMessage } from 'react-intl'
import './SignInForm.css'
import {connect} from 'react-redux'
import { Meteor } from 'meteor/meteor'
import {setUserLoggedIn} from '../Redux/actions'
import Users from '../../api/Lists/Users/users';
import { withTracker } from 'meteor/react-meteor-data';
import ErrorInForm from '../Components/Utils/ErrorInForm.js';
import { IntlMessages } from '../../../translation/IntlMessages';
import { withRouter } from 'react-router-dom'

const mapStateToProps = state =>
{
    return {userName: state.userName, locale: state.locale}
}

const mapDispatchToProps = (dispatch) =>
{
    return {onUserChange: (userName) => dispatch(setUserLoggedIn(userName))}
}


class SignInForm extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            userName: '',
            password: '',
            errorMessage: '',
            showError: false,
            user: undefined
        }
    }

    onUsernameChange = (event) =>
    {
        this.setState({userName: event.target.value});
    }

    onPasswordChange = (event) =>
    {
        this.setState({password: event.target.value});
    }

    onRememberMeClicked = (event) =>
    {
        //const chkBox = document.getElementById("Remember-Me");
        console.log(event.target.checked);
    }

    onSubmitClick = () =>
    {
        const { userName, password } = this.state;

        Meteor.loginWithPassword(userName, password, 
            (err, result) =>
            {
                if (err) 
                {
                    this.setState({showError: true, errorMessage: err.message});
                    console.log(err);
                }
            }
        )
    }

    componentDidUpdate()
    {
        if (this.props.userData.length > 0 &&
            typeof this.state.user === 'undefined')
        {
            const user = Meteor.user();
            this.setState({user: user});

            const {name,role} = this.props.userData[0];

            this.props.onUserChange(
                {
                    Name: name,
                    Email: user.emails[0].address,
                    Role: role,
                    userId: user._id,
                    InitialPath: "/" + role
                }
            )
            
            this.props.history.push('/' + role);
        }
    }

    onKeyPressed = (event) =>
    {
        if (event.key === 'Enter')
        {
            this.onSubmitClick();
        }
    }

    render()
    {
        return(
            <div className="signinform" onKeyPress={this.onKeyPressed}>
                <span 
                    onClick={this.props.onSignInClosed}
                    className="close">
                        &times;
                </span>
                <div className="innerform">
                    <fieldset id="sign_up" className="fieldset">
                        <legend className="formlegend">
                            <FormattedMessage
                                id="signinform.login"
                                defaultMessage="Log In"
                                description="Login" />
                        </legend>
                        <div className="userfield">
                            <label className="formlabel" htmlFor="username">
                                <FormattedMessage
                                    id="signinform.username"
                                    defaultMessage="Username"
                                    description="username" />
                            </label>
                            <input
                                onChange={this.onUsernameChange}
                                className="signintext"
                                type="text"
                                name="username"
                                id="username"
                            />
                        </div>
                        <div className="passwordfield">
                            <label className="formlabel" htmlFor="password">
                                <FormattedMessage
                                    id="signinform.password"
                                    defaultMessage="Password"
                                    description="Password" />
                            </label>
                            <input
                                onChange={this.onPasswordChange}
                                className="signintext"
                                type="password"
                                name="password"
                                id="password"
                            />
                        </div>
                        <label className="checkboxtext">
                            <input type="checkbox" className="checkboxsign" />
                            <FormattedMessage
                                id="signinform.rememberme"
                                defaultMessage="Remember me"
                                description="Remember me"
                            />
                        </label>
                        <label className="linkstyle">
                            <FormattedMessage
                                id="signinform.forgotlogin"
                                defaultMessage="Forgot Login Details?"
                                description="Forgot Login Details?"
                            />
                        </label>
                    </fieldset>
                    <div className="flex flex-column items-center">
                    {/* b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib */}
                        <input
                            className="loginbutton"
                            onClick={this.onSubmitClick}
                            type="submit"
                            value={
                                IntlMessages[this.props.locale].intl.formatMessage(
                                    {
                                        id: 'signinform.submit',
                                        defaultMessage: 'Login'
                                    }
                                )
                            }
                        />
                        { this.state.showError &&
                            <ErrorInForm 
                                className="pt1 errorMsg f-arial-13" 
                                Message={this.state.errorMessage}
                            />
                        }
                        <div className='flex items-center' id="signup">
                            <FormattedMessage
                                id='signinform.donthaveaccount'
                                defaultMessage="Don't have an account ?"
                                description="noaccount"
                            />
                            <div 
                                className='pointer ml2'
                                id="signuplink"
                                onClick=
                                {
                                    () => this.props.history.push('/register')
                                }
                            >
                                <FormattedMessage
                                    id='signinform.signup'
                                    defaultMessage='Sign Up'
                                    description='Sign Up'
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default 
withRouter(
    connect(mapStateToProps, mapDispatchToProps)(
        withTracker(
            () => 
            {
                const userHandle = Meteor.subscribe('users.customData');

                const loading = !userHandle.ready();

                if (!loading)
                    return {userData: Users.find({_id: Meteor.userId()}).fetch()}
                else
                    return {userData: []}
                
            }
        )
        (SignInForm)
    )
);