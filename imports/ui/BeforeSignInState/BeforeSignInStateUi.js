import React from 'react';
import { FormattedMessage } from 'react-intl';
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import Physicians from '../../api/Lists/Physicians/physicians';

import SearchDoctorBox from '../Components/SearchDoctorBox/SearchDoctorBox';
import DoctorList from '../Components/DoctorList/DoctorList';

import Remote from '../Remote';

import './BeforeSignInStateUi.css'

class BeforeSignInStateUi extends React.Component
{

    constructor(props)
    {
        super(props);

        this.state = {
            searchOpened: false,
            doctorListOpened: false
        }
    }

    onWhoWorkWithUsClick = () =>
    {
        const res = !this.state.doctorListOpened;
        this.setState({doctorListOpened: res});
    }

    onSearchedCloseClick = () =>
    {
        this.setState({searchOpened: false});
    }

    getDoctorList = () =>
    {
        return this.props.physicians;
    }

    onDoctorListClose = () =>
    {
        this.setState({doctorListOpened: false});
    }

    onDoctorListSelect = (id) =>
    {
        this.setState({doctorListOpened: false})
    }

    onSearchClicked = () =>
    {
        this.setState(
            {
                doctorListOpened: true,
                searchOpened: false
            }
        )
    }

    render()
    {
        //console.log(this.props);
        return (
            <div className="statebasic-nogrid beforesigninstate flex flex-column items-center">
                { this.state.doctorListOpened &&
                    <div className="absolute mt6">
                        <DoctorList 
                            SelectOption={false}
                            DoctorList={this.getDoctorList()} 
                            onClose={this.onDoctorListClose} 
                            onSelect={this.onDoctorListSelect}
                        />
                    </div>
                }
                <div className="whoweare">
                    <FormattedMessage
                        id = "beforesigninstateui.whoweare"
                        defaultMessage="Some interesting and nice bla bla bla that explains who we are and how we can help people, and also that we connect to life tracker"
                        description="Who we are message" />
                </div>
                <div 
                    onClick={() => this.onWhoWorkWithUsClick()}
                    className="bigbutton  pt4 pb4 pl5 pr5 shadow-3 br-pill pointer mt4" 
                >
                    <FormattedMessage
                        id="beforesigninstateui.seewhoisworkingwithus"
                        defaultMessage="See who is working with us"
                        description="See who is working with us" />
                </div>
                
            </div>
        );
    }
}

export default withTracker(

    () =>
    {
        const physiciansHandle = Remote.subscribe('physicians');
        const loading = !physiciansHandle.ready();
        //const list = Physicians.findOne(id)
        return { physicians: !loading ? Physicians.find({}).fetch() : []};
    }

)(BeforeSignInStateUi);