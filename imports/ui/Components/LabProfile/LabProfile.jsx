import React, { Component } from 'react';
import ProfileTextLine from '../PhysicianProfile/ProfileTextLine.jsx'
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { withTracker } from 'meteor/react-meteor-data';
import { withRouter } from 'react-router-dom';
import Laboratories from '../../../api/Lists/Laboratories/Laboratories';
import Remote from '../../Remote';
import FileUpload from '../Utils/FileUpload.jsx';
import UserFiles from '../../../api/Lists/UserFiles/UserFiles';

const mapStateToProps = (state) =>
{
    return {locale: state.locale, access_token: state.access_token}
}

/*

 */

class LabProfile extends Component
{
    constructor(props)
    {
      super(props);
      this.state =
        {
          userProfile: undefined
        }
      
    }
  
    onFieldChanged = (id, value) =>
    {
        const prof = this.state.userProfile === undefined ? this.props.profile[0] : this.state.userProfile;
        //console.log(prof);
        switch (id) 
        {
            case 'name':
                prof.Name = value;
                break;
            case 'address':
                prof.Address = value;
                break;
            case 'email':
                prof.Email = value;
                break;
            case 'phone':
                prof.Phone = value;
                break;
        }
        this.setState({userProfile: prof});
    }
  
    onApply = () =>
    {
        console.log(this.state.userProfile);
        Remote.call('laboratory.update',this.props.access_token, this.state.userProfile);
        this.props.history.push('/laboratory');
    }

    onClose = () =>
    {
        this.props.history.push('/laboratory')
    }

    onFileUploaded = (file) =>
    {
        const url = UserFiles.findOne({_id: file._id}).link();
        const profile = this.state.userProfile === undefined ? this.props.profile[0] : this.state.userProfile;
        profile.Image = url;
        this.setState({userProfile: profile});
    }

    onFileUploadFailed = (error) =>
    {
        console.error(error);
    }

    render()
    {
        return(
            this.props.profile.length === 0
            ?
            <div>
                {
                    IntlMessages[this.props.locale].intl.formatMessage(
                        {
                            id: 'physiciangreeting.loading',
                            defaultMessage: 'loading'
                        }
                    ) + '...'
                }
            </div>
            :
            <div className='w-80 f-arial-16-nc b--lightgray br4 shadow-6 bg-lightblue mt5'>
                <div className='flex items-center justify-between'>
                    <div className=''>
                    
                    </div>
                    <div className='f-arial-30 bold'>
                        {this.props.profile[0].Name}
                    </div>
                    <div
                        onClick={this.onClose}
                        className='close-nf mr1'
                    >
                        &#9746;
                    </div>
                </div>
                <div className='flex justify-between ma2'>
                    <div className='flex-grow-2 mr2'>
                        <ProfileTextLine 
                            id='name'
                            field=
                            {
                              IntlMessages[this.props.locale].intl.formatMessage(
                                {
                                  id: 'register.name',
                                  defaultMessage: 'Name'
                                }
                              )
                            }
                            type='text'
                            value={this.props.profile[0].Name}
                            onFieldChanged={this.onFieldChanged}
                            required={true}
                        />
                        <ProfileTextLine
                            id='address'
                            field=
                            {
                              IntlMessages[this.props.locale].intl.formatMessage(
                                {
                                  id: 'userdata.address',
                                  defaultMessage: 'Address'
                                }
                              )
                            }
                            type='text'
                            value={this.props.profile[0].Address}
                            onFieldChanged={this.onFieldChanged}
                            required={true}
                        />
                        <ProfileTextLine
                            id='email'
                            field=
                            {
                              IntlMessages[this.props.locale].intl.formatMessage(
                                {
                                  id: 'register.email',
                                  defaultMessage: 'Email'
                                }
                              )
                            }
                            type='text'
                            value={this.props.profile[0].Email}
                            onFieldChanged={this.onFieldChanged}
                            required={true}
                        />
                        <ProfileTextLine
                            id='phone'
                            field=
                            {
                              IntlMessages[this.props.locale].intl.formatMessage(
                                {
                                  id: 'register.phone',
                                  defaultMessage: 'Phone'
                                }
                              )
                            }
                            type='text'
                            value={this.props.profile[0].Phone}
                            onFieldChanged={this.onFieldChanged}
                            required={false}
                        />
                    </div>
                    <div className='flex flex-column items-center mr2 ml2'>
                        <img src={this.props.profile[0].Image} alt='profile' width='150' />
                        <FileUpload
                            className='mt3 bg-buttonblue br3 pointer shadow-3 pl3 pr3 pt2 pb2'
                            label='Update Photo'
                            onFileUploaded={this.onFileUploaded}
                            onUploadFail={this.onFileUploadFailed}
                        />
                    </div>
                </div>
                <div className='flex justify-around pa3'>
                    <button 
                        className='w4 bg-buttonblue pa2 br4 shadow-3'
                        onClick={this.onApply}
                    >
                        <FormattedMessage
                            id='visits.submit'
                            defaultMessage='Submit'
                            description='Submit'
                        />
                    </button>
                    <button 
                        className='w4 bg-buttonblue pa2 br4 shadow-3'
                        onClick={this.onClose}
                    >
                        <FormattedMessage
                            id='visits.cancel'
                            defaultMessage='Cancel'
                            description='Cancel'
                        />
                    </button>
                </div>
            </div>
        )
    }
}

export default 
withRouter
(
    connect(mapStateToProps)
    (
        withTracker(
            ({access_token}) =>
            {
                const profileHandle = Remote.subscribe('laboratories.myLab',
                    access_token
                );
                const profileLoading = !profileHandle.ready();

                return(
                    {
                        profile: !profileLoading ? Laboratories.find({}).fetch() : []
                    }
                )
            }
        )
        (LabProfile)
    )
)