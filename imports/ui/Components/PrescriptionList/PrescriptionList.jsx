import React, { Component } from 'react';
import Remote from '../../Remote';
import { Meteor } from 'meteor/meteor'
import { withTracker } from 'meteor/react-meteor-data';
import { connect } from 'react-redux';
import Prescriptions from '../../../api/Lists/Prescriptions/Prescriptions';
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../../translation/IntlMessages';
import InListSearchBox from '../Utils/InListSearchBox.js';
import Patients from '../../../api/Lists/Patients/patients';
import Physicians from '../../../api/Lists/Physicians/physicians';

const mapStateToProps = state =>
{
  return {access_token: state.access_token, locale: state.locale};
}

class PrescriptionList extends Component
{
    constructor(props)
    {
        super(props);
        this.state =
        {
            searchBox: {},
            checkBox: 
            {
                'supplystatus':
                {
                    'Supplied': true,
                    'Not Supplied': true
                }
            },
            refs:
            {
                'name': React.createRef(),
                'generic': React.createRef(),
                'patientname': React.createRef(),
                'physicianname': React.createRef(),
                'supplystatus': React.createRef()
            }
        }
    }

    onSearchBoxChanged = (searchValue, field) =>
    {
        let sb = this.state.searchBox;

        sb[field] = searchValue;

        this.setState({searchBox: sb});
    }

    onCheckChanged = (checked, value, field) =>
    {
        let cb = this.state.checkBox;

        cb[field][value] = checked;

        this.setState({checkBox: cb});
    }

    getListOfSupplyStatusValues = () =>
    {
        const { locale } = this.props;

        return(
            [
                IntlMessages[locale].intl.formatMessage(
                    {
                        id: 'pharmacy.supplied',
                        defaultMessage: 'Supplied'
                    }
                ),
                IntlMessages[locale].intl.formatMessage(
                    {
                        id: 'pharmacy.notsupplied',
                        defaultMessage: 'Not Supplied'
                    }
                )
            ]
        )
    }

    getFilteredPrescriptionList = () =>
    {
        const { prescriptions, locale } = this.props;
        const { searchBox, checkBox } = this.state;

        //console.log(searchBox['name'], prescriptions[0].Name.includes(searchBox['name']))

        const res = prescriptions.filter(
            (prescription) =>
            {
                return(
                    (
                        ('name' in searchBox)
                        ?
                            prescription.Name.toUpperCase().includes(searchBox['name'].toUpperCase())
                        :
                            true
                    )
                    &&
                    (
                        ('generic' in searchBox)
                        ?
                            prescription.GenericName.toUpperCase().includes(searchBox['generic'].toUpperCase())
                        :
                            true
                    )
                    &&
                    (
                        ('patientname' in searchBox)
                        ?
                            this.getPatientName(prescription.PatientId).toUpperCase().includes(searchBox['patientname'].toUpperCase())
                        :
                            true
                    )
                    &&
                    (
                        ('physicianname' in searchBox)
                        ?
                            this.getPhysicianName(prescription.PhysicianId).toUpperCase().includes(searchBox['physicianname'].toUpperCase())
                        :
                            true
                    )
                    &&
                    (
                        ('supplystatus' in checkBox)
                        ?
                            checkBox['supplystatus']
                            [
                                prescription.Supplied 
                                ? 
                                    IntlMessages[locale].intl.formatMessage(
                                        {
                                            id: 'pharmacy.supplied',
                                            defaultMessage: 'Supplied'
                                        }
                                    ) 
                                : 
                                    IntlMessages[locale].intl.formatMessage(
                                    {
                                        id: 'pharmacy.notsupplied',
                                        defaultMessage: 'Not Supplied'
                                    }
                                )
                            ]
                        :
                            true
                    )
                )
            }
        );

        return res;
    }

    getPatientName = (patientId) =>
    {
        const patient = this.props.patients.find((p) => p._id === patientId);

        if (patient === undefined)
            return 'loading';

        return patient.Name;
    }

    getPhysicianName = (physicianId) =>
    {
        const physician = this.props.physicians.find((p) => p._id === physicianId);

        if (physician === undefined)
            return 'loading';

        return physician.Name;
    }

    render()
    {
        // console.log('Prescription props',this.props);
        // console.log('Prescription state',this.state);

        // console.log('width', this.state.refs['name'].current === null ? 'null' : this.state.refs['name'].current.offsetWidth)

        const { locale } = this.props;

        return(
            <div className={'bg-lightblue2-opac pa3 br3 shadow-6 ' + this.props.className}>
                <div className='tc c-white bold f-arial-30 t-shadow-1 bb bw2 b--silver'>
                    <FormattedMessage
                        id='pharmacy.prescriptionlist'
                        defaultMessage="Prescriptions List"
                        description="Prescriptions List"
                    />
                </div>
                <div className='mh-500 scroll-y'>
                    <table>
                        <thead>
                            <tr className='pt2 pb2 bb tc mb2 c-white f-arial-16-nc'>
                                <th
                                    style={this.state.refs['name'].current === null ? {} : {width: this.state.refs['name'].current.offsetWidth}}
                                >
                                    <FormattedMessage
                                        id='doctorlist.name'
                                        defaultMessage='Name'
                                        description='Name'
                                    />
                                </th>
                                <th>
                                    <FormattedMessage
                                        id='pharmacy.genericname'
                                        defaultMessage='Generic Name'
                                        description='Generic Name'
                                    />
                                </th>
                                <th>
                                    <FormattedMessage 
                                        id='physicianapplist.patientname'
                                        defaultMessage='Patient Name'
                                        description='Patient Name'
                                    />
                                </th>
                                <th>
                                    <FormattedMessage 
                                        id='registerformselector.physician'
                                        defaultMessage='Physician'
                                        description='Physician'
                                    />
                                </th>
                                <th>
                                    <FormattedMessage
                                        id='pharmacy.supplystatus'
                                        defaultMessage='Supply Status'
                                        description='Supply Status'
                                    />
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <InListSearchBox
                                        Field='name'
                                        onSearchBoxChanged={this.onSearchBoxChanged}
                                    />
                                </td>
                                <td>
                                    <InListSearchBox
                                        Field='generic'
                                        onSearchBoxChanged={this.onSearchBoxChanged}
                                    />
                                </td>
                                <td>
                                    <InListSearchBox
                                        Field='patientname'
                                        onSearchBoxChanged={this.onSearchBoxChanged}
                                    />
                                </td>
                                <td>
                                    <InListSearchBox
                                        Field='physicianname'
                                        onSearchBoxChanged={this.onSearchBoxChanged}
                                    />
                                </td>
                                <td>
                                    <InListSearchBox
                                        Field='supplystatus'
                                        values={this.getListOfSupplyStatusValues()}
                                        uncheckedValues=
                                        {
                                            (
                                                () =>
                                                {
                                                    const pairs = Object.entries(this.state.checkBox['supplystatus']);
                                                    const res = [];
                                                    pairs.forEach((pair) => { if (!pair[1]) res.push(pair[0]);});
                                                    return res;
                                                }
                                            )()
                                        }
                                        onCheckChanged={this.onCheckChanged}
                                        locale={locale}
                                    />
                                </td>
                            </tr>
                            {
                                this.getFilteredPrescriptionList().map(
                                    (pr, idx) =>
                                    {
                                        return(
                                            <tr 
                                                key={idx}
                                                className={'c-white f-arial-16-nc pb2 bb ' + (!pr.Supplied ? 'pointer' : '')}
                                                onClick={() => { if (!pr.Supplied) this.props.onDetailsOpen(pr); }}
                                            >
                                                <td ref={idx === 0 ? this.state.refs['name'] : null}>
                                                    {pr.Name}
                                                </td>
                                                <td>
                                                    {pr.GenericName}
                                                </td>
                                                <td>
                                                    {this.getPatientName(pr.PatientId)}
                                                </td>
                                                <td>
                                                    {this.getPhysicianName(pr.PhysicianId)}
                                                </td>
                                                <td className='tc'>
                                                    {
                                                        pr.Supplied 
                                                        ? 
                                                            IntlMessages[locale].intl.formatMessage(
                                                                {
                                                                    id: 'pharmacy.supplied',
                                                                    defaultMessage: 'Supplied'
                                                                }
                                                            )
                                                        :
                                                            IntlMessages[locale].intl.formatMessage(
                                                                {
                                                                    id: 'pharmacy.notsupplied',
                                                                    defaultMessage: 'Not Supplied'
                                                                }
                                                            )
                                                    }
                                                </td>
                                            </tr>
                                        )
                                    }
                                )
                            }
                            </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default connect(mapStateToProps)
(
    withTracker(
        ({access_token}) =>
        {
            //console.log('wt', access_token);
            const prescriptionHandler = Remote.subscribe('prescriptions.pharmacy', access_token);
            const prescriptionsLoading = !prescriptionHandler.ready();

            const patientsHandler = Remote.subscribe('patients.pharmacy', access_token);
            const patientsLoading = !patientsHandler.ready();

            const physiciansHandler = Remote.subscribe('physicians.pharmacy',
            access_token);
            const physiciansLoading = !physiciansHandler.ready();

            return(
                {
                    prescriptions: !prescriptionsLoading ? Prescriptions.find({}).fetch() : [],
                    patients: !patientsLoading ? Patients.find({}).fetch() : [],
                    physicians: !physiciansLoading ? Physicians.find({}).fetch() : []
                }
            )
        }
    )
    (PrescriptionList)
);
