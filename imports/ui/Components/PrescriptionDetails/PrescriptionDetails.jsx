import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { withTracker } from 'meteor/react-meteor-data';
import Remote from '../../Remote';
import Patients from '../../../api/Lists/Patients/patients';
import Physicians from '../../../api/Lists/Physicians/physicians';
import { IntlMessages } from '../../../../translation/IntlMessages';

const mapStateToProps = (state) =>
{
    return ({locale: state.locale, access_token: state.access_token});
}

/*
className
prescription
patientName
physicianName
onClose
 */

const PrescriptionElement = (props) =>
{
    return(
        <div className='flex flex-column pa1 ma2'>
            <div className='f-arial-13'>
                {props.field}
            </div>
            <div>
                {props.value}
            </div>
        </div>
    );
}

class PrescriptionDetails extends Component
{
    getResolutionString = (resolution) =>
    {
        const { locale } = this.props;

        switch (resolution)
        {
            case 'h':
                return IntlMessages[locale].intl.formatMessage(
                    {
                        id: 'prescription.hour',
                        defaultMessage: 'hour'
                    }
                );
            case 'd':
                return IntlMessages[locale].intl.formatMessage(
                    {
                        id: 'prescription.day',
                        defaultMessage: 'day'
                    }
                );
            case 'w':
                return IntlMessages[locale].intl.formatMessage(
                    {
                        id: 'prescription.week',
                        defaultMessage: 'week'
                    }
                );
                        
        }
    }

    getDosage = (dosage) =>
    {
        const { locale } = this.props;

        return (
            dosage.Ammount.toString() + ' ' + 
            IntlMessages[locale].intl.formatMessage(
                {
                    id: 'prescription.per',
                    defaultMessage: 'per'
                }
            ) + ' ' +
            this.getResolutionString(dosage.Resolution)
        );
    }

    onSupplyClicked = () =>
    {
        Remote.call('prescription.updateSupplyStatus',
            this.props.access_token,
            this.props.prescription._id,
            true
        );
    }

    render()
    {
        const { prescription, patient, physician, locale } = this.props;
    
        return(
            <div className={this.props.className + ' ' + 'bg-light-gray pa3 shadow-6 mt6 f-arial-16-nc br3 flex flex-column items-center'}>
                <div className='flex justify-between w-100 bb b--gray mb2 pb3 items-center'>
                    <div></div>
                    <div className='f-arial-20-nc bold'>
                        <FormattedMessage
                            id = 'pharmacy.prescription'
                            defaultMessage='Prescription'
                            description='Prescription'
                        />
                    </div>
                    <div
                        onClick={this.props.onClose}
                        className='close-nf mr1'
                    >
                        &#9746;
                    </div>
                </div>
                <div className='flex justify-around pa2 mb2 bb b--gray w-100'>
                    <div className='flex flex-column items-start'>
                        <div className='f-arial-13'>
                            <FormattedMessage
                                id='physicianapplist.patientname'
                                defaultMessage='Patient Name'
                                description='Patient Name'
                            />
                        </div>
                        <div>
                            {patient.length === 0 ? 'Loading...' : patient[0].Name}
                        </div>
                    </div>
                    <div className='flex flex-column items-start'>
                        <div className='f-arial-13'>
                            <FormattedMessage
                                id='registerformselector.physician'
                                defaultMessage='Physician'
                                description='Physician'
                            />
                        </div>
                        <div>
                            {physician.length === 0 ? 'Loading...' : physician[0].Name}
                        </div>
                    </div>
                </div>
                <div className='flex flex-wrap bb b--gray pa2 w-100'>
                    <PrescriptionElement
                        field={
                            IntlMessages[locale].intl.formatMessage(
                                {
                                    id: 'doctorlist.name',
                                    defaultMessage: 'Name'
                                }
                            )
                        }
                        value={prescription.Name}
                    />
                    <PrescriptionElement
                        field={
                            IntlMessages[locale].intl.formatMessage(
                                {
                                    id: 'pharmacy.genericname',
                                    defaultMessage: 'Generic Name'
                                }
                            )
                        }
                        value={prescription.GenericName}
                    />
                    <PrescriptionElement
                        field={
                            IntlMessages[locale].intl.formatMessage(
                                {
                                    id: 'visits.dosage',
                                    defaultMessage: 'Dosage'
                                }
                            )
                        }
                        value={this.getDosage(prescription.Dosage)}
                    />
                </div>
                <div className='pa1 ma2 w-100'>
                    <div className='f-arial-13'>
                        <FormattedMessage
                            id='visits.notes'
                            defaultMessage='Notes'
                        />
                    </div>
                    <div className='scroll-y mh-160'>
                        {
                            
                            prescription.Notes.map(
                                (note, idx) =>
                                {
                                    return(
                                        <div key={idx}>{note}</div>
                                    )
                                }
                            )
                        }
                    </div>
                </div>
                <button
                    className='self-end pb2 pt2 pl3 pr3 br-pill bg-buttonblue shadow-3 pointer'
                    onClick={this.onSupplyClicked}
                    disabled={prescription.Supplied}
                >
                    {
                        prescription.Supplied
                        ?
                            IntlMessages[locale].intl.formatMessage(
                                {
                                    id: 'pharmacy.supplied',
                                    defaultMessage: 'Supplied'
                                }
                            )
                        :
                            IntlMessages[locale].intl.formatMessage(
                                {
                                    id: 'prescription.markassupplied',
                                    defaultMessage: 'Mark as supplied'
                                }
                            )
                    }
                </button>
            </div>
        );
    }
}

export default connect(mapStateToProps)
(
    withTracker(
        ({prescription}) =>
        {
            //console.log('wt', prescription)
            const patientHandler = Remote.subscribe('patients.ids',[prescription.PatientId]);
            const patientLoading = !patientHandler.ready();

            const physicianHandler = Remote.subscribe('physicians.ids',[prescription.PhysicianId]);
            const physicianLoading = !physicianHandler.ready();

            return(
                {
                    patient: !patientLoading ? Patients.find({_id: prescription.PatientId}).fetch() : [],
                    physician: !physicianLoading ? Physicians.find({_id: prescription.PhysicianId}).fetch() : []
                }
            )
        }
    )
    (PrescriptionDetails)
);