import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import Physicians from '../../../api/Lists/Physicians/physicians';
import './AdminApprovals.css'
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';
import Laboratories from '../../../api/Lists/Laboratories/Laboratories';
import Remote from '../../Remote';
import Pharmacies from '../../../api/Lists/Pharmacies/Pharmacies';
import { Pending, Approved, Suspended, statusId } from '../../../api/Lists/GeneralTypes/AuthorizationStatus';

const mapStateToProps = (state) =>
{
    return {locale: state.locale, access_token: state.access_token}
}

/*
className
onClose
 */

class AdminApprovals extends Component 
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            physiciansChecked: true,
            laboratoriesChecked: true,
            pharmaciesChecked: true,
        }
    }

    getCellClass = (physician) =>
    {
        //console.log(physician, Approved);
        switch (physician.AuthorizationStatus)
        {
            case Approved: return 'bg-approved';
            case Pending: return 'bg-pending';
            case Suspended: return 'bg-suspended';
        }
    }

    initializeDisplayList = () =>
    {
        const list = [];
        
        this.props.physicians.forEach(
            (physician) =>
            {
                list.push(
                    {
                        _id: physician._id,
                        userType: 'Physician',
                        Name: physician.Name,
                        AuthorizationStatus: physician.AuthorizationStatus
                    }
                )
            }
        )

        this.props.laboratories.forEach(
            (laboratory) =>
            {
                list.push(
                    {
                        _id: laboratory._id,
                        userType: 'Laboratory',
                        Name: laboratory.Name,
                        AuthorizationStatus: laboratory.AuthorizationStatus
                    }
                )
            }
        )

        this.props.pharmacies.forEach(
            (pharmacy) =>
            {
                list.push(
                    {
                        _id: pharmacy._id,
                        userType: 'Pharmacy',
                        Name: pharmacy.Name,
                        AuthorizationStatus: pharmacy.AuthorizationStatus
                    }
                )
            }
        )

        return list;
    }

    getDisplayList = () =>
    {
        const list = [];

        //console.log(this.state);

        if (this.state.physiciansChecked)
        {
            this.props.physicians.forEach(
                (physician) =>
                {
                    list.push(
                        {
                            _id: physician._id,
                            userType: 'Physician',
                            Name: physician.Name,
                            AuthorizationStatus: physician.AuthorizationStatus
                        }
                    )
                }
            )
        }

        if (this.state.laboratoriesChecked)
        {
            this.props.laboratories.forEach(
                (laboratory) =>
                {
                    list.push(
                        {
                            _id: laboratory._id,
                            userType: 'Laboratory',
                            Name: laboratory.Name,
                            AuthorizationStatus: laboratory.AuthorizationStatus
                        }
                    )
                }
            )
        }

        if (this.state.pharmaciesChecked)
        {
            this.props.pharmacies.forEach(
                (pharmacy) =>
                {
                    list.push(
                        {
                            _id: pharmacy._id,
                            userType: 'Pharmacy',
                            Name: pharmacy.Name,
                            AuthorizationStatus: pharmacy.AuthorizationStatus
                        }
                    )
                }
            )
        }

        list.sort(
            (a, b) => 
            {
                if (a.AuthorizationStatus === b.AuthorizationStatus) return 0;
                if (a.AuthorizationStatus === Pending) return -1;
                if (a.AuthorizationStatus === Suspended) return -1;
                if (a.AuthorizationStatus === Approved) return 1;

                return 1;
            }
        )

        return list;
    }

    onApprove = (user) =>
    {
        const { userType } = user;

        switch(userType)
        {
            case 'Physician':
                Remote.call('physician.updateAuthorization', 
                    this.props.access_token,
                    user._id, 
                    Approved,
                    (err, res) =>
                    {
                        if (err) {console.log(err);}
                    }
                );
                break;
            case 'Laboratory':
                Remote.call('laboratory.updateAuthorization', 
                this.props.access_token,
                user._id,
                Approved,
                    (err, res) =>
                    {
                        if (err) {console.log(err);}
                    }
                );
            case 'Pharmacy':
                Remote.call('pharmacy.updateAuthorization',
                    this.props.access_token,
                    user._id,
                    Approved,
                    (err, res) =>
                    {
                        if (err) console.log(err);
                    }
                );
        }
        
    }

    onSuspend = (user) =>
    {
        const { userType } = user;

        switch(userType)
        {
            case 'Physician':
                Remote.call('physician.updateAuthorization', 
                    this.props.access_token, 
                    user._id, 
                    Suspended,
                    (err, res) =>
                    {
                        if (err) {console.log(err);}
                    }
                );
                break;
            case 'Laboratory':
                Remote.call('laboratory.updateAuthorization', 
                    this.props.access_token, 
                    user._id,
                    Suspended,
                    (err, res) =>
                    {
                        if (err) {console.log(err);}
                    }
                );
            case 'Pharmacy':
                Remote.call('pharmacy.updateAuthorization',
                    this.props.access_token,
                    user._id,
                    Suspended,
                    (err, res) =>
                    {
                        if (err) console.log(err);
                    }
                );
            }
        
    }

    onFilterChanged = (event) =>
    {
        
        const { id, checked } = event.target;

        switch (id)
        {
            case 'physicians':
                this.setState({physiciansChecked: checked});
                break;
            case 'laboratories':
                this.setState({laboratoriesChecked: checked});
                break;
            case 'pharmacies':
                this.setState({pharmaciesChecked: checked});
                break;
        }
    }

    componentDidUpdate()
    {
        
    }

    render() 
    {
        //console.log(this.props);
        return(
            <div className={this.props.className + ' ' + 'br3 adminapprove shadow-6'}>
                {
                    ((this.props.physicians.length === 0) && (this.props.laboratories.length === 0) && (this.props.pharmacies.length === 0))
                    ?
                        <div className='pa7 f-arial-30 bold'>
                            {
                                IntlMessages[this.props.locale].intl.formatMessage(
                                    {
                                        id: 'physiciangreeting.loading',
                                        defaultMessage: 'Loading'
                                    }
                                ) + '...'
                            }
                        </div>
                    :
                        <div>
                            <div className='flex justify-between items-center'>
                                <div></div>
                                <div className='f-arial-20-nc bold'>
                                    <FormattedMessage
                                        id='adminapprovals.approvalspending'
                                        defaultMessage='Approvals Pending'
                                        description='Approvals Pending'
                                    />
                                </div>
                                <div
                                    onClick={this.props.onClose}
                                    className='close-nf mr1'
                                >
                                    &#9746;
                                </div>
                            </div>
                            <div className='ml3 mt2 mb2 mr3 f-arial-16-nc flex items-center justify-around'>
                                <div className='flex items-center'>
                                    <input
                                        type='checkbox'
                                        id='physicians'
                                        checked={this.state.physiciansChecked}
                                        onChange={this.onFilterChanged}
                                    />
                                    <label
                                        className='ml1'
                                        htmlFor='physicians'
                                    >
                                        <FormattedMessage
                                            id='admin.physicians'
                                            defaultMessage='Physicians'
                                            description='Physicians'
                                        />
                                    </label>
                                </div>
                                <div className='flex items-center'>
                                    <input
                                        type='checkbox'
                                        id='laboratories'
                                        checked={this.state.laboratoriesChecked}
                                        onChange={this.onFilterChanged}
                                    />
                                    <label
                                        className='ml1'
                                        htmlFor='laboratories'
                                    >
                                        <FormattedMessage
                                            id='admin.laboratories'
                                            defaultMessage='Laboratories'
                                            description='Laboratories'
                                        />
                                    </label>
                                </div>
                                <div className='flex items-center'>
                                    <input
                                        type='checkbox'
                                        id='pharmacies'
                                        checked={this.state.pharmaciesChecked}
                                        onChange={this.onFilterChanged}
                                    />
                                    <label
                                        className='ml1'
                                        htmlFor='pharmacies'
                                    >
                                        <FormattedMessage
                                            id='admin.pharmacies'
                                            defaultMessage='Pharmacies'
                                            description='Pharmacies'
                                        />
                                    </label>
                                </div>
                            </div>
                            <div className='bg-white ma3 scroll-y mh-500'>
                                <table>
                                    <thead className='bb b--light-gray'>
                                        <tr className=''>
                                            <th className='pa2 tc'>
                                                <FormattedMessage
                                                    id='register.name'
                                                    defaultMessage='Name'
                                                    description='Name'
                                                />
                                            </th>
                                            <th className='pa2 tc'>
                                                <FormattedMessage
                                                    id='admin.usertype'
                                                    defaultMessage='User type'
                                                    description='User type'
                                                />
                                            </th>
                                            <th className='pa2 tc'>
                                                <FormattedMessage
                                                    id='physicianstate.status'
                                                    defaultMessage='Status'
                                                    description='Status'
                                                />
                                            </th>
                                            <th className='pa2 tc'>
                                                <FormattedMessage
                                                    id='adminapprovals.action'
                                                    defaultMessage='Action'
                                                    description='Action'
                                                />
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody className=''>
                                        {
                                            this.getDisplayList().map(
                                                (user, idx) =>
                                                {
                                                    //console.log(user);
                                                    return(
                                                        <tr className='bb b--light-gray' key={idx}>
                                                            <td className={'pa2 ' + this.getCellClass(user)}>{user.Name}</td>
                                                            <td className={'pa2 ' + this.getCellClass(user)}>
                                                                {user.userType}
                                                            </td>
                                                            <td className={'pa2 ' + this.getCellClass(user)}>
                                                                <FormattedMessage
                                                                    id={statusId[user.AuthorizationStatus]}
                                                                    defaultMessage='Status'
                                                                />
                                                            </td>
                                                            <td className='pa2'>
                                                                <div className='flex justify-around items-center'>
                                                                    <button
                                                                        className='mr1 bg-buttonblue br1 shadow-3 pl2 pr2 pt1 pb1'
                                                                        disabled={user.AuthorizationStatus === Approved}
                                                                        onClick={() => this.onApprove(user)}
                                                                    >
                                                                        <FormattedMessage
                                                                            id='adminapprovals.approve'
                                                                            defaultMessage='Approve'
                                                                            description='Approve'
                                                                        />
                                                                    </button>
                                                                    <button
                                                                        className='mr1 bg-buttonblue br1 shadow-3 pl2 pr2 pt1 pb1'
                                                                        disabled={user.AuthorizationStatus !== Approved}
                                                                        onClick={() => this.onSuspend(user)}
                                                                    >
                                                                        <FormattedMessage
                                                                            id='adminapprovals.suspend'
                                                                            defaultMessage='Suspend'
                                                                            description='Suspend'
                                                                        />
                                                                    </button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    );
                                                }
                                            )
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                }
            </div>
        )
    }
}

export default 
connect(mapStateToProps)
(
    withTracker(
        () =>
        {
            const handle = Remote.subscribe('physicians.admin');
            const loading = !handle.ready();
            const labHandle = Remote.subscribe('laboratories.admin');
            const loadingLabs = !labHandle.ready();
            const pharmacyHandle = Remote.subscribe('pharmacies.admin');
            const loadingPharmacy = !pharmacyHandle.ready();

            return (
                {
                    physicians: !loading ? Physicians.find({}).fetch() : [],
                    laboratories: !loadingLabs ? Laboratories.find({}).fetch() : [],
                    pharmacies: !loadingPharmacy ? Pharmacies.find({}).fetch() : []
                }
            )
        }
    )(AdminApprovals)
);