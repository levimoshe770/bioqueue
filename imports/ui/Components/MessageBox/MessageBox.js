import React from 'react';
import './MessageBox.css'

/*
Properties:

Message: message displayed
Buttons: Array of strings, for each a button will be created

Events:
onButtonClose(string): 
onButtonCancel:

*/

const MessageBox = (props) =>
{
    return(
      <div className={props.className + " ba tc messagebox br3 shadow-4"}>
        <span className="close" onClick={props.onButtonCancel}>&#9746;</span>
        <div className="ma4 f-poppins-30 c-offwhite">
          {props.Message}
        </div>
        <div>
          {props.Buttons.map(
              (element,idx) => 
              {
                return(
                  <button 
                    key={idx}
                    className="f-oswald-20 ml3 mr3 mb4 pb2 pt2 pl5 pr5 bg-buttonblue br3 shadow-5"
                    onClick={() => props.onButtonClose(element)}
                  >
                    {element}
                  </button>
                );
              }
            )
          }
        </div>
      </div>
      
    )
}

export default MessageBox;