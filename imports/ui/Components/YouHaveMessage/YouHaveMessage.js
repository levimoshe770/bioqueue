import React from 'react';
import './YouHaveMessage.css'

/*
Props:

NumOfMessages: Number of messages waiting

Events:

onMessagesRead

 */

const YouHaveMessage = (props) =>
{
  return(
    <div className={
      props.className === undefined ? "" : props.className + 
        " pointer z-index-1 ma3 dib"}>
      <img 
        alt='mail' 
        src='icons/mail2.png' 
        width="40" 
        onClick={props.onMessagesRead}
      />
      {
        props.NumOfMessages > 0 &&
        <div 
          className='bg-yellow-tr f-arial-16-nc number br-100 relative h2 w2 tc pa2'
        >
          {props.NumOfMessages}
        </div>
      }
    </div>
  );
}

export default YouHaveMessage;