import React, { Component } from 'react';
import InListSearchBox from '../Utils/InListSearchBox';
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}


/*
className
patients
permissions
onClose
onMROpen
 */

class MyPatientsList extends Component
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            displayedPatients: this.props.patients
        }
    }

    physicianHasPermission = (patientId) =>
    {
        const p = this.props.permissions.find(
            (perm) => perm.PatientId === patientId
            
        );

        //console.log(patient);

        return p !== undefined;
    }

    renderTable()
    {
        return(                            
            <div className='bg-white mh-500 scroll-y bl br bb'>
                <table>
                    <thead className='bb b--light-gray'>
                        <tr className=''>
                            <th className='pa2 tc br b--light-gray'>
                                <FormattedMessage
                                    id='register.name'
                                    defaultMessage='Name'
                                    description='Name'
                                />
                            </th>
                            <th className='pa2 tc'>
                                <FormattedMessage
                                    id='medicalrecord.mr'
                                    defaultMessage='MR'
                                />
                            </th>
                        </tr>
                    </thead>
                    <tbody className=''>
                        {
                            this.state.displayedPatients.map(
                                (patient, idx) =>
                                {
                                    return(
                                        <tr className='bb b--light-gray' key={idx}>
                                            <td className='pa2 br b--light-gray'>
                                                {patient.Name}
                                            </td>
                                            <td className='pa2 tc'>
                                                <button
                                                    className='mr1 bg-buttonblue br1 shadow-3 pl2 pr2 pt1 pb1'
                                                    disabled={!this.physicianHasPermission(patient._id)}
                                                    onClick={() => this.props.onMROpen(patient)}
                                                >
                                                    <FormattedMessage
                                                        id='medicalrecord.mr'
                                                        defaultMessage='MR'
                                                    />
                                                </button>
                                            </td>
                                        </tr>
                                    );
                                }
                            )
                        }
                    </tbody>
                </table>
            </div>
        );
    }

    onSearchBoxChanged = (value, field) =>
    {
        const { patients } = this.props;
        this.setState(
            {
                displayedPatients: patients.filter(
                    (patient) =>
                    {
                        if (value.length === 0)
                            return true;

                        return patient.Name.toLowerCase().includes(value.toLowerCase());
                    }
                )
            }
        )
    }

    render()
    {
        //console.log(this.props);
        return(
            <div className={this.props.className + ' ' + 'br3 adminapprove shadow-6 pa3'}>
                <div className='flex justify-between items-center'>
                    <div></div>
                    <div className='f-arial-20-nc bold'>
                        <FormattedMessage
                            id='physicianstate.mypatients'
                            defaultMessage='My Patients'
                            description='My Patients'
                        />
                    </div>
                    <div
                        onClick={this.props.onClose}
                        className='close-nf mr1'
                    >
                        &#9746;
                    </div>
                </div>
                <div className=''>
                    <InListSearchBox
                        Field='names'
                        onSearchBoxChanged={this.onSearchBoxChanged}
                    />
                </div>
                {
                    this.props.patients.length === 0
                        ? 
                        <div>
                            {
                                IntlMessages[this.props.locale].intl.formatMessage(
                                    {
                                        id: 'physiciangreeting.loading',
                                        defaultMessage: 'loading'
                                    }
                                ) + '...'
                            }
                        </div>
                        : 
                        this.renderTable()
                }
            </div>
        )
    }
}

export default connect(mapStateToProps)(MyPatientsList);