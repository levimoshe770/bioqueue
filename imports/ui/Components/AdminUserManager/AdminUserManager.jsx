import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import Users from '../../../api/Lists/Users/users';
import '../AdminApprovals/AdminApprovals.css';

/*
className
onClose
 */

class AdminUserManager extends Component
{
    getCellClass = (user) =>
    {

    }

    renderTable()
    {
        return(
            <div className={this.props.className + ' ' + 'br3 adminapprove shadow-6'}>
                <div className='flex justify-between items-center'>
                    <div></div>
                    <div>
                        User List
                    </div>
                    <div
                        onClick={this.props.onClose}
                        className='close-nf mr1'
                    >
                        &#9746;
                    </div>

                </div>
                <div className='bg-white ma3 scroll-y mh-500'>
                    <table>
                        <thead className='bb b--light-gray'>
                            <tr className=''>
                                <th className='pa2 tc'>Name</th>
                                <th className='pa2 tc'>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.props.users.map(
                                    (user, idx) =>
                                    {
                                        return(
                                            <tr className='bb b--light-gray' key={idx}>
                                                <td className={'pa2 ' + this.getCellClass(user)}>{user.Name}</td>
                                                <td className='pa2'>
                                                    <button 
                                                        className='mr1 bg-buttonblue br1 shadow-3 pl2 pr2 pt1 pb1'
                                                    >
                                                        Block
                                                    </button>
                                                </td>
                                            </tr>
                                        )
                                    }
                                )
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
    render()
    {
        return(
            <div>
                {
                    this.props.users.length === 0
                        ? <div>Loading...</div>
                        : this.renderTable()
                }
            </div>
        );
    }
}

export default withTracker(
    () =>
    {

    }
)(AdminUserManager);