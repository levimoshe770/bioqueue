import React, { Component } from 'react';
import './ProfileTextLine.css'
import Dropdown from './Dropdown.jsx';
import AvailabilityCreator from './AvailabiltyCreator.jsx';
import { DatePicker } from 'antd';
import moment from 'moment';
import 'antd/dist/antd.css';

/*
id
field
value
type ('text','datepicker','combo','custom')
comboValues
required: boolean
onFieldChanged(id, value)
 */

export default class ProfileTextLine extends Component
{
  constructor(props)
  {
    super(props);
    this.state = 
    {
      inputDisabled: true,
      inputText: props.value,
      inputText1: props.value1,
      inputText2: props.value2,
      unit: props.unit
    }
  }

  getFieldClass = () =>
  {
    if (this.props.required && this.state.inputText === undefined)
      return ' required';

    return '';
  }

  getInputClass = () =>
  {
    if (this.props.required && this.state.inputText === undefined)
      return 'input-required';

    return 'no-border';
  }

  onEditClick = () =>
  {
    this.setState({inputDisabled: !this.state.inputDisabled});
    this.props.onFieldChanged(this.props.id, this.state.inputText);
  }

  onTextInputChange = (event) =>
  {
    const id = event.target.id;

    if (id.includes('_first'))
    {
      this.setState({inputText1: event.target.value});
      this.props.onFieldChanged(id, event.target.value);
      return;
    }

    if (id.includes('_second'))
    {
      this.setState({inputText2: event.target.value});
      this.props.onFieldChanged(id, event.target.value);
      return;
    }

    this.setState({inputText: event.target.value});
    this.props.onFieldChanged(this.props.id, event.target.value);
  }

  onTextKeyPress = (event) =>
  {
    if (event.key === 'Enter')
    {
      this.setState({inputDisabled: true})
      this.props.onFieldChanged(this.props.id, this.state.inputText);
    }
  }

  onDropdownSelectionChanged = (id, value) =>
  {
    console.log(id, value);
    if (id.includes('units_'))
    {
      this.setState({unit: value})
      this.props.onFieldChanged(id, value);
      return;
    }

    this.setState({inputText: value});
    this.props.onFieldChanged(this.props.id, value);
  }

  onAvailabilityChanged = (value) =>
  {
    this.setState({inputText: value});
    this.props.onFieldChanged(this.props.id, value);
  }

  onDateChange = (value) =>
  {
    this.setState({inputText: value});
    this.props.onFieldChanged(this.props.id, value);
  }

  onRadioChanged = (e) =>
  {
    this.setState({inputText: e.target.value});
    this.props.onFieldChanged(this.props.id, e.target.value);
  }

  renderInput = () =>
  {
    if (this.props.type === 'text')
      return (
          <input 
            className={'w5 ' + this.getInputClass()} 
            disabled={this.state.inputDisabled}
            value={this.state.inputText}
            type='text' 
            id={this.props.id}
            ref={this.state.inputFocus}
            onChange={this.onTextInputChange}
            onKeyPress={this.onTextKeyPress}
          />
      )
    else if (this.props.type === 'combo')
    {
      return(
        <div className=''>
          <Dropdown
            className='w5'
            id={this.props.id}
            description=''
            disabled={this.state.inputDisabled}
            required={this.props.required}
            value={this.state.inputText}
            items={this.props.comboValues}
            onSelectionChanged={this.onDropdownSelectionChanged}
          />
        </div>
      )
    }
    else if (this.props.type === 'datepicker')
    {
      //console.log(this.props.value);
      return(
        <div className={'w5 ' + this.getInputClass()}>
          <DatePicker
            onChange={this.onDateChange}
            disabled={this.state.inputDisabled}
            defaultValue={new moment(this.props.value)}
          />
        </div>
      )
    }
    else if (this.props.type === 'textwithunits')
    {
      return(
        <div className={'flex items-center w5 ' + this.getInputClass()}>
          <input
            className={'' + this.getInputClass()}
            disabled={this.state.inputDisabled}
            value={this.state.inputText}
            type='text' 
            id={this.props.id}
            ref={this.state.inputFocus}
            onChange={this.onTextInputChange}
            onKeyPress={this.onTextKeyPress}          
          />
          <Dropdown
            className=''
            disabled={this.state.inputDisabled}
            required={this.props.required}
            value={this.state.unit}
            id={'units_' + this.props.id}
            items={this.props.comboValues}
            onSelectionChanged={this.onDropdownSelectionChanged}
          />
        </div>
      )
    }
    else if (this.props.type === 'doubleinput')
    {
      return(
        <div className={'flex items-center w5'}>
          <input
            className={'' + this.getInputClass()}
            disabled={this.state.inputDisabled}
            value={this.state.inputText1}
            type='text' 
            id={this.props.id + '_first'}
            ref={this.state.inputFocus}
            onChange={this.onTextInputChange}
            onKeyPress={this.onTextKeyPress}          
          />
          <input
            className={'' + this.getInputClass()}
            disabled={this.state.inputDisabled}
            value={this.state.inputText2}
            type='text' 
            id={this.props.id + '_second'}
            ref={this.state.inputFocus}
            onChange={this.onTextInputChange}
            onKeyPress={this.onTextKeyPress}          
          />
        </div>
      )
    }
    else if (this.props.type === 'radio')
    {
      //console.log(this.)
      return(
        <div className='flex justify-start items-center w5'>
          {
            this.props.radioLabels.map(
              (lab, idx) =>
              {
                return(
                  <div key={idx} className='pr2 flex items-center'>
                    <input 
                      className=''
                      type='radio' 
                      id={lab} 
                      name={this.props.field} 
                      value={lab} 
                      checked={this.state.inputText === lab}
                      disabled={this.state.inputDisabled}
                      onChange={this.onRadioChanged}
                    />
                    <label className='pl1' htmlFor={lab}>
                      {lab}
                    </label>
                  </div>
                )
              }
            )
          }
        </div>
      )
    }
    else if (this.props.type === 'custom')
    {
      return(
        <AvailabilityCreator 
            className={'w5'}
            id={this.props.id}
            description=''
            disabled={this.state.inputDisabled}
            required={this.props.required}
            value={this.state.inputText}
            onSelectionChanged={this.onAvailabilityChanged}
        />
      );
    }
  }

  render()
  {
    //console.log(this.state);
    return(
      <div className='flex items-center justify-between mb2'>
        <label className='w4'>{this.props.field}</label>
        {this.renderInput()}
        <div className='pointer' onClick={this.onEditClick}>✐</div>
      </div>
    )
  }
}