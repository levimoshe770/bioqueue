import React, {Component} from 'react';
import ProfileTextLine from './ProfileTextLine.jsx';
import './PhysicianProfile.css';
import { connect } from 'react-redux';
import { IntlMessages } from '../../../../translation/IntlMessages';
import FileUpload from '../Utils/FileUpload.jsx';
import UserFiles from '../../../api/Lists/UserFiles/UserFiles';

const mapStateToProps = state =>
{
  return {patientId: state.patientId, locale: state.locale};
}

/*
className
Physician
onProfileUpdate
onClose
 */

class PhysicianProfile extends Component
{
  constructor(props)
  {
    super(props);
    this.state=
    {
      Physician: this.props.Physician
    };
  }

  onFieldChanged = (id, value) =>
  {
    const physician = this.state.Physician;

    switch (id)
    {
      case 'name':
        physician.Name = value;
        break;
      case 'address':
        physician.Address = value;
        break;
      case 'email':
        physician.Email = value;
        break;
      case 'phone':
        physician.Phone = value;
        break;
      case 'specialty':
        physician.Specialty = value;
        break;
      case 'availability':
        physician.Availability = value;
        break;
      case 'resolution':
        physician.Resolution = parseInt(value);
        break;
      default:
        break;    
    }

    this.setState({Physician: physician});
  }

  onApply = () =>
  {
    this.props.onProfileUpdate(this.state.Physician);
  }

  getSpecialties = () =>
  {
    const { intl } = IntlMessages[this.props.locale];
    const list = 
    [
      intl.formatMessage(
        {
          id: 'specialties.opthalmologist',
          defaultMessage: 'Opthalmologist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.pathologist',
          defaultMessage: 'Pathologist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.surgeon',
          defaultMessage: 'Surgeon'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.dermatologist',
          defaultMessage: 'Dermatologist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.proctologist',
          defaultMessage: 'Proctologist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.gastrologist',
          defaultMessage: 'Gastrologist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.urologist',
          defaultMessage: 'Urologist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.gynecologist',
          defaultMessage: 'Gynecologist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.venereologist',
          defaultMessage: 'Venereologist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.otolaryngologist',
          defaultMessage: 'Otolaryngologist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.pulmonologist',
          defaultMessage: 'Pulmonologist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.nephrologist',
          defaultMessage: 'Nephrologist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.orthopedist',
          defaultMessage: 'Orthopedist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.psychiatrist',
          defaultMessage: 'Psychiatrist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.neuropathologist',
          defaultMessage: 'Neuropathologist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.nutritionist',
          defaultMessage: 'Nutritionist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.hematologist',
          defaultMessage: 'Hematologist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.oncologist',
          defaultMessage: 'Oncologist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.therapist',
          defaultMessage: 'Therapist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.pediator',
          defaultMessage: 'Pediator'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.geriator',
          defaultMessage: 'Geriator'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.allergist',
          defaultMessage: 'Allergist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.cardiologist',
          defaultMessage: 'Cardiologist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.radiologist',
          defaultMessage: 'Radiologist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.endocrinologist',
          defaultMessage: 'Endocrinologist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.narcologist',
          defaultMessage: 'Narcologist'
        }
      ),
      intl.formatMessage(
        {
          id: 'specialties.anesthesiologist',
          defaultMessage: 'Anesthesiologist'
        }
      )
    ]

    return list;
  }

  onFileUploaded = (file) =>
  {
    const url = UserFiles.findOne({_id: file._id}).link();
    const profile = this.state.Physician;
    profile.Image = url;
    this.setState({Physician: profile});
  }

  onFileUploadFailed = (error) =>
  {
    console.error(error);
  }

  render()
  {
    //console.log(this.props);
    return(
      <div className={this.props.className + ' w-90 f-arial-16-nc'}>
        <div className='b--lightgray br4 shadow-6 bg-profile '>
            <div className='flex items-center justify-between'>
              <div className=''>
                
              </div>
              <div className='f-arial-20-nc bold'>
                {this.props.Physician.Name}
              </div>
              <div
                  onClick={this.props.onClose}
                  className='close-nf mr1'
                >
                  &#9746;
              </div>
            </div>
            <div className='flex justify-between ma2 f-arial-16-nc'>
              <div className='flex-grow-2 mr2'>
                <ProfileTextLine 
                  id='name'
                  field=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: 'register.name',
                        defaultMessage: 'Name'
                      }
                    )
                  }
                  type='text'
                  value={this.props.Physician.Name}
                  onFieldChanged={this.onFieldChanged}
                  required={true}
                />
                <ProfileTextLine
                  id='address'
                  field=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: 'userdata.address',
                        defaultMessage: 'Address'
                      }
                    )
                  }
                  type='text'
                  value={this.props.Physician.Address}
                  onFieldChanged={this.onFieldChanged}
                  required={true}
                />
                <ProfileTextLine
                  id='email'
                  field=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: 'register.email',
                        defaultMessage: 'Email'
                      }
                    )
                  }
                  type='text'
                  value={this.props.Physician.Email}
                  onFieldChanged={this.onFieldChanged}
                  required={true}
                />
                <ProfileTextLine
                  id='phone'
                  field=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: 'register.phone',
                        defaultMessage: 'Phone'
                      }
                    )
                  }
                  type='text'
                  value={this.props.Physician.Phone}
                  onFieldChanged={this.onFieldChanged}
                  required={false}
                />
                <ProfileTextLine
                  id='specialty'
                  field=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: 'physicianprofile.specialty',
                        defaultMessage: 'Specialty'
                      }
                    )
                  }
                  type='combo'
                  comboValues={this.getSpecialties()}
                  value={this.props.Physician.Specialty}
                  onFieldChanged={this.onFieldChanged}
                  required={true}
                />
                <ProfileTextLine
                  id='availability'
                  field=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: 'physicianprofile.availability',
                        defaultMessage: 'Availability'
                      }
                    )
                  }
                  type='custom'
                  value={this.props.Physician.Availability}
                  onFieldChanged={this.onFieldChanged}
                  required={true}
                />
                <ProfileTextLine
                  id='resolution'
                  field=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: 'physicianprofile.resolution',
                        defaultMessage: 'Resolution'
                      }
                    )
                  }
                  type='text'
                  value={this.props.Physician.Resolution}
                  onFieldChanged={this.onFieldChanged}
                  required={true}
                />
              </div>
              <div className='flex flex-column items-center mr2 ml2'>
                <img src={this.props.Physician.Image} alt='profile' width='150' />
                <FileUpload
                  className='mt3 bg-buttonblue br3 pointer shadow-3 pl3 pr3 pt2 pb2'
                  label='Update Photo'
                  onFileUploaded={this.onFileUploaded}
                  onUploadFail={this.onFileUploadFailed}
                />
              </div>
            </div>
            <div className='flex justify-around pa3'>
              <button 
                className='w4 bg-buttonblue pa2 br4 shadow-3'
                onClick={this.onApply}
              >
                Apply
              </button>
              <button 
                className='w4 bg-buttonblue pa2 br4 shadow-3'
                onClick={this.props.onClose}
              >
                Close
              </button>
            </div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps)(PhysicianProfile);