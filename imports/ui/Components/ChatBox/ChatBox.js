import React, { Component } from 'react';
import MessageBox from './MessageBox';
import MessageBoard from './MessageBoard';
import { date, hacker } from 'faker';
import './ChatBox.css';
import { withTracker } from 'meteor/react-meteor-data'
import { Meteor } from 'meteor/meteor';
import Messages from '../../../api/Lists/Messages/Messages';

/*
className: Class for the external container
senderId: id of sender
senderRole: role of sender
senderName: name of sender
recipientId: Name of the other side of the conversation
recipientRole
recipientName

onClose
onShareMedicalRecord
 */

class ChatBox extends Component 
{
  constructor(props)
  {
    super(props);
  }

  onMessageSend = (message) =>
  {
    const { senderId, senderName, senderRole, recipientId, recipientName, recipientRole } = this.props;

    const msg = 
    {
      from: {id: senderId, name: senderName, role: senderRole},
      to: {id: recipientId, name: recipientName, role: recipientRole},
      content: message,
      datetime: new Date(),
      unread: 1
    }

    Meteor.call('message.send',
      msg
    )
  }

  onMessageDelete = (message) =>
  {
    console.log('Deleting', message);

    Meteor.call('message.delete', message);
  }

  // createRandomMessageList(numOfMessages) {
  //   const { recipientName } = this.props;
  //   let i;

  //   let res = [];

  //   for (i = 0; i < numOfMessages; i++) {
  //     res.push(
  //       {
  //         from: ((i % 2 === 0) ? "Me" : recipientName),
  //         to: ((i % 2 !== 0) ? "Me" : recipientName),
  //         datetime: date.recent(),
  //         message: hacker.phrase()
  //       }
  //     );
  //   }

  //   return res;
  // }

  render()
  {
    return(
      <div 
        className="flex flex-column mw-540"
        style={{width: 540}}
      >
        <div className={this.props.className + " mt4 ml2 ba b--white-50 shadow-3 bg-lightblue2-opac"}>
          <div className="bb b--white-50 flex items-center justify-between">
              { this.props.senderRole === 'patient'
                ? <button 
                    className='pointer ml2 pa1 br3 bg-lightblue2 ba b--washed-blue'
                    onClick={() => this.props.onShareMedicalRecord(this.props.recipientId, this.props.recipientName)}
                  >
                    <img 
                      src='icons/icons8-share-3-48.png' alt='share' width='22' height='22' 
                    />
                  </button>
                :
                <div></div>
              }
              <div className="pa3 tc f-courgette-22 c-white">
                {this.props.recipientName}
              </div>
              <div
                onClick={this.props.onClose}
                className="close-nf mr2 mt0"
              >
                &#9746;
              </div>
          </div>
          <div className="h-93">
            <div className="flex flex-column">
              <MessageBoard 
                className="flex-grow-1"
                recipientName={this.props.recipientName}
                messageList={this.props.messages}
                onMessageDelete={this.onMessageDelete}
              />
              <MessageBox 
                className="" 
                onMessageSend={this.onMessageSend}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withTracker(
  ({recipientId}) =>
  {
      const messageHandle = Meteor.subscribe('user.singleChat', recipientId);

      const loading = !messageHandle.ready();

      return (
        {
            messages: !loading ? Messages.find({}).fetch() : []
        }
      );
  }
)
(ChatBox);