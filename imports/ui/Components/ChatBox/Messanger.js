import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { withRouter } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { setPatientMR } from '../../Redux/actions';
import { connect } from 'react-redux';
import MedicalRecordPermissions from '../../../api/Lists/MedicalRecordPermissions/MedicalRecordPermissions.js';
import './Messanger.css'
import { FormattedMessage } from 'react-intl';

/*
className: class name for the first container
role: Role of the caller
ownerId: user id of caller
messageList: array of chatObjects
    {
      name, image, numOfUnread
    }
onChatSelection (chatObject)
onCloseChatBox ()
 */

const mapStateToProps = state =>
{
  return {patientId: state.patientId};
}

const mapDispatchToProps = (dispatch) =>
{
  return {onPatientMRIdChange: (patientId) => dispatch(setPatientMR(patientId))}
}

class Messanger extends Component 
{
  constructor(props)
  {
    super(props);
    this.state = {selectedId: -1}
  }

  calcCellClassName = (messageObj, idx) =>
  {
    const base = "pa2 f20 pointer ba"
    return base + " " + (messageObj.numOfUnread > 0 ? "unread" : "") + 
           (idx === this.state.selectedId ? " selectedChat" : "");
  }

  permissionGrantedFor = (patientId) =>
  {
    if (this.props.role !== 'physician')
      return false;

    const permission = this.props.permissions.find(
      (p) =>
      {
        return p.PatientId === patientId;
      }
    );

    return permission !== undefined;
  }

  calcMRButtonClass = (messageObj, idx) =>
  {

  }

  onSelect = (messageObj, idx) =>
  {
    let selected;
    if (idx === this.state.selectedId)
      selected = -1;
    else
      selected = idx;

    this.setState({selectedId: selected});

    if (selected >= 0)
      this.props.onChatSelection(messageObj);
    else 
      this.props.onCloseChatBox();
  }

  onMedicalRecordClick = (messageObj, idx) =>
  {
    this.props.onPatientMRIdChange(messageObj.id);
    this.props.history.push('/medicalrecord');
  }

  render()
  {
    //console.log(this.props);
    const { className, messageList } = this.props;
    return(
      <div 
        className={className + " backlinear ba b--light-gray br4 shadow-6 flex flex-column items-center"}>
        <div 
          className="close ma1 self-end"
          onClick={this.props.onClose}
        >
          &#9746;
        </div>
        <div className='ma2 height-control-500 pa1'>
          <table className=''>
            <thead>
              <tr>
                <th 
                  className='ba tc bold f-arial-16-b'
                  colSpan={this.props.role === 'physician' ? "4" : '3'}
                >
                  <FormattedMessage
                    id='messanger.messages'
                    defaultMessage='Messages'
                    description='Messages'
                  />
                </th>
              </tr>
            </thead>
            <tbody>
            {
              messageList.map(
                (messageObj,idx) =>
                {
                  return(
                    <tr key={idx}>
                      <td 
                        className={this.calcCellClassName(messageObj, idx)}
                        id={"img_" + idx.toString()}
                        onClick={()=>this.onSelect(messageObj, idx)}
                      >
                        <img className="br-100" alt="" src={messageObj.image} height="30"/>
                      </td>
                      <td 
                        className={this.calcCellClassName(messageObj, idx)}
                        id={"name_" + idx.toString()}
                        onClick={()=>this.onSelect(messageObj, idx)}
                      >
                        {messageObj.name}
                      </td>
                      <td 
                        className={this.calcCellClassName(messageObj, idx)}
                        id={"numOfUnread_" + idx.toString()}
                        onClick={()=>this.onSelect(messageObj, idx)}
                      >
                        {messageObj.numOfUnread}
                      </td>
                      {
                        this.props.role === 'physician' &&
                        <td className='ba'>
                          <div className='flex justify-center'>
                            <button 
                              className={this.calcMRButtonClass(messageObj,idx)}
                              disabled={!this.permissionGrantedFor(messageObj.id)}
                              onClick={() => this.onMedicalRecordClick(messageObj,idx)}
                            >
                              <FormattedMessage
                                id='medicalrecord.mr'
                                defaultMessage='MR'
                              />
                            </button>
                          </div>
                        </td>  
                      }
                    </tr>
                  );
                }
              )
            }
          </tbody>
        </table>
        </div>
      </div>
    )
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(
  withTracker(
    ({role}) =>
    {
      if (role !== 'physician')
        return;

      const permissionHandler = Meteor.subscribe('medicalrecordpermissions.physicianTotal');
      const permissionLoading = !permissionHandler.ready();

      return(
        {permissions: !permissionLoading ? MedicalRecordPermissions.find({}).fetch() : []}
      )
    }
  )
  (Messanger)
)
);