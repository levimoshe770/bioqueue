import React, { Component } from 'react';
import './MessageBoard.css'
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';
import { Text, File } from '../../../api/Lists/Messages/MessagesSchema';
import { FormattedMessage } from 'react-intl';

const mapStateToProps = (state) =>
{
    return {locale: state.locale, access_token: state.access_token}
}

/*
className
recipientName
messageList: Array of objects
{
  from: string
  to: string
  datetime:
  message: string
}
*/

class MessageBoard extends Component
{
  constructor(props)
  {
    super(props);
    this.state = 
    {
      userId: '',
      showDeleteIcon: -1,
      count: this.props.messageList.length,
      timer: null,
      visibilityTimers: new Map()
    }

  }


  getMe = () =>
  {
    return IntlMessages[this.props.locale].intl.formatMessage(
      {
        id: 'chatbox.me',
        defaultMessage: 'Me'
      }
    );
  }

  getMessageClass = (from, to) =>
  {
    const base = "pa2 mw5 ba mb2 ml1 mr1 br4 shadow-5"

    const meClass="me";
    const otherClass="other";

    const res = base + " " + (from === this.getMe() ? meClass : otherClass)
    return (res);
  }

  getFileIcon = (fileType) =>
  {
    switch (fileType) {
      case 'image/png':
      case 'image/jpeg':
        return('icons/icons8-image-file-200.png')
      case 'application/pdf':
        return('icons/icons8-pdf-80.png');
      case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
        return('icons/icons8-word-96.png')
      default:
        break;
    }
  }

  getDisplayText = (text) =>
  {
    return text;
  }

  getContent = (content) =>
  {
    switch (content.msgType)
    {
      case Text:
        return <div>{content.message}</div>;
      case File:
        return (
          <div className='flex flex-column items-center'>
            <img
              className='pointer'
              onClick={() => this.onClickContent(content)}
              src={this.getFileIcon(content.message.fileType)}
              alt={content.msgType}
              width='100'
            />
            <div className='mw5 mt2 tc'>
              {this.getDisplayText(content.message.displayText)}
            </div>
          </div>
        )
    }
  }

  getContentClass = (content) =>
  {
    switch (content.msgType)
    {
      case Text:
        return '';
      case File:
        return 'c-deepblue underline pointer';
    }
  }

  onClickContent = (content) =>
  {
    const url = content.message.url;

    const params = 'resizable=no,status=no,menubar=no';

    window.open(url, url, params);

  }

  onTimeOut = () =>
  {
    this.updateVisibilityTimers();
  }

  componentDidMount()
  {

    Meteor.call('user.getRealUserId',
      (err, id) =>
      {
        if (err)
        {
          console.log(err);
          return;
        }

        this.setState({userId: id});
      }
    );

    const t = setInterval(this.onTimeOut, 500);
    this.setState({timer: t});
  }

  componentDidUpdate()
  {
    const { count } = this.state;
    const { messageList } = this.props;

    if (messageList.length > count)
    {
      this.setState({count: messageList.length});
      this.scrollBottom();
    }
  }

  componentWillUnmount()
  {
    clearInterval(this.state.timer);
  }

  elementVisible = (idx) =>
  {
    const el = document.getElementById('msg_' + idx.toString());
    const parent = el.parentElement;

    const top = el.offsetTop - parent.offsetTop;
    const bottom = top + el.offsetHeight;
    const scrollTop = parent.scrollTop;
    const scrollBottom = scrollTop + parent.offsetHeight;

    //console.log('scroll', scrollTop*100/parent.scrollHeight);

    return top >= scrollTop - 40 && bottom <= scrollBottom;
  }

  updateVisibilityTimers = () =>
  {
    const { messageList } = this.props;
    const tms = this.state.visibilityTimers;
    const currentTime = new Date();
    const timeThresshold = 3000;

    let i;
    for (i = 0; i < messageList.length; i++)
    {
      if (!((messageList[i]).unread === 1 && 
             messageList[i].to.id === this.state.userId))
        continue;

      if (this.elementVisible(i))
      { 

        const visibility = tms.has(messageList[i]._id);

        // If not in list, add to list with current date
        if (!visibility)
        {
          // Add to the list
          tms.set(messageList[i]._id, currentTime)
        }
        else 
        {
          // Check how much time passed since it was in the list
          const timePassed = currentTime.getTime() - tms.get(messageList[i]._id).getTime();

          // If time passed, set read, and remove from list
          if (timePassed >= timeThresshold)
          {
            Meteor.call('message.read', messageList[i]._id);

            tms.delete(messageList[i]._id);
          }
        }
      }
      else
      {
        const visibility = tms.has(messageList[i]._id);

        // if element is in the list
        if (visibility)
        {
          // Remove from list
          tms.delete(messageList[i]._id);
        }
      }
    }

    this.setState({visibilityTimers: tms});
  }

  onScroll = (event) =>
  {
    this.updateVisibilityTimers();
  }

  scrollBottom = () =>
  {
    const el = document.getElementById('messageBoard');
    el.scrollTop = el.scrollHeight;
  }

  render()
  {
    const { userId } = this.state;
    const { messageList } = this.props;

    return(
      <div
          className={this.props.className + " scroll-smooth scroll-y bb b--white-50 backlinear flex flex-column "}
          style={{height: 400}}
          onScroll={this.onScroll}
          id='messageBoard'
      >
        {
          messageList.map(
            (messageObj, idx) =>
            {
              const from = (messageObj.from.id === userId ? this.getMe() : this.props.recipientName);
              const to = (messageObj.to.id === userId ? this.getMe() : this.props.recipientName)
              return(
                <div 
                    className={this.getMessageClass(from, to)}
                    key={idx}
                    id={'msg_' + idx.toString()}
                    onMouseEnter={() => this.setState({showDeleteIcon: idx})}
                    onMouseLeave={() => this.setState({showDeleteIcon: -1})}
                >
                  <div className="fromStyle pb2">
                    {from}
                  </div>
                  <div className=
                    {
                      'flex ' +
                      ((messageObj.unread === 1 && 
                        messageObj.to.id === userId)
                      ? 'bold' : '')
                    }
                  >
                    {this.getContent(messageObj.content)}
                  </div>
                  {
                    this.state.showDeleteIcon === idx
                    &&
                    <div 
                      className = 'ba br3 b--light-gray pl2 pr2 o-50 pointer z-index-100 di'
                      onClick={() => this.props.onMessageDelete(messageObj)}
                    >
                      <FormattedMessage
                        id='system.delete'
                        defaultMessage='Delete'
                        description='Delete'
                      />
                    </div>
                  }
                  <div className="dateStyle pt2">
                    {
                      messageObj.datetime.toLocaleString(this.props.locale,
                        {
                          day: "numeric",
                          month: "short",
                          year: "numeric",
                          hour: "numeric",
                          minute: "2-digit"
                        }
                      )
                    }
                  </div>
                </div>
              )
            }
          )
        }
      </div>
    )
  }
}

export default connect(mapStateToProps)(MessageBoard);