import React, { Component } from 'react';
import InputEmoji from 'react-input-emoji';
import './MessageBox.css'
import FileUpload from '../Utils/FileUpload.jsx';
import { Text, File } from '../../../api/Lists/Messages/MessagesSchema';
import UserFiles from '../../../api/Lists/UserFiles/UserFiles';

/*
className
onMessageSend
 */

class MessageBox extends Component
{
  constructor(props)
  {
    super(props);
    this.state = 
    {
      textMessage: "",
      msgType: -1,
      url: ''
    }
  }

  onTextMessageChanged = (text) =>
  {
    this.setState({textMessage: text});
  }

  sendMessage = () =>
  {
    this.props.onMessageSend(
      {
        msgType: Text,
        message: this.state.textMessage
      }
    );
    this.setState({textMessage: ""})
  }

  getFileUrl = (file) =>
  {
    return UserFiles.findOne({_id: file._id}).link();
  }

  onFileUploaded = (file) =>
  {
    console.log('called', file);
    if (UserFiles.findOne({_id: file._id}) === undefined)
      return;

    this.props.onMessageSend(
      {
        msgType: File,
        message: 
        {
          displayText: file.name,
          fileType: file.mime,
          url: this.getFileUrl(file)
        }
      }
    );

    console.log('file uploaded', file);
  }

  onFileUploadFailed = (err) =>
  {
    console.log(err);
  }
   
  render()
  {
    return(
      <div className={this.props.className + " bg-linear ba ma0 pa1 flex justify-around items-center"}>
        <FileUpload
          className='pl1 pr1 pointer'
          label='upload'
          imgSrc='icons/attach2.png'
          imgWidth='30'
          onFileUploaded={this.onFileUploaded}
          onUploadFail={this.onFileUploadFailed}
        />
        <img 
          className="pl1 pr1 pointer"
          alt="attach" 
          src="icons/icons8-video-camera-90.png" 
          width="25" 
        />
        <img 
          className="pl1 pointer"
          alt="attach" 
          src="icons/icons8-microphone-100.png" 
          width="25" 
        />
        <InputEmoji 
          onChange={this.onTextMessageChanged}
          value=""
          cleanOnEnter={true}
          borderRadius={10}
          onEnter={this.sendMessage}
          placeholder=""
        />
      </div>
    )
  }
}

export default MessageBox;