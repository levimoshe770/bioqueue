import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { Meteor } from 'meteor/meteor';
import { connect } from 'react-redux';
import { IntlMessages } from '../../../../translation/IntlMessages';
import './LabResults.css'
import { Numeric, Image, Video, Voice, Text } from '../../../api/Lists/LabTests/LabTestsCollection';
import AddImageResult from '../LabTestDetails/AddImageResult.jsx';
import AddNumericResult from '../LabTestDetails/AddNumericResult.jsx'
import LabResult from '../MedicalRecord/LabResult.jsx';
import AddTextResult from './AddTextResult';
import Remote from '../../Remote';

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}

/*
className
labTest
 */

class LabResults extends Component
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            showResultTypes: false,
            showAddResult: false,
            addResultType: -1
        }
    }

    getResultTypes = () =>
    {
        const { locale } = this.props;

        return (
            [
                IntlMessages[locale].intl.formatMessage(
                    {
                        id: 'labresults.numeric',
                        defaultMessage: 'Numeric'
                    }
                ),
                IntlMessages[locale].intl.formatMessage(
                    {
                        id: 'labresults.image',
                        defaultMessage: 'Image'
                    }
                ),
                IntlMessages[locale].intl.formatMessage(
                    {
                        id: 'labresults.video',
                        defaultMessage: 'Video'
                    }
                ),
                IntlMessages[locale].intl.formatMessage(
                    {
                        id: 'labresults.voice',
                        defaultMessage: 'Voice'
                    }
                ),
                IntlMessages[locale].intl.formatMessage(
                    {
                        id: 'labresults.Text',
                        defaultMessage: 'Text'
                    }
                )
            ]
        )
    }

    addImageResult = (resultName, fileLink) =>
    {
        console.log(fileLink);

        Remote.call('labtest.addResults',
            this.props.labTest._id,
            new Date(),
            {
                Name: resultName,
                Type: Image,
                Result: fileLink
            }
        );

        this.setState({showAddResult: false})
    }

    onNumericResultAdded = (resultName, value, min, max) =>
    {
        Remote.call('labtest.addResults',
            this.props.labTest._id,
            new Date(),
            {
                Name: resultName,
                Type: Numeric,
                Result: value,
                Min: min,
                Max: max
            }
        );
        this.setState({showAddResult: false});
    }

    onTextResultAdded = (resultName, text) =>
    {
        Remote.call('labtest.addResults',
            this.props.labTest._id,
            new Date(),
            {
                Name: resultName,
                Type: Text,
                Result: text
            }
        );
        this.setState({showAddResult: false});
    }

    renderAddImageResult()
    {
        return(
            <AddImageResult
                className=''
                onImageResultAdded={this.addImageResult}
                onCancel={() => this.setState({showAddResult: false})}
            />
        )
    }

    renderAddNumericResult()
    {
        return(
            <AddNumericResult
                className=''
                label={this.props.labTest.Name}
                onNumericResultAdded={this.onNumericResultAdded}
                onCancel={() => this.setState({showAddResult: false})}
            />
        )
    }

    renderAddTextResult()
    {
        return(
            <AddTextResult
                className=''
                label={this.props.labTest.Name}
                onTextResultAdded={this.onTextResultAdded}
                onCancel={() => this.setState({showAddResult: false})}
            />
        )
    }

    renderNumericResult = (result, idx) =>
    {
        return(
            <LabResult
                key={idx}
                name={result.Name}
                type={Numeric}
                result={result.Result}
                min={result.Min}
                max={result.Max}
            />
        )
    }

    renderImageResult = (result, idx) =>
    {
        return(
            <LabResult
                key={idx}
                name={result.Name}
                type={Image}
                result={result.Result}
            />
        )
    }

    renderTextResult = (result, idx) =>
    {
        return(
            <LabResult
                key={idx}
                name={result.Name}
                type={Text}
                result={result.Result}
            />
        )
    }

    renderResults()
    {
        const { Results } = this.props.labTest;
        if (Results === undefined)
            return;

        return(
            <div>
                {
                    Results.map(
                        (r, idx) =>
                        {
                            switch(r.Type)
                            {
                                case Numeric:
                                    return this.renderNumericResult(r, idx);
                                case Video:
                                case Voice:
                                case Image:
                                    return this.renderImageResult(r, idx);
                                case Text:
                                    return this.renderTextResult(r ,idx);
                            }
                        }
                    )
                }
            </div>
        )
    }

    renderAddResult()
    {
        /*
        0 - Numeric
        1 - Image
        2 - Video
        3 - Voice
        4 - Text
        */

        switch(this.state.addResultType)
        {
            case 0:
                return this.renderAddNumericResult();
            case 1:
            case 2:
            case 3:
                return this.renderAddImageResult();
            case 4:
                return this.renderAddTextResult();
        }
    }

    onResultTypeSet = (idx) =>
    {
        this.setState(
            {
                showResultTypes: false,
                showAddResult: true,
                addResultType: idx
            }
        );
    }

    onMouseEnter = (event) =>
    {
        const el = document.getElementById(event.target.id);
        el.setAttribute("class", "item-selected pointer");
    }

    onMouseLeave = (event) =>
    {
        const el = document.getElementById(event.target.id);
        el.removeAttribute('class');
    }

    render()
    {
        console.log(this.props);
        return(
            <div 
                className={this.props.className + ' flex flex-column bb b--gray'}
            >
                <div className='pa3 shadow-3 br3 mt2'>
                    {this.renderResults()}
                </div>
                {
                    this.state.showAddResult &&
                    <div 
                        className='absolute bg-white pa3 br3 shadow-3 self-center'
                    >
                        {this.renderAddResult()}
                    </div>
                }
                {
                    this.state.showResultTypes &&
                    <div 
                        className='absolute bg-light-gray pa2 br3 shadow-3 self-center'
                    >
                        {
                            this.getResultTypes().map(
                                (rt, idx) => 
                                {
                                    return(
                                        <div 
                                            key={idx}
                                            id={'typ_' + idx.toString()}
                                            onClick={() => this.onResultTypeSet(idx)}
                                            onMouseEnter={this.onMouseEnter}
                                            onMouseLeave={this.onMouseLeave}
                                        >
                                            {rt}
                                        </div>
                                    );
                                }
                            )
                        }
                    </div>
                }
                <button
                    className='self-center bg-buttonblue br-pill pl3 pr3 pt2 pb2 shadow-3 mt2 mb2'
                    onClick={() => this.setState({showResultTypes: !this.state.showResultTypes})}
                >
                    <FormattedMessage
                        id='labresults.addresult'
                        defaultMessage='Add Result'
                    />
                </button>
            </div>
        )
    }
}

export default 
connect(mapStateToProps) 
(
    LabResults
);