import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';

/*
className
label
onNumericResultAdded
onCancel
 */

class AddNumericResult extends Component
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            value: '',
            name: '',
            min: '',
            max: ''
        }
    }

    onSave = () =>
    {
        this.props.onNumericResultAdded(
            this.state.name,
            parseFloat(this.state.value),
            parseFloat(this.state.min),
            parseFloat(this.state.max)
        )
    }

    onCancel = () =>
    {
        this.props.onCancel();
    }

    isNumeric = (str) =>
    {
        return !isNaN(parseFloat(str))
    }

    render()
    {
        return(
            <div className={this.props.className}>
                <div className='flex flex-column mb2'>
                    <label
                        className='f-arial-13'
                        htmlFor='id_name'
                    >
                        <FormattedMessage
                            id='doctorlist.name'
                            defaultMessage='Name'
                            description='Name'
                        />
                    </label>
                    <input
                        className=''
                        type='text'
                        id='id_name'
                        onChange={(e) => this.setState({name: e.target.value})}
                    />
                </div>
                <div className='flex flex-column mb2'>
                    <label
                        className='f-arial-13'
                        htmlFor='valueName'
                    >
                        <FormattedMessage
                            id='labresults.value'
                            defaultMessage='Value'
                            description='Value'
                        />
                    </label>
                    <input
                        className=''
                        type='text'
                        id='valueName'
                        onChange=
                        {
                            (e) => 
                            {   
                                if (this.isNumeric(e.target.value))
                                    this.setState({value: e.target.value})
                            }
                        }
                    />
                </div>
                <div className='flex flex-column mb2'>
                    <label
                        className='f-arial-13'
                        htmlFor='id_min'
                    >
                        <FormattedMessage
                            id='labresults.min'
                            defaultMessage='Min'
                            description='Min'
                        />
                    </label>
                    <input
                        className=''
                        type='text'
                        id='id_min'
                        onChange=
                        {
                            (e) => 
                            {
                                if (this.isNumeric(e.target.value))
                                    this.setState({min: e.target.value})
                            }
                        }
                    />
                </div>
                <div className='flex flex-column mb2'>
                    <label
                        className='f-arial-13'
                        htmlFor='id_max'
                    >
                        <FormattedMessage
                            id='labresults.max'
                            defaultMessage='Max'
                            description='Max'
                        />
                    </label>
                    <input
                        className=''
                        type='text'
                        id='id_max'
                        onChange=
                        {
                            (e) => 
                            {
                                if (this.isNumeric(e.target.value))
                                    this.setState({max: e.target.value})
                            }
                        }
                    />
                </div>
                <div className='flex justify-around items-center'>
                    <button
                        className='bg-buttonblue pointer shadow-3 br-pill pr3 pl3 pb2 pt2'
                        onClick={this.onSave}
                    >
                        <FormattedMessage
                            id='labresults.addimage'
                            defaultMessage='Save'
                            description='Save'
                        />
                    </button>
                    <button
                        className='bg-buttonblue pointer shadow-3 br-pill pr3 pl3 pb2 pt2'
                        onClick={this.onCancel}
                    >
                        <FormattedMessage
                            id='visits.cancel'
                            defaultMessage='Cancel'
                            description='Cancel'
                        />
                    </button>
                </div>
            </div>
        )
    }
}

export default AddNumericResult;