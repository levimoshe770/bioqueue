import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';

/*
className
label
onNumericResultAdded
onCancel
 */

class AddTextResult extends Component
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            value: '',
            name: ''
        }
    }

    onSave = () =>
    {
        this.props.onTextResultAdded(
            this.state.name,
            this.state.value
        )
    }

    onCancel = () =>
    {
        this.props.onCancel();
    }

    render()
    {
        return(
            <div className={this.props.className}>
                <div className='flex flex-column mb2'>
                    <label
                        className='f-arial-13'
                        htmlFor='id_name'
                    >
                        <FormattedMessage
                            id='doctorlist.name'
                            defaultMessage='Name'
                            description='Name'
                        />
                    </label>
                    <input
                        className=''
                        type='text'
                        id='id_name'
                        onChange={(e) => this.setState({name: e.target.value})}
                    />
                </div>
                <div className='flex flex-column mb2'>
                    <label
                        className='f-arial-13'
                        htmlFor='valueName'
                    >
                        <FormattedMessage
                            id='labresults.value'
                            defaultMessage='Value'
                            description='Value'
                        />
                    </label>
                    <textarea
                        id='valueName'
                        onChange={(e) => this.setState({value: e.target.value})}
                        cols='55'
                        rows='4'
                    />
                </div>
                <div className='flex justify-around items-center'>
                    <button
                        className='bg-buttonblue pointer shadow-3 br-pill pr3 pl3 pb2 pt2'
                        onClick={this.onSave}
                    >
                        <FormattedMessage
                            id='labresults.addimage'
                            defaultMessage='Save'
                            description='Save'
                        />
                    </button>
                    <button
                        className='bg-buttonblue pointer shadow-3 br-pill pr3 pl3 pb2 pt2'
                        onClick={this.onCancel}
                    >
                        <FormattedMessage
                            id='visits.cancel'
                            defaultMessage='Cancel'
                            description='Cancel'
                        />
                    </button>
                </div>
            </div>
        )
    }

}

export default AddTextResult;