import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import { connect } from 'react-redux';
import LabTestsCollection from '../../../api/Lists/LabTests/LabTestsCollection';
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../../translation/IntlMessages';
import LabResults from './LabResults.jsx';
import { Numeric, Image, Video, Voice, Text } from '../../../api/Lists/LabTests/LabTestsCollection';
import Remote from '../../Remote';

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}

class LabTestDetails extends Component
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            showStatusOptions: false,
            top: 0,
            right: 0
        }
    }

    getStatusNames = () =>
    {
        const statusNames = 
        [
            IntlMessages[this.props.locale].intl.formatMessage(
                {
                    id: 'laborderlist.closed',
                    defaultMessage: 'Closed'
                }
            ),
            IntlMessages[this.props.locale].intl.formatMessage(
                {
                    id: 'laborderlist.opened',
                    defaultMessage: 'Opened'
                }
            ),
            IntlMessages[this.props.locale].intl.formatMessage(
                {
                    id: 'laborderlist.inprogress',
                    defaultMessage: 'In Progress'
                }
            )
        ];

        return statusNames;
    }

    getStatus = (status) =>
    {
        return this.getStatusNames()[status];
    }

    getOrderClass = () =>
    {
        if (this.props.labTests.length === 0)
            return '';

        const { Status } = this.props.labTests[0];

        switch (Status)
        {
            case 0: return 'c-white bg-gray';
            case 1: return 'c-white bg-red';
            case 2: return 'bg-yellow';
            default: return '';
        }
    }

    onChangeStatusClick = (e) =>
    {
        // const right = e.target.parentNode.parentNode.offsetWidth -
        //               e.target.offsetWidth -
        //               e.target.offsetLeft;
        // console.log('click', e.target.offsetLeft, e.target.parentNode.offsetWidth, e.target.offsetWidth, right);

        this.setState({showStatusOptions: !this.state.showStatusOptions});
    }

    updateStatus = (status) =>
    {
        Remote.call(
            'labtest.updateStatus',
            this.props.labTests[0]._id,
            status
        );

        // If status === 0 (closed) send a message to the patient, and the doctor, and set date of results
        if (status === 0)
        {
            Remote.call('labtest.updateDateOfResults',
                this.props.labTests[0]._id,
                new Date()
            );

            const messageToPhysician =
            {
                from: 
                {
                    id: this.props.labTests[0].LabId,
                    name: this.props.lab,
                    role: 'laboratory'
                },
                to: 
                {
                    id: this.props.labTests[0].PhysicianId,
                    name: this.props.physicianName,
                    role: 'physician'
                },
                content: 
                {
                    msgType: 0,
                    message: 'Results for test number ' + this.props.labTests[0].OrderNum.toString() + ' are ready'
                },
                datetime: new Date(),
                unread: 1
            }
            Meteor.call('message.send', messageToPhysician,
                (err, res) =>
                {
                    if (err)
                    {
                        console.error('Send to doctor',err);
                        return;
                    }

                    console.log('Send to doctor',res);
                }
            );

            const messageToPatient = 
            {
                from: 
                {
                    id: this.props.labTests[0].LabId,
                    name: this.props.lab,
                    role: 'laboratory'
                },
                to: 
                {
                    id: this.props.labTests[0].PatientId,
                    name: this.props.patientName,
                    role: 'patient'
                },
                content:  
                {
                    msgType: 0,
                    message: 'Results for test number ' + this.props.labTests[0].OrderNum.toString() + ' are ready'
                },
                datetime: new Date(),
                unread: 1
            }
            Meteor.call('message.send', messageToPatient,
                (err, res) =>
                {
                    if (err)
                    {
                        console.error('Send to patient',err);
                        return;
                    }

                    console.log('Send to patient',res);
                }
            );
        }

        // If status === 2 (In progress) update date of perfomance
        if (status === 2)
        {
            console.log('Calling update date of perf');
            Remote.call('labtest.updateDateOfPerformance', 
                this.props.labTests[0]._id,
                new Date()
            );
        }

        this.setState({showStatusOptions: false});
    }

    onMouseEnter = (event) =>
    {
        const el = document.getElementById(event.target.id);
        el.setAttribute('style','color:white;background:black');
    }

    onMouseLeave = (event) =>
    {
        const el = document.getElementById(event.target.id);
        el.setAttribute('style','color:black;background:#EEEEEE');
    }

    render()
    {
        return(
            <div className={this.props.className + ' bg-light-gray pa3 shadow-6 mt7 f-arial-16-nc br3 w-90'}>
                {
                    this.props.labTests.length > 0 &&
                    <div>
                        <div className='flex items-center justify-between'>
                            <div></div>
                            <div className='f-arial-30 f-shadow-1 tc'>
                                {   
                                    IntlMessages[this.props.locale].intl.formatMessage(
                                        {
                                            id: 'visits.test',
                                            defaultMessage: 'Test'
                                        }
                                    ) +
                                    ' ' + 
                                    (this.props.labTests.length > 0 
                                        ? this.props.labTests[0].OrderNum.toString()
                                        : '')
                                }
                            </div>
                            <div
                                onClick={this.props.onClose}
                                className='close-nf mr1'
                            >
                                &#9746;
                            </div>
                        </div>
                        <div className='f-arial-20-nc bold tc bb b--gray'>
                            {this.props.labTests[0].Name}
                        </div>
                        <div className={'f-arial-20-nc bold tc bb b--gray flex justify-around items-center ' + this.getOrderClass()}>
                            <div>
                                {this.getStatus(this.props.labTests[0].Status)}
                            </div>
                            <div>
                                <button
                                    className='pl3 pr3 pt2 pb2 br-pill bg-buttonblue f-arial-16-nc ma2'
                                    onClick={this.onChangeStatusClick}
                                >
                                    <FormattedMessage
                                        id='labtestdetails.updatestatus'
                                        defaultMessage='Update Status'
                                        description='Update Status'
                                    />
                                </button>
                                {
                                    this.state.showStatusOptions &&
                                    <div 
                                        className='absolute bg-light-gray f-arial-16-nc tl normal shadow-3 pa2 br3'
                                    >
                                        {
                                            this.getStatusNames().map(
                                                (s, idx) =>
                                                {
                                                    return(
                                                        <div 
                                                            onClick={() => this.updateStatus(idx)}
                                                            onMouseEnter={this.onMouseEnter}
                                                            onMouseLeave={this.onMouseLeave}
                                                            id={'st_' + idx.toString()}
                                                            key={idx}
                                                            className='pointer'
                                                        >
                                                            {s}
                                                        </div>
                                                    );
                                                }
                                            )
                                        }
                                    </div>
                                }
                            </div>
                        </div>
                        <div className='flex items-center justify-around pt3 pb3 bb b--gray'>
                            <div className='flex mr4'>
                                <div className='mr2'>
                                    <FormattedMessage
                                        id='registerformselector.patient'
                                        defaultMessage='Patient'
                                    />
                                </div>
                                <div className='bold'>{this.props.patientName}</div>
                            </div>
                            <div className='flex ml4'>
                                <div className='mr2'>
                                    <FormattedMessage
                                        id='registerformselector.physician'
                                        defaultMessage='Physician'
                                    />
                                </div>
                                <div className='bold'>{this.props.physicianName}</div>
                            </div>
                        </div>
                        <div className='flex flex-wrap bb b--gray pa2'>
                            <div className='pa1 ma2'>
                                <div className='f-arial-13'>
                                    <FormattedMessage
                                        id='labtestdetails.dateoforder'
                                        defaultMessage='Date of order'
                                    />
                                </div>
                                <div>
                                    {
                                        this.props.labTests[0].DateOfOrder.toLocaleDateString(
                                            this.props.locale,
                                            {
                                                day: 'numeric',
                                                month: 'short',
                                                year: 'numeric'
                                            }
                                        )
                                    }
                                </div>
                            </div>
                            <div className='pa1 ma2'>
                                <div className='f-arial-13'>
                                    <FormattedMessage
                                        id='labtestdetails.dateofperformance'
                                        defaultMessage='Date of performance'
                                    />
                                </div>
                                <div>
                                    {
                                        this.props.labTests[0].DateOfPerformance !== undefined
                                        ?
                                        this.props.labTests[0].DateOfPerformance.toLocaleDateString(
                                            this.props.locale,
                                            {
                                                day: 'numeric',
                                                month: 'short',
                                                year: 'numeric'
                                            }
                                        )
                                        : ''
                                    }
                                </div>
                            </div>
                            <div className='pa1 ma2'>
                                <div className='f-arial-13'>
                                    <FormattedMessage
                                        id='labtestdetails.dateofresult'
                                        defaultMessage='Date of results'
                                    />
                                </div>
                                <div>
                                    {
                                        this.props.labTests[0].DateOfResults !== undefined
                                        ?
                                        this.props.labTests[0].DateOfResults.toLocaleDateString(
                                            this.props.locale,
                                            {
                                                day: 'numeric',
                                                month: 'short',
                                                year: 'numeric'
                                            }
                                        )
                                        : ''
                                    }
                                </div>
                            </div>
                            <div className='pa1 ma2'>
                                <div className='f-arial-13'>
                                    <FormattedMessage
                                        id='registerformselector.laboratory'
                                        defaultMessage='Laboratory'
                                    />
                                </div>
                                <div>
                                    {
                                        this.props.lab
                                    }
                                </div>
                            </div>
                            <div className='pa1 ma2 flex flex-column items-start'>
                                <div className='f-arial-13'>
                                    <FormattedMessage
                                        id='labtests.aproximateduration'
                                        defaultMessage='Approximate Duration'
                                    />
                                </div>
                                <div className='self-center'>
                                    {
                                        this.props.labTests[0].ApproximateDuration !== undefined
                                        ?
                                        this.props.labTests[0].ApproximateDuration.toString()
                                        :
                                        ''
                                    }
                                </div>
                            </div>
                            <div className='pa1 ma2'>
                                <div className='f-arial-13'>
                                    <FormattedMessage
                                        id='labtests.specialconsiderations'
                                        defaultMessage='Special Considerations'
                                    />
                                </div>
                                <div>
                                    {
                                        this.props.labTests[0].SpecialConsiderations
                                    }
                                </div>
                            </div>
                            <div className='pa1 ma2'>
                                <div className='f-arial-13'>
                                    <FormattedMessage
                                        id='visits.notes'
                                        defaultMessage='Notes'
                                    />
                                </div>
                                <div className='scroll-y mh-100'>
                                    {
                                        
                                        this.props.labTests[0].Notes.map(
                                            (note, idx) =>
                                            {
                                                return(
                                                    <div key={idx}>{note}</div>
                                                )
                                            }
                                        )
                                    }
                                </div>
                            </div>
                        </div>
                        <LabResults 
                            className=''
                            labTest={this.props.labTests[0]}
                        />
                    </div>
                }
            </div>
        )
    }
}

export default connect(mapStateToProps)
(
    withTracker(
        ({testId}) =>
        {
            const testHandle = Remote.subscribe('labtest.labDetails', testId);
            const testLoading = !testHandle.ready();

            return (
                {labTests: !testLoading ? LabTestsCollection.find({_id: testId}).fetch() : []}
            )
        }
    )
    (LabTestDetails)
);
