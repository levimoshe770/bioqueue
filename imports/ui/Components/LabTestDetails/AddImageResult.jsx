import React, { Component } from 'react';
import UserFiles from '../../../api/Lists/UserFiles/UserFiles';
import { FormattedMessage } from 'react-intl';
import { Meteor } from 'meteor/meteor';
import FileUpload from '../Utils/FileUpload.jsx';
import { withTracker } from 'meteor/react-meteor-data';
import { connect } from 'react-redux';
import { IntlMessages } from '../../../../translation/IntlMessages';

/*
className
onImageResultAdded(resultName, imageLink)
onCancel
 */

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}

class AddImageResult extends Component
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            resultName: '',
            fileId: ''
        }
    }

    fileSaved = () =>
    {
        const file = UserFiles.findOne({_id: this.state.fileId});

        return file !== undefined;
    }

    onFileUploaded = (file) =>
    {
        this.setState({fileId: file._id});
    }

    onFileUploadFailed = (err) =>
    {
        console.log(err);
    }

    onSave = () =>
    {
        const { resultName } = this.state;
        const link = UserFiles.findOne({_id: this.state.fileId}).link();
        this.props.onImageResultAdded(resultName, link);
    }

    onCancel = () =>
    {
        this.props.onCancel();
    }

    render()
    {
        // console.log('Props', this.props);
        // console.log('State', this.state);
        return(
            <div className={this.props.className + ' '}>
                <div className='flex flex-column mb3'>
                    <label
                        className='f-arial-13'
                        htmlFor='imageName'
                    >
                        <FormattedMessage
                            id='doctorlist.name'
                            defaultMessage='Name'
                            description='Name'
                        />
                    </label>
                    <input
                        className=''
                        type='text'
                        id='imageName'
                        onChange={(e) => this.setState({resultName: e.target.value})}
                    />
                </div>
                <FileUpload
                    className='pa2 bg-buttonblue pointer shadow-3 br3 mb2 mt2'
                    label=
                    {
                        IntlMessages[this.props.locale].intl.formatMessage(
                            {
                                id: 'lab.presstoupload',
                                defaultMessage: 'Press to upload file'
                            }
                        ) + ' ...'
                    }
                    onFileUploaded={this.onFileUploaded}
                    onUploadFail={this.onFileUploadFailed}
                />
                <div className='flex justify-around items-center'>
                    <button
                        className='bg-buttonblue pointer shadow-3 br-pill pr3 pl3 pb2 pt2'
                        disabled={!this.fileSaved()}
                        onClick={this.onSave}
                    >
                        <FormattedMessage
                            id='labresults.addimage'
                            defaultMessage='Save'
                            description='Save'
                        />
                    </button>
                    <button
                        className='bg-buttonblue pointer shadow-3 br-pill pr3 pl3 pb2 pt2'
                        onClick={this.onCancel}
                    >
                        <FormattedMessage
                            id='visits.cancel'
                            defaultMessage='Cancel'
                            description='Cancel'
                        />
                    </button>
                </div>
            </div>
        )
    }
}

export default
connect(mapStateToProps)
(
    withTracker(
        () =>
        {
            const filesHandle = Meteor.subscribe('files.uploads');
            const loadingFiles = !filesHandle.ready();

            return(
                {
                    files: !loadingFiles ? UserFiles.find({}).fetch() : []
                }
            )
        }
    )
    (AddImageResult)
);