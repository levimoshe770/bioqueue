import React, { Component } from 'react';


class ProgressBar extends Component
{
    constructor(props)
    {
        super(props);
        this.state =
        {
            ref: React.createRef(),
            dims: {}
        }
        this.myRef = React.createRef();
    }

    getCharacter = (amount) =>
    {
        const ch = '▉';

        const res = [];
        let i;

        for (i=0; i<amount; i++)
        {
            res.push(ch);
        }

        return res;
    }

    numOfBars = 20;

    calcWHRT = () =>
    {
        let el = this.myRef.current;

        if (el === null)
            return;

        el = el.parentElement;

        let offsetTop = 0;
        let offsetLeft = 0;

        let firstAbsolute = null;
        while (el !== null && el.id !== 'root')
        {
            const cl = el.getAttribute('class');

            if (cl !== null && cl.includes('absolute'))
            {
                console.log('class', cl)
                offsetTop += el.offsetTop;
                offsetLeft += el.offsetLeft;
                if (firstAbsolute === null)
                    firstAbsolute = el;
            }

            el = el.parentElement;
        }

        console.log('offsetTop',offsetTop, 'offsetLeft', offsetLeft);

        const barWidth = 386;
        const barHeight = 50;

        const windowH = window.innerHeight;
        const windowW = window.innerWidth;

        const elementWidth = firstAbsolute !== null ? firstAbsolute.offsetWidth : windowW;

        console.log(windowH, windowW, elementWidth);

        const y = (windowH - barHeight)/2 - offsetTop;

        const offsetRight = windowW - (offsetLeft + elementWidth);

        const x = (windowW - barWidth)/2 - offsetRight;

        return(
            {
                top: y,
                right: x,
                height: barHeight,
                width: barWidth
            }
        );
    }

    componentDidMount()
    {
        console.log('+++++ ref ++++++++', this.myRef);

        this.setState({dims: this.calcWHRT()});
    }

    render()
    {
        const amount = Math.floor(this.numOfBars*this.props.val);

        const { width, height, right, top } = this.state.dims;

        console.log(this.state.dims);

        return(
            <div 
                className='absolute pa3 br3 bg-light-gray shadow-3'
                style={{width: width, height: height, right: right, top: top}}
                ref={this.myRef}
            >
                <div className='flex'>
                    {
                        this.getCharacter(amount).map(
                            (c, idx) => {return(<div key={idx} className='c-blue'>{c}</div>);}
                        )
                    }
                    {
                        this.getCharacter(this.numOfBars- amount).map(
                            (c, idx) => {return(<div key={idx} className='c-white'>{c}</div>);}
                        )
                    }
                    <div className='pl2 pr2 c-blue bold'>
                        {(this.props.val * 100).toString() + '%'}
                    </div>
                </div>
            </div>
        );
    }
}

export default ProgressBar;