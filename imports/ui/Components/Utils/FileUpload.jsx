import React, { Component } from 'react';
import UserFiles from '../../../api/Lists/UserFiles/UserFiles';
import './FileUpload.css'
import FileButton from './FileButton.jsx';
import ProgressBar from './ProgressBar.jsx';
import { withTracker } from 'meteor/react-meteor-data';

/*
className
label
imgSrc
imgWidth
onFileUploaded
onUploadFail
 */

class FileUpload extends Component
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            uploading: [],
            inProgress: false,
            progress: 0,
            eventSent: true,
            lastUploaded: undefined
        }
    }

    onFileUpload = (files) =>
    {
        console.log(files);

        if (files.length === 0)
            return;

        const file = files[0];

        const uploadInstance = UserFiles.insert(
            {
                file: file,
                streams: 'dynamic',
                chunkSize: 'dynamic',
                allowWebWorkers: true
            },
            false
        );

        this.setState({inProgress: true, uploading: uploadInstance});

        uploadInstance.on('start', () => console.log('Start upload: ', file));

        uploadInstance.on('end',
            (err, fileObj) =>
            {
                if (err) 
                {
                    console.log('end',err);
                    return;   
                }

                console.log('End upload', fileObj);

                this.setState({
                    eventSent: false,
                    lastUploaded: fileObj
                });
                //this.props.onFileUploaded(fileObj);
            }
        );

        uploadInstance.on('uploaded',
            (err, fileObj) =>
            {
                if (err)
                {
                    console.log('uploaded', err);
                    return;
                }

                console.log('Uploaded', fileObj);

                this.setState(
                    {
                        inProgress: false,
                        uploading: [],
                        progress: 0
                    }
                );

                
            }
        );

        uploadInstance.on('error',
            (err, res) =>
            {
                if (err)
                {
                    console.log('error during upload', err);

                    this.props.onUploadFail(err);
                }
            }
        );

        uploadInstance.on('progress',
            (progress, fileObj) =>
            {
                console.log('progress:', progress);

                this.setState({progress: progress/100});
            }
        );

        uploadInstance.start();
    }

    componentDidUpdate()
    {
        if (!this.state.eventSent &&
            this.props.files.find(
                (f) => f._id === this.state.lastUploaded._id) !== undefined)
            {
                this.setState(
                    {
                        eventSent: true,
                        lastUploaded: undefined
                    }
                );
                
                this.props.onFileUploaded(this.state.lastUploaded);
            }
    }

    render()
    {
        //console.log(this.props);
        return(
            <div>
                <FileButton
                    className={this.props.className}
                    imgWidth={this.props.imgWidth}
                    imgSrc={this.props.imgSrc}
                    label={this.props.label}
                    onFileUpload={this.onFileUpload}
                />
                {
                    this.state.inProgress &&
                    <ProgressBar
                        val={this.state.progress}
                    />
                }
            </div>
        )
    }
}

export default withTracker(
    () =>
    {
        const filesHandle = Meteor.subscribe('files.uploads');
        const loadingFiles = !filesHandle.ready();

        return(
            {
            files: !loadingFiles ? UserFiles.find({}).fetch() : []
            }
        )
    }
)
(FileUpload);