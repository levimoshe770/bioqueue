import React from 'react';
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';
import { Approved, Pending, statusId } from '../../../api/Lists/GeneralTypes/AuthorizationStatus';
import { FormattedMessage } from 'react-intl';

const mapStateToProps = state =>
{
    return {locale: state.locale};
}

const getColors = (status) =>
{
    switch(status)
    {
        case Approved:
            return('c-white bg-green');
        case Pending:
            return('c-white bg-red');
        default:
            return('c-white bg-gray');
    }
}

const StatusSign = (props) =>
{
    const { status, locale } = props;

    // console.log(props);
    // console.log(status);
    //console.log(props);
    return(
        <div className={'ml3 f-arial-16-nc flex items-center ' + props.className}>
            <div className='mr1 c-white'>
            {
                IntlMessages[locale].intl.formatMessage(
                    {
                        id: 'physicianstate.status',
                        defaultMessage: 'Status'
                    }
                ) + ':'
            }
            </div>
            <div 
                className={'bold pa2 br3 shadow-3 ' + getColors(status)}
            >
                {
                    status === -1
                    ?
                    IntlMessages[locale].intl.formatMessage(
                        {
                            id: 'physiciangreeting.loading',
                            defaultMessage: 'Loading'
                        }
                    ) + '...'
                    :
                    IntlMessages[locale].intl.formatMessage(
                        {
                            id: statusId[status],
                            defaultMessage: 'Status'
                        }
                    )
                }
            </div>
        </div>

    )
}

export default connect(mapStateToProps)(StatusSign);