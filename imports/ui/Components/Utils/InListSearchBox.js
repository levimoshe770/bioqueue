import React from 'react';
import { FormattedMessage } from 'react-intl';

/*
Props:
  Field - Nqme of field
  values - Optional - values to be filtered
  uncheckedValues
  locale
Event:
  onSearchBoxChanged
  onCheckChanged
 */

export default class InListSearchBox extends React.Component
{

  constructor(props)
  {
    super(props);
    this.state = 
    {
      showValues: false,
      listOfValues: this.calcListOfValues(),
      boxref: React.createRef()
    }
  }

  calcListOfValues = () =>
  {
    const res = new Set(this.props.values);
    //console.log(res);
    return Array.from(res);
  }

  onClearClick = () =>
  {
    const el = document.getElementById(this.props.Field);
    el.value = '';
    this.props.onSearchBoxChanged('', this.props.Field);
  }

  onInputClick = () =>
  {
    if (this.props.values === undefined)
      return;

    this.setState({showValues: true});
  }

  onCheckChanged = (checked, value, field) =>
  {
    this.props.onCheckChanged(checked, value, field);
  }

  getSelectAllState = () =>
  {
    let res = true;
    this.state.listOfValues.forEach((value) => res = res && !this.props.uncheckedValues.includes(value));
    return res;
  }
  
  render()
  {
    //console.log(this.props);
    return(
      <div>
        <div className="ba flex items-center justify-between bg-white"
          ref = {this.state.boxref}
        >
          <input 
            className="bw0 w-100"
            id={this.props.Field}
            type="text"
            onClick={this.onInputClick}
            onChange=
            {
              (event) => this.props.onSearchBoxChanged(event.target.value, this.props.Field)
            }
          />
          <div className="bw0 pointer" onClick={this.onClearClick}>⮾</div>
        </div>
        {
          this.state.showValues &&
          <div 
            className='absolute mh-160 scroll-y bg-white shadow-3 flex flex-column items-start'
            style={{width: this.state.boxref.current.offsetWidth}}
          >
            <div
              className='self-end mb2 mr2 pointer'
              onClick={() => this.setState({showValues: false})}
            >
              ⮽
            </div>
            <div className='pl1'>
              <input
                type='checkbox'
                id='selectall'
                onChange=
                {
                  (e) =>
                  {
                    this.calcListOfValues().forEach(
                      (v) =>
                      {
                        const el = document.getElementById('v_' + v);
                        el.checked = e.target.checked;
                        this.props.onCheckChanged(e.target.checked, v, this.props.Field);
                      }
                    )
                  }
                }
                defaultChecked={this.getSelectAllState()}
              />
              <label className='ml1'>
                <FormattedMessage
                  id='searchbox.selectall'
                  defaultMessage='Select All'
                  description='Select All'
                />
              </label>
            </div>
            <div className='pl2 pb2'>
            {
              this.calcListOfValues().map(
                (v, idx) =>
                {
                  return(
                    <div key={idx}>
                      <input
                        type='checkbox'
                        id={'v_' + v}
                        onChange={(e) => this.onCheckChanged(e.target.checked, v, this.props.Field)}
                        defaultChecked={!this.props.uncheckedValues.includes(v)}
                      />
                      <label className='ml1'>
                        {v}
                      </label>
                    </div>
                  )
                }
              )
            }
            </div>
          </div>
        }
      </div>
    );
  }
}