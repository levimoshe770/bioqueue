import React from "react";
import { FormattedMessage } from "react-intl";

export default class ComboBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      comboValues: props.items,
      showCombo: false
    };
  }

  onComboClick = () => {
    this.setState({ showCombo: !this.state.showCombo });
  };

  onMouseEnterItem = event => {
    const el = document.getElementById(event.target.id);
    el.setAttribute("class", "item-selected");
  };

  onMouseLeaveItem = event => {
    const el = document.getElementById(event.target.id);
    el.setAttribute("class", "item-unselected");
  };

  onItemSelected = (event) => {
    const cb = document.getElementById("combo" + "_" + this.props.id);
    cb.value = event.target.innerText;
    this.setState({ showCombo: false });
    this.props.onSelectionChanged(this.props.id, cb.value);
  };

  render() {
    return (
      <div className="mb4">
        <label className="db f-arial-14 c-gray mb1" htmlFor="combo">
          <FormattedMessage
            id={this.props.labelid}
            defaultMessage={this.props.defaultMessage}
            description={this.props.description} />
        </label>
        <div className="shadow-5 br3 bg-white">
          <input
            className="pv2 ba b--white br3 br--left w-94"
            id={"combo"+"_"+this.props.id}
            type="text"
          />
          <button
            className="pv2 ba b--white bg-white br3 br--right"
            onClick={this.onComboClick}
          >
            ▼
          </button>
        </div>
        {this.state.showCombo && (
          <div className="db bg-white w-2 ba">
            {this.props.items.map(value => {
              return (
                <div
                  id={value}
                  key={value}
                  className="item-unselected"
                  onMouseEnter={this.onMouseEnterItem}
                  onMouseLeave={this.onMouseLeaveItem}
                  onClick={this.onItemSelected}
                >
                  {value}
                </div>
              );
            })}
          </div>
        )}
      </div>
    );
  }
}
