import React from 'react';

const ErrorInForm = (props) =>
{
    return(
        <div className={props.className}>
            {props.Message}
        </div>
    );
}

export default ErrorInForm;