import React, { Component } from 'react';

/*
className: defines class for clickable object
label: text to be displayed on object (or some other component)
onFileLoad: callback to return the file
 */

class FileButton extends Component
{
    constructor(props)
    {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick = () =>
    {
        this.refs.fileUploader.click();
    }

    render()
    {
        //console.log(this.props);
        return(
            <div>
                <input
                    style={{display: 'none'}}
                    ref='fileUploader'
                    id='file'
                    type='file'
                    onChange={(e) => this.props.onFileUpload(e.target.files)}
                />
                {
                    this.props.imgSrc 
                    ?
                    <img
                        className={this.props.className}
                        src={this.props.imgSrc}
                        width={this.props.imgWidth}
                        alt={this.props.label}
                        onClick={this.handleClick}
                    />
                    :
                    <div
                        className={this.props.className}
                        onClick={this.handleClick}
                    >
                        {this.props.label}
                    </div>
                }
            </div>
        );
    }
}

export default FileButton;