import React from 'react'
import { FormattedMessage } from 'react-intl'
import './SearchDoctorBox.css'
import SearchField from "./SearchField";
import ComboBox from "../Utils/ComboBox";

class SearchDoctorBox extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    onCloseClick = () =>
    {
        this.props.onSearchClosed();
    }

    onComboSelectionChanged = (id, value) =>
    {
    }

    render()
    {
        return(
            <div className="searchBox o-80 shadow-3 br4">
            <span
              onClick={() => this.onCloseClick()}
              className="close c-gray mr1 mt1"
            >
              &#9746;
            </span>
            <div className="searchBoxHeader" id="headerText">
              <FormattedMessage
                id="searchdoctorbox.searchforphysician"
                defaultMessage="Search for a Physician"
                description="box title" />
            </div>
            <div>
              <fieldset className="ba bw0">
                <SearchField />
                <ComboBox
                  id="specialty"
                  labelid="searchdoctorbox.Specialty"
                  defaultMessage="Specialty"
                  description="Specialty"
                  items={["pathologist", "nefrologist", "oncologist"]}
                  onSelectionChanged={this.onComboSelectionChanged}
                />
                <ComboBox 
                  id="area" 
                  labelid="searchdoctorbox.Area" 
                  defaultMessage="Area"
                  description="Area"
                  items={['1','2']} 
                  onSelectionChanged={this.onComboSelectionChanged} 
                />
                <ComboBox 
                  id="clinic" 
                  labelid="searchdoctorbox.Clinic"
                  defaultMessage="Clinic"
                  description="Clinic"
                  items={['3','4']} 
                  onSelectionChanged={this.onComboSelectionChanged}
                />
                <div>
                  <label className="db f-arial-14 c-gray mb1">
                    <FormattedMessage
                      id="searchdoctorbox.daysofavailability"
                      defaultMessage="Days of availability"
                      description="Days of availability" />
                  </label>
                  <div className="flex items-center ba ph2 pvp-10 bg-white br3 ba b--white shadow-3 mb5">
                    <input id="Mon" type="checkbox" name="Mon" value="Monday" />
                    <label className="f-arial-13 pr2 pl1" htmlFor="Mon">
                      <FormattedMessage
                        id="days.Monday"
                        defaultMessage="Monday"
                        description="Monday" />
                    </label>
                    <input id="Tue" type="checkbox" name="Tue" value="Tuesday" />
                    <label className="f-arial-13 pr2 pl1" htmlFor="Tue">
                      <FormattedMessage
                        id="days.Tuesday"
                        defaultMessage="Tuesday"
                        description="Tuesday" />
                    </label>
                    <input id="Wed" type="checkbox" name="Wed" value="Wednesday" />
                    <label className="f-arial-13 pr2 pl1" htmlFor="Wed">
                      <FormattedMessage
                        id="days.Wednesday"
                        defaultMessage="Wednesday"
                        description="Wednesday" />
                    </label>
                    <input id="Thu" type="checkbox" name="Thu" value="Thursday" />
                    <label className="f-arial-13 pr2 pl1" htmlFor="Thu">
                      <FormattedMessage
                        id="days.Thursday"
                        defaultMessage="Thursday"
                        description="Thu" />
                    </label>
                    <input id="Fri" type="checkbox" name="Fri" value="Friday" />
                    <label className="f-arial-13 pr2 pl1" htmlFor="Fri">
                      <FormattedMessage
                        id="days.Friday"
                        defaultMessage="Friday"
                        description="Fri" />
                    </label>
                    <input id="Sat" type="checkbox" name="Sat" value="Saturday" />
                    <label className="f-arial-13 pr2 pl1" htmlFor="Sat">
                      <FormattedMessage
                        id="days.Saturday"
                        defaultMessage="Saturday"
                        description="Sat" />
                    </label>
                  </div>
                </div>
              </fieldset>
              <div className="flex justify-center">
                <button
                  className="pa2 f-oswald-30 br4 bg-buttonblue shadow-3 ph5 mb5"
                  id="searchbutton"
                  type="submit"
                  onClick={this.props.onSearchClicked}
                >
                  <FormattedMessage
                    id="searchdoctorbox.SearchBtn"
                    defaultMessage="Search"
                    description="Search Btn" />
                </button>
              </div>
            </div>
          </div>
        )
    }
}

export default SearchDoctorBox;