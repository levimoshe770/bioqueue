import React from "react";

export default class SearchField extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: "",
      showSearchBox: false
    };
  }

  onSearchTextChanged = event => {
    this.setState({ searchText: event.target.value });
    this.setState({ showSearchBox: true });
  };

  onSearchSubmit = () => {
    console.log("Search");
  };

  onVoiceSearchClick = () => {
    console.log("Voice Search");
  };

  onClearSearchClick = () => {
    console.log("Clear");
  };

  render() {
    return (
      <div className="searchfield mb4">
        <label className="db f-arial-14 c-gray mb1" htmlFor="searchField">
          Name
        </label>
        <div className="pa0 ma0 shadow-5 br3 bg-white">
          <button
            className="pv2 ba b--white br3 br--left bg-white pointer"
            id="magnifier"
            onClick={this.onSearchSubmit}
          >
            <span>&#x1F50D;</span>
          </button>
          <input
            className="pv2 ba b--white w-84"
            id="searchField"
            type="search"
            onChange={this.onSearchTextChanged}
          />
          <div
            className="pv2 dib pa0 bg-white bt bb b--white pointer"
            onClick={this.onClearSearchClick}
          >
            &#11198;
          </div>
          <button
            className="pv2 br3 br--right bg-white ba b--white pointer"
            id="voicesearch"
            onClick={this.onVoiceSearchClick}
          >
            <i className="fa fa-microphone" />
          </button>
        </div>
      </div>
    );
  }
}
