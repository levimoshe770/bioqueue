import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';
import '../AdminApprovals/AdminApprovals.css'
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import Physicians from '../../../api/Lists/Physicians/physicians';
import Patients from '../../../api/Lists/Patients/patients';
import InListSearchBox from '../../Components/Utils/InListSearchBox';
import './LabOrderList.css';
import Remote from '../../Remote';

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}

/*
className
orderList
 */

class LabOrderList extends Component
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            filters: 
            {
                'DOO': [],
                'DOP': [],
                'DOR': [],
                'PatientName': '',
                'Physician': '',
                'TestType': [],
                'Status': [],
            }
        }
    }

    getPatientName = (patientId) =>
    {
        const patient = this.props.patients.find((p) => p._id === patientId);

        if (patient === undefined)
            return 'loading';

        return patient.Name;
    }

    getPhysicianName = (physicianId) =>
    {
        const physician = this.props.physicians.find((p) => p._id === physicianId);

        if (physician === undefined)
            return 'loading';

        return physician.Name;
    }

    getOrderStatus = (status) =>
    {
        const statusNames = 
        [
            IntlMessages[this.props.locale].intl.formatMessage(
                {
                    id: 'laborderlist.closed',
                    defaultMessage: 'Closed'
                }
            ),
            IntlMessages[this.props.locale].intl.formatMessage(
                {
                    id: 'laborderlist.opened',
                    defaultMessage: 'Opened'
                }
            ),
            IntlMessages[this.props.locale].intl.formatMessage(
                {
                    id: 'laborderlist.inprogress',
                    defaultMessage: 'In Progress'
                }
            )
        ];

        return statusNames[status];
    }

    getRowClass = (status) =>
    {
        const baseClass = ''

        switch (status) 
        {
            case 0:
                return baseClass + 'c-white bg-gray';
            case 1:
                return baseClass + 'c-white bg-red';
            case 2:
                return baseClass + 'bg-yellow';
            default:
                return '';
        }
    }

    getListOfItems = () =>
    {
        const { orderList, locale } = this.props;
        const { filters } = this.state;
        const res = orderList.filter(
            (order) =>
            {
                return (
                    (
                        !filters.DOO.includes(order.DateOfOrder.toLocaleDateString
                            (locale, {day: 'numeric', month: 'short', year: 'numeric'})
                        )
                    ) &&
                    (
                        order.DateOfResults !== undefined
                        ?
                        (
                            !filters.DOR.includes(order.DateOfResults.toLocaleDateString
                                (locale, {day: 'numeric', month: 'short', year: 'numeric'})
                            )
                        )
                        : !filters.DOR.includes('')
                    ) &&
                    (
                        order.DateOfPerformance !== undefined
                        ?
                        (
                            !filters.DOP.includes(order.DateOfPerformance.toLocaleDateString
                                (locale, {day: 'numeric', month: 'short', year: 'numeric'})
                            )
                        )
                        : !filters.DOP.includes('')
                    ) &&
                    (
                        this.getPatientName(order.PatientId).includes(this.state.filters.PatientName)
                    ) &&
                    (
                        this.getPhysicianName(order.PhysicianId).includes(this.state.filters.Physician)
                    ) &&
                    (
                        !filters.TestType.includes(order.Name)
                    ) &&
                    (
                        !filters.Status.includes(this.getOrderStatus(order.Status))
                    )
                )
            }
        )

        return res;
    }

    onSearchBoxChanged = (value, field) =>
    {
        const filters = this.state.filters;
        filters[field] = value;
        this.setState({filters: filters});
    }

    onSearchCheckChanged = (checked, value, field) =>
    {
        const filters = this.state.filters;

        if (!checked)
        {
            filters[field].push(value);
            const arr = new Set(filters[field]);
            filters[field] = Array.from(arr);
        }
        else
        {
            const idx = filters[field].indexOf(value);
            if (idx >= 0)
            {
                filters[field].splice(idx, 1);
            }
        }

        this.setState({filters: filters});
    }

    render()
    {
        const { locale } = this.props;
        return(
            <div className={'adminapprove pa3 br3 shadow-6 w-90 '+ this.props.className}>
                <div className='pb3 f-arial-30 f-shadow-1 tc'>
                    <FormattedMessage
                        id='laborderlist.labtestsummary'
                        defaultMessage='Lab Test Summary'
                        description='Lab Test Summary'
                    />
                </div>
                <div className='bg-white pa1 mh-600 scroll-y'>
                    <table className='f-arial-16-nc'>
                        <thead>
                            <tr>
                                <th className='pa2 orderlistcell tc'>Number</th>
                                <th className='pa2 orderlistcell tc'>Date of order</th>
                                <th className='pa2 orderlistcell tc'>Date of performance</th>
                                <th className='pa2 orderlistcell tc'>Date of results</th>
                                <th className='pa2 orderlistcell tc'>Patient Name</th>
                                <th className='pa2 orderlistcell tc'>Physician</th>
                                <th className='pa2 orderlistcell tc'>Test Type</th>
                                <th className='pa2 orderlistcell tc'>Status</th>
                                <th className='pa2 orderlistcell tc'>Results</th>
                            </tr>
                        </thead>
                        <tbody className=''>
                            <tr>
                                <td className='orderlistcell'></td>
                                <td className='orderlistcell'>
                                    <InListSearchBox
                                        Field='DOO'
                                        values=
                                        {
                                            this.props.orderList.map(
                                                (o) => o.DateOfOrder.toLocaleDateString(
                                                    this.props.locale,
                                                    {
                                                        day: 'numeric',
                                                        month: 'short',
                                                        year: 'numeric'
                                                    }
                                                )
                                            )
                                        }
                                        uncheckedValues={this.state.filters['DOO']}
                                        onCheckChanged={this.onSearchCheckChanged}
                                        locale={locale}
                                    />
                                </td>
                                <td className='orderlistcell'>
                                    <InListSearchBox
                                        Field='DOP'
                                        values=
                                        {
                                            this.props.orderList.map(
                                                (o) => 
                                                {
                                                    return(
                                                        o.DateOfPerformance !== undefined
                                                        ? o.DateOfPerformance.toLocaleDateString(
                                                            this.props.locale,
                                                            {
                                                                day: 'numeric',
                                                                month: 'short',
                                                                year: 'numeric'
                                                            }
                                                        )
                                                        : ''
                                                    )
                                                }
                                            )
                                        }
                                        uncheckedValues={this.state.filters['DOP']}
                                        onCheckChanged={this.onSearchCheckChanged}
                                        locale={locale}
                                    />
                                </td>
                                <td className='orderlistcell'>
                                    <InListSearchBox
                                        Field='DOR'
                                        values=
                                        {
                                            this.props.orderList.map(
                                                (o) => 
                                                {
                                                    return(
                                                        o.DateOfResults !== undefined
                                                        ? o.DateOfResults.toLocaleDateString(
                                                            this.props.locale,
                                                            {
                                                                day: 'numeric',
                                                                month: 'short',
                                                                year: 'numeric'
                                                            }
                                                        )
                                                        : ''
                                                    )
                                                }
                                            )
                                        }
                                        uncheckedValues={this.state.filters['DOR']}
                                        onCheckChanged={this.onSearchCheckChanged}
                                        locale={locale}
                                    />
                                </td>
                                <td className='orderlistcell'>
                                    <InListSearchBox
                                        Field='PatientName'
                                        locale={this.props.locale}
                                        onSearchBoxChanged={this.onSearchBoxChanged}
                                    />
                                </td>
                                <td className='orderlistcell'>
                                    <InListSearchBox
                                        Field='Physician'
                                        locale={this.props.locale}
                                        onSearchBoxChanged={this.onSearchBoxChanged}
                                    />
                                </td>
                                <td className='orderlistcell'>
                                    <InListSearchBox
                                        Field='TestType'
                                        locale={this.props.locale}
                                        values=
                                        {
                                            this.props.orderList.map(
                                                (o) => o.Name
                                            )
                                        }
                                        uncheckedValues={this.state.filters['TestType']}
                                        onCheckChanged={this.onSearchCheckChanged}
                                    />
                                </td>
                                <td className='orderlistcell'>
                                    <InListSearchBox
                                        Field='Status'
                                        locale={this.props.locale}
                                        values=
                                        {
                                            this.props.orderList.map(
                                                (o) => this.getOrderStatus(o.Status)
                                            )
                                        }
                                        uncheckedValues={this.state.filters['Status']}
                                        onCheckChanged={this.onSearchCheckChanged}
                                    />
                                </td>
                                <td className='orderlistcell'/>
                            </tr>
                            {
                                this.getListOfItems().map(
                                    (order, idx) =>
                                    {
                                        return(
                                            <tr key={idx} className={this.getRowClass(order.Status)}>
                                                <td className='orderlistcell pa2 tc'>
                                                    {order.OrderNum}
                                                </td>
                                                <td className='orderlistcell pa2 tc'>
                                                    {
                                                        order.DateOfOrder.toLocaleDateString(locale, 
                                                            {
                                                                day: 'numeric',
                                                                month: 'short',
                                                                year: 'numeric'
                                                            }
                                                        )
                                                    }
                                                </td>
                                                <td className='orderlistcell pa2 tc'>
                                                    {
                                                        order.DateOfPerformance !== undefined
                                                        ?
                                                        order.DateOfPerformance.toLocaleDateString(locale, 
                                                            {
                                                                day: 'numeric',
                                                                month: 'short',
                                                                year: 'numeric'
                                                            }
                                                        )
                                                        :
                                                        ''
                                                    }
                                                </td>
                                                <td className='orderlistcell pa2 tc'>
                                                    {
                                                        order.DateOfResults !== undefined
                                                        ?
                                                        order.DateOfResults.toLocaleDateString(locale, 
                                                            {
                                                                day: 'numeric',
                                                                month: 'short',
                                                                year: 'numeric'
                                                            }
                                                        )
                                                        :
                                                        ''
                                                    }
                                                </td>
                                                <td className='orderlistcell pa2'>
                                                    {this.getPatientName(order.PatientId)}
                                                </td>
                                                <td className='orderlistcell pa2'>
                                                    {this.getPhysicianName(order.PhysicianId)}
                                                </td>
                                                <td className='orderlistcell pa2'>
                                                    {order.Name}
                                                </td>
                                                <td className='orderlistcell pa2 tc'>
                                                    {this.getOrderStatus(order.Status)}
                                                </td>
                                                <td className='orderlistcell pa2'>
                                                    <div className='flex items-center'>
                                                        <button 
                                                            className='mr1 br-pill bg-buttonblue pb2 pt2 pr3 pl3'
                                                            onClick={() => this.props.onDetailsRequest(order._id, this.getPatientName(order.PatientId), this.getPhysicianName(order.PhysicianId))}
                                                        >
                                                            Details
                                                        </button>
                                                        <button 
                                                            className='ml1 br-pill bg-buttonblue pb2 pt2 pr3 pl3'
                                                            disabled={order.Status === 0}
                                                        >
                                                            Set
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        )
                                    }
                                )
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default 
connect(mapStateToProps)
(
    withTracker
    (
        ({orderList}) =>
        {
            const physicianHandle = Remote.subscribe('physicians.ids',
                orderList.map(
                    (o) => o.PhysicianId
                )
            );
            const loadingPhysicians = !physicianHandle.ready();
            const patientHandle = Remote.subscribe('patients.ids',
                orderList.map(
                    (o) => o.PatientId
                )
            );
            const loadingPatients = !patientHandle.ready();

            return(
                {
                    physicians: !loadingPhysicians ? Physicians.find({}).fetch() : [],
                    patients: !loadingPatients ? Patients.find({}).fetch() : []
                }
            )
        }
    )
    (LabOrderList)
);