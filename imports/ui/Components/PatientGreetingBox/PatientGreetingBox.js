import React from 'react';
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';
import './PatientGreetingBox.css'

const mapStateToProps = (state) =>
{
    return {userName: state.userName, locale: state.locale}
}

onMouseEnter = (event) =>
{
    const el = document.getElementById(event.target.id)
    const cl = el.getAttribute('class');
    el.setAttribute('class', cl + ' highlighted')
}

onMouseLeave = (event) =>
{
    const el = document.getElementById(event.target.id);
    const cl = el.getAttribute('class');
    const cl1 = cl.replace(' highlighted','');
    el.setAttribute('class', cl1);
}

const PatientGreetingBox = (props) =>
{
    return(
        <div className={props.className + " ba f-kalam-24 greetingbox"}>
            <div className="pt1 pb3">
                {
                    IntlMessages[props.locale].intl.formatMessage(
                        {
                            id: 'patientgreetingbox.hi',
                            defaultMessage: 'Hi',
                            description: 'Hi'
                        }
                    ) + ', ' + props.userName.Name
                }
            </div>
            <div className="pb3">
                <FormattedMessage
                    id='patientgreetingbox.goodmorning'
                    defaultMessage='Good Morning !'
                    description='Good Monring !'
                />
            </div>
            {
                props.appointmentList.length === 0 
                    ? <div>
                        <FormattedMessage
                            id='patientgreetingbox.noappointments'
                            defaultMessage='You have no appointments set'
                            description='You have no appointments set'
                        />
                      </div>
                    : <div>
                        <div>
                            <FormattedMessage
                                id='patientgreetingbox.yourappointments'
                                defaultMessage='Your appointments:'
                                description='Your appointments:'
                            />
                        </div>
                        <div className={"height-control-150"}>
                            {
                                props.appointmentList.map(
                                    (appointment,idx) =>
                                    {
                                        return(
                                            <div key={idx} className='flex justify-between items-center'>
                                                <div 
                                                    className='mr3' 
                                                    id={idx}
                                                >
                                                    {appointment.msg}
                                                </div>
                                                <button
                                                    className='bg-buttonblue br3 shadow-3 f-arial-13 c-white mr2'
                                                    onClick={() => props.onAppointmentSelect(appointment.ap)}
                                                >
                                                    <FormattedMessage
                                                        id='visits.cancel'
                                                        defaultMessage='Cancel'
                                                    />
                                                </button>
                                            </div>
                                        )
                                    }
                                )
                            }
                        </div>
                      </div>
            }
        </div>
    )
}

export default connect(mapStateToProps)(PatientGreetingBox);