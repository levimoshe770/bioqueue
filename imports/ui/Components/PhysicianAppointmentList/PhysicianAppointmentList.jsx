import React, { Component } from 'react'
import { DatePicker } from 'antd';
import moment from 'moment';
//import './PhysicianAppointmentList.css';
import 'antd/dist/antd.css';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';

const mapStateToProps = state =>
{
  return {locale: state.locale};
}

/*
appointments
patients
onClose
 */

class PhysicianAppointmentList extends Component
{

  constructor(props)
  {
      super(props);
      this.state = 
      {
          checkAll: true,
          filteredDate: new moment(),
          showCancelPointer: -1
      }
  }

  onDateChanged = (d) =>
  {
    this.setState({filteredDate: d});
  }

  onFilterCheck = (event) =>
  {
      this.setState({checkAll: event.target.checked});
  }

  getFilteredList = () =>
  {
    if (this.state.checkAll)
        return this.props.appointmentList;

    return this.props.appointmentList.filter(
        (ap) =>
        {
            const apDate = new Date(ap.DateTime);
            apDate.setHours(0,0,0,0);
            const filterDate = new Date(this.state.filteredDate.toDate());
            filterDate.setHours(0,0,0,0);

            return apDate.getTime() === filterDate.getTime();
        }
    )
  }

  cancelAppointment = (appointment) =>
  {
    //console.log('Canceling appointment', appointment);
    this.props.onAppointmentDelete(appointment);
  }

  render()
  {
    return(
      <div className={this.props.className + ' flex flex-column flex-stretch br4 shadow-6 background-18C7FF ba'}>
        <div className='flex justify-around pa1 items-center br4 br--top'>
          <div className='pl2 pr2 flex items-center'>
            <input 
                className=''
                type='checkbox'
                id='checkall'
                checked={this.state.checkAll}
                onChange={this.onFilterCheck}
            />
            <label
                className='pl1'
                htmlFor='checkall'
            >
                <FormattedMessage
                  id='physicianapplist.all'
                  defaultMessage='All'
                  description='All'
                />
            </label>
          </div>
          <div className='flex-grow-2 ml1'>
            <DatePicker 
              onChange={this.onDateChanged}
              defaultValue={this.state.filteredDate}
              disabled={this.state.checkAll}
            />
          </div>
          <img
            className='ml3 mr3'
            src='icons/icons8-printer-80.png'
            alt='printer'
            height='35px'
            size='small'
          />
          <div
              onClick={this.props.onClose}
              className='close-nf mr1'
            >
              &#9746;
          </div>
        </div>
        <div className='br4 br--bottom pa2'>
          <table className='ba'>
            <thead className='bg-verylightblue tc'>
                <tr>
                    <th className='pa2 ba'>
                      <FormattedMessage
                        id='physicianapplist.patientname'
                        defaultMessage='Patient Name'
                        description='Patient Name'
                      />
                    </th>
                    <th className='pa2 ba'>
                      <FormattedMessage
                        id='physicianapplist.time'
                        defaultMessage='Time'
                        description='Time'
                      />
                    </th>
                </tr>
            </thead>
            <tbody className='bg-white tc'>
              {
                this.getFilteredList().map(
                  (ap, idx) =>
                  {
                    return(
                      <tr key={idx}>
                        <td 
                          className='pa2 ba'
                          onMouseEnter={() => this.setState({showCancelPointer: idx})}
                          onMouseLeave={() => this.setState({showCancelPointer: -1})}
                        >
                          <div className='flex justify-around'>
                            {
                              this.state.showCancelPointer === idx &&
                              <div
                                className='pointer'
                                onClick={() => this.cancelAppointment(ap)}
                              >
                                ⮾
                              </div>
                            }
                            <div>
                              {ap.Name}
                            </div>
                          </div>
                        </td>
                        <td className='pa2 ba'>
                            {
                                this.state.checkAll 
                                ? moment(ap.DateTime).format('DD-MM-YYYY HH:mm')
                                : moment(ap.DateTime).format('HH:mm')
                            }
                        </td>
                      </tr>
                    );
                  }
                )
              }
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default PhysicianAppointmentList;