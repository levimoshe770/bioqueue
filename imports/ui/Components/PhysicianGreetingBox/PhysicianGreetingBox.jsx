import React, { Component } from 'react';
import './PhysicianGreetingBox.css'
import { withRouter } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';
import { Approved } from '../../../api/Lists/GeneralTypes/AuthorizationStatus';

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}

/*
className
Physician
NumOfAppointments

 */

class PhysicianGreetingBox extends Component
{
    onOpenProfile = () =>
    {
        this.props.history.push('/profile');
    }

    getGreetingMessage = () =>
    {
        const { Physician, NumOfAppointments } = this.props;

        if (Physician === undefined)
        {
            const loading = IntlMessages[this.props.locale].intl.formatMessage(
                {
                    id: 'physiciangreeting.loading',
                    defaultMessage: 'loading'
                }
            )
            return ('...' + loading);
        }

        const pendingMessage = Physician.Name + ' ' +
            IntlMessages[this.props.locale].intl.formatMessage(
                {
                    id: 'physiciangreeting.welcome',
                    defaultMessage: 'Welcome aboard. Your status is still pending. Please make sure you filled all necessary data in your profile'
                }
            );

        const approvedMessage = 
            Physician.Name + ' ' +
            IntlMessages[this.props.locale].intl.formatMessage(
                {
                    id: 'physiciangreeting.comeback',
                    defaultMessage: 'good to have you back'
                }
            )
            + '. ' + 
            (
                NumOfAppointments > 0 
                    ? 
                        IntlMessages[this.props.locale].intl.formatMessage(
                            {
                                id: 'physiciangreeting.youhave',
                                defaultMessage: 'You have'
                            }
                        )
                        + ' ' + NumOfAppointments.toString() + ' ' + 
                        IntlMessages[this.props.locale].intl.formatMessage(
                            {
                                id: 'physiciangreeting.appointmentsset',
                                defaultMessage: 'appointments set'
                            }
                        )
                    : 
                        IntlMessages[this.props.locale].intl.formatMessage(
                            {
                                id: 'physiciangreeting.noappointments',
                                defaultMessage: 'You have no appointments'
                            }
                        )
                        
            );

        return(
                Physician.AuthorizationStatus === Approved
                ?   
                    <div>
                        {approvedMessage}
                    </div>
                :
                    <div>
                        {pendingMessage}
                        <div
                            className='pointer f-link-courgette-27 mt3'
                            onClick={this.onOpenProfile}
                        >
                            <FormattedMessage
                                id='physiciangreeting.updateprofile'
                                defaultMessage='Click here to update your profile'
                                description='Click here to update your profile'
                            />
                        </div>
                    </div>
        )
            
    }

    render()
    {
        /*
        style={{height: '200px'}}
        */
        return(
            <div
                className={'ma4 pr3 pl3 pb4 pt4 f-courgette-27 br4 shadow-6 tc physiciangreetingbox ' + this.props.className}
                
            >
                {
                    this.getGreetingMessage()
                }
            </div>
        )
    }
}

export default connect(mapStateToProps)(withRouter(PhysicianGreetingBox));