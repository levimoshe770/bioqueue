import React, {Component} from 'react';
import SelectionButton from './SelectionButton.jsx'
//import moment from 'moment';
import BeforeVisit from './BeforeVisit.jsx';
import DuringVisit from './DuringVisit.jsx';
import EmptyData from './EmptyData.jsx';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { IntlMessages } from '../../../../translation/IntlMessages';
import Remote from '../../Remote';
import { DatePicker } from 'antd';
import 'antd/dist/antd.css';
import moment from 'moment';

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}


/*
visits
role
patientId,
physicianId,
physicianName,
permissions
 */

class Visits extends Component
{
  constructor(props)
  {
    super(props);
    this.state = 
    {
      tabSelected: 0,
      visitSelected: 0,
      showDateEdit: false,
      dateEdited: -1,
      showDateChange: false,
      dateChanged: -1,
      mouseInDelete: ''
    }
  }

  getSelectionTabs = () =>
  {
    const selectionTabs = 
    [
      IntlMessages[this.props.locale].intl.formatMessage(
        {
          id: 'visits.beforevisit',
          defaultMessage: 'Before Visit'
        }
      ),
      IntlMessages[this.props.locale].intl.formatMessage(
        {
          id: 'visits.duringvisit',
          defaultMessage: 'During Visit'
        }
      )
    ];

    return selectionTabs;
  }
  

  onSelectionChanged = (idx) =>
  {
    this.setState({tabSelected: idx});
  }

  onVisitSelected = (idx) =>
  {
    this.setState({visitSelected: idx});
  }

  onVisitDelete = (id) =>
  {
    if (this.props.role === 'physician')
    {
      this.setState(
        {
          visitSelected: 0,
          tabSelected: 0,
          showDateEdit: false,
          dateEdited: -1,
          showDateChange: false,
          dateChanged: -1
        }
      )
      
      Remote.call('visit.remove',id);
    }
  }

  renderVisitParts = () =>
  {
    let selected = 0;
    if (this.state.visitSelected <= this.props.visits.length - 1)
      selected = this.state.visitSelected;

    switch(this.state.tabSelected)
    {
      case 0:
        return (
          <BeforeVisit
            visit={this.props.visits[selected]} 
            patientId={this.props.patientId}
            physicianId={this.props.physicianId}
            callingRole={this.props.role}
          />
        );
      case 1:
        return (
          <DuringVisit 
            patientId={this.props.patientId}
            physicianId={this.props.physicianId}
            visit={this.props.visits[selected]} 
            callingRole={this.props.role}
          />
        );
      default:
        return '';
    }
  }

  onNewVisit = () =>
  {
    Remote.call('visit.new',
      {
        PatientId: this.props.patientId,
        PhysicianId: this.props.physicianId,
        PhysicianName: this.props.physicianName,
        DateTime: new Date(),
        BeforeVisitData:
        {
          Symptoms: [],
          Tests: [],
          Recomendations: []
        },
        DuringVisit:
        {
          QA: [],
          Diagnosis: [],
          DoctorNotes: [],
          Instructions: []
        }
      }
    );
  }

  onEditDateClick = (idx) =>
  {
    this.setState(
      {
        showDateChange: !this.state.showDateChange,
        dateChanged: this.state.showDateChange ? -1 : idx
      }
    )
  }

  onVisitDateChanged = (idx, date) =>
  {
    //console.log(date.toDate());
    this.setState(
      {
        showDateChange: false,
        dateChanged: -1
      }
    );
    
    Remote.call('visit.modifyDate', 
      this.props.visits[idx]._id,
      date.toDate()
    );
  }

  render()
  {
    const { role, permissions, visits } = this.props;
    const editingAllowed = role === 'physician' && 
      (permissions.length > 0 && permissions[0].PermissionSet.addvisits);

    //console.log(this.props);
    //console.log(this.state);

    return(
      this.props.visits.length === 0
      ?
        <div className='flex flex-column items-center'>
          <EmptyData 
            Message = {
              IntlMessages[this.props.locale].intl.formatMessage(
                {
                  id: 'visits.novisitsfound',
                  defaultMessage: 'No visits found'
                }
              )
            }
          />
          { 
            editingAllowed &&
            <button
              className='bg-buttonblue pl3 pr3 pb2 pt2 br-pill shadow-3'
              onClick={this.onNewVisit}
            >
              <FormattedMessage
                id='visits.createnewvisit'
                defaultMessage='Create New Visit'
                description='Create New Visit'
              />
            </button>
          }
        </div>
      :
        <div className='w-100'>
          <div className='flex'>
            <div className='flex flex-column w-40 br b--light-gray'>
              {
                visits.map(
                  (v,idx) =>
                  {
                    return(
                      <div key={idx}>
                        <div 
                          className={'flex items-center justify-between pa2 ma2 shadow-3 br3 pointer grow ' +
                                      (idx === this.state.visitSelected ? 'bg-gray c-offwhite' : 'bg-light-gray')}
                        >
                          <div className='flex w-100 items-center'>
                            <div 
                              className='pr2 z-index-10 close'
                              onClick={() => this.onVisitDelete(v._id)}
                            >
                              {editingAllowed ? '⮾' : ' '}
                            </div>
                            <div 
                              className='flex items-center justify-between w-100'
                              onClick={() => this.onVisitSelected(idx)}
                            >
                              <div 
                                className=
                                {
                                  'bold f-arial-20-nc' +
                                  (editingAllowed && this.state.showDateEdit && this.state.dateEdited === idx ? ' ba' : '')
                                }
                                onMouseEnter={() => this.setState({showDateEdit: true, dateEdited: idx})}
                                onMouseLeave={() => this.setState({showDateEdit: false, dateEdited: -1})}
                                onClick={() => this.onEditDateClick(idx)}
                              > 
                                {
                                  v.DateTime.toLocaleDateString(this.props.locale,
                                    {
                                      weekday: 'long',
                                      day: 'numeric',
                                      month: 'long',
                                      year: 'numeric'
                                    }
                                  )
                                }
                              </div>
                              <div className='f-arial-16-nc bold'>{v.PhysicianName}</div>
                            </div>
                          </div>
                        </div>
                        { 
                          editingAllowed && 
                          this.state.showDateChange && this.state.dateChanged === idx &&
                          <div className='absolute z-index-100'>
                            <DatePicker
                              onChange={(d) => this.onVisitDateChanged(idx, d)}
                              defaultValue={new moment(v.DateTime)}
                            />
                          </div>
                        }
                      </div>
                    )
                  }
                )
              }
              {
                  editingAllowed &&
                  <div className='self-end'>
                    <button
                      className='bg-buttonblue pl3 pr3 pb1 pt1 br-pill shadow-3 mr2 mt1'
                      onClick={this.onNewVisit}
                    >
                      <FormattedMessage
                        id='visits.createnewvisit'
                        defaultMessage='Create New Visit'
                        description='Create New Visit'
                      />
                    </button>
                  </div>
              }
            </div>
            <div className='w-60'>
              <div className='flex justify-start'>
                {
                  this.getSelectionTabs().map(
                    (st,idx) =>
                    {
                      return(
                        <SelectionButton
                          label={st}
                          idx={idx}
                          key={idx}
                          orientation='top'
                          isSelected={idx === this.state.tabSelected}
                          onClick={this.onSelectionChanged}
                        />
                      )
                    }
                  )
                }
              </div>
              <div className=''>
                {this.renderVisitParts()}
              </div>
            </div>
          </div>
        </div>
    )
  }
}

export default connect(mapStateToProps)(Visits);