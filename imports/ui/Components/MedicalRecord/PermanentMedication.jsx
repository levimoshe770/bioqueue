import React, {Component} from 'react';
import EmptyData from './EmptyData';
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}

/*
permanentMedication
 */

class PermantentMedication extends Component
{
  render()
  {
    return(
      this.props.permanentMedication.length === 0
      ?
        <EmptyData 
          Message={
            IntlMessages[this.props.locale].intl.formatMessage(
              {
                id: 'permanentmed.nomedicationfound',
                defaultMessage: 'No medication found'
              }
            )
          }
        />
      :
        <div className='ma3 w-content flex flex-wrap'>
          {
            this.props.permanentMedication.map(
              (pr, idx) =>
              {
                return(
                  <div key={idx} className='pa2 bg-light-gray ma2 br3 shadow-3'>
                    <div className='tc mb3'>
                      <div className='bold'>{pr.Name}</div>
                    </div>
                    <div className='flex mb2'>
                      <div className='w4'>
                        <FormattedMessage
                          id='visits.medication'
                          defaultMessage='Medication'
                          description='Medication'
                        />
                      </div>
                      <div>{pr.Medication}</div>
                    </div>
                    <div className='flex mb2'>
                      <div className='w4'>
                        <FormattedMessage
                          id='visits.dosage'
                          defaultMessage='Dosage'
                          description='Dosage'
                        />
                      </div>
                      <div>{pr.Dosage}</div>
                    </div>
                    <div className='flex mb2'>
                      <div className='w4'>
                        <FormattedMessage
                          id='visits.notes'
                          defaultMessage='Notes'
                          description='Notes'
                        />
                      </div>
                      <div>{pr.Notes}</div>
                    </div>
                  </div>
                );
              }
            )
          }
        </div>
    )
  }
}

export default connect(mapStateToProps)(PermantentMedication);