import React from 'react';

/*
label
orientation (top, left, right, bottom)
idx
isSelected (boolean)
onClick (callback)
 */

const chooserClass = 'bb bl br br4 b--light-gray pa3 pointer grow' 
const selectionClass = 'bg-blue c-white'

const calcClass = (isSelected, orientation) =>
{
  return (isSelected ? chooserClass + ' ' + selectionClass : chooserClass) +
    ' ' + getRounded(orientation);
}

const getRounded = (orientation) =>
{
  switch (orientation)
  {
    case 'top':
      return 'br--top';
    case 'left':
      return 'br--left';
    case 'right':
      return 'br--right';
    case 'bottom':
      return 'br--bottom';
    default:
      return '';
  }
}

const SelectionButton = (props) =>
{
  return(
    <div 
      className={calcClass(props.isSelected, props.orientation)} 
      onClick={() => props.onClick(props.idx)} 
      id={props.idx}
      key={props.idx}
    >
        {props.label}
    </div>
  )
}

export default SelectionButton;