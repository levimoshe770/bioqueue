import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'
import EditTools from './EditTools';

class QuestionAnswer extends Component
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            showEditTools: false
        }
    }

    render()
    {
        const { idx, qa, className, editingAllowed } = this.props;

        return(
            <div 
                key={idx} 
                className={className + ' flex w-100'}
                onMouseEnter={() => this.setState({showEditTools: true})}
                onMouseLeave={() => this.setState({showEditTools: false})}
            >
                <div className={'pa1 shadow-3 w-100'}>
                    <div className='flex mb1'>
                        <div className='w1 bold'>
                            <FormattedMessage
                                id='visits.q'
                                defaultMessage='Q'
                                description='Q'
                            />
                        </div>
                        <div>{qa.Q}</div>
                    </div>
                    <div className='flex'>
                        <div className='w1 bold'>
                            <FormattedMessage
                            id='visits.a'
                            defaultMessage='A'
                            description='A'
                            />
                        </div>
                        <div>{qa.A}</div>
                    </div>
                </div>
                {
                    editingAllowed && this.state.showEditTools &&
                    <EditTools
                        item={{item: qa, index: idx}}
                        className='ml1'
                        onItemEdit={() => this.props.onEditQA(qa, idx)}
                        onItemDelete={() => this.props.onDeleteQA(qa)}
                    />
                }
            </div>
        )
    }
}

export default QuestionAnswer;