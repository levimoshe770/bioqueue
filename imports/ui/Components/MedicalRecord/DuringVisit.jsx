import React, {Component} from 'react';
import InputPopup from './InputPopup.jsx'
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';
import Laboratories from '../../../api/Lists/Laboratories/Laboratories';
import Remote from '../../Remote';
import EditTools from './EditTools.jsx';
import Instruction from './Instruction.jsx';
import QuestionAnswer from './QuestionAnswer.jsx';

const mapStateToProps = (state) =>
{
    return {locale: state.locale, access_token: state.access_token}
}

class DuringVisit extends Component 
{

  constructor(props)
  {
    super(props);
    this.state = 
    {
      showNewQa: false,
      showNewDiagnosis: false,
      showNewNote: false,
      showNewInstruction: false,
      showEditTools: {item: '', idx: -1},
      action: '',
      editedItem: {}
    }
  }

  onQaNewQuestion = () =>
  {
    this.setState({showNewQa: !this.state.showNewQa, action: 'create'});
  }

  onEditQA = (qa, idx) =>
  {
    this.setState(
      {
        showNewQa: true,
        action: 'edit',
        editedItem: {item: qa, index: idx}
      }
    );
  }

  onDeleteQA = (qa) =>
  {
    Remote.call('visit.duringvisit.removeqa', 
      this.props.visit._id,
      qa
    );
  }

  onQAEdit = (qa, idx) =>
  {
    Remote.call('visit.duringvisit.editqa',
      this.props.visit._id,
      qa,
      idx
    );

    this.setState(
      {
        showNewQa: false,
        action: '',
        editedItem: {}
      }
    );
  }

  onNewQa = (q, a) =>
  {
    Remote.call('visit.duringvisit.addqa',
      this.props.visit._id,
      {
        Q: q,
        A: a
      }
    );

    this.setState({showNewQa: false, action: ''});
  }

  onNewNotes = (note, type) =>
  {
    switch (type)
    {
      case 'Diagnosis':
        this.setState({showNewDiagnosis: false, action: ''});
        Remote.call('visit.duringvisit.adddiagnosis',
          this.props.visit._id, note
        );
        break;
      case 'Notes':
        this.setState({showNewNote: false, action: ''});
        Remote.call('visit.duringvisit.addnote',
          this.props.visit._id, note
        );
        break;
      default:
        break;
    }
  }

  onEditNotes = (type, note, idx) =>
  {
    switch (type) {
      case 'Diagnosis':
        Remote.call('visit.duringvisit.editdiagnosis',
          this.props.visit._id,
          note,
          idx
        );
        this.setState(
          {
            showNewDiagnosis: false,
            action: '',
            editedItem: {}
          }
        )
        break;
      case 'Notes':
        Remote.call('visit.duringvisit.editnotes',
          this.props.visit._id,
          note,
          idx
        );
        this.setState(
          {
            showNewNote: false,
            action: '',
            editedItem: {}
          }
        );
        break;
      default:
        break;
    }
  }

  onDiagnosisDelete = (diagnosis) =>
  {
    Remote.call('visit.duringvisit.removediagnosis',
      this.props.visit._id,
      diagnosis
    );
  }

  onDiagnosisEdit = (diagnosis, idx) =>
  {
    this.setState(
      {
        showNewDiagnosis: true,
        action: 'edit',
        editedItem: {item: diagnosis, index: idx}
      }
    );
  }

  onNoteDelete = (note) =>
  {
    Remote.call('visit.duringvisit.removenote',
      this.props.visit._id,
      note
    );
  }

  onNoteEdit = (note, idx) =>
  {
    this.setState(
      {
        showNewNote: true,
        action: 'edit',
        editedItem: {item: note, index: idx}
      }
    )
  }

  onNewInstruction = (instruction) =>
  {
    Remote.call('visit.duringvisit.addInstruction',
      this.props.visit._id,
      {
        Name: instruction.Name,
        Medication: instruction.Medication,
        Dosage: instruction.Dosage,
        Permanent: instruction.Permanent,
        Lab: instruction.Lab,
        Notes: instruction.Notes
      }
    )

    if (instruction.Permanent)
    {
      Remote.call('permanentmedication.add',
        {
          PatientId: this.props.patientId,
          Name: instruction.Name,
          Medication: instruction.Medication,
          Dosage: instruction.Dosage,
          Notes: instruction.Notes
        }
      );
    }

    // If this is a lab exam, and the lab exists in our system,
    // issue labTest order
    if (instruction.Lab !== undefined)
    {
      const myLab = Laboratories.findOne({Name: instruction.Lab});
      if (myLab !== undefined)
      {
        Remote.call('labtest.create',
          {
            PatientId: this.props.patientId,
            PhysicianId: this.props.physicianId,
            LabId: myLab._id,
            Name: instruction.Name,
            DateOfOrder: new Date(),
            Notes: [instruction.Notes],
            SpecialConsiderations: '',
            Status: 1
          }
        );
      }
    }

    this.setState({showNewInstruction: false})
  }

  onEditInstruction = (instruction, idx) =>
  {
    this.setState(
      {
        showNewInstruction: true,
        action: 'edit',
        editedItem: {item: instruction, index: idx}
      }
    )
  }

  onDeleteInstruction = (instruction) =>
  {
    Remote.call('visit.duringvisit.removeInstruction',
      this.props.visit._id,
      instruction
    );
  }

  onInstructionEdit = (instruction, idx) =>
  {
    Remote.call('visit.duringvisit.editInstruction',
      this.props.visit._id,
      instruction,
      idx
    );

    this.setState(
      {
        showNewInstruction: false,
        action: 'create',
        editedItem: {}
      }
    );
  }

  getDosage = (dosage) =>
  {
    const regex = '[0-9]+';
    const regexRes = '[dwh]{1}';

    const num = dosage.match(regex)[0];
    const res = dosage.match(regexRes)[0];

    return (
      {
        Ammount: parseInt(num),
        Resolution: res
      }
    )
  }

  issuePrescription = (instruction) =>
  {

    const prescription = 
    {
      Name: instruction.Medication,
      Dosage: this.getDosage(instruction.Dosage),
      Date: new Date(),
      Validity: 3,
      PatientId: this.props.patientId,
      PhysicianId: this.props.physicianId,
      Supplied: false,
      Notes: instruction.Notes
    }

    Remote.call('prescription.add',
      this.props.access_token,
      prescription
    );
  }

  render()
  {
    
    const editAllowed = this.props.callingRole === 'physician';

    return(
      <div>
        <div className='pa2 bg-light-gray ma1'>
          <div className='f-arial-20-nc c-gray'>
            <FormattedMessage
              id='visits.questionsanswers'
              defaultMessage='Questions & Answers'
              description='Questions & Answers'
            />
          </div>
          <div className=''>
            {
              this.props.visit.DuringVisit.QA.map(
                (qa, idx) =>
                {
                  return(
                    <QuestionAnswer
                      className='ma1 mb2'
                      idx={idx}
                      qa={qa}
                      editingAllowed={editAllowed}
                      onEditQA={this.onEditQA}
                      onDeleteQA={this.onDeleteQA}
                    />
                  )
                }
              )
            }
          </div>
          {
            editAllowed &&
            <div className='flex justify-end c-gray'>
              <div 
                className='grow pointer'
                onClick={this.onQaNewQuestion}
              >
                  &#8853;
              </div>
            </div>
          }
          {
            this.state.showNewQa &&
            <InputPopup
              type='QA'
              action={this.state.action}
              values={this.state.editedItem}
              onClose={() => this.setState({showNewQa: false, action: '', editedItem: {}})}
              onNewQa={this.onNewQa}
              onEditQa={this.onQAEdit}
            />
          }
        </div>
        <div className='pa2 bg-light-gray ma1'>
          <div className='f-arial-20-nc c-gray'>
            <FormattedMessage
              id='visits.diagnosis'
              defaultMessage='Diagnosis'
              description='Diagnosis'
            />
          </div>
          <div className='flex'>
            {
              this.props.visit.DuringVisit.Diagnosis.map(
                (d, idx) =>
                {
                  if (!editAllowed)
                  {
                    return(
                      <div key={idx} className='pa1 shadow-3 ma1 mb2'>{d}</div>
                    );
                  }
                  else
                  {
                    return(
                      <div
                        key={idx}
                        className='flex'
                        onMouseEnter={
                          () => this.setState({showEditTools: {item: 'Diagnosis', idx: idx}})
                        }
                        onMouseLeave={
                          () => this.setState({showEditTools: {item: '', idx: -1}})}
                      >
                        <div className='ma1 bg-light-gray pa2 shadow-3 br1'>
                          {d}
                        </div>
                        {
                          this.state.showEditTools.idx === idx &&
                          this.state.showEditTools.item === 'Diagnosis' &&
                          <EditTools
                            item={{item: d, index: idx}}
                            className=''
                            onItemDelete={() => this.onDiagnosisDelete(d)}
                            onItemEdit={() => this.onDiagnosisEdit(d, idx)}
                          />
                        }
                      </div>
                    );
                  }
                }
              )
            }
          </div>
          {
            editAllowed &&
            <div className='flex justify-end c-gray'>
              <div 
                className='grow pointer'
                onClick={() => this.setState({showNewDiagnosis: !this.state.showNewDiagnosis, action: 'create'})}
              >
                &#8853;
              </div>
            </div>
          }
          {
            this.state.showNewDiagnosis &&
            <InputPopup
              label=
              {
                IntlMessages[this.props.locale].intl.formatMessage(
                  {
                    id: 'visits.diagnosis',
                    defaultMessage: 'Diagnosis'
                  }
                )
              }
              type='Notes'
              action={this.state.action}
              values={this.state.editedItem}
              onClose={() => this.setState({showNewDiagnosis: false})}
              onNewNotes={(note) => this.onNewNotes(note, 'Diagnosis')}
              onEditNotes={(diagnosis, idx) => this.onEditNotes('Diagnosis', diagnosis, idx)}
            />
          }
        </div>
        <div className='pa2 bg-light-gray ma1'>
          <div className='f-arial-20-nc c-gray'>
            <FormattedMessage
              id='visits.doctornotes'
              defaultMessage='Doctor Nots'
              description='Doctor Notes'
            />
          </div>
          <div className=''>
            {
              this.props.visit.DuringVisit.DoctorNotes.map(
                (dn, idx) =>
                {
                  if (!editAllowed)
                  {
                    return(
                      <div key={idx} className='pa1 shadow-3 ma1 mb2'>
                        {dn}
                      </div>
                    );
                  }
                  else{
                    return(
                      <div
                        key={idx}
                        className='flex'
                        onMouseEnter={
                          () => this.setState({showEditTools: {item: 'DoctorNotes', idx: idx}})
                        }
                        onMouseLeave={
                          () => this.setState({showEditTools: {item: '', idx: -1}})}
                      >
                        <div className='ma1 bg-light-gray pa2 shadow-3 br1'>
                          {dn}
                        </div>
                        {
                          this.state.showEditTools.idx === idx &&
                          this.state.showEditTools.item === 'DoctorNotes' &&
                          <EditTools
                            item={{item: dn, index: idx}}
                            className=''
                            onItemDelete={() => this.onNoteDelete(dn)}
                            onItemEdit={() => this.onNoteEdit(dn, idx)}
                          />
                        }
                      </div>
                    )
                  }
                }
              )
            }
          </div>
          {
            this.props.callingRole === 'physician' &&
            <div className='flex justify-end c-gray'>
              <div 
                className='grow pointer'
                onClick={() => this.setState({showNewNote: !this.state.showNewNote, action: 'create'})}
              >
                &#8853;
              </div>
            </div>
          }
          {
            this.state.showNewNote &&
            <InputPopup
              label=
              {
                IntlMessages[this.props.locale].intl.formatMessage(
                  {
                    id: 'visits.notes',
                    defaultMessage: 'Notes'
                  }
                )
              }
              type='Notes'
              action={this.state.action}
              values={this.state.editedItem}
              onClose={() => this.setState({showNewNote: false})}
              onNewNotes={(note) => this.onNewNotes(note, 'Notes')}
              onEditNotes={(note, idx) => this.onEditNotes('Notes', note, idx)}
            />
          }
        </div>
        <div className='pa2 bg-light-gray ma1'>
          <div className='f-arial-20-nc c-gray'>
            <FormattedMessage
              id='visits.instructions'
              defaultMessage='Instructions'
              description='Instructions'
            />
          </div>
          <div className='flex flex-wrap'>
            {
              this.props.visit.DuringVisit.Instructions.map(
                (instruction, idx) =>
                {
                  return(
                    <Instruction
                      idx={idx}
                      className='ma1 mb2'
                      instruction={instruction}
                      editingAllowed={editAllowed}
                      onIssuePrescription={this.issuePrescription}
                      onEditInstruction={this.onEditInstruction}
                      onDeleteInstruction={this.onDeleteInstruction}
                    />
                  )
                }
              )
            }
          </div>
          {
            this.props.callingRole === 'physician' &&
            <div className='flex justify-end c-gray'>
              <div 
                className='grow pointer'
                onClick={() => this.setState({showNewInstruction: !this.state.showNewInstruction, action: 'create'})}
              >
                &#8853;
              </div>
            </div>
          }
          {
            this.state.showNewInstruction &&
            <InputPopup
              label=
              {
                IntlMessages[this.props.locale].intl.formatMessage(
                  {
                    id: 'visits.instruction',
                    defaultMessage: 'Instruction'
                  }
                )
              }
              type='Instruction'
              action={this.state.action}
              values={this.state.editedItem}
              onClose={() => this.setState({showNewInstruction: false})}
              onNewInstruction={this.onNewInstruction}
              onEditInstruction={this.onInstructionEdit}
            />
          }
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(DuringVisit);