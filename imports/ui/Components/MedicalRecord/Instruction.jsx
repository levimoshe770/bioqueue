import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import EditTools from './EditTools.jsx';
/*
idx
className
instruction
editingAllowed

onIssuePrescription
onEditInstruction
onDeleteInstruction
 */

class Instruction extends Component
{
    constructor(props)
    {
        super(props);
        this.state =
        {
            showEditTools: false
        }
    }

    render()
    {
        const { idx, instruction, editingAllowed, className } = this.props;

        return(
            <div 
                className={className + ' flex'}
                key={idx}
                onMouseEnter={() => this.setState({showEditTools: true})}
                onMouseLeave={() => this.setState({showEditTools: false})}
            >
                <div className={'pa1 shadow-3'}>
                    <div className='flex'>
                        <div className='w4'>
                            <FormattedMessage
                                id='register.name'
                                defaultMessage='Name'
                                description='Name'
                            />
                        </div>
                        <div className='bold'>{instruction.Name}</div>
                    </div>
                    { 
                        instruction.Medication !== undefined &&
                        <div className='flex'>
                            <div className='w4'>
                                <FormattedMessage
                                    id='visits.medication'
                                    defaultMessage='Medication'
                                    description='Medication'
                                />
                                </div>
                            <div>{instruction.Medication}</div>
                        </div>
                    }
                    {
                        instruction.Dosage !== undefined &&
                        <div className='flex'>
                            <div className='w4'>
                                <FormattedMessage
                                    id='visits.dosage'
                                    defaultMessage='Dosage'
                                    description='Dosage'
                                />
                            </div>
                            <div>{instruction.Dosage}</div>
                        </div>
                    }
                    {
                        instruction.Lab !== undefined &&
                        <div className='flex'>
                            <div className='w4'>
                                <FormattedMessage
                                    id='visits.lab'
                                    defaultMessage='Lab'
                                    description='Lab'
                                />
                            </div>
                            <div>{instruction.Lab}</div>
                        </div>
                    }
                    <div className='flex'>
                        <div className='w4'>
                            <FormattedMessage
                                id='visits.notes'
                                defaultMessage='Notes'
                                description='Notes'
                            />
                        </div>
                        <div>{instruction.Notes}</div>
                    </div>
                    {
                        instruction.Medication !== undefined &&
                        editingAllowed &&
                        <button
                            className='bg-buttonblue pl3 pr3 pb2 pt2 ma2 c-white br-pill shadow-3'
                            onClick={() => this.props.onIssuePrescription(instruction)}
                        >
                            <FormattedMessage
                                id='prescription.issue'
                                defaultMessage='Issue Prescription'
                                description='Issue'
                            />
                        </button>
                    }
                </div>
                {
                    editingAllowed && this.state.showEditTools &&
                    <EditTools
                        item={{item: instruction, index: idx}}
                        className='ml1'
                        onItemEdit={() => this.props.onEditInstruction(instruction, idx)}
                        onItemDelete={() => this.props.onDeleteInstruction(instruction)}
                    />
                }
            </div>
        )
    }
}

export default Instruction;