import React, {Component} from 'react'
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import SelectionButton from './SelectionButton.jsx'
import PatientData from './PatientData.jsx';
import LabTests from './LabTests.jsx';
import Visits from './Visits.jsx';
import SimpleList from './SimpleList.jsx';
import PermanentMedication from './PermanentMedication.jsx';
import MedicalRecordCollection from '../../../api/Lists/MedicalRecord/MedicalRecordCollection';
import Patients from '../../../api/Lists/Patients/patients';
import Physicians from '../../../api/Lists/Physicians/physicians';
import LabTestsCollection from '../../../api/Lists/LabTests/LabTestsCollection';
import PermanentMedicationCollection from '../../../api/Lists/PermanentMedication/PermanentMedicationCollection';
import VisitsCollection from '../../../api/Lists/Visits/VisitsCollection';
import MedicalRecordPermissions from '../../../api/Lists/MedicalRecordPermissions/MedicalRecordPermissions';
import InputPopup from './InputPopup.jsx';
import SharingTable from './SharingTable.jsx';
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';
import Remote from '../../Remote';
import PrescriptionsRecord from './PrescriptionsRecord.jsx';

const mapStateToProps = (state) =>
{
    return {locale: state.locale, access_token: state.access_token}
}

/*
callingRole
patientId
className

onClose
 */

class MedicalRecord extends Component
{
  constructor(props)
  {
    super(props);
    this.state = 
    {
      buttonSelected: 0,
      showNewKnownProblem: false,
      showNewAlergy: false,
      showShared: false,
      action: 'create',
      editedItem: {}
    }
  }

  /*
  patient = 
  {
    Name: 'Moshe Levi',
    Email: 'levimoshe770@gmail.com',
    Address: 'Leon Blum 62, Haifa',
    Phone: '972-54-6461336',
    Avatar: '1412349_695378430473055_348033406_o.jpg',
    DOB: '11-06-1966',
    Gender: 'male'
  }

  patientHistoricData =
  {
    KnownProblems: [
      'BP',
      'Diabetes 2',
      'Obesity'
    ],
    PermanentMedication:
    [
      'Heroine','LSD','Cokaine'
    ],
    Alergies:
    [
      'Leftists','Peniciline'
    ]
  }

  visits = 
  [
    {
      DateTime: new Date('8-2-2020'),
      Description: 'Get to know each other',
      BeforeVisitData: 
      {
        Symptoms: ['Deasiness','Headache','Nosia'],
        Tests: [
          {
            Name: 'Blood Chemistry',
            DateOfOrder: new Date('7-29-2020'),
            Provider: 'Lab1'
          },
          {
            Name: 'Blood Fat profile',
            DateOfOrder: new Date('7-10-2020'),
            Provider: 'Lab1'
          },
          {
            Name: 'Urine General',
            DateOfOrder: new Date('7-29-2020'),
            Provider: 'Lab1'
          }
        ],
        Recomendations: [
          'Rest',
          'Eat well',
          'Play with the kids',
          'Be nice to yourself'
        ]
      },
      DuringVisit:
      {
        QA: [
          {
            Q: 'Since when you feel like that',
            A: 'Since I was born'
          },
          {
            Q: 'Any history of problems',
            A: 'Family issues'
          }
        ],
        Diagnosis: ['bla bla bla 1','bla bla bla 2'],
        DoctorNotes: ['this is my note 1','note 2','note 3'],
        Instructions: [
          {
            Name: 'BP pill',
            Medication: 'LOSARDEX PLUS',
            Dosage: '2 per day',
            Notes: ''
          },
          {
            Name: 'further check up',
            Notes: 'for checking bla bla bla please perform tests',
            Lab: '2cabd18c3e....'
          }
        ]
      }
    },
    {
      DateTime: new Date('8-25-2020'),
      Description: 'Follow up after treatment',
      BeforeVisitData: 
      {
        Symptoms: ['Deasiness','Headache','Nosia'],
        Tests: [
          {
            Name: 'Blood Chemistry',
            DateOfOrder: new Date('7-29-2020'),
            Provider: 'Lab1'
          },
          {
            Name: 'Blood Fat profile',
            DateOfOrder: new Date('7-29-2020'),
            Provider: 'Lab1'
          },
          {
            Name: 'Urine General',
            DateOfOrder: new Date('7-29-2020'),
            Provider: 'Lab1'
          }
        ],
        Recomendations: [
          'Rest',
          'Eat well',
          'Play with the kids'
        ]
      },
      DuringVisit:
      {
        QA: [
          {
            Q: 'Since when you feel like that',
            A: 'Since I was born'
          },
          {
            Q: 'Any history of problems',
            A: 'Family issues'
          }
        ],
        Diagnosis: ['bla bla bla 1','bla bla bla 2'],
        DoctorNotes: ['this is my note 1','note 2','note 3'],
        Instructions: [
          {
            Name: 'BP pill',
            Medication: 'LOSARDEX PLUS',
            Dosage: '2 per day',
            Notes: ''
          },
          {
            Name: 'further check up',
            Notes: 'for checking bla bla bla please perform tests',
            Lab: '2cabd18c3e....'
          }
        ]
      }
    }
  ]

  labTests = [
    {
      Name: 'Blood Chemistry total',
      DateOfOrder: new Date('8-10-2020'),
      SpecialConsiderations: 'He\'s an idiot',
      Laboratory: '2cabd18c3e....',
      Results: 
      [
        {
          Name: 'Glucose',
          Type: 'Numeric',
          Result: 200,
          Min: 100,
          Max: 120
        },
        {
          Name: 'CVC',
          Type: 'Numeric',
          Result: -0.7,
          Min: -1.0,
          Max: 1.0
        },
        {
          Name: 'Urea',
          Type: 'Numeric',
          Result: 3,
          Min: 20,
          Max: 80
        },
        {
          Name: 'HTC',
          Type: 'Numeric',
          Result: 352,
          Min: 180,
          Max: 300
        }
      ],
      DateOfPerformance: new Date('8-12-2020'),
      ApproximateDuration: '2d',
      DateOfResults: new Date('8-13-2020')
    },
    {
      Name: 'Blood Count total',
      DateOfOrder: new Date('8-12-2020'),
      SpecialConsiderations: 'He\'s an idiot, He\'s an idiot, He\'s an idiot, He\'s an idiot, ',
      Laboratory: '2cabd18c3e....',
      Results: 
      [
        {
          Name: 'Langs',
          Type: 'Image',
          Result: 'https://prod-images-static.radiopaedia.org/images/34237641/2fc529551be221fe82ed228481af7f_jumbo.jpeg'
        },
        {
          Name: 'CVC',
          Type: 'Numeric',
          Result: -0.7,
          Min: -1.0,
          Max: 1.0
        },
        {
          Name: 'Urea',
          Type: 'Numeric',
          Result: 48,
          Min: 20,
          Max: 80
        },
        {
          Name: 'HTC',
          Type: 'Numeric',
          Result: 352,
          Min: 180,
          Max: 300
        }
      ],
      DateOfPerformance: new Date('8-14-2020'),
      ApproximateDuration: '2d',
      DateOfResults: new Date('8-15-2020')
    }
  ]

  knownProblems = [
    'HYPERTENSION','DIABETES 2','OBESITY','CANDIDA'
  ]

  permanentMedication =
  [
    {
      Name: 'BP pill',
      Medication: 'LOSARDEX PLUS',
      Dosage: '2 per day',
      Notes: ''
    },
    {
      Name: 'Candida',
      Medication: 'CANDIDAN',
      Dosage: '2 per day',
      Notes: ''
    }

  ]

  alergies = 
  [
    'Penicilin',
    'G6PD deficiency'
  ]
  */
 
  getButtonList = () =>
  {
    const buttonList = 
    [
      IntlMessages[this.props.locale].intl.formatMessage(
        {
          id: 'medicalrecord.patientdata',
          defaultMessage: 'Patient Data'
        }
      ),
      IntlMessages[this.props.locale].intl.formatMessage(
        {
          id: 'medicalrecord.visits',
          defaultMessage: 'Visits'
        }
      ),
      IntlMessages[this.props.locale].intl.formatMessage(
        {
          id: 'medicalrecord.labtests',
          defaultMessage: 'Lab Tests'
        }
      ),
      IntlMessages[this.props.locale].intl.formatMessage(
        {
          id: 'pharmacy.prescriptions',
          defaultMessage: 'Prescriptions'
        }
      ),
      IntlMessages[this.props.locale].intl.formatMessage(
        {
          id: 'medicalrecord.knownproblems',
          defaultMessage: 'Known problems'
        }
      ),
      IntlMessages[this.props.locale].intl.formatMessage(
        {
          id: 'medicalrecord.permanentmedication',
          defaultMessage: 'Permanent medication'
        }
      ),
      IntlMessages[this.props.locale].intl.formatMessage(
        {
          id: 'medicalrecord.alergies',
          defaultMessage: 'Alergies'
        }
      )
    ];

    return buttonList;
  }

  onSelectionButtonClick = (idx) =>
  {
    this.setState({buttonSelected: idx});
  }

  onNewKnownProblem = () =>
  {
    this.setState({showNewKnownProblem: true, action: 'create'})
  }

  onNewAlergy = () =>
  {
    this.setState({showNewAlergy: true, action: 'create'});
  }

  onAllergyDelete = (allergy) =>
  {
    const { item } = allergy;

    Remote.call('medicalrecord.removeallergy',
      this.props.medicalRecord[0]._id,
      item
    );
  }

  onAllergyEdit = (allergy) =>
  {
    this.setState({showNewAlergy: true, action: 'edit', editedItem: allergy});
  }

  onEditAllergy = (allergy, idx) =>
  {
    Remote.call('medicalrecord.updateallergy',
      this.props.medicalRecord[0]._id,
      allergy,
      idx
    );

    this.setState({showNewAlergy: false, action: '', editedItem: {}});
  }

  onKnownProblemDelete = (problem) =>
  {
    const { item } = problem;

    Remote.call('medicalrecord.removeKnownProblem',
      this.props.medicalRecord[0]._id,
      item
    );
  }

  onKnownProblemEdit = (problem) =>
  {
    this.setState({showNewKnownProblem: true, action: 'edit', editedItem: problem})
  }

  onEditKnownProblem = (problem, idx) =>
  {
    Remote.call('medicalrecord.updateKnownProblem',
      this.props.medicalRecord[0]._id,
      problem,
      idx
    );

    this.setState({showNewKnownProblem: false, action: '', editedItem: {}});
  }

  onCreateNewAlergy = (alergy) =>
  {
    Remote.call('medicalrecord.addalergies',
      this.props.medicalRecord[0]._id,
      alergy
    );
    this.setState({showNewAlergy: false})
  }

  onCreateKnownProblem = (knownProblem) =>
  {
    this.setState({showNewKnownProblem: false})
    Remote.call('medicalrecord.addknownproblem',
      this.props.medicalRecord[0]._id,
      knownProblem
    );
  }

  renderNoPermission = () =>
  {
    return(
      <div className='f-arial-30 tc ma5'>
        You have no permission
      </div>
    )
  }

  renderScreens = () =>
  {
    if (this.props.callintRole === '')
      return;

    switch (this.state.buttonSelected)
    {
      case 0:
        return(
          <PatientData 
            patient=
            {
              this.props.patients === undefined 
                ? undefined 
                : this.props.patients[0]
            }
            role={this.props.callingRole}
            onChatBoxOpen={this.props.onChatBoxOpen}
          />
        );
      case 1:
        return(
          <Visits 
            visits=
            {
              this.props.visits
            } 
            role={this.props.callingRole}
            permissions=
            {
              this.props.callingRole === 'physician' 
                ? this.props.permissions
                : undefined
            }
            patientId={this.props.patientId}
            physicianId=
            {
              this.props.callingRole === 'physician'
                ? this.props.physician[0]._id
                : ''
            }
            physicianName=
            {
              this.props.callingRole === 'physician'
                ? this.props.physician[0].Name
                : ''
            }
          />
        );
      case 2:
        return(<LabTests labTests={this.props.labTests}/>);
      case 3:
        return(
          <PrescriptionsRecord 
            role={this.props.callingRole}
            access_token={this.props.access_token}
            patientId={this.props.patientId}
          />
        );
      case 4:
        if (this.props.callingRole === 'physician' &&
            !this.props.permissions[0].PermissionSet.kproblems)
            return(this.renderNoPermission());

        return(
          <div>
            <SimpleList 
              name={
                IntlMessages[this.props.locale].intl.formatMessage(
                  {
                    id: 'medicalrecord.knownproblems',
                    defaultMessage: 'Known problems'
                  }
                )
              }
              callingRole={this.props.callingRole}
              items=
              {
                this.props.medicalRecord.length === 0
                  ? []
                  : this.props.medicalRecord[0].KnownProblems
              }
              onNewItem={this.onNewKnownProblem}
              onItemDelete={this.onKnownProblemDelete}
              onItemEdit={this.onKnownProblemEdit}
            />
            {
              this.state.showNewKnownProblem &&
              <InputPopup
                label={
                  IntlMessages[this.props.locale].intl.formatMessage(
                    {
                      id: 'medicalrecord.newknownproblems',
                      defaultMessage: 'New known problems'
                    }
                  )
                }
                type='Notes'
                action={this.state.action}
                values={this.state.editedItem}
                onNewNotes={(note) => this.onCreateKnownProblem(note)}
                onClose={() => this.setState({showNewKnownProblem: false})}
                onEditNotes={(note, idx) => this.onEditKnownProblem(note, idx)}
              />
            }
          </div>
        );
      case 5:
        return(
          <PermanentMedication 
            permanentMedication=
            {
              this.props.medicalRecord.length === 0
                ? []
                : this.props.medication
            }
          />
        );
      case 6:
        return(
          <div>
            <SimpleList 
              name={
                IntlMessages[this.props.locale].intl.formatMessage(
                  {
                    id: 'medicalrecord.alergies',
                    defaultMessage: 'Allergies'
                  }
                )
              }
              callingRole={this.props.callingRole}
              items=
              {
                this.props.medicalRecord.length === 0 
                  ? []
                  : this.props.medicalRecord[0].Alergies
              } 
              onNewItem={this.onNewAlergy}
              onItemDelete={this.onAllergyDelete}
              onItemEdit={this.onAllergyEdit}
            />
            {
              this.state.showNewAlergy &&
              <InputPopup
                label=
                {
                  IntlMessages[this.props.locale].intl.formatMessage(
                    {
                      id: 'medicalrecord.newalergy',
                      defaultMessage: 'New Alergy'
                    }
                  )
                }
                type='Notes'
                action={this.state.action}
                values={this.state.editedItem}
                onNewNotes={(note) => this.onCreateNewAlergy(note)}
                onClose={() => this.setState({showNewAlergy: false})}
                onEditNotes={(allergy, idx) => this.onEditAllergy(allergy, idx)}
              />
            }
          </div>
        );
      default:
        return;
    }
  }

  componentDidMount()
  {
    Remote.call('medicalrecord.createIfNotExists', this.props.access_token);
  }

  render()
  {
    const patient = this.props.patients[0];

    return(
      <div className={'pa4 f-arial-16-nc ' + this.props.className}>
        <div className='br4 shadow-6 ba bw3 b--dark-blue'>
          <div className='flex justify-between bg-white items-center br4 br--top bb b--light-gray'>
            <div>
              {
                this.props.callingRole === 'patient' &&
                this.props.permissions.length > 0 &&
                <button
                  className='ml3 bg-buttonblue pl3 pr3 pb1 pt1 br3 shadow-3'
                  onClick={() => this.setState({showShared: !this.state.showShared})}
                >
                  <FormattedMessage
                    id='medicalrecord.shared'
                    defaultMessage='Shared'
                    description='Shared'
                  />
                </button>
              }
              {
                this.state.showShared &&
                <SharingTable
                  permissions={this.props.permissions}
                  physicians={this.props.physicians}
                />
              }
            </div>
            <div className='f-arial-30 bold c-dark-blue pa3'>
              {
                IntlMessages[this.props.locale].intl.formatMessage(
                  {
                    id: 'medicalrecord.medicalrecord',
                    defaultMessage: 'Medical Record'
                  }
                ) + ((patient !== undefined && this.props.callingRole === 'physician') 
                      ? ' - ' + patient.Name 
                      : ''
                    )
              }
            </div>
            <div
              onClick={this.props.onClose}
              className='close-nf mr1'
            >
              &#9746;
            </div>
          </div>
          <div className='flex bg-white br4 br--bottom pt2'>
            <div className='w5 flex flex-column items-stretch'>
              {
                this.getButtonList().map(
                  (b, idx) =>
                  {
                    return(
                      <SelectionButton 
                        label={b}
                        idx={idx}
                        key={idx}
                        orientation='left'
                        isSelected={idx === this.state.buttonSelected}
                        onClick={this.onSelectionButtonClick}
                      />
                    )
                  }
                )
              }
            </div>
            <div className='w-100'>
              {this.renderScreens()}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default 
connect(mapStateToProps)(
  withTracker(
    ({callingRole, patientId, access_token}) =>
    {
      if (callingRole === 'patient')
      {
        const medicalRecordHandle = Remote.subscribe('medicalrecord.patient',
          access_token
        );
        const loadingMedicalRecord = !medicalRecordHandle.ready();
        const patientHandle = Remote.subscribe('patients.userProfile',
          access_token
        );
        const loadingPatient = !patientHandle.ready();
        const visitHandle = Remote.subscribe('visits.patient', access_token);
        const loadingVisits = !visitHandle.ready();
        const labTestHandle = Remote.subscribe('labtests.patient', 
          access_token
        );
        const loadinglabTests = !labTestHandle.ready();
        const medicationHandle = Remote.subscribe('permanentmedication.patient',
          access_token
        );
        const loadingMedication = !medicationHandle.ready();
        const permissionHandle = Remote.subscribe('medicalrecordpermissions.patient',
          access_token
        );
        const loadingPermission = !permissionHandle.ready();
        const physiciansHandle = Remote.subscribe('physicians');
        const loadingPhysicians = !physiciansHandle.ready();

        return(
          {
            medicalRecord: !loadingMedicalRecord ? MedicalRecordCollection.find({}).fetch() : [],
            patients: !loadingPatient ? Patients.find({}).fetch() : [],
            visits: !loadingVisits ? VisitsCollection.find({}).fetch() : [],
            labTests: !loadinglabTests ? LabTestsCollection.find({}).fetch() : [],
            medication: !loadingMedication ? PermanentMedicationCollection.find({}).fetch() : [],
            permissions: !loadingPermission ? MedicalRecordPermissions.find({}).fetch() : [],
            physicians: !loadingPhysicians ? Physicians.find({}).fetch() : []
          }
        )
      }
      else if (callingRole === 'physician')
      {
        const medicalRecordHandle = Remote.subscribe('medicalrecord.physician',
          access_token,
          patientId
        );
        const loadingMedicalRecord = !medicalRecordHandle.ready();
        const permissionHandle = Remote.subscribe('medicalrecordpermissions.physicianSingle', 
          access_token,
          patientId
        );
        const loadingPermissions = !permissionHandle.ready();
        const patientHandle = Remote.subscribe('patients.physicianSingle', patientId);
        const loadingPatients = !patientHandle.ready();
        const visitHandle = Remote.subscribe('visits.physician',
          access_token,
          patientId
        );
        const loadingVisits = !visitHandle.ready();
        const physicianHandle = Remote.subscribe('physician.profile',
          access_token
        );
        const loadingPhysician = !physicianHandle.ready();
        const labTestHandle = Remote.subscribe('labtest.physician',
          access_token,
          patientId
        );
        const loadingLabTest = !labTestHandle.ready();
        const medicationHandle = Remote.subscribe('permantentmedication.physician', 
          access_token,
          patientId
        );
        const loadingMedication = !medicationHandle.ready();

        return(
          {
            medicalRecord: !loadingMedicalRecord ? MedicalRecordCollection.find({}).fetch() : [],
            permissions: !loadingPermissions ? MedicalRecordPermissions.find({}).fetch() : [],
            patients: !loadingPatients ? Patients.find({}).fetch() : [],
            visits: !loadingVisits ? VisitsCollection.find({}).fetch() : [],
            physician: !loadingPhysician ? Physicians.find({}).fetch() : [],
            labTests: !loadingLabTest ? LabTestsCollection.find({}).fetch() : [],
            medication: !loadingMedication ? PermanentMedicationCollection.find({}).fetch() : []
          }
        )
      }
    }
  )
  (MedicalRecord)
);