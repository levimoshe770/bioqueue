import React, {Component} from 'react';
import EmptyData from './EmptyData.jsx';
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';
import EditTools from './EditTools';

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}

/*
name
callingRole
items
onNewItem
 */

class SimpleList extends Component
{
  constructor(props)
  {
    super(props);
    this.state =
    {
      showEditTools: -1
    }
  }

  render()
  {
    //console.log(this.props);

    const editAllowed = (this.props.callingRole === 'physician');

    return(
      this.props.items.length === 0
      ?
        <div className='flex flex-column items-center'>
          <EmptyData 
            Message={
              IntlMessages[this.props.locale].intl.formatMessage(
                {
                  id: 'simplelist.noitemsfound',
                  defaultMessage: 'No items found'
                }
              )
            }
          />
          { 
            editAllowed &&
            <button
              className='bg-buttonblue pl3 pr3 pb1 pt1'
              onClick={this.props.onNewItem}
            >
              {
                IntlMessages[this.props.locale].intl.formatMessage(
                  {
                    id: 'simplelist.createnew',
                    defaultMessage: 'Create new'
                  }
                )
                + ' ' 
                + this.props.name
              }
            </button>
          }
        </div>
      :
        <div className='ma3 w-content flex flex-column'>
          {
            this.props.items.map(
              (pr, idx) =>
              {
                if (!editAllowed)
                  return(
                    <div key={idx} className='pa2 bg-light-gray bold ma2 br3 shadow-3'>{pr}</div>
                  );
                else
                {
                  return(
                    <div 
                      key={idx} 
                      className='flex'
                      onMouseEnter={() => this.setState({showEditTools: idx})}
                      onMouseLeave={() => this.setState({showEditTools: -1})}
                    >
                      <div className='pa2 bg-light-gray bold ma2 br3 shadow-3'>{pr}</div>
                      {
                        this.state.showEditTools === idx &&
                        <EditTools
                          item={{item: pr, index: idx}}
                          className=''
                          onItemDelete={this.props.onItemDelete}
                          onItemEdit={this.props.onItemEdit}
                        />
                      }
                    </div>
                  );
                }
              }
            )
          }
          {
            this.props.callingRole === 'physician' &&
            <div 
              className='grow pointer self-end mt2 mr2'
              onClick={this.props.onNewItem}
            >
              &#8853;
            </div>
          }
        </div>
    )
  }
}

export default connect(mapStateToProps)(SimpleList);