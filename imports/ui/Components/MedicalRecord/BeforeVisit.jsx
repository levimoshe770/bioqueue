import React, {Component} from 'react';
import InputPopup from './InputPopup.jsx';
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';
import Remote from '../../Remote';
import Laboratories from '../../../api/Lists/Laboratories/Laboratories';
import EditTools from './EditTools.jsx';
import Instruction from './Instruction.jsx';

const mapStateToProps = (state) =>
{
    return {locale: state.locale, access_token: state.access_token}
}


class BeforeVisit extends Component 
{

  constructor(props)
  {
    super(props);
    this.state =
    {
      showNewSymptom: false,
      showNewTest: false,
      showNewRecomendation: false,
      showEditTools: {item: '', idx: -1},
      action: '',
      editedItem: {}
    }
  }

  onNewTest = () =>
  {
    this.setState({showNewTest: true, action: 'create'})
  }

  onNewSymptom = () =>
  {
    this.setState({showNewSymptom: true, action: 'create'})
  }

  onNewRecomendation = () =>
  {
    this.setState({showNewRecomendation: true, action: 'create'})
  }

  onCreatedRecomendation = (note) =>
  {
    this.setState({showNewRecomendation: false});
    Remote.call('visit.beforevisit.addRecomendation',
      this.props.visit._id,
      note
    );
  }

  onEditInstruction = (instruction, idx) =>
  {
    this.setState(
      {
        showNewTest: true, 
        action: 'edit', 
        editedItem: {item: instruction, index: idx}
      }
    );
  }

  onDeleteInstruction = (instruction) =>
  {
    Remote.call('visit.beforevisit.removetest',
      this.props.visit._id,
      instruction
    );
  }

  onInstructionEdit = (instruction, idx) =>
  {
    Remote.call('visit.beforevisit.edittest',
      this.props.visit._id,
      instruction,
      idx
    );

    this.setState(
      {
        showNewTest: false,
        action: 'create',
        editedItem: {}
      }
    );
  }

  onNewInstruction = (instruction) =>
  {
    console.log(instruction);
    this.setState({showNewTest: false});

    Remote.call('visit.beforevisit.addTest',
      this.props.visit._id, 
      {
        Name: instruction.Name,
        Medication: instruction.Medication,
        Dosage: instruction.Dosage,
        Permanent: instruction.Permanent,
        Lab: instruction.Lab,
        Notes: instruction.Notes
      }
    );

    if (instruction.Permanent)
    {
      Remote.call('permanentmedication.add',
        {
          PatientId: this.props.patientId,
          Name: instruction.Name,
          Medication: instruction.Medication,
          Dosage: instruction.Dosage,
          Notes: instruction.Notes
        }
      );
    }

    // If this is a lab exam, and the lab exists in our system,
    // issue labTest order
    if (instruction.Lab !== undefined)
    {
      const myLab = Laboratories.findOne({Name: instruction.Lab});
      if (myLab !== undefined)
      {
        Remote.call('labtest.create',
          {
            PatientId: this.props.patientId,
            PhysicianId: this.props.physicianId,
            LabId: myLab._id,
            Name: instruction.Name,
            DateOfOrder: new Date(),
            Notes: [instruction.Notes],
            SpecialConsiderations: '',
            Status: 1
          }
        );
      }
    }
  }

  onNewNote = (note, type) =>
  {
    switch (type)
    {
      case 'Symptoms':
        // Add new symptom to db
        Remote.call('visit.beforevisit.addSymptom', this.props.visit._id, note);
        this.setState({showNewSymptom: false})
        break;
    }
  }

  onSymptomDelete = (symptom) =>
  {
    Remote.call('visit.beforevisit.removeSymptom',
      this.props.visit._id,
      symptom
    );
  }

  onSymptomEdit = (symptom, idx) =>
  {
    this.setState({showNewSymptom: true, action: 'edit', editedItem: {item: symptom, index: idx}});
  }

  onEditNote = (type, note, idx) =>
  {
    switch(type)
    {
      case 'Symptoms':
        Remote.call('visit.beforevisit.editSymptom',
          this.props.visit._id,
          note,
          idx
        );
        break;
      case 'Recomendations':
        Remote.call('visit.beforevisit.editRecomendation',
          this.props.visit._id,
          note,
          idx
        )
        break;
    }

    this.setState({showNewRecomendation: false, action: '', editedItem: {}})
  }

  onRecomendationDelete = (recomendation) =>
  {
    Remote.call('visit.beforevisit.removeRecomendation',
      this.props.visit._id,
      recomendation
    );
  }

  onRecomendationEdit = (recomendation, idx) =>
  {
    this.setState(
      {
        showNewRecomendation: true,
        action: 'edit',
        editedItem: {item: recomendation, index: idx}
      }
    );
  }

  getDosage = (dosage) =>
  {
    const regex = '[0-9]+';
    const regexRes = '[dwh]{1}';

    const num = dosage.match(regex);
    const res = dosage.match(regexRes)[0];

    return (
      {
        Ammount: parseInt(num),
        Resolution: res
      }
    )
  }

  issuePrescription = (instruction) =>
  {

    const prescription = 
    {
      Name: instruction.Medication,
      Dosage: this.getDosage(instruction.Dosage),
      Date: new Date(),
      Validity: 3,
      PatientId: this.props.patientId,
      PhysicianId: this.props.physicianId,
      Supplied: false,
      Notes: instruction.Notes
    }

    console.log(prescription);

    Remote.call('prescription.add',
      this.props.access_token,
      prescription
    );
  }

  render()
  {
    const editAllowed = (this.props.callingRole === 'physician');

    return(
      <div className='mr2 mb2'>
        <div className='bg-offwhite pa2 mt1 shadow-3 br1'>
          <div className='f-arial-20-nc c-gray mb1'>
            <FormattedMessage
              id='visits.symptoms'
              defaultMessage='Symptoms'
              description='Symptoms'
            />
          </div>
          <div className='flex flex-wrap'>
          {
            this.props.visit.BeforeVisitData.Symptoms.map(
              (s, idx) =>
              {
                if (!editAllowed)
                  return (
                    <div key={idx} className='ma1 bg-light-gray pa2 shadow-3 br1'>{s}</div>
                  );
                else
                {
                  return (
                    <div
                      key={idx}
                      className='flex'
                      onMouseEnter={
                        () => this.setState({showEditTools: {item: 'Symptoms', idx: idx}})
                      }
                      onMouseLeave={
                        () => this.setState({showEditTools: {item: '', idx: -1}})}
                    >
                      <div className='ma1 bg-light-gray pa2 shadow-3 br1'>
                        {s}
                      </div>
                      {
                        this.state.showEditTools.idx === idx &&
                        this.state.showEditTools.item === 'Symptoms' &&
                        <EditTools
                          item={{item: s, index: idx}}
                          className=''
                          onItemDelete={() => this.onSymptomDelete(s)}
                          onItemEdit={() => this.onSymptomEdit(s, idx)}
                        />
                      }
                    </div>
                  )
                }
              }
            )
          }
          </div>
          {
            editAllowed &&
            <div className='flex justify-end c-gray'>
              <div 
                className='grow pointer'
                onClick={this.onNewSymptom}
              >
                  &#8853;
              </div>
            </div>
          }
          {
            this.state.showNewSymptom &&
            <InputPopup
              label=
              {
                IntlMessages[this.props.locale].intl.formatMessage(
                  {
                    id: 'visits.symptom',
                    defaultMessage: 'Symptom'
                  }
                )
              }
              type='Notes'
              action={this.state.action}
              values={this.state.editedItem}
              onClose={() => this.setState({showNewSymptom: false, action: '', editedItem: {}})}
              onNewNotes={(note) => this.onNewNote(note, 'Symptoms')}
              onEditNotes={(symptom, idx) => this.onEditNote('Symptoms', symptom, idx)}
            />
          }
        </div>
        <div className='bg-offwhite pa2 mt1 shadow-3 br1'>
          <div className='f-arial-20-nc c-gray mb1'>
            <FormattedMessage
              id='visits.testsorderedbefore'
              defaultMessage='Tests ordered before visit'
              description='Tests ordered before visit'
            />
          </div>
          <div className='flex flex-column'>
          {
            this.props.visit.BeforeVisitData.Tests.map(
              (instruction, idx) =>
              {
                return(
                  <Instruction
                    idx={idx}
                    className='ma1 mb2'
                    instruction={instruction}
                    editingAllowed={editAllowed}
                    onIssuePrescription={this.issuePrescription}
                    onEditInstruction={this.onEditInstruction}
                    onDeleteInstruction={this.onDeleteInstruction}
                  />
                );            
              }
            )
          }
          </div>
          {
            this.props.callingRole === 'physician' &&
            <div className='flex justify-end c-gray'>
              <div 
                className='grow pointer'
                onClick={this.onNewTest}
              >
                  &#8853;
              </div>
            </div>
          }
          {
            this.state.showNewTest &&
            <InputPopup
              label=
              {
                IntlMessages[this.props.locale].intl.formatMessage(
                  {
                    id: 'visits.test',
                    defaultMessage: 'Test'
                  }
                )
              }
              type='Instruction'
              action={this.state.action}
              values={this.state.editedItem}
              onClose={() => this.setState({showNewTest: false})}
              onNewInstruction={this.onNewInstruction}
              onEditInstruction={this.onInstructionEdit}
            />
          }
        </div>
        <div className='bg-offwhite pa2 mt1 shadow-3 br1'>
          <div className='f-arial-20-nc c-gray mb1'>
            <FormattedMessage
              id='visits.recomendations'
              defaultMessage='Recomendations'
              description='Recomendations'
            />
          </div>
          <div className='flex flex-wrap'>
          {
            this.props.visit.BeforeVisitData.Recomendations.map(
              (s, idx) =>
              {
                if (!editAllowed)
                {
                  return (
                    <div key={idx} className='ma1 bg-light-gray pa2 shadow-3 br1'>{s}</div>
                  )
                }
                else
                {
                  return(
                    <div
                      key={idx}
                      className='flex'
                      onMouseEnter={() => this.setState({showEditTools: {item: 'Recomendations', idx: idx}})}
                      onMouseLeave={() => this.setState({showEditTools: {item: '', idx: -1}})}
                    >
                      <div className='ma1 bg-light-gray pa2 shadow-3 br1'>
                        {s}
                      </div>
                      {
                        this.state.showEditTools.idx === idx &&
                        this.state.showEditTools.item === 'Recomendations' &&
                        <EditTools
                          item={{item: s, index: idx}}
                          className=''
                          onItemDelete={() => this.onRecomendationDelete(s)}
                          onItemEdit={() => this.onRecomendationEdit(s, idx)}
                        />
                      }
                    </div>
                  )
                }
              }
            )
          }
          </div>
          {
            this.props.callingRole === 'physician' &&
            <div className='flex justify-end c-gray'>
              <div 
                className='grow pointer'
                onClick={this.onNewRecomendation}
              >
                  &#8853;
              </div>
            </div>
          }
          {
            this.state.showNewRecomendation &&
            <InputPopup
              label=
              {
                IntlMessages[this.props.locale].intl.formatMessage(
                  {
                    id: 'visits.recomendations',
                    defaultMessage: 'Recomendations'
                  }
                )
              }
              type='Notes'
              action={this.state.action}
              values={this.state.editedItem}
              onClose={() => this.setState({showNewRecomendation: false, action: '', editedItem: {}})}
              onNewNotes={(note) => this.onCreatedRecomendation(note)}
              onEditNotes={(recomendation, idx) => this.onEditNote('Recomendations', recomendation, idx)}
            />
          }

        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(BeforeVisit);