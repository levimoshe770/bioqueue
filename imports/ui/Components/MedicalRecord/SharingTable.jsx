import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';

/*
permissions
physicians
 */

export default class SharingTable extends Component
{
    getPermDisplayValue = (val) =>
    {
        if (val)
        {
            return(
            '🗸'
            )
        }
        else
        {
            return('');
        }
    }

    getPermClass = () =>
    {
        return('pa2 tc ba')
    }

    getPhysicianName = (physicianId) =>
    {
        const physician = this.props.physicians.find(
        (ph) => ph._id === physicianId
        );

        if (physician !== undefined)
        return(physician.Name);

        return('Uknown');
    }

    render()
    {
        return(
            <div
                className='absolute z-index-10 bg-gray c-offwhite shadow-3 pa2 ma2 br3'
                onClick={() => this.setState({showShared: false})}
            >
                <table className='ba'>
                    <thead>
                        <tr>
                            <th className='pa2 ba'>
                                <FormattedMessage
                                    id='register.name'
                                    defaultMessage='Name'
                                    description='Name'
                                />
                            </th>
                            <th className='pa2 ba'>
                                <FormattedMessage
                                    id='medicalrecord.visits'
                                    defaultMessage='Visits'
                                    description='Visits'
                                />
                            </th>
                            <th className='pa2 ba'>
                                <FormattedMessage
                                    id='medicalrecord.knownproblems'
                                    defaultMessage='Known Problems'
                                    description='Known Problems'
                                />
                            </th>
                            <th className='pa2 ba'>
                                <FormattedMessage
                                    id='medicalrecord.permanentmedication'
                                    defaultMessage='Permanent Medication'
                                    description='Permanent Medication'
                                />
                            </th>
                            <th className='pa2 ba'>
                                <FormattedMessage
                                    id='medicalrecord.alergies'
                                    defaultMessage='Alergies'
                                    description='Alergies'
                                />
                            </th>
                            <th className='pa2 ba'>
                                <FormattedMessage
                                    id='sharingtable.addvisits'
                                    defaultMessage='Add visits'
                                    description='Add visits'
                                />
                                
                            </th>
                            <th className='pa2 ba'>
                                <FormattedMessage
                                    id='sharingtable.ordertests'
                                    defaultMessage='Order tests'
                                    description='Order tests'
                                />
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.permissions.map(
                                (perm, idx) =>
                                {
                                    return(
                                        <tr
                                            key={idx}
                                        >
                                        <td className='pa2 ba'>
                                            {this.getPhysicianName(perm.PhysicianId)}
                                        </td>
                                        <td className={this.getPermClass()}>
                                            {
                                                this.getPermDisplayValue(perm.PermissionSet.visits)
                                            }
                                        </td>
                                        <td className={this.getPermClass()}>
                                            {
                                                this.getPermDisplayValue(perm.PermissionSet.kproblems)
                                            }
                                        </td>
                                        <td className={this.getPermClass()}>
                                            {
                                                this.getPermDisplayValue(perm.PermissionSet.medication)
                                            }
                                        </td>
                                        <td className={this.getPermClass()}>
                                            {
                                                this.getPermDisplayValue(perm.PermissionSet.alergies)
                                            }
                                        </td>
                                        <td className={this.getPermClass()}>
                                            {
                                                this.getPermDisplayValue(perm.PermissionSet.addvisits)
                                            }
                                        </td>
                                        <td className={this.getPermClass()}>
                                            {
                                                this.getPermDisplayValue(perm.PermissionSet.ordertests)
                                            }
                                        </td>
                                        </tr>
                                    )
                                }
                            )
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}