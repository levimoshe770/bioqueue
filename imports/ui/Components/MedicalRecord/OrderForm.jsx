import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import Dropdown from '../PhysicianProfile/Dropdown.jsx';

class OrderForm extends Component
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            pharmacyIdSelected: ''
        }
    }

    onSelectionChanged = (id, value) =>
    {
        const ph = this.props.pharmacies.find((p) => p.Name === value);

        console.log(ph);

        if (ph !== undefined)
        {
            this.setState({pharmacyIdSelected: ph._id});
        }
    }

    render()
    {
        console.log(this.props);
        return(
            <div className={'bg-light-gray br3 shadow-6 pa2 mt5 flex flex-column items-center ' + this.props.className}>
                <div className='flex items-center justify-between w-100 ml2 mr2'>
                    <div className=''>
                    
                    </div>
                    <div className='f-arial-30 bold'>
                        {this.props.prescription.Name}
                    </div>
                    <div
                        onClick={this.props.onClose}
                        className='close-nf mr1'
                    >
                        &#9746;
                    </div>
                </div>
                <div className='flex flex-column items-start f-arial-16-nc'>
                    <div className='f-arial-13'>
                        <FormattedMessage
                            id='registerformselector.pharmacy'
                            defaultMessage='Pharmacy'
                        />
                    </div>
                    <Dropdown
                        className=''
                        id='pharmacydd'
                        disabled={false}
                        required={false}
                        value={this.props.pharmacies.length > 0 ? this.props.pharmacies[0].Name : ''}
                        items={this.props.pharmacies.map((ph) => ph.Name)}
                        onSelectionChanged={this.onSelectionChanged}
                    />
                </div>
                <div className='flex justify-around items-center mt2'>
                    <button 
                        className='bg-buttonblue br-pill shadow-e pl3 pr3 pt2 pb2 mr1'
                        onClick=
                        {
                            () => this.props.onPlaceOrder(
                                    this.props.prescription, 
                                    this.state.pharmacyIdSelected === '' 
                                        ? this.props.pharmacies[0]._id 
                                        : this.state.pharmacyIdSelected
                            )
                        }
                    >
                        <FormattedMessage
                            id='prescription.placeorder'
                            defaultMessage='Place Order'
                        />
                    </button>
                    <button 
                        className='bg-buttonblue br-pill shadow-e pl3 pr3 pt2 pb2 ml1'
                        onClick={this.props.onClose}
                    >
                        <FormattedMessage
                            id='visits.cancel'
                            defaultMessage='Cancel'
                        />
                    </button>
                </div>
            </div>
        );
    }
}

export default OrderForm;
