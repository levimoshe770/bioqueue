import React, { Component } from 'react';

/*
item
onItemDelete
onItemEdit
className
 */

class EditTools extends Component
{
    render()
    {
        const { item, onItemDelete, onItemEdit, className } = this.props;

        return(
            <div className={'flex flex-column justify-between' + ' ' + className}>
                <div
                    className='pointer mt1'
                    onClick={() => onItemDelete(item)}
                >
                    ⮾
                </div>
                <div
                    className='pointer mb1'
                    onClick={() => onItemEdit(item)}
                >
                    <img
                        src='icons/icons8-pencil-48.png'
                        alt='edit'
                        width='15'
                    />
                </div>
            </div>
        )
    }
}

export default EditTools;