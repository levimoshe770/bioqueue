import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import Laboratories from '../../../api/Lists/Laboratories/Laboratories';
import Dropdown from '../PhysicianProfile/Dropdown.jsx';
import Remote from '../../Remote';

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}

/*
label
type: QA, SingleItem, Notes, Instruction
labs: Array of {labName, LabId}
action: 'edit', 'create'
values: Default values for edit

onClose
onNewQa
onNewNotes
onNewInstruction
onEditQa
onEditNotes
onEditInstruction
 */

class InputPopup extends Component
{
  constructor(props)
  {
    super(props);
    this.state =
    {
      question: '',
      answer: '',
      note: '',
      instructionMedical: 
      {
        Name: '',
        Medication: '',
        Dosage: '',
        Permanent: false,
        Notes: ''
      },
      instructionLaboratory:
      {
        Name: '',
        Lab: '',
        Notes: ''
      },
      instructionType: 'Medication'
    }
  }

  cols = 55

  onSubmitClick = () =>
  {
    const { type, action, values } = this.props;
    const { question, answer, note, instructionMedical, instructionLaboratory, instructionType } = this.state;

    switch (type)
    {
      case 'QA':
        if (action === 'create')
          this.props.onNewQa(question, answer);
        else
          this.props.onEditQa({Q: question, A: answer}, values.index)
        break;
      case 'Notes':
        if (action === 'create')
          this.props.onNewNotes(note);
        else
          this.props.onEditNotes(note, values.index);
        break;
      case 'Instruction':
        if (action === 'create')
        {
          this.props.onNewInstruction(
            instructionType === 'Medication'
            ? instructionMedical
            : instructionLaboratory
          );
        }
        else
        {
          this.props.onEditInstruction(
            instructionType === 'Medication'
            ? instructionMedical
            : instructionLaboratory,
            values.index
          );
        }
        break;
      default:
        return;
    }
    
  }

  onLabSelected = (id, lab) =>
  {
    let instruction = this.state.instructionLaboratory;
    instruction.Lab = lab;
    this.setState({instructionLaboratory: instruction}) 
  }

  getLabNames = () =>
  {
    if (this.props.labs.length === 0)
      return [];

    return this.props.labs.map(
      (lab) => lab.Name
    )
  }

  renderQA = () =>
  {
    const { action, values } = this.props;
    const { question, answer } = this.state;

    return(
      <div>
        <div className='ma2'>
          <div>
            <FormattedMessage
              id='visits.question'
              defaultMessage='Question'
              description='Question'
            />
          </div>
          <textarea 
            id='question' 
            rows='4' 
            cols={this.cols}
            defaultValue={this.state.question}
            onChange = {(e) => this.setState({question: e.target.value})}
          />
        </div>
        <div className='ma2'>
          <div>
            <FormattedMessage
              id='visits.answer'
              defaultMessage='Answer'
              description='Answer'
            />
          </div>
          <textarea 
            id='question' 
            rows='4' 
            cols={this.cols}
            defaultValue={this.state.answer}
            onChange = {(e) => this.setState({answer: e.target.value})}
          />
        </div>
        <div className='flex justify-around z-index-100'>
          <button
            className='mb2 ml2 pb2 pt2 pl3 pr3 bg-blue c-white shadow-3 br-pill pointer'
            onClick={this.onSubmitClick}
          >
            <FormattedMessage
              id='visits.submit'
              defaultMessage='Submit'
              description='Submit'
            />
          </button>
          <button
            className='mb2 ml2 pb2 pt2 pl3 pr3 bg-blue c-white shadow-3 br-pill pointer'
            onClick={this.props.onClose}
          >
            <FormattedMessage
              id='visits.cancel'
              defaultMessage='Cancel'
              description='Cancel'
            />
          </button>
        </div>
      </div>
    )

  }

  renderNotes = () =>
  {
    return(
      <div>
        <div className='ma2'>
          <div>{this.props.label}</div>
          {
            this.props.action === 'create'
            ?
            <textarea 
              id='note' 
              rows='4' 
              cols={this.cols}
              onChange = {(e) => this.setState({note: e.target.value})}
              onKeyPress = 
              {
                (e) => 
                {
                  if (e.key === 'Enter')
                    this.onSubmitClick();
                }
              }
            />
            :
            <textarea 
              id='note' 
              rows='4' 
              cols={this.cols}
              onChange = {(e) => this.setState({note: e.target.value})}
              onKeyPress = 
              {
                (e) => 
                {
                  if (e.key === 'Enter')
                    this.onSubmitClick();
                }
              }
            >
              {this.props.values.item}
            </textarea>
          }
        </div>
        <div className='flex justify-around'>
          <button
            className='mb2 ml2 pb2 pt2 pl3 pr3 bg-blue c-white shadow-3 br-pill pointer'
            onClick={this.onSubmitClick}
          >
            <FormattedMessage
              id='visits.submit'
              defaultMessage='Submit'
              description='Submit'
            />
          </button>
          <button
            className='mb2 ml2 pb2 pt2 pl3 pr3 bg-blue c-white shadow-3 br-pill pointer'
            onClick={this.props.onClose}
          >
            <FormattedMessage
              id='visits.cancel'
              defaultMessage='Cancel'
              description='Cancel'
            />
          </button>
        </div>
      </div>
    )
  }
  
  renderMedicationInstruction = () =>
  {
    const { instructionMedical } = this.state;
    
    return(
      <div>
        <div className='flex flex-column mb2 mr2 ml2'>
          <label 
            htmlFor='medname'
            className='f-arial-13'
          >
            <FormattedMessage
              id='register.name'
              defaultMessage='Name'
              description='Name'
            />
          </label>
          <input 
            id='medname'
            type='text' 
            value={instructionMedical.Name}
            onChange={
              (e) => 
              { 
                let instruction = this.state.instructionMedical;
                instruction.Name = e.target.value;
                this.setState({instructionMedical: instruction}) 
              }
            }
          />
        </div>
        <div className='flex flex-column mb2 mr2 ml2'>
          <label 
            htmlFor='medmedication'
            className='f-arial-13'
          >
            <FormattedMessage
              id='visits.medication'
              defaultMessage='Medication'
              description='Medication'
            />
          </label>
          <input 
            id='medmedication'
            type='text' 
            value={instructionMedical.Medication}
            onChange={
              (e) => 
              { 
                let instruction = this.state.instructionMedical;
                instruction.Medication = e.target.value;
                this.setState({instructionMedical: instruction}) 
              }
            }
          />
        </div>
        <div className='flex flex-column mb2 mr2 ml2'>
          <label
            htmlFor='meddosage'
            className='f-arial-13'
          >
            <FormattedMessage
              id='visits.dosage'
              defaultMessage='Dosage'
              description='Dosage'
            />
          </label>
          <input 
            id='meddosage'
            type='text' 
            value={instructionMedical.Dosage}
            onChange={
              (e) => 
              { 
                let instruction = this.state.instructionMedical;
                instruction.Dosage = e.target.value;
                this.setState({instructionMedical: instruction}) 
              }
            }
          />
        </div>
        <div className='flex items-center mb2 mr2 ml2'>
          <input
            id='permanent'
            type='checkbox'
            className='mr2'
            onChange={
              (e) =>
              {
                let instruction = this.state.instructionMedical;
                instruction.Permanent = e.target.checked;
                this.setState({instructionMedical: instruction})
              }
            }
          />
          <label 
            className='f-arial-13'
            htmlFor='permentent'
          >
            <FormattedMessage
              id='visits.permanentmedication'
              defaultMessage='Permanent medication'
              description='Permanent medication'
            />
          </label>
        </div>
        <div className='flex flex-column mb2 mr2 ml2'>
          <label
            htmlFor='mednote'
            className='f-arial-13'
          >
            <FormattedMessage
              id='visits.notes'
              defaultMessage='Notes'
              description='Notes'
            />
          </label>
          <textarea 
            id='mednote' 
            rows='4' 
            cols={this.cols}
            onChange = 
            {
              (e) =>
              {
                let instruction = this.state.instructionMedical;
                instruction.Notes = e.target.value;
                this.setState({instructionMedical: instruction}) 
              }
            }
          >
            {instructionMedical.Notes}
          </textarea>
        </div>
      </div>
    )
  }

  renderLabTestInstruction = () =>
  {
    const { instructionLaboratory } = this.state;
    return(
      <div>
        <div className='flex flex-column mb2 mr2 ml2'>
          <label
            htmlFor='labname'
            className='f-arial-13'
          >
            <FormattedMessage
              id='register.name'
              defaultMessage='Name'
              description='Name'
            />
          </label>
          <input 
            id='labname'
            type='text' 
            value={instructionLaboratory.Name}
            onChange={
              (e) => 
              { 
                let instruction = this.state.instructionLaboratory;
                instruction.Name = e.target.value;
                this.setState({instructionLaboratory: instruction}) 
              }
            }
          />
        </div>
        <div className='flex flex-column mb2 mr2 ml2'>
          <label
            htmlFor='lablab'
            className='f-arial-13'
          >
            <FormattedMessage
              id='visits.laboratory'
              defaultMessage='Laboratory'
              description='Laboratory'
            />
          </label>
          <Dropdown
            className=''
            id='lablab'
            disabled={false}
            required={false}
            border={true}
            items={this.getLabNames()}
            value={instructionLaboratory.Lab}
            onSelectionChanged={this.onLabSelected}
          />
        </div>
        <div className='flex flex-column mb2 mr2 ml2'>
          <label
            htmlFor='labnote'
            className='f-arial-13'
          >
            <FormattedMessage
              id='visits.notes'
              defaultMessage='Notes'
              description='Notes'
            />
          </label>
          <textarea 
            id='labnote' 
            rows='4' 
            cols={this.cols}
            onChange = {
              (e) => 
              {
                let instruction = this.state.instructionLaboratory;
                instruction.Notes = e.target.value;
                this.setState({instructionLaboratory: instruction}) 
              }
            }
          >
            {instructionLaboratory.Notes}
          </textarea>
        </div>
      </div>
    )
  }

  renderInstruction()
  {
    return(
      <div>
        <div className='pa2 flex items-center justify-center bb b--light-silver mb2'>
          <div className='mr3 flex items-center'>
            <label className='pr1' htmlFor='medication'>
              <FormattedMessage
                id='visits.medication'
                defaultMessage='Medication'
                description='Medication'
              />
            </label>
            <input 
              type='radio' 
              id='medication' 
              name='type'
              value='Medication'
              checked={this.state.instructionType === 'Medication'}
              onChange={() => this.setState({instructionType: 'Medication'})}
            />
          </div>
          <div className='mr2 flex items-center'>
            <label  className='pr1' htmlFor='labtest'>
              <FormattedMessage
                id='visits.labtest'
                defaultMessage='Lab test'
                description='Lab test'
              />
            </label>
            <input 
              type='radio' 
              id='labtest' 
              name='type'
              value='LabTest'
              checked={this.state.instructionType === 'LabTest'}
              onChange={() => this.setState({instructionType: 'LabTest'})}
            />
          </div>
        </div>
        <div>
          {
            this.state.instructionType === 'Medication'
            ? this.renderMedicationInstruction()
            : this.renderLabTestInstruction()
          }
        </div>
        <div className='flex justify-around'>
          <button
            className='mb2 ml2 pb2 pt2 pl3 pr3 bg-blue c-white shadow-3 br-pill pointer'
            onClick={this.onSubmitClick}
          >
            <FormattedMessage 
              id='visits.submit'
              defaultMessage='Submit'
              description='Submit'
            />
          </button>
          <button
            className='mb2 ml2 pb2 pt2 pl3 pr3 bg-blue c-white shadow-3 br-pill pointer'
            onClick={this.props.onClose}
          >
            <FormattedMessage
              id='visits.cancel'
              defaultMessage='Cancel'
              description='Cancel'
            />
          </button>
        </div>
      </div>
    );
  }

  renderForm = () =>
  {
    switch (this.props.type)
    {
      case 'QA':
        return(this.renderQA());
      case 'Notes':
        return(this.renderNotes());
      case 'Instruction':
        return(this.renderInstruction());
      default:
        return('');
    }
  }

  componentDidMount()
  {
    const { action, values, type } = this.props;

    if (action === 'edit')
    {
      switch (type)
      {
        case 'QA':
          this.setState(
            {
              question: values.item.Q,
              answer: values.item.A
            }
          )
          break;
        case 'Notes':
          break;
        case 'Instruction':
          this.setInstruction(values.item);
          break;
      }
    }
  }

  setInstruction = (instruction) =>
  {
    const instType = instruction.Medication !== undefined ?
          'Medication' : 'LabTest';
    
    switch (instType) 
    {
      case 'Medication':
        this.setState(
          {
            instructionMedical:
            {
              Name: instruction.Name,
              Medication: instruction.Medication,
              Dosage: instruction.Dosage,
              Permanent: false,
              Notes: instruction.Notes
            },
            instructionType: instType
          }
        )
        break;
      case 'LabTest':
        this.setState(
          {
            instructionLaboratory:
            {
              Name: instruction.Name,
              Lab: instruction.Lab,
              Notes: instruction.Notes
            },
            instructionType: instType
          }
        )
      default:
        break;
    }
  }

  render()
  {
    return(
      <div className='absolute bg-light-gray br3 shadow-6'>
        {this.renderForm()}
      </div>
    )
  }
}

export default 
connect(mapStateToProps)(
  withTracker(
    () =>
    {
      const labHandle = Remote.subscribe('laboratories.admin');
      const labLoading = !labHandle.ready();

      return(
        {
          labs: !labLoading ? Laboratories.find({}).fetch() : []
        }
      )
    }
  )
  (InputPopup)
);