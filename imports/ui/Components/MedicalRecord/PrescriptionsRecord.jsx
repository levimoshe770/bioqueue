import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import Prescriptions from '../../../api/Lists/Prescriptions/Prescriptions';
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';
import Remote from '../../Remote';
import EmptyData from './EmptyData';
import OrderForm from './OrderForm.jsx';
import Pharmacies from '../../../api/Lists/Pharmacies/Pharmacies';

const mapStateToProps = (state) =>
{
    return {locale: state.locale, access_token: state.access_token}
}

class PrescriptionsRecord extends Component
{
    constructor(props)
    {
        super(props);
        this.state =
        {
            showOrderBox: false,
            prescription: undefined
        }
    }

    getResolutionString = (resolution) =>
    {
        const { locale } = this.props;

        switch (resolution)
        {
            case 'h':
                return IntlMessages[locale].intl.formatMessage(
                    {
                        id: 'prescription.hour',
                        defaultMessage: 'hour'
                    }
                );
            case 'd':
                return IntlMessages[locale].intl.formatMessage(
                    {
                        id: 'prescription.day',
                        defaultMessage: 'day'
                    }
                );
            case 'w':
                return IntlMessages[locale].intl.formatMessage(
                    {
                        id: 'prescription.week',
                        defaultMessage: 'week'
                    }
                );
                        
        }
    }

    getDosage = (dosage) =>
    {
        const { locale } = this.props;

        return (
            dosage.Ammount.toString() + ' ' + 
            IntlMessages[locale].intl.formatMessage(
                {
                    id: 'prescription.per',
                    defaultMessage: 'per'
                }
            ) + ' ' +
            this.getResolutionString(dosage.Resolution)
        );
    }

    onOrderClicked = (prescription) =>
    {
        this.setState(
            {
                showOrderBox: true,
                prescription: prescription
            }
        )
    }

    onPlaceOrder = (prescription, pharmacyId) =>
    {
        Remote.call('prescription.assignPharmacy',
            this.props.access_token,
            prescription,
            pharmacyId,
            (err, res) =>
            {
                if (err) 
                    console.log(err);
            }
        );

        this.setState({showOrderBox: false});
    }

    getLastValidDate = (prescription) =>
    {
        const { locale } = this.props;

        const validUntil = prescription.Date;

        validUntil.setMonth(validUntil.getMonth() + prescription.Validity);

        const lastDate = validUntil.toLocaleDateString(locale, {day: 'numeric', month: 'short', year: 'numeric'});

        return lastDate;
    }

    render()
    {
        console.log(this.props);

        const { prescriptions, locale } = this.props;

        return(
            <div>
                {
                    prescriptions.length === 0 
                    ?
                    <EmptyData 
                        Message={
                            IntlMessages[locale].intl.formatMessage(
                                {
                                    id: 'physiciangreeting.loading',
                                    defaultMessage: 'Loading'
                                }
                            ) + '...'
                        }
                    />
                    :
                    <div className='bg-light-gray shadow-6 pa2 br3 ma2'>
                        {
                            this.state.showOrderBox &&
                            <OrderForm
                                className='absolute'
                                prescription={this.state.prescription}
                                pharmacies={this.props.pharmacies}
                                onClose={() => this.setState({showOrderBox: false, prescription: undefined})}
                                onPlaceOrder={this.onPlaceOrder}
                            />
                        }
                        <div className='bold f-arial-20-nc tc pb2 mb2 bb b--light-silver'>
                            <FormattedMessage
                                id='pharmacy.prescriptions'
                                defaultMessage='Prescriptions'
                                description='Prescriptions'
                            />
                        </div>
                        <div className='mh-500 scroll-y'>
                        {
                            <table>
                                <thead>
                                    <tr>
                                        <th>
                                            <FormattedMessage
                                                id='doctorlist.name'
                                                defaultMessage='Name'
                                                description='Name'
                                            />
                                        </th>
                                        <th>
                                            <FormattedMessage
                                                id='pharmacy.genericname'
                                                defaultMessage='Generic Name'
                                            />
                                        </th>
                                        <th>
                                            <FormattedMessage
                                                id='visits.dosage'
                                                defaultMessage='Dosage'
                                            />
                                        </th>
                                        <th>
                                            <FormattedMessage
                                                id='prescription.validuntil'
                                                defaultMessage='Valid Until'
                                            />
                                        </th>
                                        <th>
                                            <FormattedMessage
                                                id='physicianstate.status'
                                                defaultMessage='Status'
                                            />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        prescriptions.map(
                                            (prescription, idx) =>
                                            {
                                                return(
                                                <tr key={idx}>
                                                    <td>{prescription.Name}</td>
                                                    <td>{prescription.GenericName}</td>
                                                    <td>{this.getDosage(prescription.Dosage)}</td>
                                                    <td>{this.getLastValidDate(prescription)}</td>
                                                    <td>
                                                        {
                                                            prescription.Supplied 
                                                                ? 
                                                                <FormattedMessage
                                                                    id='pharmacy.supplied'
                                                                    defaultMessage='Supplied'
                                                                    description='Supplied'
                                                                />
                                                                : 
                                                                <FormattedMessage
                                                                    id='pharmacy.notsupplied'
                                                                    defaultMessage='Not Supplied'
                                                                    description='Not Supplied'
                                                                />
                                                        }
                                                    </td>
                                                    { 
                                                        this.props.role === 'patient' &&
                                                        <td>
                                                            <button
                                                                className='bg-buttonblue c-white br-pill pl3 pr3 pb2 pt2 shadow-3'
                                                                onClick={() => this.onOrderClicked(prescription)}
                                                                disabled={prescription.Supplied || (prescription.Date.setMonth(prescription.Date.getMonth() + prescription.Validity) < new Date())}
                                                            >
                                                                <FormattedMessage
                                                                    id='pharmacy.order'
                                                                    defaultMessage='Order'
                                                                    description='Order'
                                                                />
                                                            </button>
                                                        </td>
                                                    }
                                                </tr>
                                                );
                                            }
                                        )
                                    }
                                </tbody>
                            </table>
                        }
                        </div>
                    </div>
                }
            </div>
        )
    }
}

export default connect(mapStateToProps)(
    withTracker(
        ({access_token, role, patientId}) =>
        {
            if (role === 'patient')
            {
                const prescriptionHandler = Remote.subscribe('prescriptions.patient', access_token);
                const prescriptionLoading = !prescriptionHandler.ready();

                const pharmaciesHandler = Remote.subscribe('pharmacies.admin');
                const pharmaciesLoading = !pharmaciesHandler.ready();

                return(
                    {
                        prescriptions: !prescriptionLoading ? Prescriptions.find({},{sort: {Date: -1}}).fetch() : [],
                        pharmacies: !pharmaciesLoading ? Pharmacies.find({}).fetch() : []
                    }
                );
            }
            else if (role === 'physician')
            {
                const phPrescriptionHandler = Remote.subscribe('prescriptions.physician', access_token, patientId);
                const phPrescriptionLoading = !phPrescriptionHandler.ready();

                return(
                    {
                        prescriptions: !phPrescriptionLoading ? Prescriptions.find({},{sort: {Date: -1}}).fetch() : [],
                        pharmacies: []
                    }
                )
            }
        }
    )
    (PrescriptionsRecord)
);