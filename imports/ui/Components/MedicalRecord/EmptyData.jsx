import React from 'react'

/*
Message
 */

const EmptyData = (props) =>
{
    return(
        <div className='f-arial-30 tc ma5'>
            {props.Message}
        </div>
    )
}

export default EmptyData;