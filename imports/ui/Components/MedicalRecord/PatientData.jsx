import React, {Component} from 'react';
import moment from 'moment';
import EmptyData from './EmptyData.jsx';
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';
import { genderStr } from '../../../api/Lists/GeneralTypes/Gender';

/*
patient

 */

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}

class PatientData extends Component
{
  getGenderString = (gender) =>
  {
    return IntlMessages[this.props.locale].intl.formatMessage(
      {
        id: genderStr[gender],
        defaultMessage: 'Gender'
      }
    );
  }

  onSendMessage = () =>
  {
    this.props.onChatBoxOpen();
  }

  render()
  {
    //console.log(this.props);
    return(
      this.props.patient === undefined 
      ?
        <EmptyData Message='Loading...' />
      :
      <div className='flex items-start'>
        <div className='flex flex-column items-center br b--light-gray pl3 pr3'>
          <img className='br-100 pb3' alt={this.props.patient.Name} src={this.props.patient.Avatar} width='150' height='150'/>
          <div className='pb3 f-arial-30 bold tc'>{this.props.patient.Name}</div>
          <div className='pb3 f-arial-13 c-light-gray'>{this.props.patient.Email}</div>
          {
            this.props.role === 'physician' &&
            <button
              className='bg-buttonblue pl3 pr3 pt2 pb2 br3 shadow-3 pointer'
              onClick={this.onSendMessage}
            >
              <FormattedMessage
                id='patientdata.sendmessage'
                defaultMessage='Send Message'
                description='Send Message'
              />
            </button>
          }
        </div>
        <div className='flex flex-wrap'>
          <div className='flex flex-column items-start pa3 ma3'>
            <div className='f-arial-13 c-light-gray pb2'>
              <FormattedMessage
                id='patientdata.gender'
                defaultMessage='Gender'
                description='Gender'
              />
            </div>
            <div>{this.getGenderString(this.props.patient.Gender)}</div>
          </div>
          <div className='flex flex-column items-start pa3 ma3'>
            <div className='f-arial-13 c-light-gray pb2'>
              <FormattedMessage
                id='patientdata.birthday'
                defaultMessage='Birthday'
                description='Birthday'
              />
            </div>
            <div>{moment(this.props.patient.DOB).format('DD-MM-yyyy')}</div>
          </div>
          <div className='flex flex-column items-start pa3 ma3'>
            <div className='f-arial-13 c-light-gray pb2'>
              <FormattedMessage
                id='userdata.address'
                defaultMessage='Address'
                description='Address'
              />
            </div>
            <div>{this.props.patient.Address}</div>
          </div>
          <div className='flex flex-column items-start pa3 ma3'>
            <div className='f-arial-13 c-light-gray pb2'>
              <FormattedMessage
                id='patientdata.phone'
                defaultMessage='Phone'
                description='Phone'
              />
            </div>
            <div>{this.props.patient.Phone}</div>
          </div>
          <div className='flex flex-column items-start pa3 ma3'>
            <div className='f-arial-13 c-light-gray pb2'>
              <FormattedMessage
                id='patientdata.bloodtype'
                defaultMessage='Blood Type'
                description='Blood Type'
              />
            </div>
            <div>{this.props.patient.bloodType}</div>
          </div>
          <div className='flex flex-column items-start pa3 ma3'>
            <div className='f-arial-13 c-light-gray pb2'>
              <FormattedMessage
                id='patientdata.weight'
                defaultMessage='Weight'
                description='Weight'
              />
            </div>
            <div>
              {
                this.props.patient.weight !== undefined 
                ?
                this.props.patient.weight.amount.toString() + this.props.patient.weight.units
                :
                ''
              }
            </div>
          </div>
          <div className='flex flex-column items-start pa3 ma3'>
            <div className='f-arial-13 c-light-gray pb2'>
              <FormattedMessage
                id='patientdata.height'
                defaultMessage='Height'
                description='Height'
              />
            </div>
            <div>
              {
                this.props.patient.height !== undefined
                ?
                this.props.patient.height.amount.toString() + this.props.patient.height.units
                :
                ''
              }
            </div>
          </div>
          <div className='flex flex-column items-start pa3 ma3'>
            <div className='f-arial-13 c-light-gray pb2'>
              <FormattedMessage
                id='patientdata.sugar'
                defaultMessage='Sugar'
                description='Sugar'
              />
            </div>
            <div>
              {
                this.props.patient.suger !== undefined 
                ?
                this.props.patient.sugar.amount.toString() + this.props.patient.sugar.units
                :
                ''
              }
            </div>
          </div>
          <div className='flex flex-column items-start pa3 ma3'>
            <div className='f-arial-13 c-light-gray pb2'>
              <FormattedMessage
                id='patientdata.bloodpressure'
                defaultMessage='Blood Pressure'
                description='Blood Pressure'
              />
            </div>
            <div>
              {
                this.props.patient.bloodPressure !== undefined
                ?
                this.props.patient.bloodPressure.systolic.toString() + '/' +
                this.props.patient.bloodPressure.diastolic.toString()
                :
                ''
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps)(PatientData);