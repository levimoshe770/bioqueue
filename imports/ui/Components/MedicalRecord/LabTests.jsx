import React, {Component} from 'react';
import LabResult from './LabResult.jsx';
import EmptyData from './EmptyData.jsx';
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';
import { withTracker } from 'meteor/react-meteor-data';
import Laboratories from '../../../api/Lists/Laboratories/Laboratories';
import Remote from '../../Remote';

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}

/* 
labTests
 */

class LabTests extends Component
{

  constructor(props)
  {
    super(props);
    this.state = {labSelected: 0}
  }

  onLabSelected = (idx) =>
  {
    this.setState({labSelected: idx});
  }

  getLabName = (labId) =>
  {
    const lab = Laboratories.findOne({_id: labId});
    
    return lab !== undefined ? lab.Name : '';
  }

  render()
  {
    //console.log(this.props.labTests);
    return(
      this.props.labTests.length === 0
      ?
        <EmptyData 
          Message=
          {
            IntlMessages[this.props.locale].intl.formatMessage(
              {
                id: 'visits.notestsfound',
                defaultMessage: 'No tests found'
              }
            )
          }
        />
      :
      <div className='w-100'>
        <div className='flex'>
          <div className='ma2 br b--light-gray pr2'>
            {
              this.props.labTests.map(
                (lt, idx) =>
                {
                  return(
                    <div 
                      className={'flex items-center pa2 mb2 shadow-3 br3 pointer grow ' +
                                    (idx === this.state.labSelected 
                                        ? 'bg-gray c-offwhite' 
                                        : 'bg-light-gray')}
                      onClick={() => this.onLabSelected(idx)}
                      key={idx}
                    >
                      <div className='mr2 bold f-arial-20-nc'>
                        {
                          lt.DateOfPerformance !== undefined
                          ?
                          lt.DateOfPerformance.toLocaleDateString(
                            this.props.locale,
                            {
                              day: 'numeric',
                              month: 'short',
                              year: 'numeric'
                            }
                          )
                          :
                          lt.DateOfOrder.toLocaleDateString(
                            this.props.locale,
                            {
                              day: 'numeric',
                              month: 'short',
                              year: 'numeric'
                            }
                          )
                        }
                      </div>
                      <div className='f-arial-13-nc'>{lt.Name}</div>
                    </div>
                  )
                }
              )
            }
          </div>
          <div className='bg-light-gray shadow-6 br2 mr2'>
            <div className='pa3 tc mr1 bb b--gray'>
              <div className='f-arial-20-nc bold'>{this.props.labTests[this.state.labSelected].Name}</div>
            </div>
            <div className='flex flex-wrap bb b--gray'>
              <div className='pa1 ma2'>
                <div className='f-arial-13'>
                  <FormattedMessage
                    id='labtests.dateoforder'
                    defaultMessage='Date of order'
                    description='Date of order'
                  />
                </div>
                <div>
                  {
                    this.props.labTests[this.state.labSelected].DateOfOrder.toLocaleDateString(
                      this.props.locale,
                      {
                        day: 'numeric',
                        month: 'short',
                        year: 'numeric'
                      }
                    )
                  }
                </div>
              </div>
              <div className='pa1 ma2'>
                <div className='f-arial-13'>
                  <FormattedMessage
                    id='labtests.dateofperformance'
                    defaultMessage='Date of performance'
                    description='Date of performance'
                  />
                </div>
                <div>
                  {
                    this.props.labTests[this.state.labSelected].DateOfPerformance !== undefined
                    ?
                    this.props.labTests[this.state.labSelected].DateOfPerformance.toLocaleDateString(
                      this.props.locale,
                      {
                        day: 'numeric',
                        month: 'short',
                        year: 'numeric'
                      }
                    )
                    :
                    ''
                  }
                </div>
              </div>
              <div className='pa1 ma2'>
                <div className='f-arial-13'>
                  <FormattedMessage
                    id='labtests.dateofresults'
                    defaultMessage='Date of results'
                    description='Date of results'
                  />
                </div>
                <div>
                  {
                    this.props.labTests[this.state.labSelected].DateOfResults !== undefined 
                    ?
                    this.props.labTests[this.state.labSelected].DateOfResults.toLocaleDateString(
                      this.props.locale,
                      {
                        day: 'numeric',
                        month: 'short',
                        year: 'numeric'
                      }
                    )
                    :
                    ''
                  }
                </div>
              </div>
              <div className='pa1 ma2 mw-400'>
                <div className='f-arial-13'>
                  <FormattedMessage
                    id='labtests.specialconsiderations'
                    defaultMessage='Special Considerations'
                    description='Special Considerations'
                  />
                </div>
                <div>
                  {this.props.labTests[this.state.labSelected].SpecialConsiderations}
                </div>
              </div>
              <div className='pa1 ma2 mw-400'>
                <div className='f-arial-13'>
                  <FormattedMessage
                    id='visits.laboratory'
                    defaultMessage='Laboratory'
                    description='Laboratory'
                  />
                </div>
                <div>
                  {
                    this.getLabName(this.props.labTests[this.state.labSelected].LabId)
                  }
                </div>
              </div>
              <div className='pa1 ma2 mw-400'>
                <div className='f-arial-13'>
                  <FormattedMessage
                    id='labtests.approximateduration'
                    defaultMessage='Approximate Duration'
                    description='Approximate Duration'
                  />
                </div>
                <div>
                  {this.props.labTests[this.state.labSelected].ApproximateDuration}
                </div>
              </div>
            </div>
            <div className='mt2 ml2 mb3 mh-640 scroll-y'>
              {
                this.props.labTests[this.state.labSelected].Results === undefined
                ? ''
                :
                this.props.labTests[this.state.labSelected].Results.map(
                  (result, idx) =>
                  {
                    return(
                      <LabResult 
                        key={idx}
                        name={result.Name}
                        type={result.Type}
                        result={result.Result}
                        min={result.Min}
                        max={result.Max}
                      />
                    )
                  }
                )
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default 
connect(mapStateToProps)(
  withTracker(
    () =>
    {
      const labHandler = Remote.subscribe('laboratories.admin');
      const labLoading = !labHandler.ready();

      return(
        {
          labs: !labLoading ? Laboratories.find({}).fetch() : []
        }
      )
    }
  )
  (LabTests)
);