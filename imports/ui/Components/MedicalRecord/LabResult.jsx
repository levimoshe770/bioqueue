import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Numeric, Image, Video, Voice, Text } from '../../../api/Lists/LabTests/LabTestsCollection';
/*
name: String
type: 'Numeric','Image'
result: Number or Link
min
max
 */

const renderImageResult = (props) =>
{
  const { result } = props;

  return(
    <div className=''>
      <a href={result} target='_blank' >
        <FormattedMessage
          id='visits.presstoseeimage'
          defaultMessage='Press to see image'
          description='Press to see image'
        />
      </a>
    </div>
  )
}

const renderTextResult = (props) =>
{
  const { result } = props;
  return(
    <div>
      {result}
    </div>
  )
}

const renderNumericResult = (props) =>
{
  const { result, min, max } = props;
  const numOfDots = 50;

  const pct = Math.floor(((result-min)/(max-min))*numOfDots);

  // console.log('res',result,'min',min,'max',max,'pct',pct);

  let i;
  const dots = [];
  if (result < min)
    dots.push('*');

  dots.push('(');
  for (i=0; i<numOfDots; i++)
  {
    dots.push(i===pct ? '*' : '.');
  }
  dots.push(')');
  if (result > max)
    dots.push('*');

  return(
    <div className='flex items-center'>
      <div className={'w3 ' + ((result>max || result<min) ? 'c-red' : '')}>{result}</div>
      <div className='w2'>{min}</div>
      <div className='flex w5'>
        {
          dots.map(
            (dot, idx) =>
            {
              return(
                <div key={idx}>{dot}</div>
              )
            }
          )
        }
      </div>
      <div>{max}</div>
    </div>
  )
}

const renderResult = (props) =>
{
  switch(props.type)
  {
    case Numeric:
      return renderNumericResult(props);
    case Image:
      return renderImageResult(props);
    case Text:
      return renderTextResult(props);
  }
}

const LabResult = (props) =>
{
  return(
    <div className='flex items-center mb1' key={props.key}>
      <div className='w4'>{props.name}</div>
      {renderResult(props)}
    </div>
  )
}

export default LabResult;