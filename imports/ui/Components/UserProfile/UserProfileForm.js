import React from 'react';

/*
className
userData: Data profile from DB
userName: Redux user data
onClose
 */

const UserProfileForm = (props) =>
{
    return(
        <div className={props.className + " ba bg-lightblue pa2 w-80"} >
        {
            props.userData === undefined 
            ?   <div>Loading...</div>
            :
                <div>
                    <div className="flex items-center justify-between mt2">
                        <div>Name</div>
                        <div>{props.userData.Name}</div>
                        <button>✐</button>
                    </div>
                    <div className="flex items-center justify-between mt2">
                        <div>Email</div>
                        <div>{props.userData.Email}</div>
                        <button>✐</button>
                    </div>
                    <div className="flex items-center justify-between mt2">
                        <div>Phone</div>
                        <div>{props.userData.Phone}</div>
                        <button>✐</button>
                    </div>
                    <div className="flex items-center justify-between mt2 mb2">
                        <div>Role</div>
                        <div>{props.userName.Role}</div>
                    </div>
                </div>
        }
        </div>
    );
}

export default UserProfileForm;