import React, { Component } from 'react';
import ProfileTextLine from '../PhysicianProfile/ProfileTextLine.jsx'
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';
import { Male, Female, genderStr } from '../../../api/Lists/GeneralTypes/Gender';
import FileUpload from '../Utils/FileUpload.jsx';
import UserFiles from '../../../api/Lists/UserFiles/UserFiles';

/*
className
patient
userName
onUpdate
onClose
 */

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}

class PatientProfileForm extends Component
{
  constructor(props)
  {
    super(props);
    this.state =
      {
        userProfile: undefined
      }
    
  }

  getBloodTypes = () =>
  {
    return ['A+', 'A-', 'B+', 'B-', 'O+', 'O-', 'AB+', 'AB-'];
  }

  getWeightUnits = () =>
  {
    return ['Oz', 'Kg', 'Lb'];
  }

  getHeightUnits = () =>
  {
    return ['ft', 'cm'];
  }

  getSugarUnits = () =>
  {
    return ['mmol/L', 'mg/dL'];
  }

  getGender = (gender) =>
  {
    const genders = 
    [
      IntlMessages[this.props.locale].intl.formatMessage(
        {
          id: genderStr[Male],
          defaultMessage: 'Male'
        }
      ),
      IntlMessages[this.props.locale].intl.formatMessage(
        {
          id: genderStr[Female],
          defaultMessage: 'Female'
        }
      )
    ];

    return genders.findIndex((g) => g === gender);
  }

  onFieldChanged = (id, value) =>
  {

    const prof = this.state.userProfile === undefined ? this.props.patient : this.state.userProfile;
    //console.log(prof);
    switch (id) 
    {
      case 'name':
        prof.Name = value;
        break;
      case 'address':
        prof.Address = value;
        break;
      case 'email':
        prof.Email = value;
        break;
      case 'phone':
        prof.Phone = value;
        break;
      case 'dob':
        prof.DOB = new Date(value.format('YYYY-MM-DDTHH:mm:ssZ'));
        break;
      case 'gender':
        prof.Gender = this.getGender(value);
        break;
      case 'bloodType':
        prof.bloodType = value;
        break;
      case 'weight':
        if (prof.weight === undefined) prof.weight = {amount: 0, units: 'Kg'}
        prof.weight.amount = parseInt(value);
        break;
      case 'units_weight':
        if (prof.weight === undefined) prof.weight = {amount: 0, units: 'Kg'}
        prof.weight.units = value;
        break;
      case 'height':
        if (prof.height === undefined) prof.height = {amount: 0, units: 'cm'}
        prof.height.amount = parseInt(value);
        break;
      case 'units_height':
        if (prof.height === undefined) prof.height = {amount: 0, units: 'cm'}
        prof.height.units = value;
        break;
      case 'sugar':
        if (prof.sugar === undefined) prof.sugar = {amount: 0, units: 'mg/dL'}
        prof.sugar.amount = parseInt(value);
        break;
      case 'units_sugar':
        if (prof.sugar === undefined) prof.sugar = {amount: 0, units: 'mg/dL'}
        prof.sugar.units = value;
        break;
      case 'bloodpressure_first':
        if (prof.bloodPressure === undefined) prof.bloodPressure = {systolic: 0, diastolic: 0}
        prof.bloodPressure.systolic = parseInt(value);
        break;
      case 'bloodpressure_second':
        if (prof.bloodPressure === undefined) prof.bloodPressure = {systolic: 0, diastolic: 0}
        prof.bloodPressure.diastolic = parseInt(value);
        break;
    }
    this.setState({userProfile: prof});
  }

  onApply = () =>
  {
    this.props.onUpdate(this.state.userProfile);
  }

  onFileUploaded = (file) =>
  {
    const url = UserFiles.findOne({_id: file._id}).link();
    const profile = this.state.userProfile === undefined ? this.props.patient : this.state.userProfile;
    profile.Avatar = url;
    this.setState({userProfile: profile});
  }

  onFileUploadFailed = (error) =>
  {
    console.error(error);
  }

  render()
  {
    return(

      this.props.patient === undefined 
      ?
      <div className='bg-lightblue br4 pa6 f-arial-30 bold c-white shadow-6'>
        Loading ...
      </div>
      :
      <div className={this.props.className + ' w-90 f-arial-16-nc'}>
        <div className='b--lightgray br4 shadow-6 bg-lightblue'>
            <div className='flex items-center justify-between'>
              <div className=''>
                
              </div>
              <div className='f-arial-30 bold'>
                {this.props.patient.Name}
              </div>
              <div
                  onClick={this.props.onClose}
                  className='close-nf mr1'
                >
                  &#9746;
              </div>
            </div>
            <div className='flex justify-between ma2'>
              <div className='flex-grow-2 mr2'>
                <ProfileTextLine 
                  id='name'
                  field=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: 'register.name',
                        defaultMessage: 'Name'
                      }
                    )
                  }
                  type='text'
                  value={this.props.patient.Name}
                  onFieldChanged={this.onFieldChanged}
                  required={true}
                />
                <ProfileTextLine
                  id='address'
                  field=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: 'userdata.address',
                        defaultMessage: 'Address'
                      }
                    )
                  }
                  type='text'
                  value={this.props.patient.Address}
                  onFieldChanged={this.onFieldChanged}
                  required={true}
                />
                <ProfileTextLine
                  id='email'
                  field=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: 'register.email',
                        defaultMessage: 'Email'
                      }
                    )
                  }
                  type='text'
                  value={this.props.patient.Email}
                  onFieldChanged={this.onFieldChanged}
                  required={true}
                />
                <ProfileTextLine
                  id='phone'
                  field=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: 'register.phone',
                        defaultMessage: 'Phone'
                      }
                    )
                  }
                  type='text'
                  value={this.props.patient.Phone}
                  onFieldChanged={this.onFieldChanged}
                  required={false}
                />
                <ProfileTextLine
                  id='dob'
                  field=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: 'patientdata.birthday',
                        defaultMessage: 'Date of birth'
                      }
                    )
                  }
                  type='datepicker'
                  value={this.props.patient.DOB}
                  onFieldChanged={this.onFieldChanged}
                  required={false}
                />
                <ProfileTextLine
                  id='gender'
                  field=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: 'patientdata.gender',
                        defaultMessage: 'Gender'
                      }
                    )
                  }
                  type='radio'
                  radioLabels={
                    [
                      IntlMessages[this.props.locale].intl.formatMessage(
                        {
                          id: genderStr[Male],
                          defaultMessage: 'Male'
                        }
                      ),
                      IntlMessages[this.props.locale].intl.formatMessage(
                        {
                          id: genderStr[Female],
                          defaultMessage: 'Female'
                        }
                      )
                    ]
                  }
                  value=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: genderStr[this.props.patient.Gender],
                        defaultMessage: 'Gender'
                      }
                    )
                  }
                  onFieldChanged={this.onFieldChanged}
                  required={false}
                />
                <ProfileTextLine
                  id='bloodType'
                  field=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: 'patientdata.bloodtype',
                        defaultMessage: 'Blood Type'
                      }
                    )
                  }
                  type='combo'
                  comboValues={this.getBloodTypes()}
                  value={this.props.patient.bloodType}
                  onFieldChanged={this.onFieldChanged}
                  required={false}
                />              
                <ProfileTextLine
                  id='weight'
                  field=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: 'patientdata.weight',
                        defaultMessage: 'Weight'
                      }
                    )
                  }
                  type='textwithunits'
                  comboValues={this.getWeightUnits()}
                  value={this.props.patient.weight === undefined ? 0 : this.props.patient.weight.amount}
                  unit={this.props.patient.weight === undefined ? 'Kg' : this.props.patient.weight.units}
                  onFieldChanged={this.onFieldChanged}
                  required={false}
                />              
                <ProfileTextLine
                  id='height'
                  field=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: 'patientdata.height',
                        defaultMessage: 'Height'
                      }
                    )
                  }
                  type='textwithunits'
                  comboValues={this.getHeightUnits()}
                  value={this.props.patient.height === undefined ? 0 : this.props.patient.height.amount}
                  unit={this.props.patient.height === undefined ? 'cm' : this.props.patient.height.units}
                  onFieldChanged={this.onFieldChanged}
                  required={false}
                />              
                <ProfileTextLine
                  id='sugar'
                  field=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: 'patientdata.sugar',
                        defaultMessage: 'Sugar'
                      }
                    )
                  }
                  type='textwithunits'
                  comboValues={this.getSugarUnits()}
                  value={this.props.patient.sugar === undefined ? 0 : this.props.patient.sugar.amount}
                  unit={this.props.patient.sugar === undefined ? '' : this.props.patient.sugar.units}
                  onFieldChanged={this.onFieldChanged}
                  required={false}
                />              
                <ProfileTextLine
                  id='bloodpressure'
                  field=
                  {
                    IntlMessages[this.props.locale].intl.formatMessage(
                      {
                        id: 'patientdata.bloodpressure',
                        defaultMessage: 'Blood Pressure'
                      }
                    )
                  }
                  type='doubleinput'
                  value1={this.props.patient.bloodPressure === undefined ? 0 : this.props.patient.bloodPressure.systolic}
                  value2={this.props.patient.bloodPressure === undefined ? 0 : this.props.patient.bloodPressure.diastolic}
                  onFieldChanged={this.onFieldChanged}
                  required={false}
                />              
              </div>
              <div className='flex flex-column items-center mr2 ml2'>
                <img src={this.props.patient.Avatar} alt='profile' width='150' />
                <FileUpload
                  className='mt3 bg-buttonblue br3 pointer shadow-3 pl3 pr3 pt2 pb2'
                  label='Update Photo'
                  onFileUploaded={this.onFileUploaded}
                  onUploadFail={this.onFileUploadFailed}
                />
              </div>
            </div>
            <div className='flex justify-around pa3'>
              <button 
                className='w4 bg-buttonblue pa2 br4 shadow-3'
                onClick={this.onApply}
              >
                <FormattedMessage
                    id='visits.submit'
                    defaultMessage='Submit'
                    description='Submit'
                />
              </button>
              <button 
                className='w4 bg-buttonblue pa2 br4 shadow-3'
                onClick={this.props.onClose}
              >
                <FormattedMessage
                    id='visits.cancel'
                    defaultMessage='Cancel'
                    description='Cancel'
                />
              </button>
            </div>
        </div>
      </div>      
    )
  }
}

export default connect(mapStateToProps)(PatientProfileForm);