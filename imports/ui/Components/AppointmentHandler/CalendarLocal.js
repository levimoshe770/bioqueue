import React from 'react'
import CalendarTable from './CalendarTable'
import addMonths from 'date-fns/addMonths'
import addDays from 'date-fns/addDays'
import './CalendarLocal.css'
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}


/*
  availableDays: List of available days in the wee,
  daysOfAcceptance: List of days with available hours for appointment
*/

class CalendarLocal extends React.Component
{
  constructor(props)
  {
    super(props);
    const today = new Date();
    today.setHours(0,0,0,0);
    this.state = {
      currentDate: today
    }
  }

  calcDisabledDays()
  {
    let res = [0];
    let i;

    const { daysOfAcceptance } = this.props;
    for (i=1; i<7; i++)
    {
      if (typeof 
            daysOfAcceptance.find(
              (day) =>
              {
                return day === i;
              }
            )
        !== 'number')
        res.push(i);
    }

    return res;
  }

  inAvailableDays(date)
  {
    const { availableDays } = this.props;

    let d = date;

    let searchResult = availableDays.find(
      (date) =>
      {
        return date.getTime() === d.getTime();
      }
    )
    return typeof searchResult !== 'undefined';
  }

  inDisabledDays(date)
  {
    let d = date;
    let foundInAvailable = 
        this.props.daysOfAcceptance.find(
        (dayOfWeek) =>
        {
          return d.getDay() === dayOfWeek;
        }
      );
    
    return typeof foundInAvailable === 'undefined';
  }

  getInitialDay = (month, year) =>
  {
    return new Date(year, month, 1).getDay();
  }

  getNumberOfDays = (month, year) =>
  {
    return new Date(year, month+1, 0).getDate();
  }

  onPreviousClick = () =>
  {
    this.setState({currentDate: addMonths(this.state.currentDate, -1)})
  }

  onNextClick = () =>
  {
    this.setState({currentDate: addMonths(this.state.currentDate, 1)})
  }

  calcGreenDays = () =>
  {
    return this.props.availableDays;
  }

  calcRedDays = () =>
  {
    const endDate = this.props.availableDays[this.props.availableDays.length - 1]
    let res = [];

    let today = this.state.currentDate;
    while (today <= endDate)
    {
      if (!this.inDisabledDays(today))
      {
        if (!this.inAvailableDays(today))
        {
          res.push(today);
        }
      } 
      today = addDays(today, 1);
    }

    return res;
  }

  getWeekDays = () =>
  {
    const { locale } = this.props;

    const baseDate = new Date('2020-10-04');
    const res = [];
    let i;
    for (i = 0; i < 7; i++)
    {
      res.push(baseDate.toLocaleDateString(locale, {weekday: 'short'}));
      baseDate.setDate(baseDate.getDate() + 1);
    }

    return res;
  }

  onSelect = (date) =>
  {
    this.props.onSelectDate(date);
  }

  render()
  {
    let currentDate = this.state.currentDate;
    const monthName = currentDate.toLocaleString(this.props.locale, {month: 'long'});
    return(
      <div className = "max-width-60 bg-white shadow-3">
        <div className="ba shadow-3 flex justify-between items-center">
          <div className="pl2">
            {monthName + " " + currentDate.getFullYear().toString()}
          </div>
          <div className="pr1">
            <img src="https://img.icons8.com/fluent/40/000000/calendar.png" alt=""/>
          </div>
        </div>
        <div className="flex justify-between bl br pa2">
          <button 
              className="bg-buttonblue br3 pa2 shadow-3"
              onClick={this.onPreviousClick}
          >
            <FormattedMessage
              id='calendarlocal.previous'
              defaultMessage='Previous'
              description='Previous'
            />
          </button>
          <button 
              className="bg-buttonblue br3 pa2 shadow-3"
              onClick={this.onNextClick}
          >
            <FormattedMessage
              id='calendarlocal.next'
              defaultMessage='Next'
              description='Next'
            />
          </button>
        </div>
        <CalendarTable 
          className = "pt0 mt0"
          initialMonth = {currentDate.getMonth() + 1}
          initialYear = {currentDate.getFullYear()}
          disabledDays = {this.calcDisabledDays()}
          greenDays = {this.calcGreenDays()}
          redDays = {this.calcRedDays()}
          initialDay = {this.getInitialDay(currentDate.getMonth(), currentDate.getFullYear())}
          numOfDays = {this.getNumberOfDays(currentDate.getMonth(), currentDate.getFullYear())}
          onSelect = {this.onSelect}
          days={this.getWeekDays()}
        />
      </div>
    )
  }
}

export default connect(mapStateToProps)(CalendarLocal);