import React from 'react';
import './HourTable.css'
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}

/*
className: class for the first container
hourList: List of objects. Each object will be:
  {hour: string, available: boolean}
 */

class HourTable extends React.Component
{
  constructor(props)
  {
    super(props);
    this.state = {selectedObj: undefined}
  }

  calcClassName = (available) =>
  {
    const baseClassName = "pt2 pb2 pl4 pr4";
    return baseClassName + " " + (available ? "available" : "unavailable");
  }

  select = (element) =>
  {
    let cl = element.getAttribute("class");
    cl = cl.replace("available", "selected1");
    element.setAttribute("class",cl);
  }

  unselect = () =>
  {
    if (typeof this.state.selectedObj === 'undefined')
      return;

    const el = document.getElementById(this.state.selectedObj.hour);
    let cl = el.getAttribute("class");
    cl = cl.replace("selected1","available");
    el.setAttribute("class", cl);
  }

  onSelectHour = (hourObj) =>
  {
    if (!hourObj.available)
      return;

    this.unselect();

    const el = document.getElementById(hourObj.hour);
    this.select(el);
    this.setState({selectedObj: hourObj});

    this.props.onSelectHour(hourObj.hour);
  }

  render()
  {
    return(
      <div className={this.props.className}>
        <div className="tc pa2 bt bl br f-arial-16-b">
          <FormattedMessage
            id='hourtable.hours'
            defaultMessage='Hours'
            description='Hours'
          />
        </div>
        <div className="height-control-312">
          <table>
            <tbody className="height-control-300">
              {
                this.props.hourList.map(
                  (hourObj, idx) =>
                  {
                    return(
                      <tr key={idx}>
                        <td 
                            className={this.calcClassName(hourObj.available)}
                            id={hourObj.hour}
                            onClick={() => {this.onSelectHour(hourObj)}}
                        >
                          {hourObj.hour}
                        </td>
                      </tr>
                    );
                  }
                )
              }
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps)(HourTable);