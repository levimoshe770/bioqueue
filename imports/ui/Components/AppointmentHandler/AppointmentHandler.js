import React from 'react'
import addDays from 'date-fns/addDays'
import addMinutes from 'date-fns/addMinutes';
import CalendarLocal from './CalendarLocal'
import HourTable from './HourTable'
import format from 'date-fns/format'
import parse from 'date-fns/parse'
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import './AppointmentHandler.css'
import Appointments from '../../../api/Lists/Appointments/Appointments';
import { addHours } from 'date-fns/esm';
import moment from 'moment';
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';

/*
Props:
1. Physician object
2. className

Events:
1. onAppointmentSet
2. onClose

 */

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}

class AppointmentHandler extends React.Component
{
  constructor(props)
  {
    super(props);
    this.state = {
      showHourTable: false,
      hourList: [],
      selectedDate: new Date(),
      selectedHour: "",
      dateSelected: false,
      hourSelected: false
    }
  }

  inDaysOfAcceptance(searchedDay)
  {
    const { Availability } = this.props.Physician;
    let searchRes = Availability.find(
      (av) =>
      {
        return av.Day === searchedDay;
      }
    )

    return typeof searchRes !== 'undefined';
  }

  getAppointments = (currentDay) =>
  {
    const { availability } = this.props;

    if (availability === undefined)
      return [];

    if (availability.length === 0)
      return [];

    let res = [];

    availability.forEach(
      (appointment) =>
      {
        const day = appointment.DateTime;
        
        if (currentDay.getDate() === day.getDate() &&
            currentDay.getMonth() === day.getMonth() &&
            currentDay.getFullYear() === day.getFullYear())
          res.push(appointment.DateTime);
      }
    )

    return res;
  }

  spotIsFree = (currentTime, appointmentsToday) =>
  {
    if (appointmentsToday.length === 0)
      return true;

    const foundTime = appointmentsToday.find(
      (ap) =>
      {
        return ap.getTime() === currentTime.getTime();
      }
    )

    return foundTime === undefined;
  }

  // inAppointmentsToday = (currentTime, appointmentsToday) =>
  // {
  //   if (appointmentsToday.length === 0)
  //     return true;

  //   const foundTime = appointmentsToday.find(
  //     (ap) =>
  //     {
  //       return ap.getTime() === currentTime.getTime();
  //     }
  //   )

  //   return foundTime !== undefined;
  // }

  addStringHours = (date, hourString) =>
  {
    const strDate = moment(date).format('yyyy-MM-DD') + ' ' + hourString;

    return moment(strDate, 'dddd-MM-DD HH:mm').toDate();
  }

  atLeastOneSpotAvailable = (currentDay) =>
  {
    const { Physician } = this.props;
    const resolution = Physician.Resolution;

    const avFound = Physician.Availability.find(
      (av) =>
      {
        return av.Day === currentDay.getDay();
      }
    )

    if (avFound === undefined)
      return false;
    
    const { Start, End } = avFound;

    const appointmensToday = this.getAppointments(currentDay);

    const startTime = this.addStringHours(currentDay, Start);
    const endTime = this.addStringHours(currentDay, End);
    let currentTime = startTime;

    while (currentTime <= endTime)
    {
      if (this.spotIsFree(currentTime, appointmensToday))
        return true;

      currentTime = addMinutes(currentTime, resolution);
    }

    return false;
  }
  
  calcAvailableDays = (daysOfAcceptance) =>
  {
    let currentDay = new Date();
    currentDay.setHours(0,0,0,0);

    const res = [];

    let i=0;
    while (res.length < daysOfAcceptance)
    {
      if ((currentDay.getDay() !== 0) &&
          (this.inDaysOfAcceptance(currentDay.getDay())) &&
          (this.atLeastOneSpotAvailable(currentDay))
         )
         {
           res.push(currentDay);
         }
      
      let tomorrow = addDays(currentDay, 1);
      currentDay = tomorrow;
      i++;

      if (i>300)
      {
        console.log('BREAKING THE LOOP');
        break;
      }
    }

    return res;
  }

  onSelectDate = (date) =>
  {
    let initialTime = date;

    const { Start, End } = this.props.Physician.Availability.find(
      (av) =>
      {
        return av.Day === date.getDay();
      }
    )

    let res = [];

    let startTime = this.addStringHours(initialTime, Start);
    let endTime = this.addStringHours(initialTime, End);
    let current = startTime;
    const appointments = this.getAppointments(date);

    while (current <= endTime)
    {
      const found = appointments.find(
        (ap) =>
        {
          return ap.getTime() === current.getTime();
        }
      )

      const available = (found === undefined);

      res.push(
        {
          hour: format(current, "HH:mm"), 
          available: available
        }
      )

      let next = addMinutes(current, this.props.Physician.Resolution);
      current = next;
    }

    this.setState({showHourTable: true, hourList: res, selectedDate: date, dateSelected: true});
  }
  
  onSelectHour = (hour) =>
  {
    this.setState({selectedHour: hour, hourSelected: true});
  }

  onSetAppointmentClick = () =>
  {
    const { selectedDate, selectedHour, dateSelected, hourSelected } = this.state;
    if (!(dateSelected && hourSelected))
      return;

    const d = parse(selectedHour,"HH:mm", selectedDate);
    this.props.onAppointmentSet(d);
  }

  daysOfAcceptance = () =>
  {
    const { Availability } = this.props.Physician;

    return Availability.map((av) => av.Day);
  }

  render()
  {
    return(
      <div className={this.props.className + " pa0 w-544 shadow-6 br4"}>
        <div className="ba br4 br--top">
            <span
              onClick={this.props.onClose}
              className="close mr1 mt1"
            >
              &#9746;
            </span>
            <div className="pa3 tc f-courgette-22 c-white bg-lightblue2-opac br4 br--top">
              {
                IntlMessages[this.props.locale].intl.formatMessage(
                  {
                    id: 'appointmenthandler.setappointment',
                    defaultMessage: 'Set appointment with'
                  }
                ) +
                " " + this.props.Physician.Name
              }
            </div>
        </div>
        <div className="bl br bb bg-linear pa5 flex flex-column br4 br--bottom">
          <div className="flex justify-between items-start">
            <CalendarLocal 
              className=""
              availableDays={this.calcAvailableDays(30)}
              daysOfAcceptance={this.daysOfAcceptance()}
              onSelectDate={this.onSelectDate}
              locale={this.props.locale}
            />
            { 
              this.state.showHourTable &&
              <HourTable 
                className="shadow-3"
                hourList={this.state.hourList}
                onSelectHour={this.onSelectHour}
              />
            }
          </div>
          <button 
              className="bg-buttonbluestrong self-center pb3 pt3 pl4 pr4 ma4 br-pill shadow-3"
              onClick={this.onSetAppointmentClick}
          >
            <FormattedMessage
              id='appointmenthandler.setappointmentbt'
              defaultMessage='Set Appointment'
              description='Set Appointment'
            />
          </button>
        </div>
      </div>
    )
  }
}

export default 
connect(mapStateToProps)(
withTracker(

  (Physician) =>
  {
      const availabilityHandle = Meteor.subscribe('appointments.physicianAvailability', Physician._id);

      const loading = !availabilityHandle.ready();

      return {availability: !loading ? Appointments.find({}).fetch() : []}
  }

)
(AppointmentHandler));
