import React from 'react'


/*
className = ""
initialDay = "0"
numOfDays = "30"
disabledDays
days={['SUN','MON','TUE','WED','THU','FRI','SAT']}
 */

export default class CalendarTable extends React.Component
{

  constructor(props)
  {
    super(props);
    this.state = {
      daySelected: {day:0, weekDay:0}
    }
  }

  createRowData = (initialDay, numOfDays) =>
  {
    let res = [];
    let daysRemained = numOfDays;
    let currentDate = 1;
    let i;
    
    while (daysRemained > 0)
    {
      let week;
      if (daysRemained === numOfDays)
      {
        week = new Array(initialDay).fill('');
        for (i=0; i<7-initialDay; i++)
        {
          week.push(currentDate.toString());
          currentDate++;
        }
        daysRemained -= (7 - initialDay);
      }
      else
      {
        week = [];
        let numOfDaysInWeek = Math.min(7, daysRemained);
        for (i=0; i<numOfDaysInWeek; i++)
        {
          week.push(currentDate.toString());
          currentDate++;
        }
        if (week.length < 7)
        {
          for (i=0; i<7 - numOfDaysInWeek; i++)
          {
            week.push('');
          }
        }
  
        daysRemained -= numOfDaysInWeek;
      }
  
      res.push(week);
    }
    return res;
  }
  
  specialClass = (day, dayInWeek) =>
  {
    if (day === "")
      return "";

    let found = this.props.disabledDays.find(
      (d) =>
      {
        return d === dayInWeek;
      }
    )

    if (typeof found !== 'undefined')
      return "disabled";

    if (this.state.daySelected.day === day &&
        this.state.daySelected.weekDay === dayInWeek)
        {
          return "selected";
        }

    const { initialMonth, initialYear } = this.props;
    const myDate = new Date(initialYear, initialMonth-1, day);

    found = this.props.greenDays.find(
      (d) =>
      {
        return d.getTime() === myDate.getTime();
      }
    )

    if (typeof found !== 'undefined')
      return "available";

    found = this.props.redDays.find(
      (d) =>
      {
        return d.getTime() === myDate.getTime();
      }
    )
    if (typeof found !== 'undefined')
      return "unavailable";

    return "enabled";
  }

  componentDidMount()
  {
    this.setState({displayMonth: this.props.initialMonth, displayYear: this.props.initialYear})
  }

  onSelectDate = (day, weekDay) =>
  {
    if (day === "")
      return;

    const { disabledDays, greenDays } = this.props;
    let found = disabledDays.find(
      (dayInWeek) =>
      {
        return dayInWeek === weekDay;
      }
    )

    if (typeof found !== 'undefined')
      return;

    const { initialMonth, initialYear } = this.props;
    const myDate = new Date(initialYear, initialMonth-1, day);
    found = greenDays.find(
      (date) =>
      {
        return date.getTime() === myDate.getTime();
      }
    )
    if (typeof found === 'undefined')
      return;

    this.setState({daySelected: {day: day, weekDay: weekDay}});

    this.props.onSelect(myDate);
  }

  render()
  {
    return(
      <div className={this.props.className}>
        <table className='ba'>
          <thead>
            <tr>
            {
              this.props.days.map(
                (day, idx) =>
                {
                  return <th key={idx} className="tc pa2 ba">{day}</th>
                }
              )
            }
            </tr>
          </thead>
          <tbody>
            {
              this.createRowData(this.props.initialDay,this.props.numOfDays).map(
                (week, idx) =>
                { 
                  return(
                    <tr key={idx}>
                      {
                        week.map(
                          (day, dayidx) =>
                          {
                            return(
                              <td 
                                  key={dayidx}
                                  className={"tc pa2 ba " + this.specialClass(day, dayidx)}
                                  id={day*10}
                                  onClick = {() => {this.onSelectDate(day, dayidx)}}
                              >
                                {day}
                              </td>
                            )
                          }
                        )
                      }
                    </tr>
                  );
                }
              )
            }
          </tbody>
        </table>
      </div>
    )
  }
}