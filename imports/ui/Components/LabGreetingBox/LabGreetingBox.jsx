import React, { Component } from 'react';
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import { Approved } from '../../../api/Lists/GeneralTypes/AuthorizationStatus';

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}

/*
className
profile
numOfPending
numOfNewOrders
 */

class LabGreetingBox extends Component
{
    getApprovedMessage = () =>
    {
        const { profile, numOfPending, numOfNewOrders, locale } = this.props;

        if (profile.length === 0)
        {
            return (
                IntlMessages[locale].intl.formatMessage(
                    {
                        id: 'physiciangreeting.loading',
                        defaultMessage: 'loading'
                    }
                )
            )
        }

        const good = IntlMessages[locale].intl.formatMessage(
            {
                id: 'physiciangreeting.comeback',
                defaultMessage: 'good to have you back'
            }
        );

        const have = IntlMessages[locale].intl.formatMessage(
            {
                id: 'physiciangreeting.youhave',
                defaultMessage: 'You have'
            }
        )

        const testsPending = IntlMessages[locale].intl.formatMessage(
            {
                id: 'labgreetingbox.testspending',
                defaultMessage: 'tests pending for results, and'
            }
        )

        const newOrder = IntlMessages[locale].intl.formatMessage(
            {
                id: 'labgreetingbox.neworders',
                defaultMessage: 'new orders'
            }
        )

        return(
            profile[0].Name + ', ' +
            good + '. ' + have + ' ' +
            numOfPending.toString() + ' ' +
            testsPending + ' ' +
            numOfNewOrders.toString() + ' ' +
            newOrder
        )
    }

    getPendingMessage = () =>
    {
        return(
            this.props.profile[0].Name + ', ' +
            IntlMessages[this.props.locale].intl.formatMessage(
                {
                    id: 'labgreetingbox.pendingmsg',
                    defaultMessage: 'your status is still pending. Please make sure you have filled out all necessary data in your'
                }
            )
        )
    }

    render()
    {
        return(
            <div
                className={this.props.className + ' bg-lightblue2-opac w-70 pa3 f-courgette-27 tc c-white br3 shadow-6 mt5'}
            >
                {
                    this.props.profile.length === 0
                        ? 
                        <div>
                            {
                                IntlMessages[this.props.locale].intl.formatMessage(
                                    {
                                        id: 'physiciangreeting.loading',
                                        defaultMessage: 'loading'
                                    }
                                ) + '...'
                            }
                        </div>
                        :
                        <div>
                            {
                                this.props.profile[0].AuthorizationStatus === Approved
                                    ? this.getApprovedMessage()
                                    :
                                        <div>
                                            {this.getPendingMessage()}
                                            <Link className='ml2 c-lightpurle' to='/profile'>
                                                <FormattedMessage
                                                    id='labgreetingbox.profile'
                                                    defaultMessage='profile'
                                                    description='profile'
                                                />
                                            </Link>
                                        </div> 
                                    
                            }
                        </div>
                }
            </div>
        )
    }
}

export default connect(mapStateToProps)(LabGreetingBox);