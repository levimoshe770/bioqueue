import React, { Component } from 'react';
import './MedicalRecordPermissions.css'
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../../translation/IntlMessages';
import { connect } from 'react-redux';

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}

/*
onApply
onClose
 */

class MedicalRecordPermissions extends Component
{

  constructor(props)
  {
    super(props);
    this.state = 
    {
      visits: false,
      kproblems: true,
      medication: true,
      alergies: true,
      addvisits: true,
      ordertests: true
    }
  }

  render()
  {
    //console.log(this.state);
    return(
      <div
        className='medpermissions pb2 br3 shadow-6'
      >
        <div className='flex justify-between items-center bg-blue mb2 br3 br--top'>
          <div></div>
          <div className='f-arial-30 c-white'>
            <FormattedMessage
              id='patient.permissions'
              defaultMessage='Permissions'
            />
          </div>
          <div 
            className='close-nf mr2 mt1' 
            onClick={this.props.onClose}
          >
            &#9746;
          </div>
        </div>
        <div className='f-arial-18-nc'>
          <div className='flex items-center mr3'>
            <input
              className='ma2'
              type='checkbox'
              id='visits'
              checked={this.state.visits}
              onChange={(e) => this.setState({visits: e.target.checked})}
            />
            <label
              htmlFor='visits'
            >
              <FormattedMessage
                id='patient.permvisitothers'
                defaultMessage='Visits with other physicians'
              />
            </label>
          </div>
          <div className='flex items-center mr3'>
            <input
              className='ma2'
              type='checkbox'
              id='kproblems'
              checked={this.state.kproblems}
              onChange={(e) => this.setState({kproblems: e.target.checked})}
            />
            <label
              htmlFor='kproblems'
            >
              <FormattedMessage
                id='patient.permknownprobs'
                defaultMessage='Known problems'
              />
            </label>
          </div>
          <div className='flex items-center mr3'>
            <input
              className='ma2'
              type='checkbox'
              id='medication'
              checked={this.state.medication}
              onChange={(e) => this.setState({medication: e.target.checked})}
            />
            <label
              htmlFor='medication'
            >
              <FormattedMessage
                id='patient.permpermmed'
                defaultMessage='Permanent Medication'
              />
            </label>
          </div>
          <div className='flex items-center mr3'>
            <input
              className='ma2'
              type='checkbox'
              id='alergies'
              checked={this.state.alergies}
              onChange={(e) => this.setState({alergies: e.target.checked})}
            />
            <label
              htmlFor='alergies'
            >
              <FormattedMessage
                id='patient.permallergies'
                defaultMessage='Allergies'
              />
            </label>
          </div>
          <div className='flex items-center mr3'>
            <input
              className='ma2'
              type='checkbox'
              id='addvisits'
              checked={this.state.addvisits}
              onChange={(e) => this.setState({addvisits: e.target.checked})}
            />
            <label
              htmlFor='addvisits'
            >
              <FormattedMessage
                id='patient.permaddvisits'
                defaultMessage='Add visits'
              />
            </label>
          </div>
          <div className='flex items-center mr3'>
            <input
              className='ma2'
              type='checkbox'
              id='ordertests'
              checked={this.state.ordertests}
              onChange={(e) => this.setState({ordertests: e.target.checked})}
            />
            <label
              htmlFor='ordertests'
            >
              <FormattedMessage
                id='patient.permordertests'
                defaultMessage='Order tests'
              />
            </label>
          </div>
        </div>
        <div className='flex justify-around items-center f-arial-18-nc ma2'>
          <button
            className='bg-buttonblue pt2 pb2 pl3 pr3 br-pill'
            onClick={() => this.props.onApply(this.state)}
          >
            <FormattedMessage
              id='patient.apply'
              defaultMessage='Apply'
            />
          </button>
          <button
            className='bg-buttonblue pt2 pb2 pl3 pr3 br-pill'
            onClick={this.props.onClose}
          >
            <FormattedMessage
              id='patient.cancel'
              defaultMessage='Cancel'
            />
          </button>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps)(MedicalRecordPermissions);