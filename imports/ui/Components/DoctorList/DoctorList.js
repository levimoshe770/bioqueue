import React from 'react'
import './DoctorList.css'
import InListSearchBox from '../Utils/InListSearchBox';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl'

/*

Receives:
DoctorList: List of doctor objects
SelectOption: Boolean

Raises:
onClose: Closing of the list without selection
onSelected: Closing of the list with a selection

 */

const mapStateToProps = (state) =>
{
  return {locale: state.locale}
}

class DoctorList extends React.Component
{

  constructor(props)
  {
    super(props);
    this.state = 
    {
      selectedIdx : -1, 
      DoctorList: this.props.DoctorList, 
      FullList: this.props.DoctorList
    }
  }

  onCheckClicked = (event) =>
  {
    if (event.target.checked)
      this.setState({selectedIdx: parseInt(event.target.id)});
    else
      this.setState({selectedIdx: -1});
  }

  onSearchBoxChanged = (value, fieldName) =>
  {
    let {FullList} = this.state;
    this.setState(
      {DoctorList: FullList.filter(
        (doctor) => 
        {
          if (value.length === 0)
            return true;

          if (fieldName === 'name')
            return doctor.Name.toLowerCase().includes(value.toLowerCase());

          if (fieldName === 'specialty')
            return doctor.Specialty.toLowerCase().includes(value.toLowerCase());

          if (fieldName === 'address')
            return doctor.Address.toLowerCase().includes(value.toLowerCase());

          return false;
        }

      )}
    )

  }

  OnSelectClick = () =>
  {
    this.props.onSelected(this.state.DoctorList[this.state.selectedIdx]);
  }

  getWeekDays = () =>
  {
    const { locale } = this.props;

    const baseDate = new Date('2020-10-05');
    const res = [];
    let i;
    for (i = 0; i < 7; i++)
    {
      res.push(baseDate.toLocaleDateString(locale, {weekday: 'short'}));
      baseDate.setDate(baseDate.getDate() + 1);
    }

    return res;
  }

  days = this.getWeekDays();

  render()
  {
    // console.log('DoctorList',this.props);
    return(
      <div className="ba bg-lightblue height-control flex-display">
        <div>
            <span
              onClick={this.props.onClose}
              className="close c-gray mr1 mt1"
            >
              &#9746;
            </span>
        </div>
        <div className="pa2 height-control">
          <table className="ba">
            <thead>
              <tr className="row-color-light-blue">
                {this.props.SelectOption && <th className="pa2"></th>}
                <th className="pa2 ba">
                  <FormattedMessage
                    id='doctorlist.name'
                    defaultMessage='Name'
                    description='Name'
                  />
                </th>
                <th className="pa2 ba">
                  <FormattedMessage
                    id='doctorlist.specialty'
                    defaultMessage='Specialty'
                    description='Specialty'
                  />
                </th>
                <th className="pa2 ba">
                  <FormattedMessage
                    id='userdata.address'
                    defaultMessage='Address'
                    description='Address'
                  />
                </th>
                <th className="pa2 ba">
                  <FormattedMessage
                    id='doctorlist.acceptancehours'
                    defaultMessage='Acceptance Hours'
                    description='Acceptance Hours'
                  />
                </th>
              </tr>
            </thead>
            <tbody>
              <tr className="row-color-light-blue">
                {this.props.SelectOption && <td></td>}
                <td>
                  <InListSearchBox
                    Field="name"
                    onSearchBoxChanged={this.onSearchBoxChanged}
                  />
                </td>
                <td>
                  <InListSearchBox
                    Field="specialty"
                    onSearchBoxChanged={this.onSearchBoxChanged}
                  />
                </td>
                <td>
                  <InListSearchBox
                    Field="address"
                    onSearchBoxChanged={this.onSearchBoxChanged}
                  />
                </td>
                <td></td>
              </tr>
              {
                this.state.DoctorList.map(
                  (element,idx) => 
                    <tr className="row-color-light-blue" key={idx}>
                      { this.props.SelectOption &&
                        <td className="tc pa2 ba">
                          <input 
                            className={this.state.selectedIdx === idx ? "row-color-selected" : ""} 
                            type="checkbox" 
                            id={idx}
                            checked={this.state.selectedIdx === idx}
                            onChange={this.onCheckClicked}
                          />
                        </td>
                      }
                      <td className={"pa2 ba" + (this.state.selectedIdx === idx ? " row-color-selected" : "")}>
                        {element.Name}
                      </td>
                      <td className={"pa2 ba" + (this.state.selectedIdx === idx ? " row-color-selected" : "")}>
                        {element.Specialty}
                      </td>
                      <td className={"pa2 ba" + (this.state.selectedIdx === idx ? " row-color-selected" : "")}>
                        {element.Address}
                      </td>
                      <td className={"pa2 ba" + (this.state.selectedIdx === idx ? " row-color-selected" : "")}>
                        {
                          element.Availability === undefined ? <div></div> :
                          <div>
                          {
                            element.Availability.map(
                              (av,idx) =>
                              {
                                return (
                                  <div key={"av_" + idx.toString()}>
                                    {this.days[av.Day] + ',' + av.Start + " - " + av.End}
                                  </div>)
                              }
                            )
                          }
                          </div>
                        }
                      </td>
                    </tr>
                )
              }
            </tbody>
          </table>
        </div>
        {
        this.props.SelectOption &&
          <div className="button-grid pa2">
            <button
              className="pa2 pl4 pr4 bg-buttonblue f-oswald-20"
              onClick={this.OnSelectClick}
            >
              <FormattedMessage
                id='doctorlist.select'
                defaultMessage='Select'
                description='Select'
              />
            </button>
          </div>
        }
      </div>
    )
  }
}

export default connect(mapStateToProps)(DoctorList);