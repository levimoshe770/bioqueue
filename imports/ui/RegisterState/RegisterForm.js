import React from 'react';
import './RegisterForm.css'
import RegisterPatient from './RegisterPatient'
import RegisterPhysician from './RegisterPhysician'
import RegisterAdmin from './RegisterAdmin'
import RegisterLaboratory from './RegisterLaboratory'
import RegisterPharmacy from './RegisterPharmacy'
import RegisterFormSelector from './RegisterFormSelector'

/*
userRegistered: Callback for user registration
 */

export default class RegisterForm extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            selection: "patient"
        }
    }

    selectionChanged = (selection) =>
    {
        this.setState({selection: selection})
    }

    userRegistered = () =>
    {
        this.props.userRegistered();
    }

    renderRegisterForm()
    {
        const { selection } = this.state;
        switch (selection)
        {
            case 'patient':
                return(
                    <RegisterPatient 
                        onUserRegistered={this.userRegistered}
                    /> 
                );
            case 'physician':
                return(
                    <RegisterPhysician 
                        onUserRegistered={this.userRegistered}
                    /> 
                );
            case 'admin':
                return(
                    <RegisterAdmin 
                        onUserRegistered={this.userRegistered}
                    />
                );
            case 'laboratory':
                return(
                    <RegisterLaboratory 
                        onUserRegistered={this.userRegistered}
                    />
                );
            case 'pharmacy':
                return(
                    <RegisterPharmacy 
                        onUserRegistered={this.userRegistered}
                    /> 
                );
        }
    }

    render()
    {
        return (
            <div className="registerform">
                <span className="close" onClick={this.props.onRegisterClosed}>
                    &times;
                </span>
                <div className="flex flex-column items-center mb5 mt5">
                    <RegisterFormSelector 
                        selection={this.state.selection} 
                        onSelectionChanged={this.selectionChanged}
                    />
                    {
                        this.renderRegisterForm()
                    }
                </div>
            </div>
        )
    }
}