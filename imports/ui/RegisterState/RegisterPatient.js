import React from 'react';
import ErrorInForm from '../Components/Utils/ErrorInForm';
import { Meteor } from 'meteor/meteor';
import { FormattedMessage } from 'react-intl'
import { IntlMessages } from '../../../translation/IntlMessages';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import mrserverurl from '../mrserverurl';
import Remote from '../Remote';
import { LoginPopup } from '../Functions/LoginPopup';
import { Male, Female, genderStr } from '../../api/Lists/GeneralTypes/Gender';

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}

/*
onUserRegistered: Callback if user is registered successfully
 */

class RegisterPatient extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            Name: "",
            Email: "",
            Phone: "",
            Password: "",
            ConfirmPassword: "",
            Gender: -1,
            TermsAgree: false,
            LifeTracker: false,
            showPasswordMatch: false,
            showErrorMessage: false,
            showEmailError: false,
            errorMessage: ''
        }
    }

    onFieldChange = (event) =>
    {
        const { id, value } = event.target;

        switch (id)
        {
            case "name":
                this.setState({Name: value});
                break;
            case "email":
                this.setState(
                    {
                        Email: value,
                        showEmailError: !this.validateEmail(value)
                    }
                );
                break;
            case "phone":
                this.setState({Phone: value});
                break;
            case "password":
                this.setState(
                    {
                        Password: value, 
                        showPasswordMatch: value !== this.state.ConfirmPassword
                    }
                );
                break;
            case "confirmpwd":
                this.setState(
                    {
                        ConfirmPassword: value,
                        showPasswordMatch: value !== this.state.Password
                    }
                );
                break;
            case "agree":
                this.setState({TermsAgree: event.target.checked});
                break;
            case "agreelt":
                this.setState({LifeTracker: event.target.checked});
                break;
            
        }
    }

    validateEmail = (email) =>
    {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        return re.test(email);
    }

    inputIsLegal = () =>
    {
        const 
        { 
            Name, 
            Email, 
            Phone, 
            Password, 
            ConfirmPassword, 
            TermsAgree, 
            Gender,
            LifeTracker
        } = this.state;

        let res = true;

        res = (Password === ConfirmPassword);
        if (!res)
        {
            this.setState({errorMessage: 'Passwords dot\'t match'})
            return res;
        }

        res = this.validateEmail(Email);
        if (!res)
        {
            this.setState({errorMessage: 'Email is invalid'})
            return res;
        }

        // res = this.validatePassword(Password);
        // if (!res)
        // {

        // }

        if (!TermsAgree)
        {
            this.setState({errorMessage: 'Agree to the terms'})
            return false;
        }

        if (!LifeTracker)
        {
            this.setState({errorMessage: 'Agree to lifetracker'})
            return false;
        }

        if (Gender === -1)
        {
            this.setState({errorMessage: 'Choose gender'});
            return false;
        }

        return true;
    }

    onUserCreated = (data) =>
    {
        this.props.onUserRegistered();
        console.log('user id created:', data);

        let userId = data.data.userId;

        const profile = 
        {
            _id: userId,
            Name: this.state.Name,
            Email: this.state.Email,
            Address: '',
            Phone: this.state.Phone,
            Avatar: '',
            Gender: this.state.Gender,
            ConnectedPhysician: ''
        }
        Remote.call('patient.createProfile', profile);
    }

    onUserCreatedError = (err) =>
    {
        console.log(err);
        this.setState({errorMessage: err.details, showErrorMessage: true});
    }

    onSignUpClick = () =>
    {
        if (!this.inputIsLegal())
        {
            this.setState({showErrorMessage: true});
            return;
        }

        console.log('input legal')

        const data = 
        {
            email: this.state.Email, 
            password: this.state.Password, 
            name: this.state.Name,
            role: 'patient'
        };

        axios(
            {
                method: 'post',
                url: mrserverurl + '/register-user',
                data: { data }
            }
        )
        .then(data => this.onUserCreated(data))
        .catch(err => this.onUserCreatedError(err));
        
    }

    render()
    {
        // console.log(this.props);
        // console.log(IntlMessages[this.props.locale])
        return(
            <div 
                className="br bl bb b--light-silver w-70 flex flex-column items-center"
            >
                <div className="f-legend mt3 mb3">
                    <FormattedMessage
                        id='registerpatient.patientsignup'
                        defaultMessage='Patient Sign Up'
                        description='Patient sign up'
                    />
                </div>
                <div className="ba pt2 pb2 pl3 pr3 f-arial-13 br3 shadow-3 bg-buttonblue">
                    <FormattedMessage
                        id='registerpatient.usemylifetracker'
                        defaultMessage='Use my LifeTracker'
                        description='Use my LifeTracker'
                    />
                </div>
                <div className="pa3 w-60">
                    <div className="pt3 pb3 pl1 pr1">
                        <label 
                            className="dt f-arial-13 pb1" 
                            htmlFor="name"
                        >
                            <FormattedMessage
                                id='register.name'
                                defaultMessage='Name'
                                description='Name'
                            />
                        </label>
                        <input 
                            className="w-100 pa2 br2 ba b--gray shadow-3" 
                            id="name"
                            type="text" 
                            onChange={this.onFieldChange}
                        />
                    </div>
                    <div className="pt3 pb3 pl1 pr1">
                        <label 
                            className="dt f-arial-13 pb1" 
                            htmlFor="email"
                        >
                            <FormattedMessage
                                id='register.email'
                                defaultMessage='Email'
                                description='Email'
                            />
                        </label>
                        <input 
                            className="w-100 pa2 br2 ba b--gray shadow-3" 
                            id="email"
                            type="email" 
                            onChange={this.onFieldChange}
                        />
                        { this.state.showEmailError &&
                            <ErrorInForm 
                                className="pt1 errorMsg f-arial-13"
                                Message={
                                    IntlMessages[this.props.locale].intl.formatMessage(
                                        {
                                            id: 'register.mailinvalid',
                                            defaultMessage: 'Email is not valid'
                                        }
                                    )
                                }
                            />
                        }
                    </div>
                    <div className="pt3 pb3 pl1 pr1">
                        <label 
                            className="dt f-arial-13 pb1" 
                            htmlFor="phone"
                        >
                            <FormattedMessage
                                id='register.phone'
                                defaultMessage='Phone'
                                description='Phone'
                            />
                        </label>
                        <input 
                            className="w-100 pa2 br2 ba b--gray shadow-3" 
                            id="phone"
                            type="text" 
                            onChange={this.onFieldChange}
                        />
                    </div>
                    <div className="pt3 pb3 pl1 pr1">
                        <label 
                            className="dt f-arial-13 pb1" 
                            htmlFor="password"
                        >
                            <FormattedMessage
                                id='register.password'
                                defaultMessage='Password'
                                description='Password'
                            />
                        </label>
                        <input 
                            className="w-100 pa2 br2 ba b--gray shadow-3" 
                            id="password"
                            type="password" 
                            onChange={this.onFieldChange}
                        />
                    </div>
                    <div className="pt3 pb3 pl1 pr1">
                        <label 
                            className="dt f-arial-13 pb1" 
                            htmlFor="confirmpwd"
                        >
                            <FormattedMessage
                                id='register.confirmpassword'
                                defaultMessage='Confirm password'
                                description='Confirm password'
                            />
                        </label>
                        <input 
                            className="w-100 pa2 br2 ba b--gray shadow-3" 
                            id="confirmpwd"
                            type="password" 
                            onChange={this.onFieldChange}
                        />
                        { this.state.showPasswordMatch &&
                            <ErrorInForm 
                                className="pt1 errorMsg f-arial-13" 
                                Message={
                                    IntlMessages[this.props.locale].intl.formatMessage(
                                        {
                                            id: 'register.passworddontmatch',
                                            defaultMessage: 'Passwords don\'t match'
                                        }
                                    )
                                }
                            />
                        }
                    </div>
                    <div className='flex items-center justify-around pt3 pb3 pl1 pr1'>
                        <div className='flex items-center'>
                            <input 
                                className=''
                                type='radio'
                                id='male'
                                name='gender'
                                value='male'
                                onChange={(e) => e.target.checked ? this.setState({Gender: Male}) : this.setState({Gender: Female}) }
                            />
                            <label className='pl1' htmlFor='male'>
                                <FormattedMessage
                                    id='register.male'
                                    defaultMessage='Male'
                                    description='Male'
                                />
                            </label>
                        </div>
                        <div className='flex items-center'>
                            <input 
                                className=''
                                type='radio'
                                id='female'
                                name='gender'
                                value='female'
                                onChange={(e) => e.target.checked ? this.setState({Gender: Female}) : this.setState({Gender: Male}) }
                            />
                            <label className='pl1' htmlFor='female'>
                                <FormattedMessage
                                    id='register.female'
                                    defaultMessage='Female'
                                    description='Female'
                                />
                            </label>
                        </div>
                    </div>
                    <div className="pt3 flex items-center pb2">
                        <input 
                            type="checkbox" 
                            id="agree"
                            onChange={this.onFieldChange}
                        />
                        <label className="f-arial-13 pl2" htmlFor="agree">
                            <FormattedMessage
                                id='register.agreetoterms'
                                defaultMessage='I agree to the terms'
                                description='I agree to the terms'
                            />
                        </label>
                    </div>
                    <div className="pb3 flex items-center pb2">
                        <input type="checkbox" id="agreelt" onChange={this.onFieldChange}/>
                        <label className="f-arial-13 pl2" htmlFor="agreelt">
                            <FormattedMessage
                                id='register.agreetolifetracker'
                                defaultMessage='I agree to create a LifeTracker record for me'
                                description='I agree to create a LifeTracker record for me'
                            />
                        </label>
                    </div>
                </div>
                <div 
                    className="pa3 tc f-arial-13 br3 shadow-3 bg-buttonblue" 
                    onClick={this.onSignUpClick}
                >
                    <FormattedMessage
                        id='register.signup'
                        defaultMessage='Sign Up'
                        description='Sign Up'
                    />
                </div>
                { this.state.showErrorMessage &&
                    <ErrorInForm 
                        className="pt1 errorMsg f-arial-13" 
                        Message={this.state.errorMessage}
                    />
                }
                <div className="f-arial-14 pt3 pb3 flex items-center">
                    <FormattedMessage
                        id='register.haveaccount'
                        defaultMessage='Already have an account?'
                        description='Already have an account?'
                    />
                    <div 
                        className="f-link-14 pl2 pointer"
                        onClick=
                        {
                            () => 
                            {
                                LoginPopup(this.props.locale);
                            }
                        }
                    >
                        <FormattedMessage
                            id='register.login'
                            defaultMessage='Log In'
                            description='Log In'
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(connect(mapStateToProps)(RegisterPatient));