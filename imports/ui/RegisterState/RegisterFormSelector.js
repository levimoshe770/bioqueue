import React from 'react';
import './RegisterFormSelector.css';
import { FormattedMessage } from 'react-intl';

export default class RegisterFormSelector extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            selection: props.selection
        }
    }

    onSelectionChanged = (event) =>
    {
        this.setState({selection: event.target.id})
        this.props.onSelectionChanged(event.target.id);
    }
        
    render()
    {
        return(
            <div className="w-70">
                <div className="flex flex-row justify-between items-stretch-ns">
                    <div 
                        className={
                            "ba b--light-silver pa3 br3 br--top flex-basis-1 tc f-arial-14 pointer" + 
                            (this.state.selection === "patient" 
                                ? " selected" : "")
                        }
                        id="patient"
                        onClick={this.onSelectionChanged}    
                    >
                        <FormattedMessage
                            id='registerformselector.patient'
                            defaultMessage='Patient'
                            description='Patient'
                        />
                    </div>
                    <div 
                        className={
                            "ba b--light-silver pa3 br3 br--top flex-basis-1 tc f-arial-14 pointer" +
                            (this.state.selection === "physician" ? " selected" : "")
                        }
                        id="physician"
                        onClick={this.onSelectionChanged}
                    >
                        <FormattedMessage
                            id='registerformselector.physician'
                            defaultMessage='Physician'
                            description='Physician'
                        />
                    </div>
                    <div 
                        className={
                            "ba b--light-silver pa3 br3 br--top flex-basis-1 tc f-arial-14 pointer" +
                            (this.state.selection === "admin" ? " selected" : "")
                        }
                        id="admin"
                        onClick={this.onSelectionChanged}
                    >
                        <FormattedMessage
                            id='registerformselector.administrator'
                            defaultMessage='Administrator'
                            description='Administrator'
                        />
                    </div>
                    <div 
                        className={
                            "ba b--light-silver pa3 br3 br--top flex-basis-1 tc f-arial-14 pointer" +
                            (this.state.selection === "laboratory" ? " selected" : "")
                        }
                        id="laboratory"
                        onClick={this.onSelectionChanged}
                    >
                        <FormattedMessage
                            id='registerformselector.laboratory'
                            defaultMessage='Laboratory'
                            description='Laboratory'
                        />
                    </div>
                    <div 
                        className={
                            "ba b--light-silver pa3 br3 br--top flex-basis-1 tc f-arial-14 pointer" +
                            (this.state.selection === "pharmacy" ? " selected" : "")
                        }
                        id="pharmacy"
                        onClick={this.onSelectionChanged}
                    >
                        <FormattedMessage
                            id='registerformselector.pharmacy'
                            defaultMessage='Pharmacy'
                            description='Pharmacy'
                        />
                    </div>
                </div>
            </div>
        );
    }
}