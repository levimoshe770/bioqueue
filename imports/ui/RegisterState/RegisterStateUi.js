import React from 'react'
import RegisterForm from './RegisterForm'
import './RegisterStateUi.css'
import { FormattedMessage } from 'react-intl';
import { withRouter } from 'react-router-dom';
import { LoginPopup } from '../Functions/LoginPopup';
import { connect } from 'react-redux';

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}

class RegisterStateUi extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            showRegisterForm: true,
            showRegistrationSucceeded: false,
            pageRef: React.createRef(),
            scrolled: false
        }
    }

    onRegisterClosed = () =>
    {
        this.props.history.push("/");
    }

    userRegistered = () =>
    {
        this.setState(
            {
                showRegisterForm: false,
                showRegistrationSucceeded: true
            }
        )
    }

    scrollUp = () =>
    {
        if (this.state.pageRef.current === null)
            return;

        this.state.pageRef.current.scrollIntoView();
    }

    componentDidUpdate()
    {
        if (this.state.showRegistrationSucceeded)
            this.scrollUp();
    }

    render()
    {
        return (
            <div className="statebasic registerstate">
                {
                    this.state.showRegisterForm && 
                        <RegisterForm 
                            onRegisterClosed={this.onRegisterClosed} 
                            userRegistered={this.userRegistered}
                        />
                }
                {
                    this.state.showRegistrationSucceeded &&
                        <div 
                            className="gr-1-2 ba pa4 f-arial-30 bg-lightblue2-opac c-white br4 flex items-center"
                            ref={this.state.pageRef}
                        >
                            <FormattedMessage
                                id='register.success'
                                defaultMessage='User registered successfully'
                                description='User registered successfully'
                            />
                            <div
                                className="pl1 f-link-30 pointer"
                                onClick=
                                {
                                    () => 
                                    {
                                        LoginPopup(this.props.locale);
                                    }
                                }
                            >
                                <FormattedMessage
                                    id='register.signin'
                                    defaultMessage='Click to Sign in'
                                    description='Click to Sign in'
                                />
                            </div>
                        </div>
                }
            </div>
        );
    }
}

export default connect(mapStateToProps)(withRouter(RegisterStateUi));