import { DDP } from 'meteor/ddp-client';
import { Meteor } from 'meteor/meteor';

const Remote = DDP.connect(Meteor.settings.public.auth_server_url);

export default Remote;