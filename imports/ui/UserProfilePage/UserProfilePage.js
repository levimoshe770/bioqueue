import React, { Component } from 'react'
import { Meteor } from 'meteor/meteor'
import { withTracker } from 'meteor/react-meteor-data';
import { connect } from 'react-redux'
import { setUserLoggedIn } from '../Redux/actions'
import '../PatientStateLoggedIn/PatientStateLoggedInUi.css'
import '../PhysicianStateLoggedIn/PhysicianStateLoggedIn.css'
import '../LabStateLoggedIn/LabPage.css'
import Patients from '../../api/Lists/Patients/patients';
//import UserProfileForm from '../Components/UserProfile/UserProfileForm'
import Physicians from '../../api/Lists/Physicians/physicians';
import PhysicianProfile from '../Components/PhysicianProfile/PhysicianProfile.jsx'
import PatientProfileForm from '../Components/UserProfile/PatientProfileForm.jsx'
import LabProfile from '../Components/LabProfile/LabProfile.jsx'
import PharmacyProfile from '../Components/PharmacyProfile/PharmacyProfile.jsx';
import Remote from '../Remote';
import PharmacySchema from '../../api/Lists/Pharmacies/PharmacySchema';

const mapStateToProps = state =>
{
    return {userName: state.userName, access_token: state.access_token}
}

const mapDispatchToProps = (dispatch) =>
{
    return {onUserChange: (userName) => dispatch(setUserLoggedIn(userName))}
}

class UserProfilePage extends Component
{
    constructor(props)
    {
        super(props);
    }

    onPatientProfileUpdate = (profile) =>
    {
        Remote.call(
            'patient.updateProfile',this.props.access_token, profile,
            (err, res) =>
            {
                if (err) 
                {
                    console.log(err);
                    return;
                }

                if (this.props.userName.Name !== profile.Name ||
                    this.props.userName.Email !== profile.Email)
                {
                    // Update redux state
                    this.props.onUserChange(
                        {
                            Name: profile.Name,
                            Email: profile.Email,
                            Role: 'patient',
                            userId: profile._id,
                            InitialPath: '/patient'
                        }
                    )

                    // Update users table
                    Remote.call(
                        'user.update', this.props.access_token, profile._id, profile.Name, profile.Email
                    )
                }
            }
        );
        this.props.history.push('/' + this.props.userName.Role);
    }

    renderPatientProfile = () =>
    {
        return(
            <div className="statebasic-nogrid patientstate flex flex-column items-center">
                <PatientProfileForm 
                    className="mt3" 
                    patient={this.props.userProfile[0]} 
                    userName={this.props.userName} 
                    onUpdate={this.onPatientProfileUpdate}
                    onClose={() => this.props.history.push('/' + this.props.userName.Role)}
                />
            </div>
        );
    }

    renderLaboratoryProfile = () =>
    {
        return(
            <div className='statebasic-nogrid labstate flex flex-column items-center'>
                <LabProfile access_token={this.props.access_token}/>
            </div>
        )
    }

    renderPharmacyProfile = () =>
    {
        return(
            <div className='statebasic-nogrid pharmacystate flex flex-column items-center'>
                <PharmacyProfile
                    access_token={this.props.access_token}
                />
            </div>
        )
    }

    renderPhysicianProfile = () =>
    {
        if (this.props.userProfile.length === 0)
            return(
                <div>
                    Loading...
                </div>
            );

        return (
            <div className="statebasic-nogrid physicianstate flex flex-column items-center">
                <PhysicianProfile
                    className='mt3'
                    Physician={this.props.userProfile[0]}
                    onProfileUpdate={this.onPhysicianProfileUpdate}
                    onClose={this.onPhysicianClose}
                />
            </div>
        )
    }

    onPhysicianProfileUpdate = (physician) =>
    {
        Remote.call('physicians.updateOne', 
            this.props.access_token,
            physician,
            (err, res) =>
            {
                if (err)
                {
                    console.log(err);
                    return;
                }

                if (this.props.userName.Name !== physician.Name ||
                    this.props.userName.Email !== physician.Email)
                {
                    this.props.onUserChange(
                        {
                            Name: physician.Name,
                            Email: physician.Email,
                            Role: 'physician',
                            userId: physician._id,
                            InitialPath: '/physician'
                        }
                    );

                    Remote.call(
                        'user.update', this.props.access_token, physician._id, physician.Name, physician.Email
                    );
                }
            }
        );
    }

    onPhysicianClose = () =>
    {
        this.props.history.push('/physician');
    }

    render()
    {
        const user = Meteor.user();

        //console.log('UserProfile',this.props);

        if (user !== undefined)
        {
            switch (user.role)
            {
                case 'patient':
                    return this.renderPatientProfile();
                case 'physician':
                    return this.renderPhysicianProfile();
                case 'laboratory':
                    return this.renderLaboratoryProfile();
                case 'pharmacy':
                    return this.renderPharmacyProfile();
            }
        }

        return(
            <div>Something broke....</div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(
    withTracker(
        ({access_token}) =>
        {
            const user = Meteor.user();

            let profileHandler;
            let loading;

            //console.log('USER', user);

            if (user === undefined)
                return({userProfile: []});

            switch (user.role)
            {
                case 'patient':
                    profileHandler = Remote.subscribe('patients.userProfile',access_token);
                    loading = !profileHandler.ready();

                    return (
                        {userProfile: !loading ? Patients.find({}).fetch() : []}
                    );
                case 'physician':
                    profileHandler = Remote.subscribe('physician.profile', access_token);
                    loading = !profileHandler.ready();

                    return (
                        {userProfile: !loading ? Physicians.find({}).fetch() : []}
                    );
            }

            return ({userProfile: []});
        }
    )
    (UserProfilePage)
);