import React, { Component } from 'react';
import './PhysicianStateLoggedIn.css'
import MessageBox from '../Components/MessageBox/MessageBox'
import YouHaveMessage from '../Components/YouHaveMessage/YouHaveMessage'
import { Meteor } from 'meteor/meteor'
import { withTracker } from 'meteor/react-meteor-data';
import Physicians from '../../api/Lists/Physicians/physicians'
import Patients from '../../api/Lists/Patients/patients';
import Appointments from '../../api/Lists/Appointments/Appointments';
import Messanger from '../Components/ChatBox/Messanger';
import ChatBox from '../Components/ChatBox/ChatBox';
import Chats from '../../api/Lists/Chats/Chats';
import PhysicianGreetingBox from '../Components/PhysicianGreetingBox/PhysicianGreetingBox.jsx'
import PhysicianAppointmentList from '../Components/PhysicianAppointmentList/PhysicianAppointmentList.jsx'
import MedicalRecordPermissions from '../../api/Lists/MedicalRecordPermissions/MedicalRecordPermissions';
import MyPatientsList from '../Components/MyPatientsList/MyPatientsList.js';
import { setPatientMR } from '../Redux/actions';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../translation/IntlMessages';
import Remote from '../Remote';
import StatusSign from '../Components/Utils/StatusSign.jsx';
import { Approved } from '../../api/Lists/GeneralTypes/AuthorizationStatus';
import Laboratories from '../../api/Lists/Laboratories/Laboratories';

const mapStateToProps = state =>
{
    return {patientId: state.patientId, locale: state.locale};
}

const mapDispatchToProps = (dispatch) =>
{
    return {
        onPatientMRIdChange: (patientId) => dispatch(setPatientMR(patientId))
    }
}

class PhysicianStateLoggedIn extends Component 
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            showChatBox: false,
            showMessanger: false,
            showAppointmentList: false,
            showMyPatients: false,
            ChatRecipient: undefined,
            messageBoxMessage: '',
            messageBoxButtons: [],
            appointmentToDelete: undefined,
            showMessagebox: false
        }
    }

    calcNumberOfMessages = () =>
    {
        let cnt = 0;
        this.props.chatList.forEach(
            (chat) =>
            {
                if (chat.unread > 0)
                    cnt++;
            }
        )

        return cnt;
    }

    getStatusClass = () =>
    {
        let profile = undefined;
        if (this.props.profile.length > 0)
        {
            profile = this.props.profile[0];
        }

        return profile === undefined ? '' : 
            profile.AuthorizationStatus === Approved ? '' : 'pointer c-red'
    }

    getMessageList = () =>
    {
        const { chatList, laboratories } = this.props;

        const ret = chatList.map(
            (chat) =>
            {
                const patient = this.props.patients.find((patient) => patient._id === chat.recipient.id);

                let laboratory;
                if (patient === undefined)
                {
                    laboratory = laboratories.find(
                        (lab) => lab._id === chat.recipient.id
                    );
                }

                const userId = patient !== undefined
                    ? patient._id
                    : laboratory._id;

                const image = patient !== undefined
                    ? patient.Avatar
                    : laboratory.Image;

                return(
                    {
                        id: userId,
                        name: chat.recipient.name,
                        image: image,
                        numOfUnread: chat.unread
                    }
                )
            }
        )

        return ret;
    }

    getAppointmentList = () =>
    {
        const { appointments, patients } = this.props;

        const res = appointments.filter(
            (ap) =>
            {
                return ap.DateTime >= new Date()
            }
        ).map(
            (ap) =>
            {
                return (
                    {
                        _id: ap._id,
                        DateTime: ap.DateTime,
                        Name: patients.find((p) => p._id === ap.PatientId).Name
                    }
                )
            }
        )

        return res;
    }

    onChatSelection = (chatObj) =>
    {
        this.setState(
            {
                ChatRecipient:
                {
                    id: chatObj.id,
                    Name: chatObj.name,
                    Role: 'patient'
                },
                showChatBox: true
            }
        )
    }

    onCloseMessanger = () =>
    {
        this.setState({showMessanger: false});
    }

    onChatBoxClose = () =>
    {
        this.setState({showChatBox: false, ChatRecipient: undefined});
    }

    onMessageRead = () =>
    {
        this.setState({showMessanger: !this.state.showMessanger});
    }

    onStatusClick = () =>
    {
        console.log('status click');
    }

    onCloseAppointmentList = () =>
    {
        this.setState({showAppointmentList: false});
    }

    onAppointmentsOpen = () =>
    {
        this.setState({showAppointmentList: true});
    }

    onMyPatientsOpen = () =>
    {
        this.setState({showMyPatients: true});
    }

    onMROpen = (patient) =>
    {
        this.props.onPatientMRIdChange(patient._id);
        this.props.history.push('/medicalrecord');
    }

    onAppointmentDelete = (appointment) =>
    {
        console.log(this.props, appointment);
        this.setState(
            {
                messageBoxMessage: IntlMessages[this.props.locale].intl.formatMessage(
                    {
                        id: 'physician.deleteappointmentmessage',
                        defaultMessage: 'You are about to delete an appointm,ent. Please confirm'
                    }
                ),
                messageBoxButtons: 
                [
                    IntlMessages[this.props.locale].intl.formatMessage(
                        {
                            id: 'physician.accept',
                            defaultMessage: 'Accept'
                        }
                    ),
                    IntlMessages[this.props.locale].intl.formatMessage(
                        {
                            id: 'physician.decline',
                            defaultMessage: 'Decline'
                        }
                    )
                ],
                appointmentToDelete: appointment,
                showMessagebox: true
            }
        )
    }

    onMessageBoxClosed = (element) =>
    {
        if (element !== this.state.messageBoxButtons[0])
        {
            this.setState(
                {
                    showMessagebox: false,
                    messageBoxMessage: '',
                    messageBoxButtons: [],
                    appointmentToDelete: undefined
                }
            );

            return;
        }

        console.log(element, this.state.appointmentToDelete);

        Meteor.call('appointment.remove', 
            this.state.appointmentToDelete._id,
            (err, res) =>
            {
                if (err)
                    console.log(err);
            }
        );

        this.setState(
            {
                showMessagebox: false,
                messageBoxMessage: '',
                messageBoxButtons: [],
                appointmentToDelete: undefined
            }
        );
    }

    render()
    {
        let profile = undefined;
        if (this.props.profile.length > 0)
        {
            profile = this.props.profile[0];
        }

        let appointments = undefined;
        if (this.props.appointments.length > 0)
        {
            appointments = this.props.appointments;
        }

        return(
            <div className="statebasic-nogrid physicianstate flex flex-column items-center">
                <div
                    className='flex items-center justify-between'
                    style={{width: '100%'}}
                >
                    <StatusSign 
                        status=
                        {
                            profile === undefined ? -1 : profile.AuthorizationStatus
                        } 
                    />
                    <YouHaveMessage 
                        className="self-end" 
                        NumOfMessages={this.calcNumberOfMessages()} 
                        onMessagesRead={this.onMessageRead}
                    />
                </div>
                {
                    this.state.showMessagebox &&
                    <MessageBox
                        className='z-index-200 absolute mt6'
                        Message={this.state.messageBoxMessage}
                        Buttons={this.state.messageBoxButtons}
                        onButtonClose={this.onMessageBoxClosed}
                        onButtonCancel={() => this.setState(
                            {
                                showMessagebox: false,
                                messageBoxMessage: '',
                                messageBoxButtons: [],
                                appointmentToDelete: undefined
                            })}
                    />
                }
                {
                    this.state.showChatBox &&
                    <div className="z-index-100 absolute mt6">
                        <ChatBox 
                            className=""
                            senderId={this.props.profile[0]._id}
                            senderRole='physician'
                            senderName={this.props.profile[0].Name}
                            recipientId={this.state.ChatRecipient.id}
                            recipientRole='patient'
                            recipientName={
                                this.state.ChatRecipient.Name
                            }
                            onClose={this.onChatBoxClose}
                        />
                    </div>
                }
                {
                    this.state.showMessanger &&
                    <div className="z-index-10 absolute mt6">
                        <Messanger
                            className=""
                            messageList={this.getMessageList()}
                            role='physician'
                            ownerId={this.props.profile[0]._id}
                            onChatSelection={this.onChatSelection}
                            onCloseChatBox={this.onChatBoxClose}
                            onClose={this.onCloseMessanger}
                        />
                    </div>
                }
                {
                    this.state.showAppointmentList &&
                    <div className='z-index-10 absolute mt6'>
                        <PhysicianAppointmentList
                            className=''
                            appointmentList={this.getAppointmentList()}
                            onClose={this.onCloseAppointmentList}
                            onAppointmentDelete={this.onAppointmentDelete}
                        />
                    </div>
                }
                {
                    this.state.showMyPatients &&
                    <div className='z-index-100 absolute mt6 w-40'>
                        <MyPatientsList
                            className='w-100'
                            patients={this.props.patients}
                            permissions={this.props.permissions}
                            onClose={() => this.setState({showMyPatients: false})}
                            onMROpen={this.onMROpen}
                        />
                    </div>
                }
                <PhysicianGreetingBox 
                    className='w-80'
                    Physician={profile}
                    NumOfAppointments={
                        appointments !== undefined ? appointments.length : 0}
                />
                <div className='flex justify-around items-center'>
                    <button
                        className='pb3 pt3 pl4 pr4 mr2 bigbutton shadow-3 br-pill pointer'
                        onClick={this.onAppointmentsOpen}
                    >
                        <FormattedMessage
                            id='physicianstate.showappointments'
                            defaultMessage='Show Appointments'
                            description='Show Appointments'
                        />
                    </button>
                    <button
                        className='pb3 pt3 pl4 pr4 ml2 bigbutton shadow-3 br-pill pointer c-white'
                        onClick={this.onMyPatientsOpen}
                    >
                        <FormattedMessage
                            id='physicianstate.mypatients'
                            defaultMessage='My Patients'
                            description='My Patients'
                        />
                    </button>
                </div>
            </div>
        )
    }
}

export default 
withRouter(
    connect(mapStateToProps, mapDispatchToProps)
    (
        withTracker(
            ({access_token, patientIds}) =>
            {
                const profileHandler = Remote.subscribe('physician.profile',
                    access_token
                );
                const profileLoading = !profileHandler.ready();
                const myPatientsHandler = Remote.subscribe('patients.ids',patientIds);
                const patientsLoading = !myPatientsHandler.ready();
                const appointmentHandler = Meteor.subscribe('appointments.physician');
                const appointmentLoading = !appointmentHandler.ready();
                const chatHandle = Meteor.subscribe('user.chats');
                const loadingChat = !chatHandle.ready();
                const permissionsHandle = Remote.subscribe('medicalrecordpermissions.physicianTotal',access_token);
                const loadingPermissions = !permissionsHandle.ready();
                const laboratoriesHandle = Remote.subscribe('laboratories.admin');
                const loadingLaboratories = !laboratoriesHandle.ready();

                return(
                    {
                        profile: !profileLoading ? Physicians.find({}).fetch() : [],
                        appointments: !appointmentLoading ? Appointments.find({}).fetch() : [],
                        chatList: !loadingChat ? Chats.find({}).fetch() : [],
                        patients: !patientsLoading ? Patients.find({}).fetch() : [],
                        permissions: !loadingPermissions ? MedicalRecordPermissions.find({}).fetch() : [],
                        laboratories: !loadingLaboratories ? Laboratories.find({}).fetch() : []
                    }
                )
            }
        )
    (PhysicianStateLoggedIn)
    )
);