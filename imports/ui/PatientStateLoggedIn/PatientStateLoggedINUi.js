import React from 'react'
import './PatientStateLoggedInUi.css'
import MessageBox from '../Components/MessageBox/MessageBox'
import YouHaveMessage from '../Components/YouHaveMessage/YouHaveMessage'
import PatientGreetingBox from '../Components/PatientGreetingBox/PatientGreetingBox';
import DoctorList from '../Components/DoctorList/DoctorList';
import { Meteor } from 'meteor/meteor'
import { withTracker } from 'meteor/react-meteor-data';
import { withRouter } from 'react-router-dom';
import Physicians from '../../api/Lists/Physicians/physicians'
import Patients from '../../api/Lists/Patients/patients';
import Appointments from '../../api/Lists/Appointments/Appointments';
import AppointmentHandler from '../Components/AppointmentHandler/AppointmentHandler';
import Messanger from '../Components/ChatBox/Messanger';
import ChatBox from '../Components/ChatBox/ChatBox';
import Chats from '../../api/Lists/Chats/Chats';
import MedicalRecordPermissions from '../Components/MedicalRecordPermissions/MedicalRecordPermissions.jsx'
import { FormattedMessage } from 'react-intl';
import { IntlMessages } from '../../../translation/IntlMessages';
import { connect } from 'react-redux';
import Remote from '../Remote';
import { Text } from '../../api/Lists/Messages/MessagesSchema';
import Laboratories from '../../api/Lists/Laboratories/Laboratories';

const mapStateToProps = (state) =>
{
    return {locale: state.locale, access_token: state.access_token}
}

class PatientStateLoggedInUi extends React.Component
{
    constructor()
    {
        super();
        this.state = {
            MessageBoxShown: false,
            MessageBoxButtonClicked: 'None',
            MessageBoxMessage: '',
            MessageBoxButtons: [],
            AppointmentSelected: undefined,
            ChatRecipient: undefined,
            PermissionsToPhysicianId: '',
            confirmRequest: '',
            showDoctorList: false,
            showAppointmentForm: false,
            showChatBox: false,
            showMessanger: false,
            showMedicalRecordPermissions: false
        }
    }

    handleAppointmentUpdate = (appointment, element) =>
    {
        if (element !== 'Press to cancel')
            return;

        Meteor.call('appointment.remove', appointment._id);
    }

    messageBoxClosed = (element) =>
    {
        this.setState(
            {
                MessageBoxShown : false, 
                MessageBoxButtonClicked: element
            }
        );

        if (this.state.AppointmentSelected !== undefined)
        {
            this.handleAppointmentUpdate(this.state.AppointmentSelected, element);
            this.setState({AppointmentSelected: undefined});
        }

        if (this.state.confirmRequest === 'permissions')
        {
            if (element === 'Agree')
            {
                this.setState({showMedicalRecordPermissions: true});
            }
        }
    }

    messageBoxCanceled = () =>
    {
        this.setState(
            {
                MessageBoxShown: false,
                MessageBoxButtonClicked: 'None'
            })
    }

    onMessageRead = () =>
    {
        this.setState({showMessanger: !this.state.showMessanger});
    }

    onSearchPhysicianClick = () =>
    {
        this.setState({showDoctorList: !this.state.showDoctorList});
    }

    onDoctorSelection = (physician) =>
    {
        this.setState(
            {
                showDoctorList: false
            }
        )
        
        Remote.call('patient.ConnectToPhysician', 
            this.props.access_token,
            physician._id
        );
    }

    onDoctorListClose = () =>
    {
        this.setState({showDoctorList: false});
    }

    getConnectedPhysicianName = () =>
    {
        const {physicians, profile} = this.props;
        const physician = physicians.find((physician) => physician._id === profile[0].ConnectedPhysician);

        return physician === undefined ? '' : physician.Name;
    }

    getConnectedPhysician = () =>
    {
        const {physicians, profile} = this.props;
        const physician = physicians.find((physician) => physician._id === profile[0].ConnectedPhysician);

        return physician;
    }

    isPhysicianConnected = () =>
    {
        const { profile } = this.props;
        if (profile.length < 1)
            return false;

        if (profile[0].ConnectedPhysician === undefined || profile[0].ConnectedPhysician === '')
            return false;

        return true;
    }

    onSetAppointment = () =>
    {
        this.setState({showAppointmentForm: true});
    }

    onAppointmentFormClose = () =>
    {
        this.setState({showAppointmentForm: false});
    }

    onAppointmentSet = (date) =>
    {
        console.log('Date', date);
        Meteor.call(
            'appointment.create', 
            this.props.profile[0]._id, 
            this.props.profile[0].ConnectedPhysician, 
            date
        );

        this.setState({showAppointmentForm: false});
    }

    getAppointmentMessage = (appointment) =>
    {
        const dateFormat = appointment.DateTime.toLocaleString(
            this.props.locale,
            {
                weekDay: 'long',
                day: 'numeric',
                month: 'long',
                year: 'numeric',
                hour: 'numeric',
                minute: '2-digit'
            }
        );

        if (this.props.physicians.length === 0)
        {
            return '';
        }

        const physicianName = 
            this.props.physicians.find(
                (ph) => ph._id === appointment.PhysicianId
            ).Name;

        const res = dateFormat + ' ' +
            IntlMessages[this.props.locale].intl.formatMessage(
                {
                    id: 'patientstate.with',
                    defaultMessage: 'with',
                    description: 'with'
                }
            ) + ' ' + physicianName;

        return res;
    }

    getMessageList = () =>
    {
        const { chatList, physicians, laboratories } = this.props;

        const ret = chatList.map(
            (chat) =>
            {
                const physician = physicians.find(
                    (ph) =>
                    {
                        return(ph._id === chat.recipient.id)
                    }
                );

                let laboratory;
                if (!physician)
                {
                    laboratory = laboratories.find(
                        (lab) => lab._id === chat.recipient.id
                    );
                }

                const userId = physician !== undefined
                    ? physician._id
                    : laboratory._id;

                const image = physician !== undefined
                ? physician.Image
                : laboratory.Image;

                return(
                    {
                        id: userId,
                        name: chat.recipient.name,
                        image: image,
                        numOfUnread: chat.unread
                    }
                )
            }
        )

        return ret;
    }

    calsNumberOfMessages = () =>
    {
        let cnt = 0;
        this.props.chatList.forEach(
            (chat) =>
            {
                if (chat.unread > 0)
                    cnt++;
            }
        )

        return cnt;
    }

    onAppointmentSelect = (appointment) =>
    {
        const message = this.getAppointmentMessage(appointment);

        this.setState(
            {
                AppointmentSelected: appointment,
                MessageBoxShown: true,
                MessageBoxMessage: message,
                MessageBoxButtons: ['Press to cancel']
            }
        )
    }

    onMessagePhysician = () =>
    {
        this.setState(
            {
                ChatRecipient: 
                {
                    id: this.props.profile[0].ConnectedPhysician,
                    Name: this.props.physicians.find((ph) => ph._id === this.props.profile[0].ConnectedPhysician).Name,
                    Role: 'physician'
                },
                showChatBox: true
            }
        )
        this.setState({showChatBox: true});
    }

    onChatBoxClose = () =>
    {
        this.setState(
            {
                showChatBox: false,
                ChatRecipient: undefined
            }
        )
    }

    onChatSelection = (chatObj) =>
    {
        this.setState(
            {
                ChatRecipient:
                {
                    id: chatObj.id,
                    Name: chatObj.name,
                    Role: 'physician'
                },
                showChatBox: true
            }
        )
    }

    onCloseChatBox = () =>
    {
        this.setState(
            {
                showChatBox: false,
                ChatRecipient: undefined
            }
        )
    }

    onCloseMessanger = () =>
    {
        this.setState({showMessanger: false})
    }

    onShareMedicalRecord = (physicianId, physicianName) =>
    {
        this.setState(
            {
                MessageBoxShown: true,
                PermissionsToPhysicianId: physicianId,
                MessageBoxMessage: IntlMessages[this.props.locale].intl.formatMessage(
                    {
                        id: 'patient.msgsharepermission',
                        defaultMessage: 'You are about to share your private medical data. Do you agree ?'
                    }
                ),
                MessageBoxButtons: 
                [
                    IntlMessages[this.props.locale].intl.formatMessage(
                        {
                            id: 'patient.agree',
                            defaultMessage: 'Agree'
                        }
                    ),
                    IntlMessages[this.props.locale].intl.formatMessage(
                        {
                            id: 'patient.cancel',
                            defaultMessage: 'Cancel'
                        }
                    )
                ],
                confirmRequest: 'permissions'
            }
        );
    }

    onMedicalPermissionsApply = (permissions) =>
    {
        // console.log(permissions);
        // console.log(this.state);
        // console.log(this.props);
        //return;

        Remote.call('medicalrecord.createIfNotExists', this.props.access_token);

        Remote.call('medicalrecord.addPhysician',
            this.props.access_token,
            this.props.profile[0]._id,
            this.state.PermissionsToPhysicianId
        );

        Remote.call(
            'medicalrecordpermissions.insertOrUpdate',
            this.props.access_token,
            this.props.profile[0]._id,
            this.state.PermissionsToPhysicianId,
            permissions
        );

        this.setState({showMedicalRecordPermissions: false, confirmRequest: ''});

        const physicianName = this.props.physicians.find((p) => p._id === this.state.PermissionsToPhysicianId).Name;

        Meteor.call(
            'message.send',
            {
                from: 
                {
                    id: this.props.profile[0]._id, 
                    name: this.props.profile[0].Name,
                    role: 'patient'
                },
                to: 
                {
                    id: this.state.PermissionsToPhysicianId, 
                    name: physicianName,
                    role: 'physician'
                },
                content: 
                {
                    msgType: Text,
                    message: IntlMessages[this.props.locale].intl.formatMessage(
                        {
                            id: 'patient.accessgrant',
                            defaultMessage: "YOU HAVE GOT ACCESS TO PATIENT'S MEDICAL RECORD"
                        }
                    )
                },
                datetime: new Date(),
                unread: 1
            }
        )

        // Redirect to medical record page
        //this.props.history.push('/medicalrecord');
    }

    onMedicalPermissionsClose = () =>
    {
        //console.log('close');
        this.setState({showMedicalRecordPermissions: false, confirmRequest: ''})
    }

    physicianHasAvailabilitySet = () =>
    {
        const physicianId = this.props.profile[0].ConnectedPhysician;
        const physician = this.props.physicians.find((ph) => ph._id == physicianId);

        //console.log()

        return (
            physician === undefined ||
            physician.Availability === undefined ||
            physician.Availability.length === 0
        )
    }

    renderPhysicianConnected()
    {
        /* f-greatvibes-40 */
        return(
            <div className='flex items-center'>
                <div className="tc f-kalam-30 c-deepblue br4 shadow-6 bg-lightblue mr2 pa3">
                    <div className="">
                        <div>
                            {
                                this.getConnectedPhysicianName() + ' ' + 
                                IntlMessages[this.props.locale].intl.formatMessage(
                                    {
                                        id: 'patientstate.chosen',
                                        defaultMessage: 'is your chosen physician'
                                    }
                                )
                                
                            }
                        </div>
                        <div>
                            <FormattedMessage
                                id='patentstate.initiate'
                                defaultMessage='Initiate a contact with him'
                                description='Initiate a contact with him'
                            />
                        </div>
                    </div>
                    <div 
                        className="pointer dib f-link-16 bg-lightblue"
                        onClick={this.onSearchPhysicianClick}
                    >
                        <FormattedMessage
                            id='patentstate.change'
                            defaultMessage='Click here to change physician'
                            description='Click here to change physician'
                        />
                    </div>
                </div>
                <div className="flex flex-column items-center justify-between ml2">
                    <button
                        className="pb3 pt3 pl4 pr4 bigbutton shadow-6 br-pill pointer mb2"
                        onClick={this.onMessagePhysician}
                    >
                        <FormattedMessage
                            id='patentstate.message'
                            defaultMessage='Message Physician'
                            description='Message Physician'
                        />
                    </button>
                    <button
                        className="pb3 pt3 pl4 pr4 bigbutton shadow-6 br-pill pointer mt2 mb2"
                        onClick={this.onSetAppointment}
                        disabled={this.physicianHasAvailabilitySet()}
                    >
                        <FormattedMessage
                            id='patentstate.setappointment'
                            defaultMessage='Set An Appointment'
                            description='Set An Appointment'
                        />
                    </button>
                    <button
                        className="pb3 pt3 pl4 pr4 bigbutton shadow-6 br-pill pointer mt2"
                        onClick=
                        {
                            () => this.onShareMedicalRecord(
                                this.props.profile[0].ConnectedPhysician,
                                ''
                            )
                        }
                    >
                        <FormattedMessage
                            id='patient.sharemedicalrecord'
                            defaultMessage='Share Medical Record'
                        />
                    </button>
                </div>
            </div>
        );

    }

    renderPhysicianDisconnected()
    {
        return(
            <div className="">
                <button 
                    className="bigbutton pt4 pb4 pl5 pr5 shadow-6 br-pill pointer"
                    onClick={this.onSearchPhysicianClick}
                >
                    <FormattedMessage
                        id='patentstate.search'
                        defaultMessage='Search Physician'
                        description='Search Physician'
                    />
                </button>
            </div>
        )
    }

    render()
    {
        return (
            <div className="statebasic-nogrid patientstate flex flex-column items-center">
                <YouHaveMessage 
                    className="self-end" 
                    NumOfMessages={this.calsNumberOfMessages()} 
                    onMessagesRead={this.onMessageRead}
                />
                { 
                    this.state.MessageBoxShown &&
                    <div className="z-index-200 absolute mt7">
                        <MessageBox 
                            className="ma2"
                            Message={this.state.MessageBoxMessage}
                            Buttons={this.state.MessageBoxButtons}
                            onButtonClose={this.messageBoxClosed}
                            onButtonCancel={this.messageBoxCanceled}
                        />
                    </div>
                }
                {
                    this.state.showDoctorList &&
                    <div className="z-index-10 absolute mt6">
                        <DoctorList 
                            DoctorList={this.props.physicians}
                            SelectOption={true}
                            onClose={this.onDoctorListClose}
                            onSelected={this.onDoctorSelection}
                        />
                    </div>
                }
                {
                    this.state.showAppointmentForm &&
                    <div className="z-index-10 absolute mt6">
                        <AppointmentHandler 
                            className=""
                            Physician={this.getConnectedPhysician()}
                            onAppointmentSet={this.onAppointmentSet}
                            onClose={this.onAppointmentFormClose}
                        />
                    </div>
                }
                {
                    this.state.showChatBox &&
                    <div className="z-index-100 absolute mt6">
                        <ChatBox 
                            className=""
                            senderId={this.props.profile[0]._id}
                            senderRole='patient'
                            senderName={this.props.profile[0].Name}
                            recipientId={this.state.ChatRecipient.id}
                            recipientRole='physician'
                            recipientName={
                                this.state.ChatRecipient.Name
                            }
                            onClose={this.onChatBoxClose}
                            onShareMedicalRecord={this.onShareMedicalRecord}
                        />
                    </div>
                }
                {
                    this.state.showMessanger &&
                    <div className="z-index-10 absolute mt6">
                        <Messanger
                            className=""
                            messageList={this.getMessageList()}
                            role='patient'
                            ownerId={this.props.profile[0]._id}
                            onChatSelection={this.onChatSelection}
                            onCloseChatBox={this.onCloseChatBox}
                            onClose={this.onCloseMessanger}
                        />
                    </div>
                }
                {
                    this.state.showMedicalRecordPermissions &&
                    <div className='z-index-100 absolute mt5'>
                        <MedicalRecordPermissions 
                            onApply={this.onMedicalPermissionsApply}
                            onClose={this.onMedicalPermissionsClose}
                        />
                    </div>
                }
                <div className="flex flex-column items-center justify-start">
                    <PatientGreetingBox 
                        className="z-index-1 mb3 pa4 br4 shadow-6" 
                        appointmentList={this.props.appointments.map((ap) => {return {msg: this.getAppointmentMessage(ap), ap: ap}})}
                        onAppointmentSelect={this.onAppointmentSelect}
                    /> 
                    {
                        this.isPhysicianConnected() 
                        ?
                            this.renderPhysicianConnected()
                        :
                            this.renderPhysicianDisconnected()
                    }
                </div>
            </div>
        );
    }
}

export default withTracker(
    ({access_token}) =>
    {
        //console.log(access_token);
        const physiciansHandle = Remote.subscribe('physicians');
        const patientHandle = Remote.subscribe('patients.userProfile',access_token);
        const appointmentHandle = Meteor.subscribe('appointments.patient');
        const chatHandle = Meteor.subscribe('user.chats');
        const loadingPhysicians = !physiciansHandle.ready();
        const loadingProfile = !patientHandle.ready();
        const loadingAppointments = !appointmentHandle.ready();
        const loadingChat = !chatHandle.ready();
        const laboratoriesHandle = Remote.subscribe('laboratories.admin');
        const loadingLaboratories = !laboratoriesHandle.ready();

        return(
            {
                physicians: !loadingPhysicians ? Physicians.find({}).fetch() : [],
                profile: !loadingProfile ? Patients.find({}).fetch(): [],
                appointments: !loadingAppointments ? Appointments.find({}).fetch(): [],
                chatList: !loadingChat ? Chats.find({}).fetch() : [],
                laboratories: !loadingLaboratories ? Laboratories.find({}).fetch() : []
            }
        )
    }
)
(withRouter(connect(mapStateToProps)(PatientStateLoggedInUi)));