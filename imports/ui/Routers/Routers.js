import React from 'react'
import 
    {
        Switch,
        Route,
        Redirect
    }
from 'react-router-dom'
import BeforeSignInState from '../States/BeforeSignInState'
import PatientStateLoggedIn from '../States/PatientStateLoggedIn'
import PhysicianState from '../States/PhysicianState'
import AdminState from '../States/AdminState'
import LabState from '../States/LabState'
import RegisterState from '../States/RegisterState'
import UserProfilePage from '../UserProfilePage/UserProfilePage'
import MedicalRecordPage from '../MedicalRecord/MedicalRecordPage.jsx';
import LoggingInPage from '../LoggingInPage/LoggingInPage.jsx';
import { connect } from 'react-redux'
import PharmacyState from '../States/PharmacyState';
import SmartRoute from './SmartRoute';

/*
initialPath
 */

const mapStateToProps = state =>
{
    return {access_token: state.access_token, userName: state.userName}
}

const Routers = (props) =>
{
    //console.log(props);
    return (
        <Switch>
            <Route
                exact path='/'
                render=
                {
                    (inProps) =>
                    {
                        if (Meteor.userId() === null)
                        {
                            return(<BeforeSignInState />)
                        }
                        else
                        {
                            if (props.userData === undefined)
                                return(<div>Loading...</div>);
                            
                            switch (props.userData.role)
                            {
                                case 'patient':
                                    return(
                                        <Redirect 
                                            to='/patient'
                                        />
                                    );
                                case 'physician':
                                    return(
                                        <Redirect
                                            to='/physician'
                                        />
                                    );
                                case 'admin':
                                    return(
                                        <Redirect
                                            to='/admin'
                                        />
                                    );
                                case 'pharmacy':
                                    return(
                                        <Redirect
                                            to='/pharmacy'
                                        />
                                    );
                                case 'laboratory':
                                    return(
                                        <Redirect
                                            to='/laboratory'
                                        />
                                    )
                            }
                        }
                    }
                }
            />
            <Route exact path="/">
                <BeforeSignInState />
            </Route>
            <Route exact path='/_oauth/hsauth2server'>
                <LoggingInPage />
            </Route>
            <Route exact path="/register">
                <RegisterState />
            </Route>
            <SmartRoute 
                exact path="/patient" 
                component={PatientStateLoggedIn} 
                userData={props.userData}
            />
            <SmartRoute 
                exact path="/physician" 
                component={PhysicianState} 
                userData={props.userData}
            />
            <SmartRoute 
                exact path="/profile" 
                component={UserProfilePage} 
                access_token={props.access_token}
                userData={props.userData}
            />
            <SmartRoute 
                exact path='/medicalrecord' 
                component={MedicalRecordPage}
                userData={props.userData}
            />
            <SmartRoute
                exact path='/admin' 
                component={AdminState}
                userData={props.userData}
            />
            <SmartRoute
                exact path='/laboratory'
                component={LabState}
                userData={props.userData}
            />
            <SmartRoute
                exact path='/pharmacy'
                component={PharmacyState}
                userData={props.userData}
            />
        </Switch>
    );
}

export default connect(mapStateToProps)(Routers);