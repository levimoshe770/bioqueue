const AuthorizationTable =
[
    {
        role: 'patient',
        pages: ['/patient','/profile','/medicalrecord']
    },
    {
        role: 'physician',
        pages: ['/physician','/profile','/medicalrecord']
    },
    {
        role: 'admin',
        pages: ['/admin']
    },
    {
        role: 'laboratory',
        pages: ['/laboratory','/profile']
    },
    {
        role: 'pharmacy',
        pages: ['/pharmacy','/profile']
    }
]

export default AuthorizationTable;