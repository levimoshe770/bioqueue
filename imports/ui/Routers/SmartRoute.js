import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import AuthorizationTable from './AuthorizationTable';
import { connect } from 'react-redux';
import { setUserLoggedIn, setAccessToken } from '../Redux/actions';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { LOGINSERVICE } from '../../api/LoginService/LoginService';
import Users from '../../api/Lists/Users/users';

const mapStateToProps = (state) =>
{
    return(
        {
            userName: state.userName,
            access_token: state.access_token
        }
    )
}

const mapDispatchToProps = (dispatch) =>
{
    return (
        {
            onUserChange: (userName) => dispatch(setUserLoggedIn(userName)),
            onTokenChange: (token) => dispatch(setAccessToken(token))
        }
    );
}

const getDefaultPage = (role) =>
{
    const match = AuthorizationTable.find(
        (pair) =>
        {
            return pair.role === role;
        }
    )

    if (match === undefined)
        return ('/');

    return match.pages[0];
}

const getAccessToken = (search) =>
{
    const urlSearchParams = new URLSearchParams(search);

    if (urlSearchParams.has('token'))
        return urlSearchParams.get('token');

    return undefined;
}

const UpdateGlobalData = (userId, rest) =>
{
    Meteor.call('user.getUserData', userId,
        (err, user) =>
        {
            if (err)
            {
                console.error(err);

                return;
            }

            rest.onUserChange(
                {
                    Name: user.name,
                    Email: user.emails[0].address,
                    Role: user.role,
                    userId: user._id,
                    InitialPath: '/' + user.role
                }
            );

            rest.onTokenChange(user.services.hsloginservice.access_token);
        }
    );
}

const LoginWithToken = (token, rest) =>
{
    Meteor.call('getUserData', token,
        (err, userData) =>
        {
            if (err)
            {
                console.error(err);
                return;
            }

            Accounts.callLoginMethod(
                {
                    methodArguments:
                    [
                        {
                            custom: LOGINSERVICE,
                            token: token,
                            user: 
                            {
                                _id: userData._id,
                                email: userData.emails[0].address,
                                name: userData.name,
                                role: userData.role
                            }
                        }
                    ],
                    validateResult:
                        (res) => 
                        {
                            UpdateGlobalData(res.id, rest);
                            console.log(res);
                            return true;
                        },
                    userCallback: (err) => console.log(err)
                }
            )
        }
    );
}

const userAuthorizedToPage = (role, page) =>
{
    const match = AuthorizationTable.find(
        (pair) =>
        {
            return (pair.role === role) && (pair.pages.includes(page))
        }
    );

    return match !== undefined;
}

const SmartRoute = ({component: Component, ...rest}) =>
{
    return(
        <Route
            {...rest}
            render=
            {
                (props) =>
                {
                    const userId = Meteor.userId();

                    // If a user is logged in
                    if (userId !== null)
                    {
                        // TODO: Currently, if access token is provided, it will 
                        // TODO: be ignored, but finally we need to take care of it

                        // If redux is unset, set redux
                        if (rest.userName.userId === null ||
                            rest.userName.userId === '')
                        {
                            UpdateGlobalData(userId, rest);
                        }

                        if (rest.userData === undefined)
                            return(<div>Loading...</div>)

                        const defaultPage = getDefaultPage(rest.userData.role);

                        if (userAuthorizedToPage(rest.userData.role, rest.path))
                            return(<Component {...props} {...rest}/>);
                        else
                            return(<Redirect to={defaultPage}/>);

                    }
                    else
                    {
                        // If an access token is set as param to the page
                        const token = getAccessToken(props.location.search);
                        if (token !== undefined)
                        {
                            // Login according to token
                            console.log('got token', token);

                            LoginWithToken(token, rest);

                            return (<Component {...props} {...rest}/>);
                        }
                    }

                    return (<Redirect to='/'/>)
                }
            }
        />
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(SmartRoute);