import React, { Component } from 'react';
import './AdminPage.css'
import YouHaveMessage from '../Components/YouHaveMessage/YouHaveMessage';
import AdminApprovals from '../Components/AdminApprovals/AdminApprovals.jsx';
import { FormattedMessage } from 'react-intl';

class AdminPage extends Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            showApprovals: false
        }
    }

    calcNumberOfMessages = () =>
    {
        return 0;
    }

    render()
    {
        return(
            <div className='statebasic-nogrid adminstate flex flex-column items-center'>
                <YouHaveMessage 
                    className="ma2 self-end" 
                    NumOfMessages={this.calcNumberOfMessages()} 
                    onMessagesRead={() => console.log('read messages')}
                />                
                {
                    this.state.showApprovals &&
                    <AdminApprovals
                        className='absolute ma6'
                        onClose={() => this.setState({showApprovals: false})}
                    />
                }
                <button
                    className='bigbutton pt4 pb4 pl5 pr5 shadow-6 br-pill pointer mt6'
                    onClick={() => this.setState({showApprovals: true})}
                >
                    <FormattedMessage
                        id='adminpage.approvals'
                        defaultMessage='Approvals'
                        description='Approvals'
                    />
                </button>
                <button
                    className='bigbutton pt4 pb4 pl5 pr5 shadow-6 br-pill pointer mt5'
                    disabled={true}
                >
                    <FormattedMessage
                        id='adminpage.usermanagement'
                        defaultMessage='User Management'
                        description='User Management'
                    />
                </button>
            </div>
        )
    }
}

export default AdminPage;