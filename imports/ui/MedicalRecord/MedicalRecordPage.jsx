import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import MedicalRecord from '../Components/MedicalRecord/MedicalRecord.jsx';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import ChatBox from '../Components/ChatBox/ChatBox.js';
import Patients from '../../api/Lists/Patients/patients';
import './MedicalRecordPage.css'

const mapStateToProps = (state) =>
{
    return {userName: state.userName, patientId: state.patientId, access_token: state.access_token}
}

class MedicalRecordPage extends Component
{
    constructor(props)
    {
        super(props);
        this.state =
        {
            showChatBox: false,
            patientName: '',
            physicianId: ''
        }
    }

    getStateClass = (role) =>
    {
        return role === 'patient' ? 'patientrecord' : 'physicianrecord'
    }

    onClose = (role) =>
    {
        this.props.history.push('/' + role);
    }

    componentDidMount()
    {
        if (this.props.userName.Role === 'physician')
        {
            const patient = Patients.findOne({_id: this.props.patientId});
            
            this.setState(
                {
                    patientName: patient.Name
                }
            );

            Meteor.call('user.getRealUserId',
                (err, id) =>
                {   
                    if (err)
                    {
                        console.log(err);
                        return;
                    }

                    this.setState({physicianId: id});
                }
            );
        }
    }

    render()
    {
        const user = Meteor.user();
        // console.log(this.props);
        // console.log(user);
        return(
            <div className={'statebasic-nogrid flex flex-column items-center ' + this.getStateClass(user === undefined ? '' : user.role)}>
                {
                    this.state.showChatBox && user.role === 'physician' &&
                    <div className='z-index-100 absolute mt6'>
                        <ChatBox
                            className=''
                            senderId={this.state.physicianId}
                            senderRole='physician'
                            senderName={user.name}
                            recipientId={this.props.patientId}
                            recipientRole='patient'
                            recipientName={this.state.patientName}
                            onClose={() => this.setState({showChatBox: false})}
                            onShareMedicalRecord={() => {}}
                        />
                    </div>
                }
                <MedicalRecord
                    className='w-90'
                    patientId={this.props.patientId}
                    callingRole={user === undefined ? '' : user.role}
                    onClose={() => this.onClose(user.role)}
                    access_token={this.props.access_token}
                    onChatBoxOpen={() => this.setState({showChatBox: true})}
                />
            </div>
        )
    }
}

export default connect(mapStateToProps)(withRouter(MedicalRecordPage));