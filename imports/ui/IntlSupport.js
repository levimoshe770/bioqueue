import React from 'react'
import {IntlProvider} from 'react-intl'
import App from './App'
import { connect } from 'react-redux';
import { ConfigProvider } from 'antd';
import { IntlMessages } from '../../translation/IntlMessages';

const mapStateToProps = state =>
{
    return {locale: state.locale}
}

const IntlSupport = (props) =>
{
    //console.log(props);
    return(
        <IntlProvider 
            locale={props.locale} 
            messages={IntlMessages[props.locale].messages}
        >
            <App />
        </IntlProvider>
    )
}

export default connect(mapStateToProps)(IntlSupport);