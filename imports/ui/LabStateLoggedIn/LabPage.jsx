import React, { Component } from 'react'
import { withTracker } from 'meteor/react-meteor-data';
import { IntlMessages } from '../../../translation/IntlMessages';
import { connect } from 'react-redux';
import YouHaveMessage from '../Components/YouHaveMessage/YouHaveMessage';
import './LabPage.css'
import Laboratories from '../../api/Lists/Laboratories/Laboratories';
import LabGreetingBox from '../Components/LabGreetingBox/LabGreetingBox.jsx'
import LabOrderList from '../Components/LabOrderList/LabOrderList.jsx';
import LabTestsCollection from '../../api/Lists/LabTests/LabTestsCollection';
import LabTestDetails from '../Components/LabTestDetails/LabTestDetails.jsx';
import StatusSign from '../Components/Utils/StatusSign.jsx';
import Remote from '../Remote';
import { StaticRouter } from 'react-router-dom';

const mapStateToProps = (state) =>
{
    return {locale: state.locale}
}

class LabPage extends Component
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            showTestDetails: false,
            selectedTestId: '',
            selectedPatient: '',
            selectedPhysician: ''
        }
    }

    calcNumOfNew = () =>
    {
        const { labTests } = this.props;

        const res = labTests.filter((lt) => lt.Status === 1).length;

        return res;
    }

    calcNumOfPending = () =>
    {
        const { labTests } = this.props;

        const res = labTests.filter((lt) => lt.Status === 2).length;

        return res;
    }

    onDetailsRequest = (testId, patientName, physicianName) =>
    {
        this.setState(
            {
                showTestDetails: true,
                selectedTestId: testId,
                selectedPatient: patientName,
                selectedPhysician: physicianName
            }
        );
    }

    render()
    {
        //console.log(this.props);
        return(
            <div className='statebasic-nogrid labstate flex flex-column items-center'>
                <div 
                    className='flex items-center justify-between'
                    style={{width: '100%'}}
                >
                    <StatusSign
                        status=
                        {
                            this.props.profile.length === 0 ? -1 : this.props.profile[0].AuthorizationStatus
                        }
                    />
                    <YouHaveMessage
                        className='ma2'
                        NumOfMessages='0'
                        onMessagesRead={() => console.log('read')}
                    />
                </div>
                {
                    this.state.showTestDetails &&
                    <LabTestDetails
                        className='absolute'
                        testId={this.state.selectedTestId}
                        patientName={this.state.selectedPatient}
                        physicianName={this.state.selectedPhysician}
                        lab={this.props.profile[0].Name}
                        onClose=
                        {
                            () => this.setState(
                                {showTestDetails: false, selectedTestId: ''}
                            )
                        }
                    />
                }
                <LabGreetingBox
                    className=''
                    profile={this.props.profile}
                    numOfPending={this.calcNumOfPending()}
                    numOfNewOrders={this.calcNumOfNew()}
                />
                <LabOrderList
                    className='mt4'
                    orderList={this.props.labTests}
                    onDetailsRequest={this.onDetailsRequest}
                />
            </div>
        )
    }
}

export default 
connect(mapStateToProps)(
    withTracker(
        ({access_token}) =>
        {
            const profileHandle = Remote.subscribe('laboratories.myLab', access_token);
            const profileLoading = !profileHandle.ready();
            const testHandle = Remote.subscribe('labtest.laboratory', access_token);
            const testLoading = !testHandle.ready();

            return(
                {
                    profile: !profileLoading ? Laboratories.find({}).fetch() : [],
                    labTests: !testLoading ? LabTestsCollection.find({}).fetch() : []
                }
            )
        }
    )
    (LabPage)
);