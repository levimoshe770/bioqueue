import React from 'react'
import PatientStateLoggedInUi from '../PatientStateLoggedIn/PatientStateLoggedINUi'
import { connect } from 'react-redux';

const mapStateToProps = (state) =>
{
    return {access_token: state.access_token}
}

class PatientStateLoggedIn extends React.Component
{
    render()
    {
        return (
            <PatientStateLoggedInUi access_token={this.props.access_token}/>
        );
    }
}

export default connect(mapStateToProps)(PatientStateLoggedIn);