import React from 'react'
import LabPage from '../LabStateLoggedIn/LabPage.jsx';
import { connect } from 'react-redux';

const mapStateToProps = (state) =>
{
    return ({access_token: state.access_token});
}

class LabState extends React.Component
{
    render()
    {
        return (
            <LabPage access_token={this.props.access_token} />
        );
    }
}

export default connect(mapStateToProps)(LabState);