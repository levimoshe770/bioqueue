import React from 'react'
import PhysicianStateLoggedIn from '../PhysicianStateLoggedIn/PhysicianStateLoggedIn';
import { connect } from 'react-redux';
import { Meteor } from 'meteor/meteor';

const mapStateToProps = (state) =>
{
    return {access_token: state.access_token}
}

class PhysicianState extends React.Component
{
    constructor()
    {
        super();
        this.state = 
        {
            ids: []
        }
    }

    componentDidMount()
    {
        Meteor.call('physician.myPatients',
            (err, ids) =>
            {
                if (err)
                {
                    console.log(err);
                    return;
                }

                this.setState({ids: ids});
            }
        );
    }

    render()
    {
        return(
            <div>
                <PhysicianStateLoggedIn 
                    access_token={this.props.access_token} 
                    patientIds={this.state.ids}
                />
            </div>
        );
    }
}

export default connect(mapStateToProps)(PhysicianState);