import React from 'react'
import SignInStateUi from '../SignInState/SignInStateUi'
import {withRouter} from 'react-router-dom'

class SignInState extends React.Component
{
    render()
    {
        return (
            <SignInStateUi history={this.props.history}/>
        );
    }
}

export default withRouter(SignInState);