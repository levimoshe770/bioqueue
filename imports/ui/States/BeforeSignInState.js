import React from 'react'
import BeforeSignInStateUi from '../BeforeSignInState/BeforeSignInStateUi'

class BeforeSignInState extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return (
            <BeforeSignInStateUi openSignIn={this.props.openSignIn}/>
        );
    }
}

export default BeforeSignInState;