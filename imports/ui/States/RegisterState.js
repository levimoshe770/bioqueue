import React from 'react'
import RegisterStateUi from '../RegisterState/RegisterStateUi'
import {withRouter} from 'react-router-dom'

class RegisterState extends React.Component
{
    render()
    {
        return (
            <RegisterStateUi history={this.props.history}/>
        );
    }
}

export default withRouter(RegisterState);