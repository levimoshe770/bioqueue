import React from 'react';
import { connect } from 'react-redux';
import PharmacyPage from '../PharmacyState/PharmacyPage.jsx';

const mapStateToProps = (state) =>
{
    return ({access_token: state.access_token});
}

class PharmacyState extends React.Component
{
    render()
    {
        return (
            <div>
                <PharmacyPage access_token={this.props.access_token} />
            </div>
        );
    }
}

export default connect(mapStateToProps)(PharmacyState);