import { Meteor } from 'meteor/meteor';

let winHandle = null;

const LoginPopup = (locale) =>
{
    const domain = Meteor.settings.public.auth_server_url;
    const clientId = Meteor.settings.public.client_data.ClientId;
    const redirect_uri = Meteor.absoluteUrl() + '_oauth/hsauth2server';
    const redirectLink = domain + 'login' + '?' +
        'client_id=' + clientId + '&' +
        'redirect_uri=' + redirect_uri + '&' +
        'response_type=code' + '&' +
        'locale=' + locale;

    const winHeight = $(window).height();
    const winWidth = $(window).width();

    const popupHeight = 500;
    const popupWidth = 500;

    const left = (winWidth - popupWidth)/2;
    const top = (winHeight - popupHeight)/2;

    const params = 'resizable=no,status=no,location=no,menubar=no,' +
        'width=' + popupWidth.toString() + ',' +
        'height=' + popupHeight.toString() + ',' +
        'left=' + left.toString() + ','+ 
        'top=' + top.toString();

    //console.log(params);

    winHandle = window.open(redirectLink, 'BioQueue Login', params);
}

const CloseLoginPopup = () =>
{
    winHandle.close();
}

export { LoginPopup, CloseLoginPopup };