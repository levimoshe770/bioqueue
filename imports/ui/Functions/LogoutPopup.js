import { Meteor } from 'meteor/meteor';

const LogoutPopup = (token) =>
{
    const url = Meteor.settings.public.auth_server_url + 
        'logout?' +
        'token=' + token

    const params = 'resizable=no,status=no,location=no,menubar=no,width=100,height=100,left=300,top=300';

    window.open(url, 'BioQueue Logout', params);
}

export default LogoutPopup;