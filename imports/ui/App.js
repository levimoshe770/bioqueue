import React from 'react';
import './App.css'
import 'tachyons';
import Routers from './Routers/Routers'
import SignInBox from './SignInBox/SignInBox'
import {connect} from 'react-redux'
import { Meteor } from 'meteor/meteor'
import {setUserLoggedIn, setLocale, setAccessToken} from './Redux/actions';
import Users from './../api/Lists/Users/users';
import { withTracker } from 'meteor/react-meteor-data';
import AuthorizationTable from './Routers/AuthorizationTable';
import { StaticRouter, withRouter } from 'react-router-dom';
import { withCookies } from 'react-cookie';

const mapStateToProps = state =>
{
    return {userName: state.userName, locale: state.locale, access_token: state.access_token}
}

const mapDispatchToProps = (dispatch) =>
{
    return (
        {
            onUserChange: (userName) => dispatch(setUserLoggedIn(userName)),
            onLocaleChange: (locale) => dispatch(setLocale(locale)),
            onTokenChange: (token) => dispatch(setAccessToken(token))
        }
    );
}

class App extends React.Component
{
    componentDidMount()
    {
        this.loadLocale();
    }

    loadLocale() 
    {
        const { cookies } = this.props;
        const locale = cookies.get('locale');
        if (locale === undefined) 
        {
            cookies.set('locale', this.props.locale, '/');
        }
        else 
        {
            if (this.props.locale !== locale) 
            {
                this.props.onLocaleChange(locale);
            }
        }
    }

    render() 
    {
        //console.log(this.props);
        return(
            <div>
                <SignInBox />
                <Routers userData = {this.props.userData[0]}/>
            </div>
        );
    }
}


export default 
withCookies(
    withRouter(
        connect(mapStateToProps, mapDispatchToProps)(
            withTracker(
                () => 
                {
                    const userHandle = Meteor.subscribe('users.customData');

                    const loading = !userHandle.ready();

                    return(
                        {
                            userData: !loading ? Users.find({}).fetch() : [],
                        }
                    )
                }
            )
            (App)
        )
    )
);