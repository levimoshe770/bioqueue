import React from 'react';
import Logo from './Logo'
import './SignInBox.css';
import SignInLink from './SignInLink'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import LocaleSelector from './LocaleSelector.jsx';

const mapStateToProps = 
    state => {return {userName: state.userName} }

class SignInBox extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state =
        {
            width: 0
        }
    }

    render()
    {
        //this.setState({authenticated: authobj.isAuthenticated});
        return(
            <div className="signinbox flex items-center justify-between">
                <LocaleSelector width={this.state.width} />
                <Logo 
                    HomePath={this.props.userName.InitialPath} 
                    width={this.state.width}
                />
                <SignInLink 
                    history={this.props.history}
                    widthSet={(w) => this.setState({width: w})}
                />
            </div>
        )
    }
}

export default withRouter(connect(mapStateToProps)(SignInBox));