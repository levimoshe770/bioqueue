import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setLocale } from '../Redux/actions';
import { IntlMessages } from '../../../translation/IntlMessages';
import { withCookies } from 'react-cookie';

const mapStateToProps = state =>
{
    return {locale: state.locale}
}

const mapDispatchToProps = (dispatch) =>
{
    return {onLocaleChange: (locale) => dispatch(setLocale(locale))}
}

/*
className
 */

class LocaleSelector extends Component
{
    constructor(props)
    {
        super(props);
        this.state = 
        {
            showList: false
        }
    }

    localeToFlag =
    {
        'en': {flag: 'icons/iconfinder_274_Ensign_Flag_Nation_states_2634451.png', name: 'English'},
        'ru': {flag: 'icons/iconfinder_210_Ensign_Flag_Nation_russia_2634398.png', name: 'русский'}
    }

    getLocaleList = () =>
    {
        let res = [];

        for (var key in IntlMessages)
        {
            res.push(key);
        }

        return res;
    }

    renderList(localeList)
    {
        return(
            <div className='absolute bg-white c-gray pa2 pointer shadow-3'>
                {
                    localeList.map(
                        (l, idx) => 
                            <div
                                className='pa1'
                                key={idx}
                                onClick=
                                {
                                    () =>
                                    {
                                        this.props.onLocaleChange(l);
                                        this.props.cookies.set('locale',l,'/');
                                        this.setState({showList: false})
                                    }
                                }
                            >
                                <div className='flex items-center'>
                                    <img
                                        src={this.localeToFlag[l].flag}
                                        alt={l}
                                        height='28'
                                    />
                                    <div className='pl2'>
                                        {this.localeToFlag[l].name}
                                    </div>
                                </div>
                            </div>
                    )
                }
            </div>
        )
    }

    render()
    {
        return(
            <div 
                className='ml4 f-arial-20-nc'
                style={{width: this.props.width}}
            >
                <div 
                    className='c-white pointer'
                    onClick={() => this.setState({showList: !this.state.showList})}
                >
                    <img
                        src={this.localeToFlag[this.props.locale].flag}
                        alt={this.props.locale}
                        height='32'
                    />
                </div>
                {
                    this.state.showList &&
                    this.renderList(this.getLocaleList())
                }
            </div>
        )
    }
}

export default 
withCookies(connect(mapStateToProps, mapDispatchToProps)(LocaleSelector));