import React from 'react'
import {connect} from 'react-redux'
import {FormattedMessage} from 'react-intl'
import UserProfile from './UserProfile'
import { Meteor } from 'meteor/meteor';
import {setUserLoggedIn, setAccessToken} from '../Redux/actions'
import { LoginPopup } from '../Functions/LoginPopup';
import LogoutPopup from '../Functions/LogoutPopup';

const mapStateToProps = state =>
{
    return {userName: state.userName, locale: state.locale, access_token: state.access_token}
}

const mapDispatchToProps = (dispatch) =>
{
    return (
        {
            onUserChange: (userName) => dispatch(setUserLoggedIn(userName)),
            onTokenChange: (token) => dispatch(setAccessToken(token))
        }
    )
}

class SignInLink extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state =
        {
            width: 0,
            ref: React.createRef()
        }
    }

    onLogout = () =>
    {
        Meteor.call('getUserToken',
            (err, token) =>
            {
                if (err)
                {
                    console.log(err);
                    return;
                }

                Meteor.logout(
                    (err) => 
                    {
                        if (!err)
                        {
                            this.props.onUserChange(
                                {
                                    Name: '',
                                    Email: '',
                                    Role: '',
                                    userId: Meteor.userId(),
                                    InitialPath: '/'
                                }
                            );

                            this.props.onTokenChange('');
        
                            return;
                        }
        
                        console.log(err);
                    }
                );

                LogoutPopup(token);
            }
        )

    }

    onEditProfile = () =>
    {
        this.props.history.push('/profile');
    }

    onSigninClick = () =>
    {
        //this.props.history.push('/signin');
        LoginPopup(this.props.locale);
    }

    onSignupClick = () =>
    {
        this.props.history.push('/register')
    }

    onMedicalRecord = () =>
    {
        this.props.history.push('/medicalrecord');
    }

    componentDidMount()
    {
        this.props.widthSet(this.state.ref.current.offsetWidth);
    }

    render()
    {
        if (this.props.userName.Email !== '')
        {
            return (
                <div 
                    className=""
                    ref = {this.state.ref}
                >
                    <UserProfile 
                        className="mr4" 
                        Profile={this.props.userName} 
                        access_token={this.props.access_token}
                        onLogout={this.onLogout} 
                        onEditProfile={this.onEditProfile}
                        onMedicalRecord={this.onMedicalRecord}
                    />
                </div>
            )
        }
        else
        {
            return (
                <div 
                    className='flex items-center'
                    ref = {this.state.ref}
                >
                    <div 
                        className="signinlink" 
                        onClick={this.onSigninClick}
                    >
                        <FormattedMessage
                                id="signinbox.signinlink"
                                defaultMessage="Sign In"
                                description="Sign in link"
                        />
                    </div>
                    <div className="registerlink" onClick={this.onSignupClick}>
                        <FormattedMessage
                                id="register.signup"
                                defaultMessage="Sign Up"
                                description="Sign up link"
                        />
                    </div>
                </div>
            )
        }
    }
        
}

export default connect(mapStateToProps, mapDispatchToProps)(SignInLink);