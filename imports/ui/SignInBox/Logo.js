import React from 'react';
import { withRouter } from 'react-router-dom';

const onLogoClick = (path, history) =>
{
    history.push(path);
}

const Logo = (props) =>
{
    return(
        <div 
            className="ma1 pointer flex flex-column items-center"
            style={{width: props.width}}
        >
            <img 
                className='pointer'
                id="logoimg" 
                src="..\..\..\images\Logo.png" 
                onClick={() => onLogoClick(props.HomePath, props.history)}
            />
        </div>
    );
}

export default withRouter(Logo);