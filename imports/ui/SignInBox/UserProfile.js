import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import Patients from '../../api/Lists/Patients/patients';
import Physicians from '../../api/Lists/Physicians/physicians';
import {FormattedMessage} from 'react-intl'
import './UserProfile.css'
import { connect } from 'react-redux';
import Remote from '../Remote';
import Laboratories from '../../api/Lists/Laboratories/Laboratories';
import Pharmacies from '../../api/Lists/Pharmacies/Pharmacies';

const mapStateToProps = state =>
{
  return {patientId: state.patientId, locale: state.locale};
}

/*

Profile: User profile object
className: CSS classes

onLogout: Callback for handling logout event

 */

class UserProfile extends React.Component 
{

  constructor(props)
  {
    super(props);
    this.state = 
    {
      profileIsOpen: false,
      profileWidth: 0
    }
  }

  openProfile = (event) =>
  {
    const width = event.target.nodeName === 'IMG' ? event.target.parentNode.offsetWidth : event.target.offsetWidth;
    //console.log('width of profile',event.target.nodeName);
    this.setState(
      {
        profileWidth: width,
        profileIsOpen: !this.state.profileIsOpen
      }
    );
  }

  onMouseEnter = (event) =>
  {
    const el = document.getElementById(event.target.id);
    let cl = el.getAttribute("class");
    cl = cl + " profile-item-highlighted";
    el.setAttribute("class", cl);
  }

  onMouseLeave = (event) =>
  {
    const el = document.getElementById(event.target.id);
    let cl = el.getAttribute("class");
    cl = cl.replace("profile-item-highlighted","");
    el.setAttribute("class", cl);
  }

  onLogout = () =>
  {
    this.props.onLogout();
    this.setState({profileIsOpen: false});
  }

  onEditProfile = () =>
  {
    this.props.onEditProfile();
    this.setState({profileIsOpen: false});
  }

  baseClassName = () =>
  {
    let res = this.props.className + " pa0 dib tl b--light-gray"
    if (this.state.profileIsOpen)
      return res + " ba";
    else
      return res;
  }

  onMedicalRecord = () =>
  {
    this.props.onMedicalRecord();
    this.setState({profileIsOpen: false});
  }

  render()
  {
    return(
      <div className={this.baseClassName()}>
        <div className="pr2 flex items-center pointer bg-blue b c-white" onClick={this.openProfile}>
          <img 
            className="ma2"
            src=
            {
              this.props.userProfile.length > 0 
                  ? (this.props.Profile.Role === 'patient' 
                      ? this.props.userProfile[0].Avatar 
                      : this.props.userProfile[0].Image)
                  : 'images/default-non-user-no-photo-1.jpg'
            }
            width="30" 
            alt='face'/>
          {this.props.Profile.Name}
        </div>
        {this.state.profileIsOpen &&
          <div 
            className="profile-menu z-index-100 ba b--white"
            style={{width: this.state.profileWidth}}
          >
            <ul className="pa1">
              {
                (this.props.Profile.Role === 'patient' ||
                 this.props.Profile.Role === 'physician' ||
                 this.props.Profile.Role === 'laboratory' ||
                 this.props.Profile.Role === 'pharmacy') &&
                <li 
                  className="pl1 pb2 pt2 pointer" 
                  id="editprofile"
                  onMouseEnter={this.onMouseEnter}
                  onMouseLeave={this.onMouseLeave}
                  onClick={this.onEditProfile}
                >
                  <FormattedMessage
                    id='userprofile.edit'
                    defaultMessage='Edit profile'
                    description='Edit profile'
                  />
                </li>
              }
              {
                this.props.Profile.Role === 'patient' &&
                <li
                  className="pl1 pb2 pt2 pointer" 
                  id="medicalrecord"
                  onMouseEnter={this.onMouseEnter}
                  onMouseLeave={this.onMouseLeave}
                  onClick={this.onMedicalRecord}
                >
                  <FormattedMessage
                    id='userprofile.mr'
                    defaultMessage='Medical Record'
                    description='Medical Record'
                  />
                </li>
              }
              <li 
                className="pl1 pb2 pt2 pointer" 
                id="logout"
                onMouseEnter={this.onMouseEnter}
                onMouseLeave={this.onMouseLeave}
                onClick={this.onLogout}
              >
                <FormattedMessage
                  id='userprofile.logout'
                  defaultMessage='Logout'
                  description='Logout'
                />
              </li>
            </ul>
          </div>
        }
      </div>
    );
  }
}

export default connect(mapStateToProps)(withTracker(
  ({Profile, access_token}) =>
  {
    if (Profile.userId === '')
      return({userProfile: []});

    switch (Profile.Role)
    {
      case 'patient':
        profileHandler = Meteor.subscribe('patients.userProfile', access_token);
        loading = !profileHandler.ready();

        return(
          {userProfile: !loading ? Patients.find({}).fetch() : []}
        );

      case 'physician':
        profileHandler = Remote.subscribe('physician.profile', access_token);
        loading = !profileHandler.ready();

        return (
            {userProfile: !loading ? Physicians.find({}).fetch() : []}
        );
      case 'laboratory':
        profileHandler = Remote.subscribe('laboratories.myLab', access_token);
        loading = !profileHandler.ready();

        return (
            {userProfile: !loading ? Laboratories.find({}).fetch() : []}
        );
      case 'pharmacy':
        profileHandler = Remote.subscribe('pharmacies.myPharmacy', access_token);
        loading = !profileHandler.ready();

        return (
            {userProfile: !loading ? Pharmacies.find({}).fetch() : []}
        );
    }

    return({userProfile: []})
  }
)
(UserProfile)
);