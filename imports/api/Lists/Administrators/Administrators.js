import { Mongo } from 'meteor/mongo';
import AdministratorsSchema from './AdministratorsSchema';
import Remote from '../../../ui/Remote';

const Administrators = new Mongo.Collection('administrators', { connection: Remote });

Administrators.attachSchema(AdministratorsSchema);

export default Administrators;