import SimpleSchema from 'simpl-schema';
import { Pending, Approved, Suspended } from '../GeneralTypes/AuthorizationStatus';

const AdministratorsSchema = new SimpleSchema(
    {
        _id: {type: String, regEx: SimpleSchema.RegEx.Id},
        Name: {type: String},
        Email: {type: String, regEx: SimpleSchema.RegEx.Email},
        Phone: {type: String, regEx: SimpleSchema.RegEx.Phone, required: false},
        AuthorizationStatus: {type: SimpleSchema.Integer, allowedValues: [Pending, Approved, Suspended]},
        Image: {type: String, required: false, defaultValue: 'images/default-non-user-no-photo-1.jpg'}
    }
);

export default AdministratorsSchema;