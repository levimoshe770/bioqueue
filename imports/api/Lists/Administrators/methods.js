import { Meteor } from 'meteor/meteor';
import Users from '../Users/users';
import Administrators from './Administrators';

Meteor.methods(
    {
        'admin.insert'(data)
        {
            const user = Users.findOne({_id: data._id});
            if (typeof user === 'undefined')
            {
                throw new Meteor.Error(500, 'userid_invalid', 'user id is invalid');
            }

            try
            {
                Administrators.insert(data);
            }
            catch(err)
            {
                throw err;
            }
        },
        'admin.updateOne' (data) 
        {
            try
            {
                Administrators.update({_id: data._id}, {$set: data});
            }
            catch(err)
            {
                throw err;
            }
        }
    }
)