import SimpleSchema from 'simpl-schema';
import { Pending, Approved, Suspended } from '../GeneralTypes/AuthorizationStatus';

const PhysicianSchema = new SimpleSchema(
    {
        _id: {type: String},
        Name: {type: String},
        Address: {type: String, required: false},
        Email: {type: String, regEx: SimpleSchema.RegEx.Email},
        Phone: {type: String, regEx: SimpleSchema.RegEx.Phone, required: false},
        Specialty: {type: String, required: false},
        Availability: {type: Array, minCount: 1, maxCount: 6, required: false},
        'Availability.$': {type: Object},
        'Availability.$.Day': {type: SimpleSchema.Integer, min: 0, max: 6},
        'Availability.$.Start': {type: String, regEx: /[0-2]?\d:[0-5]\d/},
        'Availability.$.End': {type: String, regEx: /[0-2]?\d:[0-5]\d/},
        Resolution: {type: SimpleSchema.Integer, min: 5, max: 120, required: false},
        AuthorizationStatus: {type: SimpleSchema.Integer, allowedValues: [Pending, Approved, Suspended]},
        Image: {type: String, required: false, defaultValue: 'images/default-non-user-no-photo-1.jpg'}
    }
)

export default PhysicianSchema;