import { Mongo } from 'meteor/mongo';
import PhysicianSchema from './PhysiciansSchema';
import Remote from '../../../ui/Remote';

const Physicians = new Mongo.Collection('physicians', {connection: Remote});

Physicians.attachSchema(PhysicianSchema);

export default Physicians;