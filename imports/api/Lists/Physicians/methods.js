import { Meteor } from 'meteor/meteor';

import Appointments from '../Appointments/Appointments';
import Messages from '../Messages/Messages';
import Users from '../Users/users';

Meteor.methods(
    {
        'physician.myPatients'()
        {
            const userId = this.userId;

            if (!userId)
                return this.ready();
    
            const physicianId = Users.findOne({_id: this.userId}).services.hsloginservice.id;
    
            // Get patients that have appointments, and/or have initiated chat with me
            const ids = [];
    
            patientsWithAppointments = Appointments.find({PhysicianId: physicianId},{fields: {PatientId: 1}});
            patientsWithReceivedMessages = Messages.find({'from.id': physicianId},{fields: {to: 1}});
            patientsWithSendMessage = Messages.find({'to.id': physicianId},{fields: {from: 1}});
    
            ids.push(patientsWithAppointments.map((p) => p.PatientId));
            ids.push(patientsWithReceivedMessages.map((p) => p.to.id));
            ids.push(patientsWithSendMessage.map((p) => p.from.id));
    
            const setIds = new Set(ids.flat());
            const finalIds = [...setIds];

            return finalIds;
        }
    }
)