import { Mongo } from 'meteor/mongo';

export default EmptyFlag = new Mongo.Collection('emptyflag');