import { Meteor } from 'meteor/meteor';
import MedicalRecordPermissions from './MedicalRecordPermissions';
import Patients from '../Patients/patients';
import Physicians from '../Physicians/physicians';
import Users from '../Users/users';

Meteor.methods(
    {
        // 'medicalrecordpermissions.add'(patientid, physicianid, permissionSet)
        // {
        //     const userId = this.userId
        //     if (userId !== patientid)
        //     {
        //         throw new Meteor.Error(500, 'usernotpatient','invalid patient id');
        //     }

        //     let c = Patients.find({_id: patientid}).count();
        //     if (c === 0)
        //     {
        //         throw new Meteor.Error(500, 'invalidpatientid','invalid patient id');
        //     }

        //     c = Physicians.find({_id: physicianid}).count();
        //     if (c === 0)
        //     {
        //         throw new Meteor.Error(500, 'invalidphysicianid','invalid physician id');
        //     }

        //     try
        //     {
        //         MedicalRecordPermissions.insert(
        //             {
        //                 PatientId: patientid,
        //                 PhysicianId: physicianid,
        //                 PermissionSet: permissionSet
        //             }
        //         )
        //     }
        //     catch(err)
        //     {
        //         throw err;
        //     }
        // },
        'medicalrecordpermissions.insertOrUpdate'(patientid, physicianId, permissionSet)
        {
            const userId = Users.findOne({_id: this.userId}).services.hsloginservice.id;

            if (userId !== patientid)
            {
                throw new Meteor.Error(500, 'usernotpatient','invalid patient id');
            }

            let c = Patients.find({_id: patientid}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'invalidpatientid','invalid patient id');
            }

            c = Physicians.find({_id: physicianId}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'invalidphysicianid','invalid physician id');
            }

            try
            {
                c = MedicalRecordPermissions.find(
                    {
                        $and: 
                        [
                            { PatientId: patientid },
                            { PhysicianId: physicianId }
                        ]
                    }
                ).count();

                if (c === 0)
                {
                    MedicalRecordPermissions.insert(
                        {
                            PatientId: patientid,
                            PhysicianId: physicianId,
                            PermissionSet: permissionSet
                        }
                    );

                    return;
                }

                MedicalRecordPermissions.update(
                    {
                        $and: 
                        [
                            { PatientId: patientid },
                            { PhysicianId: physicianId }
                        ]
                    },
                    {
                        $set: { PermissionSet: permissionSet }
                    }
                )
            }
            catch(err)
            {
                throw err;
            }
        },
        'medicalrecordpermissions.remove'(patientId, physicianId)
        {
            const userId = Users.findOne({_id: this.userId}).services.hsloginservice.id;
            if (userId !== patientid)
            {
                throw new Meteor.Error(500, 'usernotpatient','invalid patient id');
            }

            let c = Patients.find({_id: patientid}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'invalidpatientid','invalid patient id');
            }

            c = Physicians.find({_id: physicianid}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'invalidphysicianid','invalid physician id');
            }

            try
            {
                MedicalRecordPermissions.remove(
                    {
                        $and: 
                        [
                            {PatientId: patientId},
                            {PhysicianId: physicianId}
                        ]
                    }
                )
            }
            catch(err)
            {
                throw err;
            }
        }
    }
)