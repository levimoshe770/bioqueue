import SimpleSchema from 'simpl-schema';

const MedicalRecordPermissionsSchema = new SimpleSchema(
    {
        PatientId: {type: String, regEx: SimpleSchema.RegEx.Id},
        PhysicianId: {type: String, regEx: SimpleSchema.RegEx.Id},
        PermissionSet: {type: Object},
        'PermissionSet.visits': {type: Boolean},
        'PermissionSet.kproblems': {type: Boolean},
        'PermissionSet.medication': {type: Boolean},
        'PermissionSet.alergies': {type: Boolean},
        'PermissionSet.addvisits': {type: Boolean},
        'PermissionSet.ordertests': {type: Boolean},
    }
)

export default MedicalRecordPermissionsSchema;