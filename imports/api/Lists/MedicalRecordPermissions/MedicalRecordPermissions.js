import { Mongo } from 'meteor/mongo'
import MedicalRecordPermissionsSchema from './MedicalRecordPermissionsSchema';
import Remote from '../../../ui/Remote';

const MedicalRecordPermissions = new Mongo.Collection('medicalrecordpermissions', { connection: Remote });

MedicalRecordPermissions.attachSchema(MedicalRecordPermissionsSchema);

export default MedicalRecordPermissions;