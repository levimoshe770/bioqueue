import { Meteor } from 'meteor/meteor';
import MedicalRecordPermissions from './MedicalRecordPermissions';
import Patients from '../Patients/patients';
import Physicians from '../Physicians/physicians';
import Users from '../Users/users';

Meteor.publish('medicalrecordpermissions.patient',
    function()
    {
        const userId = Users.findOne({_id: this.userId}).services.hsloginservice.id;
        const c = Patients.find({_id: userId}).count();
        if (c === 0)
            return this.ready();

        return MedicalRecordPermissions.find({PatientId: userId});
    }
);

Meteor.publish('medicalrecordpermissions.physicianSingle',
    function(patientId)
    {
        const userId = Users.findOne({_id: this.userId}).services.hsloginservice.id;
        const c = Physicians.find({_id: userId}).count();
        if (c === 0)
            return this.ready();

        return MedicalRecordPermissions.find(
            {
                $and:
                [
                    {PatientId: patientId},
                    {PhysicianId: userId}
                ]
            }
        )
    }
);

Meteor.publish('medicalrecordpermissions.physicianTotal',
    function()
    {
        const userId = Users.findOne({_id: this.userId}).services.hsloginservice.id;
        const c = Physicians.find({_id: userId}).count();
        if (c === 0)
            return this.ready();

        return MedicalRecordPermissions.find(
            {
                PhysicianId: userId
            }
        );
    }
);