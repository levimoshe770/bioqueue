import { Meteor } from 'meteor/meteor';
import Patients from './patients';
import Users from '../Users/users';

Meteor.publish('patients.userProfile',
    function()
    {
        const userId = this.userId;

        if (!userId)
            return this.ready();

        const patientId = Users.findOne({_id: userId}).services.hsloginservice.id;

        const selector = {_id: patientId};

        return Patients.find(selector);
    }
);

Meteor.publish('patients.physicianSingle',
    function(patientId)
    {
        return Patients.find({_id: patientId});
    }
);

Meteor.publish('patients.ids',
    function(idList)
    {
        return Patients.find({_id: { $in: idList }});
    }
);