import SimpleSchema from 'simpl-schema';
import { Male, Female } from '../GeneralTypes/Gender';

const PatientSchema = new SimpleSchema(
    {
        _id : {type: String},
        Name: {type: String},
        Email: {type: String, regEx: SimpleSchema.RegEx.Email},
        Address: {type: String, required: false},
        Phone: {type: String, regEx: SimpleSchema.RegEx.Phone, required: false},
        Avatar: {type: String, required: false, defaultValue: 'images/default-non-user-no-photo-1.jpg'},
        ConnectedPhysician: {type: String, required: false},
        DOB: {type: Date, required: false},
        Gender: {type: SimpleSchema.Integer, allowedValues: [Male,Female]}
    }
)

export default PatientSchema;