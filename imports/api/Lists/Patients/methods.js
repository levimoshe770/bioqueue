import { Meteor } from 'meteor/meteor';
import Patients from './patients';
import Physicians from '../Physicians/physicians'
import Users from '../Users/users';

Meteor.methods(
    {
        'patient.createProfile' (profile)
        {
            // const user = Users.findOne({_id: profile._id});
            // if (typeof user === 'undefined')
            // {
            //     throw new Meteor.Error(500, 'userid_invalid', 'user id is invalid');
            // }

            const count = Patients.find({_id: profile._id}).count();
            if (count > 0)
            {
                throw new Meteor.Error(500, 'user_exists', 'patient allready exists');
            }

            try
            {
                Patients.insert(profile);
            }
            catch(err)
            {
                throw err;
            }
        },
        'patient.ConnectToPhysician' (physicianId)
        {
            const userId = this.userId;
            const patientId = Users.findOne({_id: userId}).services.hsloginservice.id;

            //console.log('User', userId, 'patientId', patientId);

            const patient = Patients.findOne({_id: patientId});

            if (typeof patient === 'undefined')
            {
                throw new Meteor.Error(500, 'patientid_invalid', 'patient id doesn\'t exist');
            }

            const physician = Physicians.findOne({_id: physicianId});
            if (typeof physician === 'undefined')
            {
                throw new Meteor.Error(500, 'physicianId_invalid', 'physician id doesn\'t exist');
            }

            try
            {
                Patients.update(
                    {_id: patientId}, 
                    {$set: {ConnectedPhysician: physicianId}}
                );
            }
            catch(err)
            {
                throw err;
            }

        },
        'patient.updateProfile' (profile)
        {
            try
            {
                Patients.update({_id: profile._id},{$set: profile});
            }
            catch(err)
            {
                throw err;
            }
        },
        'patient.remove' (_id)
        {
            try
            {
                Patients.remove(_id);
            }
            catch(err)
            {
                throw err;
            }
        },
        'patient.getSingleProfile' (id)
        {
            const options = 
            {
                fields: 
                {
                    Name: 1,
                    Avatar: 1,
                }
            }

            const patient = Patients.findOne({_id: id}, options);

            if (patient === undefined)
            {
                throw new Meteor.Error(500, 'patientId_invalid', 'patient id doesn\'t exist');
            }

            return patient;

        },
        'patient.getMultipleProfiles'(ids)
        {
            const options = 
            {
                fields: 
                {
                    Name: 1,
                    Avatar: 1,
                }
            }

            try
            {
                const patients = Patients.find({_id: {$in: ids}}, options).fetch();

                return patients;
            }
            catch(err)
            {
                throw err;
            }

        }
    }
)