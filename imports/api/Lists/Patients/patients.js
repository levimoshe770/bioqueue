import { Mongo } from 'meteor/mongo';
import PatientSchema from './patientsschema';
import Remote from '../../../ui/Remote';

const Patients = new Mongo.Collection('patients', { connection: Remote });

Patients.attachSchema(PatientSchema);

export default Patients;
