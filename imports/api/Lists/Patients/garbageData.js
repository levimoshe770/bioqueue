import Patients from './patients'
import { address, name, internet, phone, date } from 'faker';
import Users from '../Users/users';

const getRandomGender = () =>
{
    return Math.floor(Math.random() * 2) === 1 ? 'Male' : 'Female'
}

const FillPatients = (numOfPatients) =>
{
    let i;
    for (i=0; i<numOfPatients; i++)
    {
        const patient = 
        {
            Name: name.firstName() + ' ' + name.lastName(),
            Email: internet.email(),
            Address: address.streetName().concat(" ", address.streetAddress(), ", ", address.city(), " ", address.zipCode()),
            Phone: phone.phoneNumber(),
            Avatar: internet.avatar(),
            DOB: date.past(),
            Gender: getRandomGender()
        }

        Accounts.createUser(
            {
                email: patient.Email,
                password: '123456'
            }
        )

        const newUser = Users.findOne({'emails.0.address': patient.Email});

        Users.update(newUser._id,
            {
                $set:
                {
                    name: patient.Name,
                    role: 'patient'
                }
            }
        );

        patient._id = newUser._id;

        Patients.insert(patient);
    }
}

export default FillPatients;