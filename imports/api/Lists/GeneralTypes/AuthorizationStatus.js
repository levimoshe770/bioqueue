const Pending = 0;
const Approved = 1;
const Suspended = 2;

const statusId =
[
    'system.pending',
    'system.approved',
    'system.suspended'
];

export { Pending, Approved, Suspended, statusId };