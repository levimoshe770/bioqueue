const Male = 0;
const Female = 1;

const genderStr =
[
    'patient.male',
    'patient.female'
]

export { Male, Female, genderStr };