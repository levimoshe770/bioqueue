import { Meteor } from 'meteor/meteor';
import VisitsCollection from './VisitsCollection';
import Patients from '../Patients/patients';
import Physicians from '../Physicians/physicians';

Meteor.methods(
    {
        'visit.new'(visit)
        {
            let c = Patients.find({_id: visit.PatientId}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'patientinvalid', 'patient id invalid');
            }

            c = Physicians.find({_id: visit.PhysicianId}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'physicianinvalid', 'physician id invalid');
            }

            try
            {
                VisitsCollection.insert(visit);
            }
            catch(err)
            {
                throw err;
            }

        },
        'visit.beforevisit.addSymptom'(visitId, symptom)
        {
            const v = VisitsCollection.find({_id: visitId}).fetch();
            if (v === undefined)
            {
                throw new Meteor.Error(500, 'visitinvalid', 'visit id invalid');
            }

            try
            {
                VisitsCollection.update(
                    {_id: visitId},
                    {$push: {'BeforeVisitData.Symptoms': symptom}}
                )
            }
            catch(err)
            {
                throw err;
            }
        },
        'visit.beforevisit.removeSymptom'(visitId, symptom)
        {
            const v = VisitsCollection.find({_id: visitId}).fetch();
            if (v === undefined)
            {
                throw new Meteor.Error(500, 'visitinvalid', 'visit id invalid');
            }

            try
            {
                VisitsCollection.update(
                    {_id: visitId},
                    {$pull: {'BeforeVisitData.Symptoms': symptom}}
                )
            }
            catch(err)
            {
                throw err;
            }
        },
        'visit.beforevisit.addTest'(visitId, test)
        {
            const v = VisitsCollection.find({_id: visitId}).fetch();
            if (v === undefined)
            {
                throw new Meteor.Error(500, 'visitinvalid', 'visit id invalid');
            }

            //console.log(test);

            try
            {
                VisitsCollection.update(
                    {_id: visitId},
                    {$push: {'BeforeVisitData.Tests': test}}
                )
            }
            catch(err)
            {
                throw err;
            }
        },
        'visit.beforevisit.removetest'(visitId, test)
        {
            const c = VisitsCollection.find({_id: visitId}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'visitinvalid', 'visit id invalid');
            }

            try
            {
                VisitsCollection.update(
                    {_id: visitId},
                    {$pull: {'BeforeVisitData.Tests': test}}
                )
            }
            catch(err)
            {
                throw err;
            }
        },
        'visit.beforevisit.addRecomendation'(visitId, recomendation)
        {
            const v = VisitsCollection.find({_id: visitId}).fetch();
            if (v === undefined)
            {
                throw new Meteor.Error(500, 'visitinvalid', 'visit id invalid');
            }

            try
            {
                VisitsCollection.update(
                    {_id: visitId},
                    {$push: {'BeforeVisitData.Recomendations': recomendation}}
                )
            }
            catch(err)
            {
                throw err;
            }
        },
        'visit.beforevisit.removeRecomendation'(visitId, recomendation)
        {
            const c = VisitsCollection.find({_id: visitId}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'visitinvalid', 'visit id invalid');
            }

            try
            {
                VisitsCollection.update(
                    {_id: visitId},
                    {$pull: {'BeforeVisitData.Recomendations': recomendation}}
                )
            }
            catch(err)
            {
                throw err;
            }

        },
        'visit.duringvisit.addqa'(visitId, qa)
        {
            const v = VisitsCollection.find({_id: visitId}).fetch();
            if (v === undefined)
            {
                throw new Meteor.Error(500, 'visitinvalid', 'visit id invalid');
            }

            try
            {
                VisitsCollection.update(
                    {_id: visitId},
                    {$push: {'DuringVisit.QA': qa}}
                )
            }
            catch(err)
            {
                throw err;
            }
        },
        'visit.duringvisit.removeqa'(visitId, qa)
        {
            const c = VisitsCollection.find({_id: visitId}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'visitinvalid', 'visit id invalid');
            }

            try
            {
                VisitsCollection.update(
                    {_id: visitId},
                    {$pull: {'DuringVisit.QA': qa}}
                )
            }
            catch(err)
            {
                throw err;
            }
        },
        'visit.duringvisit.adddiagnosis'(visitId, diagnosis)
        {
            const v = VisitsCollection.find({_id: visitId}).fetch();
            if (v === undefined)
            {
                throw new Meteor.Error(500, 'visitinvalid', 'visit id invalid');
            }

            try
            {
                VisitsCollection.update(
                    {_id: visitId},
                    {$push: {'DuringVisit.Diagnosis': diagnosis}}
                )
            }
            catch(err)
            {
                throw err;
            }
        },
        'visit.duringvisit.removediagnosis'(visitId, diagnosis)
        {
            const c = VisitsCollection.find({_id: visitId}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'visitinvalid', 'visit id invalid');
            }

            try
            {
                VisitsCollection.update(
                    {_id: visitId},
                    {$pull: {'DuringVisit.Diagnosis': diagnosis}}
                )
            }
            catch(err)
            {
                throw err;
            }
        },
        'visit.duringvisit.addnote'(visitId, note)
        {
            const v = VisitsCollection.find({_id: visitId}).fetch();
            if (v === undefined)
            {
                throw new Meteor.Error(500, 'visitinvalid', 'visit id invalid');
            }

            try
            {
                VisitsCollection.update(
                    {_id: visitId},
                    {$push: {'DuringVisit.DoctorNotes': note}}
                )
            }
            catch(err)
            {
                throw err;
            }
        },
        'visit.duringvisit.removenote'(visitId, note)
        {
            const c = VisitsCollection.find({_id: visitId}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'visitinvalid', 'visit id invalid');
            }

            try
            {
                VisitsCollection.update(
                    {_id: visitId},
                    {$pull: {'DuringVisit.DoctorNotes': note}}
                )
            }
            catch(err)
            {
                throw err;
            }
        },
        'visit.duringvisit.addInstruction'(visitId, instruction)
        {
            const v = VisitsCollection.find({_id: visitId}).fetch();
            if (v === undefined)
            {
                throw new Meteor.Error(500, 'visitinvalid', 'visit id invalid');
            }

            try
            {
                VisitsCollection.update(
                    {_id: visitId},
                    {$push: {'DuringVisit.Instructions': instruction}}
                )
            }
            catch(err)
            {
                throw err;
            }
        },
        'visit.duringvisit.removeInstruction'(visitId, instruction)
        {
            const c = VisitsCollection.find({_id: visitId}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'visitinvalid', 'visit id invalid');
            }

            try
            {
                VisitsCollection.update(
                    {_id: visitId},
                    {$pull: {'DuringVisit.Instructions': instruction}}
                )
            }
            catch(err)
            {
                throw err;
            }
        },
    }
)