import { Mongo } from 'meteor/mongo';
import VisitsSchema from './VisitsSchema';
import Remote from '../../../ui/Remote';

const VisitsCollection = new Mongo.Collection('visits', { connection: Remote });

VisitsCollection.attachSchema(VisitsSchema);

export default VisitsCollection;