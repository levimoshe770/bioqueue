import { Meteor } from 'meteor/meteor';
import VisitsCollection from './VisitsCollection';
import MedicalRecordPermissions from '../MedicalRecordPermissions/MedicalRecordPermissions';

Meteor.publish('visits.patient',
    function()
    {
        const userId = Users.findOne({_id: this.userId}).services.hsloginservice.id;
        
        return VisitsCollection.find({PatientId: userId});
    }
);

Meteor.publish('visits.physician',
    function(patientId)
    {
        const userId = Users.findOne({_id: this.userId}).services.hsloginservice.id;

        const permissions = MedicalRecordPermissions.find(
            {
                $and:
                [
                    {PatientId: patientId},
                    {PhysicianId: userId}
                ]
            }
        ).fetch();

        //console.log(permissions);

        if (permissions.length === 0)
            return this.ready();

        //console.log('...continuing')

        let filter;
        if (permissions[0].PermissionSet.visits)
        {
            filter = {PatientId: patientId}
        }
        else
        {
            filter = 
            {
                $and:
                [
                    {PhysicianId: userId},
                    {PatientId: patientId}
                ]
            }
        }

        return VisitsCollection.find(filter);
    }
);