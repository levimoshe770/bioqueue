import SimpleSchema from 'simpl-schema';

const VisitsSchema = new SimpleSchema(
    {
        _id: {type: String, regEx: SimpleSchema.RegEx.Id},
        PatientId: {type: String, regEx: SimpleSchema.RegEx.Id},
        PhysicianId: {type: String, regEx: SimpleSchema.RegEx.Id},
        DateTime: {type: Date},
        PhysicianName: {type: String},
        BeforeVisitData: {type: Object},
        'BeforeVisitData.Symptoms': {type: Array},
        'BeforeVisitData.Symptoms.$': {type: String},
        'BeforeVisitData.Tests': {type: Array},
        'BeforeVisitData.Tests.$': {type: Object},
        'BeforeVisitData.Tests.$.Name': {type: String},
        'BeforeVisitData.Tests.$.DateOfOrder': {type: Date},
        'BeforeVisitData.Tests.$.Provider': {type: String, required: false},
        'BeforeVisitData.Recomendations': {type: Array},
        'BeforeVisitData.Recomendations.$': {type: String},
        DuringVisit: {type: Object},
        'DuringVisit.QA': {type: Array},
        'DuringVisit.QA.$': {type: Object},
        'DuringVisit.QA.$.Q': {type: String},
        'DuringVisit.QA.$.A': {type: String},
        'DuringVisit.Diagnosis': {type: Array},
        'DuringVisit.Diagnosis.$': {type: String},
        'DuringVisit.DoctorNotes': {type: Array},
        'DuringVisit.DoctorNotes.$': {type: String},
        'DuringVisit.Instructions': {type: Array},
        'DuringVisit.Instructions.$': {type: Object},
        'DuringVisit.Instructions.$.Name': {type: String},
        'DuringVisit.Instructions.$.Medication': {type: String, optional: true},
        'DuringVisit.Instructions.$.Permanent': {type: Boolean, defaultValue: false},
        'DuringVisit.Instructions.$.Dosage': {type: String, optional: true},
        'DuringVisit.Instructions.$.Lab': {type: String, optional: true},
        'DuringVisit.Instructions.$.Notes': {type: String}
    }
);

export default VisitsSchema;