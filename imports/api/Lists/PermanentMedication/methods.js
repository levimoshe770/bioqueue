import { Meteor } from 'meteor/meteor';
import PermanentMedicationCollection from './PermanentMedicationCollection';

Meteor.methods(
    {
        'permanentmedication.add'(medication)
        {
            //console.log(medication);
            try
            {
                PermanentMedicationCollection.insert(medication);
            }
            catch(err)
            {
                throw err;
            }
        },
        'permanentmedication.update'(medication)
        {
            try
            {
                PermanentMedicationCollection.update(
                    {_id: medication._id},
                    {$set: medication}
                );
            }
            catch(err)
            {
                throw err;
            }
        },
        'permanentmedication.remove'(medicationId)
        {
            try
            {
                PermanentMedicationCollection.remove(medicationId);
            }
            catch(err)
            {
                throw err;
            }
        }
    }
)