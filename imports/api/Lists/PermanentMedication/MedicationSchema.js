import SimpleSchema from 'simpl-schema';

const MedicationSchema = new SimpleSchema(
    {
        PatientId: {type: String, regEx: SimpleSchema.RegEx.Id},
        Name: {type: String},
        Medication: {type: String},
        Dosage: {type: String},
        Notes: {type: Array},
        'Notes.$': {type: String}
    }
);

export default MedicationSchema;