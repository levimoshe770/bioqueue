import { Mongo } from 'meteor/mongo';
import MedicationSchema from './MedicationSchema';
import Remote from '../../../ui/Remote';

const PermanentMedicationCollection = new Mongo.Collection('permanentmedication', { connection: Remote });

PermanentMedicationCollection.attachSchema(MedicationSchema);

export default PermanentMedicationCollection;
