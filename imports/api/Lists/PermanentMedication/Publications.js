import { Meteor } from 'meteor/meteor';
import PermanentMedicationCollection from './PermanentMedicationCollection';
import Patients from '../Patients/patients';
import MedicalRecordPermissions from '../MedicalRecordPermissions/MedicalRecordPermissions';

Meteor.publish('permanentmedication.patient',
    function()
    {
        const userId = Users.findOne({_id: this.userId}).services.hsloginservice.id;
        const c = Patients.find({_id: userId}).count();
        if (c === 0)
            return this.ready();

        return PermanentMedicationCollection.find({PatientId: userId});
    }
);

Meteor.publish('permantentmedication.physician',
    (patientId) =>
    {
        const userId = Users.findOne({_id: this.userId}).services.hsloginservice.id;

        const permissions = MedicalRecordPermissions.find(
            {
                $and:
                [
                    {PatientId: patientId},
                    {PhysicianId: userId}
                ]
            }
        ).fetch();

        //console.log(permissions);

        if (permissions.length === 0)
        {
            this.ready();
            return;
        }

        if (!permissions[0].PermissionSet.medication)
        {
            this.ready();
            return;
        }

        //console.log('continue...')

        return PermanentMedicationCollection.find({PatientId: patientId});

    }
);