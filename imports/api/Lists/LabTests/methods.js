import { Meteor } from 'meteor/meteor';
import LabTestsCollection from './LabTestsCollection';
import Patients from '../Patients/patients';
import Physicians from '../Physicians/physicians';
import Laboratories from '../Laboratories/Laboratories';

Meteor.methods(
    {
        'labtest.create'(labTest)
        {
            let c = Patients.find({_id: labTest.PatientId}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'patientinvalid', 'patient id invalid');
            }

            c = Physicians.find({_id: labTest.PhysicianId}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'physicianinvalid', 'physician id invalid');
            }

            c = Laboratories.find({_id: labTest.LabId}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'labinvalid', 'lab id is invalid');
            }

            try
            {
                if (labTest.OrderNum === undefined)
                {
                    labTest.OrderNum = LabTestsCollection.find({}).count() + 1;
                }
                LabTestsCollection.insert(labTest);
            }
            catch(err)
            {
                throw err;
            }
        },
        'labtest.updateDateOfPerformance'(labTestId, date)
        {
            const lab = LabTestsCollection.findOne({_id: labTestId});
            if (lab === undefined)
            {
                throw new Meteor.Error(500, 'labtestinvalid', 'lab test id is invalid');
            }

            try
            {
                LabTestsCollection.update(
                    { _id: labTestId },
                    { $set: { DateOfPerformance: date } }
                );
            }
            catch(err)
            {
                throw err;
            }
        },
        'labtest.updateDateOfResults'(labTestId, date)
        {
            const lab = LabTestsCollection.findOne({_id: labTestId});
            if (lab === undefined)
            {
                throw new Meteor.Error(500, 'labtestinvalid', 'lab test id is invalid');
            }

            try
            {
                LabTestsCollection.update(
                    { _id: labTestId },
                    { $set: { DateOfResults: date } }
                );
            }
            catch(err)
            {
                throw err;
            }
        },
        'labtest.updateStatus'(labTestId, status)
        {
            const lab = LabTestsCollection.findOne({_id: labTestId});
            if (lab === undefined)
            {
                throw new Meteor.Error(500, 'labtestinvalid', 'lab test id is invalid');
            }

            try
            {
                LabTestsCollection.update(
                    { _id: labTestId },
                    { $set: { Status: status } }
                );
            }
            catch(err)
            {
                throw err;
            }
        },
        'labtest.addResults'(labTestId, date, results)
        {
            const lab = LabTestsCollection.findOne({_id: labTestId});
            if (lab === undefined)
            {
                throw new Meteor.Error(500, 'labtestinvalid', 'lab test id is invalid');
            }

            try
            {
                LabTestsCollection.update(
                    { _id: labTestId },
                    { 
                        $push: { Results: results },
                        $set:  { DateOfResults: date }
                    }
                );
            }
            catch(err)
            {
                throw err;
            }
        },
        'labtest.addNotes'(labTestId, notes)
        {
            const lab = LabTestsCollection.findOne({_id: labTestId});
            if (lab === undefined)
            {
                throw new Meteor.Error(500, 'labtestinvalid', 'lab test id is invalid');
            }

            try
            {
                LabTestsCollection.update(
                    { _id: labTestId },
                    { 
                        $set: { Notes: notes }
                    }
                );
            }
            catch(err)
            {
                throw err;
            }
        }
    }
)