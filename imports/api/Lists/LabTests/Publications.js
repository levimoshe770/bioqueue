import { Meteor } from 'meteor/meteor';
import LabTestsCollection from './LabTestsCollection';
import Laboratories from '../Laboratories/Laboratories';
import Users from '../Users/users';

Meteor.publish('labtests.patient',
    function()
    {
        const patientId = Users.findOne({_id: this.userId}).services.hsloginservice.id;

        return LabTestsCollection.find({PatientId: patientId});
    }
);

Meteor.publish('labtest.physician',
    function(patientId)
    {
        const physicianId = Users.findOne({_id: this.userId}).services.hsloginservice.id;

        return LabTestsCollection.find(
            {
                $and:
                [
                    {PhysicianId: physicianId},
                    {PatientId: patientId}
                ]
            }
            
        );
    }
);

Meteor.publish('labtest.laboratory',
    function()
    {
        const labId = Users.findOne({_id: this.userId}).services.hsloginservice.id;
        const c = Laboratories.find({_id: labId}).count();
        if (c === 0)
        {
            return this.ready();
        }

        return LabTestsCollection.find({LabId: labId},{sort: [['Status', 'desc'],['DateOfOrder', 'asc']]});
    }
);

Meteor.publish('labtest.labDetails',
    function(id)
    {
        return LabTestsCollection.find({_id: id});
    }
);