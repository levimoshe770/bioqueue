import { Mongo } from 'meteor/mongo';
import LabTestsCollectionSchema from './LabTestsSchema';
import Remote from '../../../ui/Remote';

const LabTestsCollection = new Mongo.Collection('labtests', { connection: Remote });

LabTestsCollection.attachSchema(LabTestsCollectionSchema);

const Numeric = 0;
const Image = 1;
const Video = 2;
const Voice = 3;
const Text = 4;

export default LabTestsCollection;
export { Numeric, Image, Video, Voice, Text };