import LabTestsCollection from './LabTestsCollection';
import Patients from '../Patients/patients';
import Physicians from '../Physicians/physicians';
import Laboratories from '../Laboratories/Laboratories';
import { date, hacker } from 'faker';
import moment from 'moment';

const getRandomPatientId = () =>
{
    const patient = Patients.aggregate([{ $sample: {size: 1} }]);
    //console.log(patient);
    return patient[0]._id;
}

const getRandomPhysicianId = () =>
{
    const physician = Physicians.aggregate([{ $sample: {size: 1} }]);
    return physician[0]._id;
}

const getRandomLabId = () =>
{
    const laboratory = Laboratories.aggregate([{ $sample: {size: 1} }]);
    return laboratory[0]._id;
}

const getRandomTestName = () =>
{
    const tests = 
    [
        'CBC',
        'PT',
        'Basic Metabolic Panel',
        'Comprehensive Metabolic Panel',
        'Lipid Panel',
        'Liver Panel',
        'TSH',
        'HA1C',
        'URINE',
        'CULTURES'
    ];

    const idx = getRandomInt(0, tests.length - 1);
    return tests[idx];
}

const getRandomInt = (minInt, maxInt) =>
{
    const res = minInt + Math.floor(Math.random() * (maxInt - minInt + 1));

    return res;
}

const getRandomResults = (testName) =>
{
    
}

const getRandomNotes = () =>
{
    const numOfNotes = getRandomInt(1,10);
    const res = [];
    let i;
    for (i=0; i<numOfNotes; i++)
    {
        res.push(hacker.phrase());
    }

    return res;
}

const getRandomStatus = () =>
{
    return getRandomInt(0, 2);
}

const getNextNum = () =>
{
    const c = LabTestsCollection.find({}).count();
    return c + 1;
}

const FillLabTests = (numOfTests) =>
{
    //console.log('FillLabTests',numOfTests);
    let i;

    for (i=0; i<numOfTests; i++)
    {
        const status = getRandomStatus();

        const dateOfOrder = date.recent();
        const dateOfPerformance = new moment(dateOfOrder).add(getRandomInt(1,3),'d').toDate();
        const dateOfResults = new moment(dateOfPerformance).add(getRandomInt(2,4),'d').toDate();

        //console.log('DO', dateOfOrder, 'DOP', dateOfPerformance, 'DOR', dateOfResults);

        const labTest = 
        {
            OrderNum: getNextNum(),
            PatientId: getRandomPatientId(),
            PhysicianId: getRandomPhysicianId(),
            LabId: getRandomLabId(),
            Name: getRandomTestName(),
            Status: status,
            DateOfOrder: dateOfOrder,
            DateOfPerformance: (status === 0 || status === 2) ? dateOfPerformance : undefined,
            DateOfResults: (status === 0) ? dateOfResults : undefined,
            ApproximateDuration: getRandomInt(2,5),
            SpecialConsiderations: hacker.phrase(),
            Results: getRandomResults(),
            Notes: getRandomNotes()
        }

        //console.log(labTest);
        LabTestsCollection.insert(labTest);
    }
}

const clearTests = () =>
{
    LabTestsCollection.remove({});
}

export default FillLabTests;
export { clearTests };