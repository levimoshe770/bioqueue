import SimpleSchema from 'simpl-schema';

const LabTestsCollectionSchema = new SimpleSchema(
    {
        OrderNum: {type: SimpleSchema.Integer},
        PatientId: {type: String, regEx: SimpleSchema.RegEx.Id},
        PhysicianId: {type: String, regEx: SimpleSchema.RegEx.Id},
        LabId: {type: String, regEx: SimpleSchema.RegEx.Id},
        Name: {type: String},
        DateOfOrder: {type: Date},
        DateOfPerformance: {type: Date, required: false},
        DateOfResults: {type: Date, required: false},
        ApproximateDuration: {type: Number, required: false},
        SpecialConsiderations: {type: String, required: false},
        Results: {type: Array, required: false},
        Status: {type: Number, allowedValues: [0,1,2]},
        'Results.$': {type: Object},
        'Results.$.Name': {type: String},
        'Results.$.Type': {type: SimpleSchema.Integer, min: 0, max: 4},
        'Results.$.Result': SimpleSchema.oneOf(Number, String),
        'Results.$.Min': {type: Number, required: false},
        'Results.$.Max': {type: Number, required: false},
        Notes: {type: Array, required: false},
        'Notes.$': {type: String}
    }
)

export default LabTestsCollectionSchema;