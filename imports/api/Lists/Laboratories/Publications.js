import { Meteor } from 'meteor/meteor';
import Laboratories from './Laboratories';
import Users from '../Users/users';

Meteor.publish('laboratories.admin',
    function()
    {
        return Laboratories.find({});
    }
);

Meteor.publish('laboratories.myLab',
    function()
    {
        const userId = this.userId;
        if (!userId)
            return this.ready();

        const labId = Users.findOne({_id: this.userId}).services.hsloginservice.id;

        return Laboratories.find({_id: labId});
    }
);