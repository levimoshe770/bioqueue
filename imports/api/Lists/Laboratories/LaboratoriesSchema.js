import SimpleSchema from 'simpl-schema';
import { Pending, Approved, Suspended } from '../GeneralTypes/AuthorizationStatus';

const LaboratoriesSchema = new SimpleSchema(
    {
        Name: {type: String},
        Email: {type: String, regEx: SimpleSchema.RegEx.Email},
        Phone: {type: String, regEx: SimpleSchema.RegEx.Phone},
        AuthorizationStatus: {type: SimpleSchema.Integer, allowedValues: [Pending, Approved, Suspended]},
        Address: {type: String, optional: true}
    }
);

export default LaboratoriesSchema;