import { Mongo } from 'meteor/mongo';
import LaboratoriesSchema from './LaboratoriesSchema';
import Remote from '../../../ui/Remote';

const Laboratories = new Mongo.Collection('laboratories', { connection: Remote });

Laboratories.attachSchema(LaboratoriesSchema);

export default Laboratories;