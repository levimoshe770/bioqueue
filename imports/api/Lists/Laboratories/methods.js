import { Meteor } from 'meteor/meteor';
import Laboratories from './Laboratories';
import Users from '../Users/users';

Meteor.methods(
    {
        'laboratory.add' (laboratory)
        {
            const c = Users.find({_id: laboratory._id}).count();

            if (c === 0)
            {
                throw new Meteor.Error(500,'illegallabid','Laboratory does not correspond to user id');
            }

            try
            {
                Laboratories.insert(laboratory);
            }
            catch(err)
            {
                throw err;
            }
        },
        'laboratory.update' (laboratory)
        {
            try
            {
                Laboratories.update(
                    {_id: laboratory._id},
                    {$set: laboratory}
                );
            }
            catch(err)
            {
                throw err;
            }
        },
        'laboratory.remove' (id)
        {
            try
            {
                Laboratories.remove(id);
            }
            catch(err)
            {
                throw err;
            }
        },
        'laboratory.updateAuthorization'(id, authorizationState)
        {
            try
            {
                Laboratories.update(
                    {_id: id},
                    {$set: {AuthorizationStatus: authorizationState}}
                )
            }
            catch(err)
            {
                throw err;
            }
        }
    }
)