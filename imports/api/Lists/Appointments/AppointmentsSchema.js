import SimpleSchema from 'simpl-schema';

const AppointmentsSchema = new SimpleSchema(
    {
        _id: {type: String},
        PhysicianId: {type: String},
        PatientId: {type: String},
        DateTime: {type: Date}
    }
)

export default AppointmentsSchema;