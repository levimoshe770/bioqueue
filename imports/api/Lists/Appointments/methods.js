import { Meteor } from 'meteor/meteor';
import Patients from '../Patients/patients';
import Physicians from '../Physicians/physicians';
import Appointments from './Appointments';

Meteor.methods(
    {
        'appointment.create' (patientId, physicianId, datetime)
        {
            // const patient = Patients.findOne({_id: patientId});
            // const physician = Physicians.findOne({_id: physicianId});

            // if ((typeof patient === 'undefined') || (typeof physician === 'undefined'))
            // {
            //     throw new Meteor.Error(500, 'invalid_ids', 'patientId or physicianId are invalid');
            // }

            try
            {
                Appointments.insert(
                    {
                        PhysicianId: physicianId,
                        PatientId: patientId,
                        DateTime: datetime
                    }
                )
            }
            catch(err)
            {
                throw err;
            }
        },
        'appointment.remove' (appointmentId)
        {
            try
            {
                Appointments.remove(appointmentId);
            }
            catch(err)
            {
                throw err;
            }
        },
        'appointment.update' (appointmentId, newDate)
        {
            try
            {
                Appointments.update({_id: appointmentId},{$set: {DateTime: newDate}});
            }
            catch(err)
            {
                throw err;
            }
        }
    }

)