import Appointments from './Appointments';
import Physicians from '../Physicians/physicians';
import Patients from '../Patients/patients';

const createRandomAppointment = (patientId, physicianId) =>
{
    const app = 
    {
        PhysicianId: physicianId,
        PatientId: patientId,
        
    }
}

export default function FillAppointments(numOfAppointments)
{
    let i;
    for (i=0; i<numOfAppointments; i++)
    {
        // Get random patientId
        const patient = Patients.aggregate(
            [
                { $sample: { size: 1} }
            ]
        )[0];

        const physician = Physicians.aggregate(
            [
                { $sample: { size: 1} }
            ]
        )[0];

        createRandomAppointment(patient.patientId, physician.physicianId);
    }
}