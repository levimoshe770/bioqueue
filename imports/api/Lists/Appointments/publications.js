import { Meteor } from 'meteor/meteor';
import Appointments from './Appointments';
import Physicians from '../Physicians/physicians';
import Users from '../Users/users';

Meteor.publish('appointments.patient',
    function()
    {
        //console.log(this.userId);
        if (this.userId === null)
            return this.ready();

        const patientId = Users.findOne({_id: this.userId}).services.hsloginservice.id;

        if (!patientId)
            return this.ready();

        const selector = {PatientId: patientId};

        return Appointments.find(selector, {sort: {DateTime: 1}});
    }
);

Meteor.publish('appointments.physician',
    function()
    {
        const physicianId = Users.findOne({_id: this.userId}).services.hsloginservice.id;

        if (!physicianId)
            return this.ready();

        return Appointments.find(
            {PhysicianId: physicianId},
            {sort: {DateTime: 1}}
        );
    }
);

Meteor.publish('appointments.physicianAvailability',
    function(physicianId)
    {
        const cnt = Physicians.find({_id: physicianId}).count();
        if (cnt === 0)
            return this.ready();

        const selector = {PhysicianId: physicianId};

        const options = {fields: {DateTime: 1, PatientId: 0}}

        return Appointments.find(selector, options);
    }
);
