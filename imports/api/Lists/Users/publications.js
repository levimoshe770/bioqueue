import { Meteor } from 'meteor/meteor';
import Users from './users';
import Physicians from '../Physicians/physicians';

Meteor.publish('users.customData',
    function ()
    {
        const userId = this.userId;

        if (!userId)
            return this.ready();

        const selector = 
        {
            _id: userId        
        };

        const options = 
        {
            fields: {name: 1, role: 1}
        }

        const c = Users.find(selector, options);

        return c;
    }
);
