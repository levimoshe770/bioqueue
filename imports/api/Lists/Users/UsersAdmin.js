import { Mongo } from 'meteor/mongo';

const UsersAdmin = new Mongo.Collection('usersAdmin');

export default UsersAdmin;