import { Meteor } from 'meteor/meteor';
import Users from './users';
import { Accounts } from 'meteor/accounts-base';

Meteor.methods(
    {
        // 'user.register' (data) 
        // {
        //     if (typeof Users !== 'undefined')
        //     {
        //         const user = Users.findOne({'emails.0.address': data.email});

        //         if (user)
        //         {
        //             throw new Meteor.Error(500, 'email_exists', 'Email used by other user');
        //         }
        //     }

        //     Accounts.createUser(
        //         {
        //             email: data.email,
        //             password: data.password
        //         }
        //     );

        //     const newUser = Users.findOne({'emails.0.address': data.email});

        //     Meteor.users.update(newUser._id,
        //         {
        //             $set: {
        //                 name: data.name,
        //                 role: data.role
        //             }  
        //         }
        //     );

        //     return (newUser._id);

        // },
        // 'user.update' (id, name, email)
        // {
        //     const userId = this.userId;
        //     const patientId = Users.findOne({_id: userId}).services.hsloginservice.id;
            
        //     if (id !== patientId)
        //     {
        //         throw new Meteor.Error(500,'unauthorized','Attemp of unauthorized change of details');
        //     }

        //     try
        //     {
        //         Meteor.users.update(userId,
        //             {
        //                 $set: 
        //                 {
        //                     name: name,
        //                     'emails[0].address': email
        //                 }
        //             }
        //         );
        //     }
        //     catch(err)
        //     {
        //         throw err;
        //     }
        // },
        'user.getRealUserId'()
        {
            const userId = this.userId;

            const user = Users.findOne({_id: userId});
            if (user === undefined)
            {
                throw new Meteor.Error(500, 'haven\'t found user');
            }

            return user.services.hsloginservice.id;
        },
        'user.getUserData'(id)
        {
            const user = Users.findOne({_id: id});
            if (user === undefined)
            {
                throw new Meteor.Error(500, 'user not found');
            }

            return user;
        }
    
    }
)