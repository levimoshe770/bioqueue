import Users from './users';
import Patients from '../Patients/patients';
import Physicians from '../Physicians/physicians';

const CreatePatient = () =>
{
    Accounts.createUser(
        {
          email: 'levimoshe770@gmail.com',
          password: '123456'
        }
      )
  
    const newUser = Users.findOne(
        {'emails.0.address': 'levimoshe770@gmail.com'}
    );

    Users.update(newUser._id,
        {
            $set: 
            {
                name: 'Moshe Levi',
                role: 'patient'
            }
        }
    );

    Patients.insert(
        {
            _id: newUser._id,
            Name: 'Moshe Levi',
            Email: 'levimoshe770@gmail.com',
            Address: 'Leon Blum 62, Haifa, Israel',
            Phone: '0546461337',
            Avatar: 'images/1412349_695378430473055_348033406_o.jpg',
            DOB: new Date('1966-06-11'),
            Gender: 'Male'
        }
    )

}

const CreatePhysician = () =>
{
    Accounts.createUser(
        {
            email: 'dulitel.eufimia@hospital.com',
            password: '123456'
        }
    )

    const newUser = Users.findOne(
        {'emails.0.address': 'dulitel.eufimia@hospital.com'}
    );

    Users.update(newUser._id,
        {
            $set: 
            {
                name: 'Dr Dulitel Eufimia',
                role: 'physician'
            }
        }
    );

    Physicians.insert(
        {
            _id: newUser._id,
            Name: 'Dr Dulitel Eufimia',
            Address: '770 Eastern Parkway, Brooklyn, New York',
            Email: 'dulitel.eufimia@hospital.com',
            Phone:  '0582770770',
            Specialty: 'Psychologist',
            Availability:
            [
                {Day: 1, Start: '10:00', End: '15:00'},
                {Day: 4, Start: '10:00', End: '19:00'}
            ],
            Resolution: 15,
            AuthorizationStatus: 'Approved'
        }
    )

}

const CreateDefaultAdmin = () =>
{
    Accounts.createUser(
        {
            email: 'admin@admin.admin',
            password: '123456'
        }
    )

    const newUser = Users.findOne(
        {'emails.0.address': 'admin@admin.admin'}
    );

    Users.update(newUser._id,
        {
            $set: 
            {
                name: 'Administrator',
                role: 'admin'
            }
        }
    );

}

const CreateDummyUsers = () =>
{
    // Create my user for conveniency
    CreatePatient();

    CreatePhysician();

    CreateDefaultAdmin();
}

export default CreateDummyUsers;