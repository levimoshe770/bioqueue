import { Mongo } from 'meteor/mongo'
import ChatsSchema from './ChatsSchema';

const Chats = new Mongo.Collection('chats');

//Chats.attachSchema(ChatsSchema);

export default Chats;