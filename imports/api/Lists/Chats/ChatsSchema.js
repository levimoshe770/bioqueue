import SimpleSchema from 'simpl-schema';

const ChatsSchema = new SimpleSchema(
    {
        from: {type: Object},
        'from.id': {type: String},
        'from.role': {type: String},
        Unread: {type: SimpleSchema.Integer, min: 0},
        LastMessageDate: {type: Date}
    }
);

export default ChatsSchema;