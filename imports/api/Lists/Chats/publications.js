import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import Messages from '../Messages/Messages';
import Users from '../Users/users';

Meteor.publish('user.chats',
    function()
    {
        if (this.userId === null)
          return;

        //console.log(this.userId);
        const userId = Users.findOne({_id:this.userId}).services.hsloginservice.id;

        if (userId === null)
            this.ready();

        const groupResult = Messages.aggregate(
            [
                {
                  '$match': 
                  {
                    '$or': 
                    [
                      {
                        'to.id': userId
                      }, 
                      {
                        'from.id': userId
                      }
                    ]
                  }
                }, 
                {
                  '$group': 
                  {
                    '_id': 
                    {
                      'from': '$from', 
                      'to': '$to'
                    }, 
                    'cnt': 
                    {
                      '$sum': 1
                    }, 
                    'unread': 
                    {
                      '$sum': '$unread'
                    }, 
                    'lasttime': 
                    {
                      '$max': '$datetime'
                    }
                  }
                }, 
                {
                  '$addFields': 
                  {
                    'recipient': 
                    {
                      '$cond': 
                      {
                        'if': 
                        {
                          '$eq': 
                          [
                            '$_id.from.id', userId
                          ]
                        }, 
                        'then': '$_id.to', 
                        'else': '$_id.from'
                      }
                    }, 
                    'unread2': 
                    {
                      '$cond': 
                      {
                        'if': 
                        {
                          '$eq': 
                          [
                            '$_id.to.id', userId
                          ]
                        }, 
                        'then': '$unread', 
                        'else': 0
                      }
                    }
                  }
                }, 
                {
                  '$project': 
                  {
                    '_id': false,
                    'unread': false
                  }
                }, 
                {
                  '$group': 
                  {
                    '_id': '$recipient', 
                    'cnt': 
                    {
                      '$sum': '$cnt'
                    }, 
                    'unread': 
                    {
                      '$sum': '$unread2'
                    }, 
                    'lasttime': 
                    {
                      '$max': '$lasttime'
                    }
                  }
                }, 
                {
                  '$addFields': 
                  {
                    'recipient': '$_id'
                  }
                }, 
                {
                  '$sort': 
                  {
                    'lasttime': -1
                  }
                }
              ]
        );

        groupResult.forEach(
            (r) =>
            {
                this.added(
                    'chats',
                    Random.id(),
                    r
                )
            }
        )

        this.ready();
    }
);