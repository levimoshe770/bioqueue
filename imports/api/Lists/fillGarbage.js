import FillAppointments from './Appointments/garbageData';
import FillMessages from './Messages/garbageData'
import Appointments from './Appointments/Appointments';
import Messages from './Messages/Messages';

const FillGarbageData = (switches) =>
{
    if (switches.appointments)
        FillAppointments(5);

    if (switches.messages)
        FillMessages(50);
}

const cleanDB = (switches) =>
{
    if (switches.appointments)
        Appointments.remove({});

    if (switches.messages)
        Messages.remove({});
}

export {FillGarbageData, cleanDB};