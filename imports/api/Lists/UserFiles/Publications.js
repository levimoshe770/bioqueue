import { Meteor } from 'meteor/meteor';
import UserFiles from './UserFiles_server';

Meteor.publish('files.uploads',
    function()
    {
        return UserFiles.find({}).cursor;
    }
);