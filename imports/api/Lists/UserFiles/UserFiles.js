import { FilesCollection } from 'meteor/ostrio:files';

let UserFiles;

if (Meteor.isClient)
{
    UserFiles = new FilesCollection(
        {
            collectionName: 'userFiles'
        }
    )
}

export default UserFiles;