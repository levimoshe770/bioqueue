import { FilesCollection } from 'meteor/ostrio:files';
import { Meteor } from 'meteor/meteor';
import { _ } from 'meteor/underscore';
import { Random } from 'meteor/random';
import stream from 'stream';

import S3 from 'aws-sdk/clients/s3';
import fs from 'fs';

let UserFiles;

if (Meteor.isServer)
{

    const s3Conf = Meteor.settings.s3;
    const bound = Meteor.bindEnvironment((callback) => {return callback();});

    const s3 = new S3(
        {
            secretAccessKey: s3Conf.secret,
            accessKeyId: s3Conf.key,
            region: s3Conf.region,
            httpOptions:
            {
                timeout: 10000,
                agent: false
            }
        }
    );

    UserFiles = new FilesCollection(
        {
            debug: true,
            storagePath: 'assets/app/uploads/uploadedFiles',
            collectionName: 'userFiles',
            allowClientCode: true,

            onAfterUpload(fileRef) 
            {
                _.each(fileRef.versions, 
                    (vRef, version) =>
                    {
                        const filePath = 'files/' + (Random.id()) + '-' + version + '.' + fileRef.extension;

                        console.log(filePath);

                        s3.putObject(
                            {
                                StorageClass: 'STANDARD',
                                Bucket: s3Conf.bucket,
                                Key: filePath,
                                Body: fs.createReadStream(vRef.path),
                                ContentType: vRef.type
                            },
                            (error) =>
                            {
                                bound(
                                    () =>
                                    {
                                        if (error)
                                        {
                                            console.error('++++Error++++',error);
                                        }
                                        else
                                        {
                                            const upd = { $set: {} };
                                            upd['$set']['versions.' + version + '.meta.pipePath'] = filePath;

                                            this.collection.update(
                                                {
                                                    _id: fileRef._id
                                                },
                                                upd,
                                                (updError) =>
                                                {
                                                    if (updError)
                                                    {
                                                        console.error('UPDATE ERROR',updError);
                                                    }
                                                    else
                                                    {
                                                        this.unlink(this.collection.findOne({_id: fileRef._id}), version);
                                                    }
                                                }
                                            )
                                        }
                                        
                                    }
                                )
                            }
                        )
                    }
                )
            },

            interceptDownload(http, fileRef, version)
            {
                //console.log('+++++++++ Intercepting Download ++++++++++++++', fileRef);
                let path;

                if (fileRef && fileRef.versions && fileRef.versions[version] &&
                    fileRef.versions[version].meta && fileRef.versions[version].meta.pipePath)
                    {
                        path = fileRef.versions[version].meta.pipePath;
                    }

                if (path)
                {
                    const opts =
                    {
                        Bucket: s3Conf.bucket,
                        Key: path
                    };

                    if (http.request.headers.range)
                    {
                        const vRef = fileRef.versions[version];
                        let range = _.clone(http.request.headers.range);
                        const array = range.split(/bytes=([0-9]*)-([0-9]*)/);
                        const start = parseInt(array[1]);
                        let end = parseInt(array[2]);

                        if (isNaN(end))
                        {
                            end = (start + this.chunkSize) - 1;
                            if (end >= vRef.size)
                            {
                                end = vRef.size - 1;
                            }
                        }

                        opts.Range = `bytes=${start}-${end}`;
                        http.request.headers.range = `bytes=${start}-${end}`;
                    }

                    const fileColl = this;
                    s3.getObject(opts,
                        function(error)
                        {
                            if (error)
                            {
                                console.error(error);
                                if (!http.response.finished)
                                {
                                    http.response.end();
                                }
                            }
                            else
                            {
                                if (http.request.headers.range && this.httpResponse.headers['content-range'])
                                {
                                    http.request.headers.range = this.httpResponse.headers['content-range'].split('/')[0].replace('bytes ', 'bytes=');
                                }

                                const dataStream = new stream.PassThrough();
                                fileColl.serve(http, fileRef, fileRef.versions[version], version, dataStream);
                                dataStream.end(this.data.Body);
                            }
                        }
                    );

                    return true;
                }

                return false;
            }
        }
    );

    const _origRemove = UserFiles.remove;
    UserFiles.remove = function(search)
    {
        //console.log('remove',search);
        const cursor = this.collection.find(search);
        cursor.forEach(
            (fileRef) =>
            {
                console.log('versions',fileRef.versions);
                _.each(fileRef.versions,
                    (vRef) =>
                    {
                        if (vRef && vRef.meta && vRef.meta.pipePath)
                        {
                            console.log(vRef.meta.pipePath);
                            s3.deleteObject(
                                {
                                    Bucket: s3Conf.bucket,
                                    Key: vRef.meta.pipePath
                                }
                            ),
                            (error, data) =>
                            {
                                bound(
                                    () =>
                                    {
                                        console.log('+++++ DONE +++++',data,error);
                                        if (error)
                                        {
                                            console.error('DELETE ERROR',error);
                                        }
                                    }
                                )
                            }
                        }
                    }
                );
            }
        );

        _origRemove.call(this, search);
    }
}

export default UserFiles;