import { Meteor } from 'meteor/meteor';
import Messages from './Messages';
import Users from '../Users/users';

Meteor.publish('user.singleChat',
    function(side2Id)
    {
        const userId = Users.findOne({_id:this.userId}).services.hsloginservice.id;

        if (userId == null)
            this.ready();

        const selector = 
        {
            $or: 
            [
                {$and: [{'from.id': { $eq: userId}},{'to.id' : {$eq: side2Id}}]},
                {$and: [{'to.id': { $eq: userId}},{'from.id' : {$eq: side2Id}}]}
            ]
        }

        return Messages.find(selector, {sort: {datetime: 1}});
    }
);