import Messages from './Messages';
//import Users from '../Users/users';
import Patients from '../Patients/patients';
import Physicians from '../Physicians/physicians';
import { random, date } from 'faker';
import { Suspended, Approved, Pending } from '../GeneralTypes/AuthorizationStatus';
import { Text } from './MessagesSchema';

import moment from 'moment';

const getRandomPatient = () =>
{
    return Patients.findOne({Name: 'Moshe Levi'});

    // return Patients.aggregate(
    //     [
    //         { $sample: { size: 1 }}
    //     ]
    // )[0];
}

const getRandomPhysician = () =>
{
    return Physicians.findOne({Name: 'Dr Dulitel Eufimia'});
    // return Physicians.aggregate(
    //     [
    //         { $match: {'AuthorizationStatus': Approved} },
    //         { $sample: { size: 1 } }
    //     ]
    // )[0];
}

const FillMessages = (numOfMessages) =>
{
    let i;
    let d = moment();
    d.add(-10,'h');
    for (i=0; i<numOfMessages; i++)
    {
        const patient = getRandomPatient();
        const physician = getRandomPhysician();

        console.log('patient', patient);
        console.log('physician', physician);

        // const sendingRole = Math.floor(Math.random() * 2) === 1 ? 'patient' : 'physician';
        const sendingRole = 'physician';
        let side1Id, side2Id, side1role, side2role;
        if (sendingRole === 'patient')
        {
            side1Id = 'TP7Mg5DTDFpXMP7Sk';
            side1Name = 'Moshe Levi';
            side1role = 'patient';
            side2Id = 'ifXsn5NR67mFGduaq';
            side2Name = 'Dr Dulitel Eufimia';
            side2role = 'physician';
        }
        else
        {
            side1Id = 'ifXsn5NR67mFGduaq';
            side1Name = 'Dr Dulitel Eufimia';
            side1role = 'physician';
            side2Id = 'TP7Mg5DTDFpXMP7Sk';
            side2Name = 'Moshe Levi';
            side2role = 'patient';
        }

        const content = random.words();

        const ts = date.recent();
        
        const unread = Math.floor(Math.random()*2);

        d = d.add(Math.floor(Math.random()*10)+1, 'minute');

        //console.log('DATE',d);

        const message = 
        {
            from: {id: side1Id, name: side1Name, role: side1role},
            to: {id: side2Id, name: side2Name, role: side2role},
            content: 
            {
                msgType: Text,
                message: content
            },
            datetime: d.toDate(),
            unread: 1
        }

        //console.log(message);
        Messages.insert(message);

    }
}

export default FillMessages;