import { Meteor } from 'meteor/meteor';
import Users from '../Users/users';
import Messages from './Messages';
import { File } from './MessagesSchema';

Meteor.methods(
    {
        'message.send' (message)
        {
            const {from, to} = message;

            const senderId = Users.findOne({_id: this.userId}).services.hsloginservice.id;

            if (senderId !== from.id)
            {
                throw new Meteor.Error(500, 'fakesender','from user id dosn\'t match current user');
            }

            if (Users.find({'services.hsloginservice.id' : to.id}).count() !== 1)
            {
                console.log('Warning recipientId has not been found', to.id);
            }

            try
            {
                Messages.insert(message);
            }
            catch(err)
            {
                throw err;
            }
        },
        'message.read'(id)
        {
            const userId = Users.findOne({_id: this.userId}).services.hsloginservice.id;

            const message = Messages.findOne({_id: id});

            if (message.to.id !== userId)
            {
                throw new Meteor(500, 'illegal user id','illegaluser');
            }

            try
            {
                Messages.update(
                    { _id: id },
                    { $set: { unread: 0 } }
                );
            }
            catch(err)
            {
                throw err;
            }
        },
        'message.delete'(message)
        {
            const userId = Users.findOne({_id: this.userId}).services.hsloginservice.id;

            if (message.from.id !== userId)
            {
                throw new Meteor(500, 'only sender may delete a message','illegaluser');
            }

            try
            {
                if (message.content.msgType === File)
                {
                    // TODO: DELETE FILE
                }

                Messages.remove({_id: message._id});
            }
            catch(err)
            {
                throw err;
            }
        }
    }
)