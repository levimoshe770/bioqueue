import Messages from '../Messages/Messages';
import {Text, File} from '../Messages/MessagesSchema';
import UserFiles from '../UserFiles/UserFiles';

const fixMessagesData = () =>
{
    const msgs = Messages.find({});

    msgs.forEach(
        (message) =>
        {
            const txt = message.content;

            if (txt.msgType === File)
            {
                const file = txt.message.displayText;
                const url = txt.message.url;

                const fileType = UserFiles.findOne({name: file}).mime;

                txt.message =
                {
                    displayText: file,
                    fileType: fileType,
                    url: url
                }
            }

            try
            {
                Messages.update(
                    { _id: message._id },
                    { $set: {content: txt} }
                )
            }
            catch(err)
            {
                console.log(err);
                throw err;
            }
        }
    )
}

export default fixMessagesData;