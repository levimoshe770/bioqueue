import SimpleSchema from 'simpl-schema';

const FileSchema = new SimpleSchema(
    {
        displayText: {type: String},
        fileType: {type: String, allowedValues: ['image/jpeg','application/pdf','image/png','application/vnd.openxmlformats-officedocument.wordprocessingml.document']},
        url: {type: String}
    }
);

const Text = 0;
const File = 1;

const MessagesSchema = new SimpleSchema(
    {
        from: {type: Object},
        'from.id': {type: String},
        'from.name': {type: String},
        'from.role': {type: String},
        to: {type: Object},
        'to.id': {type: String},
        'to.name': {type: String},
        'to.role': {type: String},
        content: {type: Object},
        'content.msgType': {type: SimpleSchema.Integer, allowedValues: [Text, File]},
        'content.message': SimpleSchema.oneOf(
            {type: String},
            FileSchema,
        ),
        datetime: {type: Date},
        unread: {type: SimpleSchema.Integer, allowedValues : [0,1]}
    }
);

export default MessagesSchema;
export {Text, File};