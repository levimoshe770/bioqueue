import fixMessagesData from '../Lists/Messages/fixData'

const fixData = (switches) =>
{
    if (switches.messages)
        fixMessagesData();
}

export default fixData;