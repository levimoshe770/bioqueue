import SimpleSchema from 'simpl-schema';

const PrescriptionSchema = new SimpleSchema(
    {
        Name: {type: String},
        GenericName: {type: String, optional: true},
        Dosage: {type: Object},
        'Dosage.Ammount': {type: SimpleSchema.Integer, min:1, max: 30},
        'Dosage.Resolution': {type: String, allowedValues: ['d','w','h']},
        PatientId: {type: String, regEx: SimpleSchema.RegEx.Id},
        PhysicianId: {type: String, regEx: SimpleSchema.RegEx.Id},
        PharmacyId: {type: String, regEx: SimpleSchema.RegEx.Id, optional: true},
        Supplied: {type: Boolean, defaultValue: false},
        Notes: {type: Array, optional: true},
        'Notes.$': {type: String}
    }
);

export default PrescriptionSchema;