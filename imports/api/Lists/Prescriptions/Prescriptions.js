import { Mongo } from 'meteor/mongo';
import PrescriptionSchema from './PrescriptionSchema';
import Remote from '../../../ui/Remote';

const Prescriptions = new Mongo.Collection('prescriptions', { connection: Remote });

Prescriptions.attachSchema(PrescriptionSchema);

export default Prescriptions;