import { Meteor } from 'meteor/meteor';
import MedicalRecordCollection from './MedicalRecordCollection';
import Patients from '../Patients/patients';
import Physicians from '../Physicians/physicians';
import VisitsCollection from '../Visits/VisitsCollection';
import LabTestsCollection from '../LabTests/LabTestsCollection';
import PermanentMedicationCollection from '../PermanentMedication/PermanentMedicationCollection';
import Users from '../Users/users';

Meteor.methods(
    {
        'medicalrecord.create' (medicalrecord)
        {

            const patient = Patients.findOne({_id: medicalrecord.PatientId});
            if (patient === undefined)
            {
                throw new Meteor.Error(500, 'patientinvalid', 'patient id is invalid');
            }

            const physicianCount = Physicians.find({_id: {$in: medicalrecord.Physicians}}).count();

            if (physicianCount !== medicalrecord.Physicians.length)
            {
                throw new Meteor.Error(500,'physicianinvalid', 'physician id is invalid');
            }

            try
            {
                MedicalRecordCollection.insert(medicalrecord);
            }
            catch(err)
            {
                throw err;
            }

        },
        'medicalrecord.createIfNotExists'()
        {
            const patientId = Users.findOne({_id: this.userId}).services.hsloginservice.id;

            const patient = Patients.findOne({_id: patientId});
            if (patient === undefined)
            {
                return;
            }

            const c = MedicalRecordCollection.find({PatientId: patientId}).count();
            if (c === 0)
            {
                try
                {
                    MedicalRecordCollection.insert(
                        {
                            PatientId: patientId,
                            Physicians: [],
                            Visits: [],
                            LabTestsCollection: [],
                            KnownProblems: [],
                            PermanentMedication: [],
                            Alergies: []
                        }
                    )
                }
                catch(err)
                {
                    throw err;
                }
            }
        },
        'medicalrecord.addPhysician'(patientId, physicianId)
        {
            const pId = Users.findOne({_id: this.userId}).services.hsloginservice.id;
            if (pId !== patientId)
            {
                throw new Meteor.Error(500,'invaliduser','user should be patient only');
            }

            const physician = Physicians.findOne({_id: physicianId});
            if (physician === undefined)
            {
                throw new Meteor.Error(500, 'physicianinvalid', 'physician id is invalid');
            }

            try
            {
                MedicalRecordCollection.update(
                    { PatientId: patientId },
                    { $addToSet: { Physicians: physicianId } }
                )
            }
            catch(err)
            {
                throw err;
            }
        },
        'medicalrecord.removePhysician'(medicalRecordId, physicianId)
        {
            const c = MedicalRecordCollection.find({_id: medicalRecordId}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'medicalrecordinvalid', 'medicalrecord id is invalid');
            }

            try
            {
                MedicalRecordCollection.update(
                    { _id: medicalRecordId },
                    { $pull: {Physicians: physicianId}}
                );
            }
            catch(err)
            {
                throw err;
            }

        },
        'medicalrecord.addVisit'(medicalRecordId, visitId)
        {
            let c = MedicalRecordCollection.find({_id: medicalRecordId}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'medicalrecordinvalid', 'medicalrecord id is invalid');
            }

            c = VisitsCollection.find({_id: visitId}).count();
            if (c === 0)
            {
                throw new Meteor.err(500, 'visididinvalid', 'visit id is ;invalid');
            }

            try
            {
                MedicalRecordCollection.update(
                    { _id: medicalRecordId },
                    { $addToSet: {Visits: visitId} }
                )
            }
            catch(err)
            {
                throw err;
            }
        },
        'medicalrecord.addLabTest'(medicalRecordId, labTestId)
        {
            let c = MedicalRecordCollection.find({_id: medicalRecordId}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'medicalrecordinvalid', 'medicalrecord id is invalid');
            }

            c = LabTestsCollection.find({_id: labTestId}).count();
            if (c === 0)
            {
                throw new Meteor.err(500, 'labtestidinvalid', 'labTestId id is ;invalid');
            }

            try
            {
                MedicalRecordCollection.update(
                    { _id: medicalRecordId },
                    { $addToSet: {LabTestsCollection: labTestId} }
                )
            }
            catch(err)
            {
                throw err;
            }
        },
        'medicalrecord.addknownproblem'(medicalRecordId, knownProblem)
        {
            let c = MedicalRecordCollection.find({_id: medicalRecordId}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'medicalrecordinvalid', 'medicalrecord id is invalid');
            }

            try
            {
                MedicalRecordCollection.update(
                    { _id: medicalRecordId },
                    { $addToSet: { KnownProblems: knownProblem } }
                );
            }
            catch(err)
            {
                throw err;
            }
        },
        'medicalrecord.addPermanentMedication'(medicalRecordId, medicationId)
        {
            let c = MedicalRecordCollection.find({_id: medicalRecordId}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'medicalrecordinvalid', 'medicalrecord id is invalid');
            }

            c = PermanentMedicationCollection.find({_id: medicationId}).count();
            if (c === 0)
            {
                throw new Meteor.err(500, 'medicationid', 'medicationId id is ;invalid');
            }

            try
            {
                MedicalRecordCollection.update(
                    { _id: medicalRecordId },
                    { $addToSet: {PermanentMedication: medicationId} }
                )
            }
            catch(err)
            {
                throw err;
            }
        },
        'medicalrecord.addalergies'(medicalRecordId, alergy)
        {
            let c = MedicalRecordCollection.find({_id: medicalRecordId}).count();
            if (c === 0)
            {
                throw new Meteor.Error(500, 'medicalrecordinvalid', 'medicalrecord id is invalid');
            }

            try
            {
                MedicalRecordCollection.update(
                    { _id: medicalRecordId },
                    { $addToSet: { Alergies: alergy } }
                );
            }
            catch(err)
            {
                throw err;
            }
        }

    }
)
