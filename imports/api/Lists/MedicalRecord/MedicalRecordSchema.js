import SimpleSchema from 'simpl-schema';

const MedicalRecordSchema = new SimpleSchema(
    {
        PatientId: {type: String, regEx: SimpleSchema.RegEx.Id},
        Physicians: {type: Array},
        'Physicians.$': {type: String, regEx: SimpleSchema.RegEx.Id},
        Visits: {type: Array, required: false},
        'Visits.$': {type: String, regEx: SimpleSchema.RegEx.Id},
        LabTestsCollection: {type: Array, required: false},
        'LabTestsCollection.$': {type: String, regEx: SimpleSchema.RegEx.Id},
        KnownProblems: {type: Array, required: false},
        'KnownProblems.$': {type: String},
        PermanentMedication: {type: Array, required: false},
        'PermanentMedication.$': {type: String, regEx: SimpleSchema.RegEx.Id},
        Alergies: {type: Array},
        'Alergies.$': {type: String, required: false}
    }
);

export default MedicalRecordSchema;