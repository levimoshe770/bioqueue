import { Mongo } from 'meteor/mongo';
import MedicalRecordSchema from './MedicalRecordSchema';
import Remote from '../../../ui/Remote';

const MedicalRecordCollection = new Mongo.Collection('medicalrecord', { connection: Remote });

MedicalRecordCollection.attachSchema(MedicalRecordSchema);

export default MedicalRecordCollection;