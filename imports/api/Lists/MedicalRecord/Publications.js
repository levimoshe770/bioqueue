import { Meteor } from 'meteor/meteor';
import MedicalRecordCollection from './MedicalRecordCollection';
import Patients from '../Patients/patients';
import Physicians from '../Physicians/physicians';
import Users from '../Users/users';

Meteor.publish('medicalrecord.patient',
    function()
    {
        const patientId = Users.findOne({_id: this.userId}).services.hsloginservice.id;

        const c = Patients.find({_id: patientId}).count();
        if (c === 0)
            return this.ready();

        return MedicalRecordCollection.find({PatientId: patientId});
    }
);

Meteor.publish('medicalrecord.physician',
    function(patientId)
    {
        const physicianId = Users.findOne({_id: this.userId}).services.hsloginservice.id;

        const c = Physicians.find({_id: physicianId}).count();
        if (c === 0)
            return this.ready();

        return MedicalRecordCollection.find(
            {
                $and:
                [
                    {Physicians: {$all: [physicianId]}},
                    {PatientId: patientId}
                ]
            }
        );
    }
);