import { Mongo } from 'meteor/mongo';
import PharmacySchema from './PharmacySchema';
import Remote from '../../../ui/Remote';

const Pharmacies = new Mongo.Collection('pharmacies', { connection: Remote });

Pharmacies.attachSchema(PharmacySchema);

export default Pharmacies;