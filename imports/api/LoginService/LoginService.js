import { Accounts } from 'meteor/accounts-base';
import Users from '../Lists/Users/users';

const LOGINSERVICE = 'hsloginservice';

const LoginCallback = (options) =>
{
    //console.log('options',options);

    const user = options.user;

    Accounts.updateOrCreateUserFromExternalService(
        LOGINSERVICE,
        {
            id: user._id,
            access_token: options.token
        },
        {
            op: 'optionmoshe'
        }
    );

    const userId = Users.findOne({'services.hsloginservice.id': user._id})._id;

    Users.update(userId,
        {
            $set:
            {
                emails: [{address: user.email, verified: false}],
                name: user.name,
                role: user.role
            }
        }
    );

    returnObject =
    {
        userId: userId,
        options: {name: user.name, role: user.role}
    }

    //throw new Meteor.Error(500,'not implemented yet');
    return returnObject;
}

export {LOGINSERVICE, LoginCallback};