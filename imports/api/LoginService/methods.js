import { Meteor } from 'meteor/meteor';
import Users from '../Lists/Users/users';

const axios = require('axios');
const qs = require('qs');

Meteor.methods(
    {
        'requestToken'(data)
        {
            const requestData = qs.stringify(data);

            var config = 
            {
                method: 'post',
                url: Meteor.settings.public.auth_server_url + 'oauth/token',
                headers: 
                { 
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data : requestData
            };

            return(
                axios(config)
                .then(function (response) 
                    {
                        return response.data;
                    }
                )
                .catch(function (error) 
                    {
                        throw new Meteor.Error(500, error);
                    }
                )
            );

        },
        'getUserToken'()
        {
            const userId = this.userId;

            if (userId === null || userId === undefined)
                return;

            const token = Users.findOne({_id: userId}).services.hsloginservice.access_token;

            return(token);
        },  
        'getUserData'(token)
        {
            const config = 
            {
                method: 'get',
                url: Meteor.settings.public.auth_server_url + 'api/getUserId',
                params:
                {
                    access_token: token
                }
            }

            return(
                axios(config)
                .then(
                    function(response)
                    {
                        //console.log('response:',response.data)
                        //Meteor.loginWithToken(response.data);
                        return response.data;
                    }
                )
                .catch(
                    function(err)
                    {
                        throw new Meteor.Error(500, err);
                    }
                )
            );
        }
    }
)