import messages_en from './en.json';
import messages_ru from './ru.json';
//import locale from 'antd/es/locale/ru-RU'
import { createIntl, createIntlCache } from 'react-intl';

const cache = createIntlCache();

const getDefaultLocale = () =>
{
    const existingLocale = ['en','ru'];
    const navigatorDefault = navigator.language.substring(0,2);
    if (existingLocale.includes(navigatorDefault))
        return navigatorDefault;

    return 'en';
}

const DefaultLocale = getDefaultLocale();

const IntlMessages = {
    'en': 
    {
        messages: messages_en,
        intl: createIntl (
            {
                locale: 'en',
                defaultLocale: DefaultLocale,
                messages: messages_en
            },
            cache
        )/*,
        antdlocale: require('antd/es/locale/en_US')*/
    },
    'ru': 
    {
        messages: messages_ru,
        intl: createIntl (
            {
                locale: 'ru',
                defaultLocale: DefaultLocale,
                messages: messages_ru
            },
            cache
        )/*,
        antdlocale: require('antd/es/locale/ru_RU')*/
    }
}

export { IntlMessages, DefaultLocale };